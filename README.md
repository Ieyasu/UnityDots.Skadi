# Skadi

Skadi is node based GPU accelerated procedural terrain generator for Unity.

## Features

* Noise nodes for base terrain generation
* Hydraulic/thermal erosion
* Advanced object/tree/foliage placement
* Road generation
* Biomes
* Reusable subgraphs
* GPU accelerated terrain generation
* Easy-to-use node editor based on Unity's Shader Graph
* Tiling for large terrains
* Custom foliage system for fast instantiated rendering
* Custom water shader with refraction, screen space reflections, and caustics
* Custom terrain shader with triplanar mapping and distance based blending to eliminate texture tiling

## Gallery

![mountain_render](resources/mountain_scene_render.jpg)*A procedurally generated mountain scene*
![mountain_graph](resources/mountain_scene_graph.jpg)*Graph used to generate the above scene*

## Getting Started

To include the package in your Unity project, add the following git packages using the Package Manager UI:

```
https://gitlab.com/Ieyasu/UnityDots.Mathematics.git?path=/src
https://gitlab.com/Ieyasu/UnityDots.Common.git?path=/src
https://gitlab.com/Ieyasu/UnityDots.Graph.git?path=/src
https://gitlab.com/Ieyasu/UnityDots.Skadi.git?path=/src
```

You can create a new generator graph from within the Asset Creation Menu under *Skadi -> Terrain Generator*. Double clicking the created asset will open up the editor window for the graph. By default the graph contains a Simplex Noise node and a Heightmap Output Node. To create new nodes, right click anywhere in the window.

To generate a terrain using the generator, create a new Game Object and add a SkadiTerrain component to it. Drag the generator asset to the SkadiTerrain components *Generator* field and press the *Generate* button.

## TODO

Nodes:
* Add pathfinding to road node
* Add node that lets us place objects with more precision

Usability:
* Terrain tiling in the terrain preview window
* Resizable terrain preview window
* Make sure graphs don't contain cycles (including subgraphs)
* Remember graph position and zoom
