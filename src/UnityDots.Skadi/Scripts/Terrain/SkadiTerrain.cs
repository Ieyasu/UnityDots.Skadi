using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

[assembly: InternalsVisibleTo("UnityDots.Skadi")]

namespace UnityDots.Skadi
{
    public sealed class SkadiTerrain : MonoBehaviour
    {
        public float3 Position => transform.position;

        // General settings

        [SerializeField]
        private TerrainGenerator _generator = null;
        public TerrainGenerator Generator
        {
            get => _generator;
            set => _generator = value;
        }

        [SerializeField]
        private Terrain _groundTemplate;
        public Terrain GroundTemplate
        {
            get => _groundTemplate;
            set => _groundTemplate = value;
        }

        [SerializeField]
        private Terrain _waterTemplate;
        public Terrain WaterTemplate
        {
            get => _waterTemplate;
            set => _waterTemplate = value;
        }

        // Object settings

        [SerializeField]
        private bool _drawObjects = true;
        public bool DrawObjects
        {
            get => _drawObjects;
            set => _drawObjects = value;
        }

        // Tiling settings

        [SerializeField]
        private TerrainTilingMode _tilingMode = TerrainTilingMode.Single;
        public TerrainTilingMode TilingMode
        {
            get => _tilingMode;
            set => _tilingMode = value;
        }

        [SerializeField]
        private int _tilingGridSize = 16;
        public int TilingGridSize
        {
            get => _tilingGridSize;
            set => _tilingGridSize = value;
        }

        // Detail settings

        [SerializeField]
        private bool _drawDetails = true;
        public bool DrawDetails
        {
            get => _drawDetails;
            set => _drawDetails = value;
        }

        [SerializeField]
        private bool _drawDetailDebug = false;
        public bool DrawDetailDebug
        {
            get => _drawDetailDebug;
            set => _drawDetailDebug = value;
        }

        [SerializeField]
        private int _detailResolutionPerPatch = 8;
        public int DeatilResolutionPerPatch
        {
            get => _detailResolutionPerPatch;
            set => _detailResolutionPerPatch = value;
        }

        [SerializeField]
        private int _detailDensity = 10;
        public int DetailDensity
        {
            get => _detailDensity;
            set => _detailDensity = value;
        }

        [SerializeField]
        private SkadiTerrainData _data = null;
        public SkadiTerrainData Data => _data ?? (_data = new SkadiTerrainData());

        private TerrainDetails _details;
        internal TerrainDetails Details => _details ?? (_details = new TerrainDetails(this));

        private TerrainTiling _tiling;
        internal TerrainTiling Tiling => _tiling ?? (_tiling = new TerrainTiling(this));

        private void OnDestroy()
        {
            Data.Dispose();
            Details.Dispose();
            Tiling.Dispose();
        }

        public void Initialize(RawTerrainData rawData)
        {
            Data.Initialize(rawData);
            Refresh();
        }

        public void SyncHeightmap()
        {
            Tiling.SyncHeightmap();
        }

        public void SyncWatermap()
        {
            Tiling.SyncWatermap();
        }

        public void Refresh()
        {
            DestroyChildren(transform);
            Tiling.Initialize();
            Details.Initialize();
        }

        private void OnDrawGizmosSelected()
        {
            Details.DrawDebugGizmos();
        }

        private void LateUpdate()
        {
            Details.Draw();
            Tiling.Update();
        }

        private void DestroyChildren(Transform transform)
        {
            for (var i = transform.childCount - 1; i >= 0; --i)
            {
                var child = transform.GetChild(i);
                if (Application.isPlaying)
                {
                    Destroy(child.gameObject);
                }
                else
                {
                    DestroyImmediate(child.gameObject);
                }
            }
        }
    }
}
