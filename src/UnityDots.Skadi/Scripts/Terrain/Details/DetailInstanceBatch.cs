using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    internal sealed class DetailInstanceBatch
    {
        private const int LOD_BINS = 16;
        private const int MAX_INSTANCES = 1023;
        private static readonly MaterialPropertyBlock[] _lodPropertyBlocks;

        private DetailPrototype _prototype;
        private readonly Matrix4x4[] _matrices;

        public int DetailCount { get; private set; }
        public bool IsFull => DetailCount >= MAX_INSTANCES;

        static DetailInstanceBatch()
        {
            // Calculate the unity_LODFade needed for crossfading details
            // https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
            // Use two arrays, one reversed, to make sure the sum of lod fade is always 1 during transition
            _lodPropertyBlocks = new MaterialPropertyBlock[LOD_BINS];
            var lods = new Vector4[MAX_INSTANCES];
            for (var i = 0; i < LOD_BINS; ++i)
            {
                // For some reason lod = 0 breaks crossfading, make sure it won't happen
                var lod = clamp(i / ((float)LOD_BINS - 1), 0.001f, 0.999f);
                var lodQuantisized = min((int)(lod * 16), 15) / 15.0f;
                for (var j = 0; j < MAX_INSTANCES; ++j)
                {
                    // Lod quantisized to 16 levels
                    lods[j] = new float4(lod, lodQuantisized, 0, 0);
                }

                _lodPropertyBlocks[i] = new MaterialPropertyBlock();
                _lodPropertyBlocks[i].SetVectorArray("unity_LODFade", lods);
            }
        }

        public DetailInstanceBatch()
        {
            _matrices = new Matrix4x4[MAX_INSTANCES];
        }

        public void Initialize(DetailPrototype prototype)
        {
            _prototype = prototype;
            DetailCount = 0;
        }

        public void AddInstance(Matrix4x4 matrix)
        {
            if (IsFull)
            {
                Debug.LogError($"Can't add more detail instances. Maximum of {MAX_INSTANCES} reached");
                return;
            }

            _matrices[DetailCount++] = matrix;
        }

        public void Draw(Camera camera, float distance)
        {
            // Only draw if layer enabled in camera culling mask
            if (((1 << _prototype.Layer) & camera.cullingMask) != 0)
            {
                // return;
            }

            if (_matrices == null)
            {
                return;
            }

            // DrawLOD(camera, distance, _prototype.LODs[0].Mesh, _prototype.LodMaterial, null);
            // return;

            for (var j = _prototype.LODs.Length - 1; j >= 0; --j)
            {
                var lodMesh = _prototype.LODs[j].Mesh;
                var lodEndDistance = _prototype.LODs[j].Distance;
                var lodStartDistance = j > 0 ? _prototype.LODs[j - 1].Distance : 0;
                var lodEndTransitionDistance = _prototype.LODs[j].TransitionDistance;
                var lodStartTransitionDistance = j > 0 ? _prototype.LODs[j - 1].TransitionDistance : 0;

                // Cull.
                if (distance >= lodEndDistance)
                {
                    break;
                }
                // LOD end transition.
                else if (distance >= lodEndDistance - lodEndTransitionDistance)
                {
                    var lod = (lodEndDistance - distance) / lodEndTransitionDistance;
                    var lodIndex = min(LOD_BINS - 1, (int)(lod * LOD_BINS));
                    var lodBlock = _lodPropertyBlocks[lodIndex];
                    DrawLOD(camera, distance, lodMesh, _prototype.LodMaterial, lodBlock);
                }
                // LOD fully visible.
                else if (distance >= lodStartDistance - lodStartTransitionDistance)
                {
                    DrawLOD(camera, distance, lodMesh, _prototype.LodMaterial, _lodPropertyBlocks[LOD_BINS - 1]);
                }
            }
        }

        private void DrawLOD(Camera camera, float distance, Mesh mesh, Material material, MaterialPropertyBlock lodPropertyBlock)
        {
            // Determine if shadows should be used.
            var shadowCastingMode = ShadowCastingMode.Off;
            var receiveShadows = false;
            if (_prototype.ShadowCullDistance > distance)
            {
                shadowCastingMode = ShadowCastingMode.On;
                receiveShadows = true;
            }

            Graphics.DrawMeshInstanced(mesh, 0, material, _matrices, DetailCount, lodPropertyBlock, shadowCastingMode, receiveShadows, _prototype.Layer, camera);
        }
    }
}
