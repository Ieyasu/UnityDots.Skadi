using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal struct CameraDetailPatches : IEquatable<CameraDetailPatches>
    {
        public Camera Camera { get; }
        public List<DetailPatch> Patches { get; }

        public CameraDetailPatches(Camera camera)
        {
            Camera = camera;
            Patches = new List<DetailPatch>();
        }

        public override bool Equals(object obj)
        {
            return obj is CameraDetailPatches other && Equals(other);
        }

        public bool Equals(CameraDetailPatches other)
        {
            return EqualityComparer<Camera>.Default.Equals(Camera, other.Camera)
                   && EqualityComparer<List<DetailPatch>>.Default.Equals(Patches, other.Patches);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Camera, Patches);
        }
    }
}
