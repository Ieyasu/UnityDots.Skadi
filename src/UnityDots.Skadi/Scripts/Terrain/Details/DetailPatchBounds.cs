using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal struct DetailPatchBounds : IEquatable<DetailPatchBounds>
    {
        public DetailPatch Patch { get; set; }
        public Bounds Value { get; set; }
        public float RenderDistance { get; set; }

        public override bool Equals(object obj)
        {
            return obj is DetailPatchBounds other && Equals(other);
        }

        public bool Equals(DetailPatchBounds other)
        {
            return EqualityComparer<DetailPatch>.Default.Equals(Patch, other.Patch)
                   && EqualityComparer<Bounds>.Default.Equals(Value, other.Value)
                   && RenderDistance == other.RenderDistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Patch, Value, RenderDistance);
        }
    }
}
