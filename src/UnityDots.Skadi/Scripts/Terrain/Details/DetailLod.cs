using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [System.Serializable]
    public struct DetailLod : IEquatable<DetailLod>
    {
        [SerializeField]
        [Tooltip("Mesh used to render this LOD.")]
        private Mesh _mesh;
        public Mesh Mesh => _mesh;

        [SerializeField]
        [Tooltip("Distance at which to fully cull this LOD.")]
        private float _distance;
        public float Distance => _distance;

        [SerializeField]
        [Tooltip("Transition distance to next LOD.")]
        private float _transitionDistance;
        public float TransitionDistance => _transitionDistance;

        public static DetailLod Create()
        {
            return new DetailLod
            {
                _mesh = null,
                _distance = 100,
                _transitionDistance = 10,
            };
        }

        public override bool Equals(object obj)
        {
            return obj is DetailLod other && Equals(other);
        }

        public bool Equals(DetailLod other)
        {
            return EqualityComparer<Mesh>.Default.Equals(Mesh, other.Mesh)
                    && Distance == other.Distance
                    && TransitionDistance == other.TransitionDistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Mesh, Distance, TransitionDistance);
        }
    }
}
