using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    internal sealed class DetailPatch
    {
        private static readonly Queue<DetailInstanceBatch> _pooledInstanceBatches = new Queue<DetailInstanceBatch>();

        private DetailPatchBounds _bounds;
        private float[,,] _detailmaps;
        private readonly TerrainDetails _details;
        private readonly List<DetailInstanceBatch> _instanceBatches;

        public int2 Coordinate { get; private set; }
        public bool IsInstantiated { get; private set; }
        public float RenderDistance { get; private set; }
        public Bounds Bounds { get; private set; }

        public bool IsVisible { get; set; }

        private SkadiTerrain Terrain => _details.Terrain;

        public DetailPatch(TerrainDetails details, int2 coordinate)
        {
            _details = details;
            _instanceBatches = new List<DetailInstanceBatch>();
            Coordinate = coordinate;
        }

        public void Initialize()
        {
            Destroy();

            RenderDistance = _details.MaxPatchRenderDistance;
            Bounds = CalculateBounds();

            // Preload the alphamaps for each patch in advance to speed up things when instantiating
            var resolution = _details.ResolutionPerPatch;
            var x = Coordinate.x * resolution;
            var y = Coordinate.y * resolution;

            if (_detailmaps == null || _detailmaps.GetLength(0) != resolution || _detailmaps.GetLength(2) != Terrain.Data.DetailmapCount)
            {
                _detailmaps = new float[resolution, resolution, Terrain.Data.DetailmapCount];
            }

            Terrain.Data.GetDetailmaps(x, y, _detailmaps);
        }

        public void Instantiate()
        {
            if (IsInstantiated)
            {
                Debug.LogError("Detail patch already instantiated");
                return;
            }

            IsInstantiated = true;

            // Create detail instances
            var random = new Unity.Mathematics.Random(hash(Coordinate));
            var maxRenderDistance = 0.0f;
            for (var i = 0; i < Terrain.Data.DetailmapCount; ++i)
            {
                // If the given prototype has instances in this patch, make sure to change the culling variables
                // to take the prototypes radius and rendering distance into account
                if (InstantiateLayer(i, random))
                {
                    var prototype = Terrain.Data.DetailPrototypes[i];
                    maxRenderDistance = max(maxRenderDistance, prototype.LODs[prototype.LODs.Length - 1].Distance);
                }
            }

            _bounds.RenderDistance = maxRenderDistance;
        }

        private bool InstantiateLayer(int i, Unity.Mathematics.Random random)
        {
            var prototype = Terrain.Data.DetailPrototypes[i];
            if (!prototype.IsValid)
            {
                return false;
            }

            // Initially null to avoid an allocation in case we don't get any instances to add
            DetailInstanceBatch detailInstanceBatch = null;
            var start = Coordinate * _details.ResolutionPerPatch;
            for (var y = 0; y < _details.ResolutionPerPatch; ++y)
            {
                for (var x = 0; x < _details.ResolutionPerPatch; ++x)
                {
                    var density = round(_detailmaps[y, x, i] * _details.Density);
                    for (var k = 0; k < density; ++k)
                    {
                        var xNorm = (start.x + x + random.NextFloat()) / (Terrain.Data.DetailmapResolution - 1);
                        var zNorm = (start.y + y + random.NextFloat()) / (Terrain.Data.DetailmapResolution - 1);
                        var yNorm = Terrain.Data.GetInterpolatedHeight(xNorm, zNorm);
                        var position = (float3)_details.WorldBounds.min + _details.WorldBounds.size * new float3(xNorm, yNorm, zNorm);

                        var randomAngle = random.NextFloat(0, 360);
                        var normal = Terrain.Data.GetInterpolatedNormal(xNorm, zNorm);
                        var rotation = Quaternion.FromToRotation(Vector3.up, normal) * Quaternion.Euler(0, randomAngle, 0);

                        var randomScale = random.NextFloat(prototype.MinScale, prototype.MaxScale);
                        var scale = new float3(randomScale, randomScale, randomScale);

                        var matrix = Matrix4x4.TRS(position, rotation, scale);
                        if (detailInstanceBatch == null || detailInstanceBatch.IsFull)
                        {
                            detailInstanceBatch = NewInstanceBatch(prototype);
                        }
                        detailInstanceBatch.AddInstance(matrix);
                    }
                }
            }

            return detailInstanceBatch != null;
        }

        public void Destroy()
        {
            if (!IsInstantiated)
            {
                return;
            }

            IsInstantiated = false;

            // Instead of just releasing the memory of the matrix arrays, we
            // pool them so that other patches can reuse them. This removes
            // constant garbage collection of the arrays.
            foreach (var instanceBatch in _instanceBatches)
            {
                _pooledInstanceBatches.Enqueue(instanceBatch);
            }
            _instanceBatches.Clear();
        }

        public void Draw(Camera camera)
        {
            var cameraPosition = camera.transform.position;
            var closestPoint = Bounds.ClosestPoint(cameraPosition);
            var distance = length(closestPoint - cameraPosition);
            foreach (var instanceBatch in _instanceBatches)
            {
                instanceBatch.Draw(camera, distance);
            }
        }

        private DetailInstanceBatch NewInstanceBatch(DetailPrototype prototype)
        {
            var batch = _pooledInstanceBatches.Count > 0 ? _pooledInstanceBatches.Dequeue() : new DetailInstanceBatch();
            batch.Initialize(prototype);
            _instanceBatches.Add(batch);
            return batch;
        }

        private Bounds CalculateBounds()
        {
            if (Terrain.Data.Heightmap == null)
            {
                var center = (float3)_details.WorldBounds.min + _details.PatchSize * new float3(Coordinate.x, 0, Coordinate.y);
                return new Bounds(center, new float3(0));
            }

            // // Find the minimum and maximum heights for the bounds based on the terrain heightmap
            var minHeight = _details.WorldBounds.size.y;
            var maxHeight = 0.0f;
            var xStart = Coordinate.x * _details.ResolutionPerPatch;
            var yStart = Coordinate.y * _details.ResolutionPerPatch;
            for (var y = 0; y < _details.ResolutionPerPatch + 1; ++y)
            {
                var yNorm = (yStart + y) / ((float)Terrain.Data.DetailmapResolution - 1);
                for (var x = 0; x < _details.ResolutionPerPatch + 1; ++x)
                {
                    var xNorm = (xStart + x) / ((float)Terrain.Data.DetailmapResolution - 1);
                    var height = Terrain.Data.GetInterpolatedHeight(xNorm, yNorm);
                    minHeight = min(height, minHeight);
                    maxHeight = max(height, maxHeight);
                }
            }

            // Calculate the bounds for the specific patch
            var bounds = new Bounds();
            var boundsMin = (float3)_details.WorldBounds.min + _details.PatchSize * new float3(Coordinate.x, minHeight, Coordinate.y);
            var boundsMax = (float3)_details.WorldBounds.min + _details.PatchSize * new float3(Coordinate.x + 1, maxHeight, Coordinate.y + 1);
            bounds.SetMinMax(boundsMin, boundsMax);
            return bounds;
        }
    }
}
