using System.Collections.Generic;
using Unity.Mathematics;
using UnityDots.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    internal sealed class TerrainDetails
    {
        private readonly static Plane[] _planeBuffer = new Plane[6];

        private DetailPatch[,] _patches;
        private readonly ResizableArray<Camera> _cameras = new ResizableArray<Camera>();
        private readonly List<CameraDetailPatches> _patchesByCamera = new List<CameraDetailPatches>();
        private readonly QuadTree<DetailPatchBounds> _patchBounds = new QuadTree<DetailPatchBounds>();

        public bool IsInitialized { get; private set; }
        public SkadiTerrain Terrain { get; private set; }
        public Bounds WorldBounds { get; private set; }

        public int PatchCount { get; private set; }
        public float MaxPatchRenderDistance { get; private set; }
        public float3 PatchSize { get; private set; }
        public int ResolutionPerPatch { get; private set; }
        public float Density { get; private set; }

        public TerrainDetails(SkadiTerrain terrain)
        {
            Terrain = terrain;
        }

        internal void Initialize()
        {
            if (!Terrain.DrawDetails || Terrain.Data.DetailmapCount == 0)
            {
                return;
            }

            IsInitialized = true;
            WorldBounds = new Bounds(Terrain.Position + 0.5f * Terrain.Data.TerrainSize, Terrain.Data.TerrainSize);
            Density = Terrain.DetailDensity;

            // Find maximum rendering distance
            MaxPatchRenderDistance = 0;
            foreach (var prototype in Terrain.Data.DetailPrototypes)
            {
                var prototypeRenderDistance = prototype.LODs[prototype.LODs.Length - 1].Distance;
                MaxPatchRenderDistance = max(MaxPatchRenderDistance, prototypeRenderDistance);
            }

            // Initialize patches
            DestroyPatches();
            ResolutionPerPatch = clamp(Mathf.NextPowerOfTwo(Terrain.DeatilResolutionPerPatch), 8, Terrain.Data.DetailmapResolution);
            PatchCount = Terrain.Data.DetailmapResolution / ResolutionPerPatch;
            PatchSize = WorldBounds.size / new float3(PatchCount, 1, PatchCount);

            if (_patches == null || _patches.GetLength(0) != PatchCount)
            {
                _patches = new DetailPatch[PatchCount, PatchCount];
                for (var y = 0; y < PatchCount; ++y)
                {
                    for (var x = 0; x < PatchCount; ++x)
                    {
                        _patches[y, x] = new DetailPatch(this, new int2(x, y));
                    }
                }
            }

            foreach (var patch in _patches)
            {
                patch.Initialize();
            }

            // Initialize patch bounds
            _patchBounds.Resize(PatchCount);

            foreach (var patch in _patches)
            {
                _patchBounds.SetLeafData(patch.Coordinate.x, patch.Coordinate.y, new DetailPatchBounds
                {
                    Patch = patch,
                    Value = patch.Bounds,
                    RenderDistance = patch.RenderDistance
                });
            }

            _patchBounds.TraverseReverseBFS((node) =>
            {
                if (!_patchBounds.IsLeaf(node))
                {
                    var bounds1 = _patchBounds.GetChildData(node, 0);
                    var bounds2 = _patchBounds.GetChildData(node, 1);
                    var bounds3 = _patchBounds.GetChildData(node, 2);
                    var bounds4 = _patchBounds.GetChildData(node, 3);

                    bounds1.RenderDistance = max(bounds1.RenderDistance, bounds2.RenderDistance);
                    bounds1.RenderDistance = max(bounds1.RenderDistance, bounds3.RenderDistance);
                    bounds1.RenderDistance = max(bounds1.RenderDistance, bounds4.RenderDistance);

                    var bounds = bounds1.Value;
                    bounds.Encapsulate(bounds2.Value);
                    bounds.Encapsulate(bounds3.Value);
                    bounds.Encapsulate(bounds4.Value);
                    bounds1.Value = bounds;

                    _patchBounds.SetNodeData(node, bounds1);
                }
            });
        }

        internal void Dispose()
        {
            IsInitialized = false;

            DestroyPatches();

            _cameras.Clear();
            _patchesByCamera.Clear();
        }

        internal void Draw()
        {
            if (!IsInitialized)
            {
                return;
            }

            if (!Terrain.DrawDetails)
            {
                Dispose();
                return;
            }

            ResetPatchesByCamera();
            UpdateVisiblePatches();

            // Draw patches for each camera
            foreach (var patchList in _patchesByCamera)
            {
                foreach (var patch in patchList.Patches)
                {
                    patch.Draw(patchList.Camera);
                }
            }
        }

        internal void DrawDebugGizmos()
        {
            if (!IsInitialized || !Terrain.DrawDetailDebug)
            {
                return;
            }

            foreach (var patch in _patches)
            {
                Gizmos.color = patch.IsVisible ? Color.green : Color.red;
                Gizmos.DrawCube(patch.Bounds.center, patch.Bounds.size);
            }
        }

        private void DestroyPatches()
        {
            if (_patches == null)
            {
                return;
            }

            foreach (var patch in _patches)
            {
                if (patch.IsInstantiated)
                {
                    patch.Destroy();
                }
            }
        }

        private void ResetPatchesByCamera()
        {
            UpdateCameras();

            // Remove patch maps for all cameras that no longer exist
            for (var i = _patchesByCamera.Count - 1; i >= 0; --i)
            {
                if (_patchesByCamera[i].Camera == null || !_patchesByCamera[i].Camera.enabled)
                {
                    _patchesByCamera.RemoveAt(i);
                    break;
                }
            }

            // Add maps for missing cameras
            for (var i = 0; i < _cameras.Count; ++i)
            {
                if (_patchesByCamera.FindIndex(x => x.Camera == _cameras[i]) < 0)
                {
                    _patchesByCamera.Add(new CameraDetailPatches(_cameras[i]));
                }
            }
        }

        private void UpdateVisiblePatches()
        {
            // Reset patch visibility
            foreach (var patch in _patches)
            {
                patch.IsVisible = false;
            }

            // Mark all visible patches
            foreach (var patchList in _patchesByCamera)
            {
                var cameraPosition = patchList.Camera.transform.position;
                GeometryUtility.CalculateFrustumPlanes(patchList.Camera, _planeBuffer);
                patchList.Patches.Clear();

                _patchBounds.TraverseDFS((node) =>
                {
                    var bounds = _patchBounds.GetData(node);
                    var closestPoint = bounds.Value.ClosestPoint(cameraPosition);
                    var distanceSqr = lengthsq(closestPoint - cameraPosition);
                    var isInRenderDistance = distanceSqr < bounds.RenderDistance * bounds.RenderDistance;
                    var isVisible = isInRenderDistance && GeometryUtility.TestPlanesAABB(_planeBuffer, bounds.Value);

                    if (isVisible && _patchBounds.IsLeaf(node))
                    {
                        var patch = bounds.Patch;
                        patch.IsVisible = true;
                        patchList.Patches.Add(patch);
                    }

                    return isVisible;
                });
            }

            // Pool patches that are no longer visible and instantiate patches that are newly visible
            foreach (var patch in _patches)
            {
                if (patch.IsVisible && !patch.IsInstantiated)
                {
                    patch.Instantiate();
                }
                else if (!patch.IsVisible && patch.IsInstantiated)
                {
                    patch.Destroy();
                }
            }
        }

        private void UpdateCameras()
        {
            _cameras.Clear();

            // Getting the cameras of the active scene is easy
            if (Terrain.gameObject.scene == SceneManager.GetActiveScene())
            {
                _cameras.Resize(Camera.allCamerasCount);
                Camera.GetAllCameras(_cameras.Items);
                return;
            }

            // In case of a preview scene, we have to fetch the cameras manually
            using (var rootGameObjectBuffer = NonAllocList<GameObject>.Get())
            {
                Terrain.gameObject.scene.GetRootGameObjects(rootGameObjectBuffer.Data);
                foreach (var rootGameObject in rootGameObjectBuffer)
                {
                    using (var cameraBuffer = NonAllocList<Camera>.Get())
                    {
                        rootGameObject.GetComponentsInChildren(cameraBuffer.Data);
                        _cameras.AddRange(cameraBuffer.Data);
                    }
                }
            }
        }
    }
}
