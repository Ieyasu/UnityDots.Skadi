using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal sealed class TerrainTile
    {
        private readonly static TerrainBuilder _terrainBuilder = new TerrainBuilder();

        private readonly TerrainTiling _tiling;
        private readonly string _groundTerrainName;
        private readonly string _waterTerrainName;
        private readonly List<RawObjectData> _objects = new List<RawObjectData>();

        private float3 _size;
        private float[,] _heightmap;
        private float[,] _watermap;
        private float[,,] _alphamaps;
        private bool _hasBeenInstantiated;

        public bool IsVisible { get; set; }
        public bool IsInstantiated { get; private set; }
        public int2 Coordinate { get; private set; }

        // Instantiated data
        public Terrain GroundTerrain { get; private set; }
        public Terrain WaterTerrain { get; private set; }

        private SkadiTerrain Terrain => _tiling.Terrain;

        public TerrainTile(TerrainTiling tiling, int2 coordinate)
        {
            _tiling = tiling;
            Coordinate = coordinate;
            _groundTerrainName = $"Ground ({Coordinate.x}, {Coordinate.y})";
            _waterTerrainName = $"Water ({Coordinate.x}, {Coordinate.y})";
        }

        public void Initialize()
        {
            Destroy();
            _objects.Clear();

            _size = new float3(Terrain.Data.TerrainSize.x / _tiling.GridSize, Terrain.Data.TerrainSize.y, Terrain.Data.TerrainSize.z / _tiling.GridSize);
            _hasBeenInstantiated = false;
        }

        public void Instantiate()
        {
            if (IsInstantiated)
            {
                return;
            }

            IsInstantiated = true;

            if (Terrain.Data.SyncHeightmapToCPU && !_hasBeenInstantiated && Terrain.Data.Heightmap != null)
            {
                var resolution = _tiling.TileHeightmapResolution;
                var x = Coordinate.x * (resolution - 1);
                var y = Coordinate.y * (resolution - 1);

                if (_heightmap == null || _heightmap.GetLength(0) != resolution)
                {
                    _heightmap = new float[resolution, resolution];
                }

                Terrain.Data.GetHeightmap(x, y, _heightmap);
            }

            if (Terrain.Data.SyncWatermapToCPU && !_hasBeenInstantiated && Terrain.Data.Watermap != null)
            {
                var resolution = _tiling.TileHeightmapResolution;
                var x = Coordinate.x * (resolution - 1);
                var y = Coordinate.y * (resolution - 1);

                if (_watermap == null || _watermap.GetLength(0) != resolution)
                {
                    _watermap = new float[resolution, resolution];
                }

                Terrain.Data.GetWatermap(x, y, _watermap);
            }

            if (Terrain.Data.SyncAlphamapsToCPU && !_hasBeenInstantiated)
            {
                var resolution = _tiling.TileAlphamapResolution;
                var x = Coordinate.x * resolution;
                var y = Coordinate.y * resolution;

                if (_alphamaps == null || _alphamaps.GetLength(0) != resolution || _alphamaps.GetLength(2) != Terrain.Data.AlphamapLayers)
                {
                    _alphamaps = new float[resolution, resolution, Terrain.Data.AlphamapLayers];
                }

                Terrain.Data.GetAlphamaps(x, y, _alphamaps);
            }

            FinishInstantiation();
        }

        private void FinishInstantiation()
        {
            if (Terrain.Data.Heightmap != null)
            {
                GroundTerrain = CreateTerrain(Terrain.GroundTemplate, _groundTerrainName);
                ApplyHeightmap(GroundTerrain);
                ApplyAlphamaps(GroundTerrain);

                if (Terrain.DrawObjects)
                {
                    InstantiateGameObjects(GroundTerrain);
                }
            }

            if (Terrain.Data.Watermap != null)
            {
                WaterTerrain = CreateTerrain(Terrain.WaterTemplate, _waterTerrainName);
                ApplyWatermap(WaterTerrain);
                ApplyWaterHoles(WaterTerrain);
            }

            _hasBeenInstantiated = true;
        }

        public void Destroy()
        {
            if (!IsInstantiated)
            {
                return;
            }

            IsInstantiated = false;

            if (GroundTerrain != null)
            {
                for (var i = GroundTerrain.transform.childCount - 1; i >= 0; --i)
                {
                    Destroy(GroundTerrain.transform.GetChild(i).gameObject);
                }
                if (!_tiling.TerrainPool.Pool(GroundTerrain.gameObject))
                {
                    Destroy(GroundTerrain.gameObject);
                }
                GroundTerrain = null;
            }

            if (WaterTerrain != null)
            {
                if (!_tiling.TerrainPool.Pool(WaterTerrain.gameObject))
                {
                    Destroy(WaterTerrain.gameObject);
                }
                WaterTerrain = null;
            }
        }

        public void AddObject(RawObjectData obj)
        {
            _objects.Add(obj);
        }

        public void UpdateNeighbours()
        {
            if (GroundTerrain != null)
            {
                var left = _tiling.GetTile(Coordinate - new int2(1, 0))?.GroundTerrain;
                var top = _tiling.GetTile(Coordinate + new int2(0, 1))?.GroundTerrain;
                var right = _tiling.GetTile(Coordinate + new int2(1, 0))?.GroundTerrain;
                var bottom = _tiling.GetTile(Coordinate - new int2(0, 1))?.GroundTerrain;
                GroundTerrain.SetNeighbors(left, top, right, bottom);
            }

            if (WaterTerrain != null)
            {
                var left = _tiling.GetTile(Coordinate - new int2(1, 0))?.WaterTerrain;
                var top = _tiling.GetTile(Coordinate + new int2(0, 1))?.WaterTerrain;
                var right = _tiling.GetTile(Coordinate + new int2(1, 0))?.WaterTerrain;
                var bottom = _tiling.GetTile(Coordinate - new int2(0, 1))?.WaterTerrain;
                WaterTerrain.SetNeighbors(left, top, right, bottom);
            }
        }

        private Terrain CreateTerrain(Terrain template, string name)
        {
            if (template == null)
            {
                return null;
            }

            Terrain terrain;
            if (!_tiling.TerrainPool.IsEmpty)
            {
                terrain = _tiling.TerrainPool.Get(true).GetComponent<Terrain>();
                TerrainBuilder.ApplySettings(template, terrain);
            }
            else
            {
                terrain = _terrainBuilder.SetPrefab(template)
                                                .SetSize(_size)
                                                .SetAlphamapResolution(_tiling.TileAlphamapResolution)
                                                .SetHeightmapResolution(_tiling.TileHeightmapResolution)
                                                .SetBasemapResolution(template.terrainData.baseMapResolution)
                                                .Build();
            }

            if (terrain.terrainData.size != (Vector3)_size)
            {
                terrain.terrainData.size = _size;
            }

            terrain.name = name;
            terrain.transform.SetParent(Terrain.transform);
            terrain.transform.localPosition = new float3(_size.x * Coordinate.x, 0, _size.z * Coordinate.y);
            return terrain;
        }

        private void ApplyHeightmap(Terrain terrain)
        {
            // Resize the terrain heightmap if needed
            if (terrain.terrainData.heightmapResolution != _tiling.TileHeightmapResolution)
            {
                terrain.terrainData.heightmapResolution = _tiling.TileHeightmapResolution;
            }

            if (terrain.terrainData.size != (Vector3)_size)
            {
                terrain.terrainData.size = _size;
            }

            if (Terrain.Data.SyncHeightmapToCPU && _heightmap != null)
            {
                terrain.terrainData.SetHeights(0, 0, _heightmap);
            }
            else
            {
                ApplyHeightmapGPU(terrain);
            }
        }

        private void ApplyHeightmapGPU(Terrain terrain)
        {
            if (Terrain.Data.Heightmap == null)
            {
                return;
            }

            var syncControl = Terrain.Data.SyncHeightmapToCPU ? TerrainHeightmapSyncControl.HeightAndLod : TerrainHeightmapSyncControl.None;
            using (var renderTexture = TextureUtility.SwapActiveRenderTexture(Terrain.Data.Heightmap))
            {
                var resolution = _tiling.TileHeightmapResolution;
                var sourceRect = new RectInt(Coordinate.x * (resolution - 1), Coordinate.y * (resolution - 1), resolution, resolution);
                terrain.terrainData.CopyActiveRenderTextureToHeightmap(sourceRect, Vector2Int.zero, syncControl);
            }
        }

        private void ApplyWatermap(Terrain terrain)
        {
            // Resize the terrain heightmap if needed
            if (terrain.terrainData.heightmapResolution != _tiling.TileHeightmapResolution)
            {
                terrain.terrainData.heightmapResolution = _tiling.TileHeightmapResolution;
            }

            if (terrain.terrainData.size != (Vector3)_size)
            {
                terrain.terrainData.size = _size;
            }

            if (Terrain.Data.SyncWatermapToCPU && _watermap != null)
            {
                terrain.terrainData.SetHeights(0, 0, _watermap);
            }
            else
            {
                ApplyWatermapGPU(terrain);
            }
        }

        private void ApplyWatermapGPU(Terrain terrain)
        {
            if (Terrain.Data.Watermap == null)
            {
                return;
            }

            var syncControl = Terrain.Data.SyncWatermapToCPU ? TerrainHeightmapSyncControl.HeightAndLod : TerrainHeightmapSyncControl.None;
            using (var renderTexture = TextureUtility.SwapActiveRenderTexture(Terrain.Data.Watermap))
            {
                var resolution = _tiling.TileHeightmapResolution;
                var sourceRect = new RectInt(Coordinate.x * (resolution - 1), Coordinate.y * (resolution - 1), resolution, resolution);
                terrain.terrainData.CopyActiveRenderTextureToHeightmap(sourceRect, Vector2Int.zero, syncControl);
            }
        }

        private void ApplyWaterHoles(Terrain terrain)
        {
            if (Terrain.Data.WaterHolesTexture == null)
            {
                return;
            }

            using (var renderTexture = TextureUtility.SwapActiveRenderTexture(Terrain.Data.WaterHolesTexture))
            {
                var resolution = _tiling.TileHeightmapResolution - 1;
                var sourceRect = new RectInt(Coordinate.x * resolution, Coordinate.y * resolution, resolution, resolution);
                terrain.terrainData.CopyActiveRenderTextureToTexture(TerrainData.HolesTextureName, 0, sourceRect, Vector2Int.zero, true);
            }
        }

        private void ApplyAlphamaps(Terrain terrain)
        {
            terrain.terrainData.terrainLayers = Terrain.Data.UnityTerrainLayers;

            // Resize the terrain alphamap if needed
            if (terrain.terrainData.alphamapResolution != _tiling.TileAlphamapResolution)
            {
                terrain.terrainData.alphamapResolution = _tiling.TileAlphamapResolution;
            }

            if (Terrain.Data.SyncAlphamapsToCPU && _alphamaps != null)
            {
                terrain.terrainData.SetAlphamaps(0, 0, _alphamaps);
            }
            else
            {
                ApplyAlphamapsGPU(terrain);
            }
        }

        private void ApplyAlphamapsGPU(Terrain terrain)
        {
            for (var i = 0; i < Terrain.Data.AlphamapCount; ++i)
            {
                var alphamap = Terrain.Data.GetAlphamap(i);
                using (_ = TextureUtility.SwapActiveRenderTexture(alphamap))
                {
                    var resolution = _tiling.TileAlphamapResolution;
                    var sourceRect = new RectInt(Coordinate.x * resolution, Coordinate.y * resolution, resolution, resolution);
                    terrain.terrainData.CopyActiveRenderTextureToTexture(TerrainData.AlphamapTextureName, i, sourceRect, Vector2Int.zero, true);
                }
            }
        }

        private void InstantiateGameObjects(Terrain terrain)
        {
            foreach (var obj in _objects)
            {
                var prefab = Terrain.Data.ObjectPrototypes[obj.PrototypeID].Prefab;
                var gameObject = Object.Instantiate(prefab, terrain.transform);
                gameObject.transform.localScale = new float3(obj.Scale);
                gameObject.transform.position = Terrain.Position + obj.Position;
                gameObject.transform.rotation = obj.Rotation;
            }
        }

        private void Destroy(GameObject gameObject)
        {
            if (Application.isPlaying)
            {
                Object.Destroy(gameObject);
            }
            else
            {
                Object.DestroyImmediate(gameObject);
            }
        }
    }
}
