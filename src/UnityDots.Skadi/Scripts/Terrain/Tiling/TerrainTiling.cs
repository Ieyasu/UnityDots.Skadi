using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityDots.Collections;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    internal sealed class TerrainTiling
    {
        private TerrainTile[,] _tiles;
        private readonly List<TileDistance> _visibleTiles = new List<TileDistance>();

        public SkadiTerrain Terrain { get; private set; }
        public ObjectPool TerrainPool { get; private set; }

        public TerrainTilingMode TilingMode { get; private set; }
        public bool IsInitialized { get; private set; }
        public int GridSize { get; private set; }
        public int TileAlphamapResolution { get; private set; }
        public int TileHeightmapResolution { get; private set; }
        public int TileHolesResolution { get; private set; }

        public TerrainTiling(SkadiTerrain terrain)
        {
            Terrain = terrain;
            TerrainPool = new ObjectPool(400);
        }

        public void Initialize()
        {
            IsInitialized = true;

            // Update variables
            TilingMode = Terrain.TilingMode;
            GridSize = TilingMode == TerrainTilingMode.Single ? 1 : Terrain.TilingGridSize;
            TileAlphamapResolution = Terrain.Data.AlphamapResolution / GridSize;
            TileHeightmapResolution = (Terrain.Data.HeightmapResolution - 1) / GridSize + 1;
            TileHolesResolution = Terrain.Data.HolesTextureResolution / GridSize;

            // Destroy all instantiated tiles
            DestroyTiles();

            // Create new tile array if needed
            if (_tiles == null || _tiles.GetLength(0) != GridSize)
            {
                _tiles = new TerrainTile[GridSize, GridSize];
                for (var y = 0; y < GridSize; ++y)
                {
                    for (var x = 0; x < GridSize; ++x)
                    {
                        _tiles[y, x] = new TerrainTile(this, new int2(x, y));
                    }
                }
            }

            // Initialize tiles with data
            foreach (var tile in _tiles)
            {
                tile.Initialize();
            }

            // Add objects to corresponding tiles
            for (var i = 0; i < Terrain.Data.ObjectCount; ++i)
            {
                var obj = Terrain.Data.GetObject(i);
                var coordinate = LocalPositionToCoordinate(obj.Position);
                _tiles[coordinate.y, coordinate.x].AddObject(obj);
            }

            UpdateTileInstances();
        }

        public void Dispose()
        {
            IsInitialized = false;

            DestroyTiles();

            if (TerrainPool != null)
            {
                TerrainPool.Clear();
            }
        }

        public TerrainTile GetTile(int2 coordinate)
        {
            if (!IsInitialized || coordinate.x < 0 || coordinate.x >= GridSize || coordinate.y < 0 || coordinate.y >= GridSize)
            {
                return null;
            }

            return _tiles[coordinate.y, coordinate.x];
        }

        public void Update()
        {
            if (!IsInitialized)
            {
                return;
            }

            UpdateGridSize();
            UpdateTileInstances();
        }

        public void SyncHeightmap()
        {
            if (!IsInitialized)
            {
                return;
            }

            foreach (var tile in _tiles)
            {
                if (tile.IsInstantiated && tile.GroundTerrain != null)
                {
                    tile.GroundTerrain.terrainData.SyncHeightmap();
                }
            }
        }

        public void SyncWatermap()
        {
            if (!IsInitialized)
            {
                return;
            }

            foreach (var tile in _tiles)
            {
                if (tile.IsInstantiated && tile.WaterTerrain != null)
                {
                    tile.WaterTerrain.terrainData.SyncHeightmap();
                }
            }
        }

        private void DestroyTiles()
        {
            if (_tiles == null)
            {
                return;
            }

            foreach (var tile in _tiles)
            {
                tile.Destroy();
            }

            _visibleTiles.Clear();
        }

        private void UpdateGridSize()
        {
            // Watch for changes in settings and apply if needed
            var newGridSize = TilingMode == TerrainTilingMode.Single ? 1 : Terrain.TilingGridSize;
            if (GridSize != newGridSize || TilingMode != Terrain.TilingMode)
            {
                Initialize();
            }
        }

        private void UpdateTileInstances()
        {
            if (_tiles == null)
            {
                return;
            }

            UpdateVisibility();

            // First destroy all tiles that are not visible
            foreach (var tile in _tiles)
            {
                if (!tile.IsVisible && tile.IsInstantiated)
                {
                    tile.Destroy();
                }
            }

            // Next create tiles that are newly visible
            foreach (var tileDistance in _visibleTiles)
            {
                var tile = tileDistance.Tile;
                if (!tile.IsInstantiated)
                {
                    tile.Instantiate();
                    tile.UpdateNeighbours();

                    if (Application.isPlaying)
                    {
                        break;
                    }
                }
            }
        }

        private void UpdateVisibility()
        {
            switch (Terrain.TilingMode)
            {
                case TerrainTilingMode.Single:
                    UpdateSingleVisiblity();
                    break;
                case TerrainTilingMode.Tiled:
                    UpdateTiledVisiblity();
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(Terrain.TilingMode.ToString());
            }
        }

        private void UpdateSingleVisiblity()
        {
            foreach (var tile in _tiles)
            {
                tile.IsVisible = true;
                _visibleTiles.Add(new TileDistance(tile, 0));
            }
        }

        private void UpdateTiledVisiblity()
        {
            _visibleTiles.Clear();
            foreach (var tile in _tiles)
            {
                tile.IsVisible = false;
            }

            foreach (var revealer in TerrainTileRevealer.ActiveInstances)
            {
                var coordinate = WorldPositionToCoordinate(revealer.transform.position);
                foreach (var tile in _tiles)
                {
                    var delta = coordinate - tile.Coordinate;
                    var distance = length(delta);
                    if (distance <= revealer.RevealRadius)
                    {
                        tile.IsVisible = true;
                        _visibleTiles.Add(new TileDistance(tile, distance));
                    }
                }
            }

            _visibleTiles.Sort(TileDistance.CompareDistances);
        }

        private int2 WorldPositionToCoordinate(float3 position)
        {
            return LocalPositionToCoordinate(position - Terrain.Position);
        }

        private int2 LocalPositionToCoordinate(float3 position)
        {
            var coordinate = new int2(GridSize * position.xz / Terrain.Data.TerrainSize.xz);
            return clamp(coordinate, 0, GridSize - 1);
        }
    }
}
