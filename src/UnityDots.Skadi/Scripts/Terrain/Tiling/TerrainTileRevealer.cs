using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [ExecuteAlways]
    public sealed class TerrainTileRevealer : MonoBehaviour
    {
        public static List<TerrainTileRevealer> ActiveInstances { get; } = new List<TerrainTileRevealer>();

        [SerializeField]
        private int _revealRadius = 3;
        public int RevealRadius
        {
            get => _revealRadius;
            set => _revealRadius = value;
        }

        private void OnEnable()
        {
            ActiveInstances.Add(this);
        }

        private void OnDisable()
        {
            ActiveInstances.Remove(this);
        }
    }
}
