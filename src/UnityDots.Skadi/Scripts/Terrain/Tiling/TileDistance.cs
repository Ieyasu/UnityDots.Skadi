using System;
using System.Collections.Generic;

namespace UnityDots.Skadi
{
    internal struct TileDistance : IEquatable<TileDistance>
    {
        public float Distance { get; set; }
        public TerrainTile Tile { get; set; }

        public TileDistance(TerrainTile tile, float distance)
        {
            Tile = tile;
            Distance = distance;
        }

        public static int CompareDistances(TileDistance value1, TileDistance value2)
        {
            if (value1.Distance < value2.Distance)
            {
                return -1;
            }
            if (value1.Distance > value2.Distance)
            {
                return 1;
            }
            return 0;
        }

        public override bool Equals(object obj)
        {
            return obj is TileDistance other && Equals(other);
        }

        public bool Equals(TileDistance other)
        {
            return Distance == other.Distance && EqualityComparer<TerrainTile>.Default.Equals(Tile, other.Tile);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Distance, Tile);
        }
    }
}

