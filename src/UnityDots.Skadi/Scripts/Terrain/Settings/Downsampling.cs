namespace UnityDots.Skadi
{
    public enum Downsampling
    {
        None = 0,
        Half,
        Quarter,
        Eighth,
        Sixteenth,
    }
}
