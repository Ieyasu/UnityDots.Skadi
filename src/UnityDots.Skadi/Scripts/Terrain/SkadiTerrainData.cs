using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityDots.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SkadiTerrainData
    {
        // Data read from GPU to CPU
        private List<NativeArray<byte>> _alphamapsData = new List<NativeArray<byte>>();
        private List<NativeArray2D<float>> _detailmapsData = new List<NativeArray2D<float>>();
        private NativeArray2D<float> _heightmapData;
        private NativeArray2D<float> _watermapData;
        private NativeArray2D<float3> _normalmapData;

        public int ObjectCount => _objectData.Count;
        public int AlphamapCount => _alphamaps.Count;
        public int DetailmapCount => _detailmaps.Count;
        public int HolesTextureResolution => HeightmapResolution - 1;

        [SerializeField]
        private List<RenderTexture> _alphamaps = new List<RenderTexture>();

        [SerializeField]
        private List<RenderTexture> _detailmaps = new List<RenderTexture>();

        [SerializeField]
        private List<RawObjectData> _objectData = new List<RawObjectData>();

        [SerializeField]
        private int _alphamapLayers;
        public int AlphamapLayers => _alphamapLayers;

        [SerializeField]
        private int _alphamapResolution;
        public int AlphamapResolution => _alphamapResolution;

        [SerializeField]
        private int _detailmapResolution;
        public int DetailmapResolution => _detailmapResolution;

        [SerializeField]
        private int _heightmapResolution;
        public int HeightmapResolution => _heightmapResolution;

        [SerializeField]
        private float3 _terrainSize;
        public float3 TerrainSize => _terrainSize;

        // GPU data
        [SerializeField]
        private RenderTexture _heightmap;
        public RenderTexture Heightmap => _heightmap;

        [SerializeField]
        private RenderTexture _normalmap;
        public RenderTexture Normalmap => _normalmap;

        [SerializeField]
        private RenderTexture _watermap;
        public RenderTexture Watermap => _watermap;

        [SerializeField]
        private RenderTexture _waterHolesTexture;
        public RenderTexture WaterHolesTexture => _waterHolesTexture;

        [SerializeField]
        private DetailPrototype[] _detailPrototypes;
        public DetailPrototype[] DetailPrototypes => _detailPrototypes;

        [SerializeField]
        private ObjectPrototype[] _objectPrototypes;
        public ObjectPrototype[] ObjectPrototypes => _objectPrototypes;

        [SerializeField]
        private TerrainLayer[] _unityTerrainLayers;
        public TerrainLayer[] UnityTerrainLayers => _unityTerrainLayers;

        [SerializeField]
        private bool _syncHeightmapToCPU = true;
        public bool SyncHeightmapToCPU
        {
            get => _syncHeightmapToCPU;
            set => _syncHeightmapToCPU = value;
        }

        [SerializeField]
        private bool _syncWatermapToCPU = true;
        public bool SyncWatermapToCPU
        {
            get => _syncWatermapToCPU;
            set => _syncWatermapToCPU = value;
        }

        [SerializeField]
        private bool _syncAlphamapsToCPU = true;
        public bool SyncAlphamapsToCPU
        {
            get => _syncAlphamapsToCPU;
            set => _syncAlphamapsToCPU = value;
        }

        [SerializeField]
        private Downsampling _heightmapDownsampling = Downsampling.None;
        public Downsampling HeightmapDownsampling
        {
            get => _heightmapDownsampling;
            set => _heightmapDownsampling = value;
        }

        [SerializeField]
        private Downsampling _alphamapDownsampling = Downsampling.None;
        public Downsampling AlphamapDownsampling
        {
            get => _alphamapDownsampling;
            set => _alphamapDownsampling = value;
        }

        [SerializeField]
        private Downsampling _detailmapDownsampling = Downsampling.Half;
        public Downsampling DetailmapDownsampling
        {
            get => _detailmapDownsampling;
            set => _detailmapDownsampling = value;
        }

        internal void Initialize(RawTerrainData data)
        {
            Dispose();

            _alphamapResolution = data.AlphamapResolution >> (int)AlphamapDownsampling;
            _detailmapResolution = data.DetailmapResolution >> (int)DetailmapDownsampling;
            _heightmapResolution = ((data.HeightmapResolution - 1) >> (int)HeightmapDownsampling) + 1;
            _terrainSize = data.TerrainSize;

            _detailPrototypes = data.DetailPrototypes;
            _objectPrototypes = data.ObjectPrototypes;
            _unityTerrainLayers = data.UnityTerrainLayers;

            // Generate heightmap and normalmap
            if (data.HeightmapCount > 0)
            {
                var heightmap = data.GetHeightmap(0);
                _heightmap = CreateHeightmapTexture(heightmap);
                _normalmap = CreateNormalmapTexture(heightmap);
            }

            // Gneerate watermaps
            if (data.WatermapCount > 0)
            {
                var watermap = data.GetWatermap(0);
                _watermap = CreateHeightmapTexture(watermap);

                if (data.HolesTexturesCount > 0)
                {
                    var holesTexture = data.GetHolesTextures(0);
                    _waterHolesTexture = CreateHolesTexture(holesTexture);
                }
            }

            // Generate detailmaps
            for (var i = 0; i < data.DetailmapCount; ++i)
            {
                _detailmaps.Add(CreateDetailmapTexture(data.GetDetailmap(i)));
            }
            _detailmapsData.Resize(data.DetailmapCount);

            // Generate alphamaps
            for (var i = 0; i < data.AlphamapCount; i += 4)
            {
                var alphamap1 = data.GetAlphamap(i);
                var alphamap2 = i + 1 < data.AlphamapCount ? data.GetAlphamap(i + 1) : null;
                var alphamap3 = i + 2 < data.AlphamapCount ? data.GetAlphamap(i + 2) : null;
                var alphamap4 = i + 3 < data.AlphamapCount ? data.GetAlphamap(i + 3) : null;
                _alphamaps.Add(CreateAlphamapTexture(alphamap1, alphamap2, alphamap3, alphamap4, false));
            }
            _alphamapsData.Resize(data.AlphamapCount);
            _alphamapLayers = data.AlphamapCount;

            // Read object data from GPU
            for (var i = 0; i < data.ObjectBufferCount; ++i)
            {
                var buffer = data.GetObjectBuffer(i);
                ReadObjectBufferToCPU(buffer);
            }

            // Dipose of raw terrain data
            data.Dispose();
        }

        public void Dispose()
        {
            _objectData.Clear();

            DisposeTextures(_alphamaps);
            DisposeTextures(_detailmaps);
            DisposeArrays(_alphamapsData);
            DisposeArrays(_detailmapsData);

            if (_heightmapData.IsCreated)
            {
                _heightmapData.Dispose();
            }

            if (_watermapData.IsCreated)
            {
                _watermapData.Dispose();
            }

            if (_normalmapData.IsCreated)
            {
                _normalmapData.Dispose();
            }

            if (_heightmap != null)
            {
                _heightmap.Release();
                _heightmap = null;
            }

            if (_normalmap != null)
            {
                _normalmap.Release();
                _normalmap = null;
            }

            if (_watermap != null)
            {
                _watermap.Release();
                _watermap = null;
            }

            if (_waterHolesTexture != null)
            {
                _waterHolesTexture.Release();
                _waterHolesTexture = null;
            }

            _detailPrototypes = null;
            _objectPrototypes = null;
            _unityTerrainLayers = null;
        }

        public float GetHeight(int x, int y)
        {
            var heightmapData = GetHeightmapData();
            return heightmapData[y, x];
        }

        public float GetInterpolatedHeight(float x, float y)
        {
            var heightmapData = GetHeightmapData();

            var xC = x * HeightmapResolution;
            var yC = y * HeightmapResolution;
            var xLo = (int)clamp(xC, 0, HeightmapResolution - 1);
            var yLo = (int)clamp(yC, 0, HeightmapResolution - 1);
            var xHi = xLo == HeightmapResolution - 1 ? xLo : xLo + 1;
            var yHi = yLo == HeightmapResolution - 1 ? yLo : yLo + 1;
            var xFrac = xC - xLo;
            var yFrac = yC - yLo;

            var val11 = heightmapData[yLo, xLo];
            var val12 = heightmapData[yLo, xHi];
            var val1 = val11 + xFrac * (val12 - val11);
            var val21 = heightmapData[yHi, xLo];
            var val22 = heightmapData[yHi, xHi];
            var val2 = val21 + xFrac * (val22 - val21);
            return val1 + yFrac * (val2 - val1);
        }

        public float3 GetInterpolatedNormal(float x, float y)
        {
            var normalmapData = GetNormalmapData();

            var xC = x * HeightmapResolution;
            var yC = y * HeightmapResolution;
            var xLo = (int)clamp(xC, 0, HeightmapResolution - 1);
            var yLo = (int)clamp(yC, 0, HeightmapResolution - 1);
            var xHi = xLo == HeightmapResolution - 1 ? xLo : xLo + 1;
            var yHi = yLo == HeightmapResolution - 1 ? yLo : yLo + 1;
            var xFrac = xC - xLo;
            var yFrac = yC - yLo;

            var val11 = normalmapData[yLo, xLo];
            var val12 = normalmapData[yLo, xHi];
            var val1 = val11 + xFrac * (val12 - val11);
            var val21 = normalmapData[yHi, xLo];
            var val22 = normalmapData[yHi, xHi];
            var val2 = val21 + xFrac * (val22 - val21);
            return val1 + yFrac * (val2 - val1);
        }

        public RawObjectData GetObject(int index)
        {
            return _objectData[index];
        }

        public RenderTexture GetAlphamap(int index)
        {
            return _alphamaps[index];
        }

        public RenderTexture GetDetailmap(int index)
        {
            return _detailmaps[index];
        }

        public void GetHeightmap(int x, int y, float[,] heightmap)
        {
            if (Heightmap == null)
            {
                return;
            }

            var heightmapData = GetHeightmapData();
            var width = heightmap.GetLength(1);
            var height = heightmap.GetLength(0);
            for (var cy = 0; cy < height; ++cy)
            {
                var hy = (y + cy) * HeightmapResolution;
                for (var cx = 0; cx < width; ++cx)
                {
                    var hx = x + cx;
                    heightmap[cy, cx] = heightmapData[hy + hx];
                }
            }
        }

        public void GetWatermap(int x, int y, float[,] watermap)
        {
            if (Watermap == null)
            {
                return;
            }

            var watermapData = GetWatermapData();
            var width = watermap.GetLength(1);
            var height = watermap.GetLength(0);
            for (var cy = 0; cy < height; ++cy)
            {
                var hy = (y + cy) * HeightmapResolution;
                for (var cx = 0; cx < width; ++cx)
                {
                    var hx = x + cx;
                    watermap[cy, cx] = watermapData[hy + hx];
                }
            }
        }

        public void GetAlphamaps(int x, int y, float[,,] alphamaps)
        {
            for (var i = 0; i < AlphamapLayers; ++i)
            {
                GetAlphamap(x, y, i, alphamaps);
            }
        }

        public void GetAlphamap(int x, int y, int i, float[,,] alphamaps)
        {
            if (AlphamapLayers <= i)
            {
                return;
            }

            var index = i / 4;
            if (!_alphamapsData[index].IsCreated)
            {
                _alphamapsData[index] = TextureUtility.ReadTextureAsBytes(_alphamaps[index]);
            }

            var alphamapData = _alphamapsData[index];
            var channel = i % 4;

            var width = alphamaps.GetLength(1);
            var height = alphamaps.GetLength(0);
            for (var cy = 0; cy < height; ++cy)
            {
                var hy = (y + cy) * AlphamapResolution;
                for (var cx = 0; cx < width; ++cx)
                {
                    var hx = x + cx;
                    var pixel = hy + hx;
                    alphamaps[cy, cx, i] = alphamapData[4 * pixel + channel] * (1.0f / 255);
                }
            }
        }

        public void GetDetailmaps(int x, int y, float[,,] detailmaps)
        {
            for (var i = 0; i < DetailmapCount; ++i)
            {
                GetDetailmap(x, y, i, detailmaps);
            }
        }

        public void GetDetailmap(int x, int y, int i, float[,,] detailmaps)
        {
            if (DetailmapCount <= i)
            {
                return;
            }

            var detailmapData = GetDetailmapData(i);
            var width = detailmaps.GetLength(1);
            var height = detailmaps.GetLength(0);
            for (var cy = 0; cy < height; ++cy)
            {
                var hy = (y + cy) * DetailmapResolution;
                for (var cx = 0; cx < width; ++cx)
                {
                    var hx = x + cx;
                    detailmaps[cy, cx, i] = detailmapData[hy + hx];
                }
            }
        }

        private NativeArray2D<float> GetDetailmapData(int i)
        {
            // Read heightmap to CPU if not already done
            var detailmapData = _detailmapsData[i];
            if (!detailmapData.IsCreated)
            {
                var data = TextureUtility.ReadTextureAsFloats(GetDetailmap(i));
                detailmapData = new NativeArray2D<float>(data, DetailmapResolution, DetailmapResolution);
                _detailmapsData[i] = detailmapData;
            }
            return detailmapData;
        }

        private NativeArray2D<float> GetHeightmapData()
        {
            // Read heightmap to CPU if not already done
            if (!_heightmapData.IsCreated)
            {
                var data = TextureUtility.ReadTextureAsFloats(Heightmap);
                _heightmapData = new NativeArray2D<float>(data, HeightmapResolution, HeightmapResolution);

                for (var i = 0; i < _heightmapData.Length; ++i)
                {
                    _heightmapData[i] *= 2;
                }
            }

            return _heightmapData;
        }

        private NativeArray2D<float3> GetNormalmapData()
        {
            // Read heightmap to CPU if not already done
            if (!_normalmapData.IsCreated)
            {
                var data = TextureUtility.ReadTextureAsBytes(Normalmap);
                _normalmapData = new NativeArray2D<float3>(HeightmapResolution, HeightmapResolution, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
                for (var y = 0; y < HeightmapResolution; ++y)
                {
                    for (var x = 0; x < HeightmapResolution; ++x)
                    {
                        //https://aras-p.info/texts/CompactNormalStorage.html
                        var index = 2 * (y * HeightmapResolution + x);
                        var fenc = 4.0f * new float2(data[index], data[index + 1]) / 255.0f - 2.0f;
                        var f = dot(fenc, fenc);
                        var normal = new float3();
                        normal.xy = fenc * sqrt(1.0f - 0.25f * f);
                        normal.z = 1.0f - 0.5f * f;
                        _normalmapData[y, x] = normal;
                    }
                }
                data.Dispose();
            }

            return _normalmapData;
        }

        private NativeArray2D<float> GetWatermapData()
        {
            // Read watermap to CPU if not already done
            if (!_watermapData.IsCreated)
            {
                var data = TextureUtility.ReadTextureAsFloats(Watermap);
                _watermapData = new NativeArray2D<float>(data, HeightmapResolution, HeightmapResolution);

                for (var i = 0; i < _watermapData.Length; ++i)
                {
                    _watermapData[i] *= 2;
                }
            }

            return _watermapData;
        }

        private void DisposeArrays<T>(List<NativeArray<T>> list) where T : struct
        {
            foreach (var array in list)
            {
                if (array.IsCreated)
                {
                    array.Dispose();
                }
            }
            list.Clear();
        }

        private void DisposeArrays<T>(List<NativeArray2D<T>> list) where T : struct
        {
            foreach (var array in list)
            {
                if (array.IsCreated)
                {
                    array.Dispose();
                }
            }
            list.Clear();
        }

        private void DisposeTextures(List<RenderTexture> list)
        {
            foreach (var texture in list)
            {
                if (texture != null)
                {
                    texture.Release();
                }
            }
            list.Clear();
        }

        private void ReadObjectBufferToCPU(ComputeBuffer buffer)
        {
            // Read data from GPU
            var request = AsyncGPUReadback.Request(buffer);
            request.WaitForCompletion();
            var data = request.GetData<float>();

            // Count the number of active objects
            var objSize = buffer.stride / 4;
            var objCount = 0;
            for (var j = 0; j < buffer.count; ++j)
            {
                var dataIndex = j * objSize;

                // Count only active objects
                var isActive = (int)data[dataIndex + 11];
                if (isActive <= 0)
                {
                    continue;
                }

                // Count only objects that can be instantiated
                var prototypeID = (int)data[dataIndex];
                if (prototypeID < 0)
                {
                    continue;
                }

                objCount++;
            }

            // Create usable structs from the raw data
            for (var j = 0; j < buffer.count; ++j)
            {
                var dataIndex = j * objSize;

                // Skip inactive objects
                var isActive = (int)data[dataIndex + 11];
                if (isActive <= 0)
                {
                    continue;
                }

                // Count only objects that can be instantiated
                var prototypeID = (int)data[dataIndex];
                if (prototypeID < 0)
                {
                    continue;
                }

                var obj = new RawObjectData
                {
                    PrototypeID = prototypeID,
                    Position = new float3(data[dataIndex + 1], data[dataIndex + 2], data[dataIndex + 3]),
                    Rotation = new quaternion(data[dataIndex + 5], data[dataIndex + 6], data[dataIndex + 7], data[dataIndex + 8]),
                    Scale = data[dataIndex + 4],
                };
                _objectData.Add(obj);
            }
        }

        private RenderTexture CreateHeightmapTexture(Texture input)
        {
            var format = Terrain.heightmapFormat;
            var descriptor = TextureUtility.GetRenderTextureDescriptor(format, HeightmapResolution, HeightmapResolution, false);
            var output = new RenderTexture(descriptor);
            output.Create();

            var shader = Resources.Load<ComputeShader>("GPU/Terrain/CreateHeightmap");
            var kernelName = format == GraphicsFormat.R8G8_UNorm ? "CS_R8G8" : "CS_R16";
            var kernelID = shader.FindKernel(kernelName);
            shader.SetVector("_OutputResolution", TextureUtility.GetSizeVector(output));
            shader.SetTexture(kernelID, "_Input", input);
            shader.SetTexture(kernelID, "_Output", output);
            shader.DispatchUnsafe(kernelID, output.width, output.height, 1);

            return output;
        }

        private RenderTexture CreateNormalmapTexture(Texture input)
        {
            var format = TextureUtility.NormalMapFormat;
            var descriptor = TextureUtility.GetRenderTextureDescriptor(format, HeightmapResolution, HeightmapResolution, false);
            var output = new RenderTexture(descriptor);
            output.Create();

            var shader = Resources.Load<ComputeShader>("GPU/Util/CalculateNormals");
            var terrainSizeVector = TextureUtility.GetSizeVector(TerrainSize.xz);
            var resolutionVector = TextureUtility.GetSizeVector(output);
            var cellSize = terrainSizeVector * resolutionVector.zwxy;
            shader.SetFloat("skadi_TerrainHeight", TerrainSize.y);
            shader.SetVector("skadi_CellSize", cellSize);
            shader.SetVector("_OutputNormalmapResolution", resolutionVector);
            shader.SetTexture(0, "_InputHeightmap", input);
            shader.SetTexture(0, "_OutputNormalmap", output);
            shader.DispatchUnsafe(0, output.width, output.height, 1);
            return output;
        }

        private RenderTexture CreateDetailmapTexture(Texture input)
        {
            var descriptor = TextureUtility.GetRenderTextureDescriptor(input.graphicsFormat, DetailmapResolution, DetailmapResolution, false);
            var output = new RenderTexture(descriptor);
            output.Create();

            var shader = Resources.Load<ComputeShader>("GPU/Terrain/CreateDetailmap");
            shader.SetVector("_OutputResolution", TextureUtility.GetSizeVector(output));
            shader.SetTexture(0, "_Input", input);
            shader.SetTexture(0, "_Output", output);
            shader.DispatchSafe(0, output.width, output.height, 1);

            return output;
        }

        private RenderTexture CreateHolesTexture(Texture input)
        {
            var descriptor = TextureUtility.GetRenderTextureDescriptor(Terrain.holesFormat, HolesTextureResolution, HolesTextureResolution, false);
            var output = new RenderTexture(descriptor);
            output.Create();

            var shader = Resources.Load<ComputeShader>("GPU/Terrain/CreateHolesTexture");
            shader.SetVector("_OutputResolution", TextureUtility.GetSizeVector(output));
            shader.SetTexture(0, "_Input", input);
            shader.SetTexture(0, "_Output", output);
            shader.DispatchSafe(0, output.width, output.height, 1);
            return output;
        }

        private RenderTexture CreateAlphamapTexture(Texture input1, Texture input2, Texture input3, Texture input4, bool flip)
        {
            var format = TextureUtility.AlphamapFormat;
            var descriptor = TextureUtility.GetRenderTextureDescriptor(format, AlphamapResolution, AlphamapResolution, true);
            descriptor.autoGenerateMips = false;
            var output = new RenderTexture(descriptor);
            output.Create();

            var kernelName = "CS_RGBA";
            if (input2 == null)
            {
                kernelName = "CS_R";
            }
            else if (input3 == null)
            {
                kernelName = "CS_RG";
            }
            else if (input4 == null)
            {
                kernelName = "CS_RGB";
            }

            var shader = Resources.Load<ComputeShader>("GPU/Terrain/CreateAlphamap");
            var kernelID = shader.FindKernel(kernelName);

            // For some reason when syncing to CPU, the alphamaps are wrong way around
            // We fix it by creating the alphamap the wrong way ourselves
            // Sometimes two wrongs make a right...
            shader.SetFloat("_Flip", flip ? 1 : 0);
            shader.SetVector("_OutputResolution", TextureUtility.GetSizeVector(output));
            shader.SetTexture(kernelID, "_InputR", input1);
            if (input2 != null)
            {
                shader.SetTexture(kernelID, "_InputG", input2);
            }
            if (input3 != null)
            {
                shader.SetTexture(kernelID, "_InputB", input3);
            }
            if (input4 != null)
            {
                shader.SetTexture(kernelID, "_InputA", input4);
            }
            shader.SetTexture(kernelID, "_Output", output);
            shader.DispatchUnsafe(kernelID, output.width, output.height, 1);

            output.GenerateMips();

            return output;
        }
    }
}
