using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    public class TerrainBuilder
    {
        private Terrain _prefab;
        private string _name;
        private float3 _size;
        private int _alphamapResolution;
        private int _basemapResolution;
        private int _heightmapResolution;

        public TerrainBuilder SetPrefab(Terrain prefab)
        {
            _prefab = prefab;
            return this;
        }

        public TerrainBuilder SetName(string name)
        {
            _name = name;
            return this;
        }

        public TerrainBuilder SetSize(float3 size)
        {
            _size = size;
            return this;
        }

        public TerrainBuilder SetAlphamapResolution(int resolution)
        {
            _alphamapResolution = resolution;
            return this;
        }

        public TerrainBuilder SetBasemapResolution(int resolution)
        {
            _basemapResolution = resolution;
            return this;
        }

        public TerrainBuilder SetHeightmapResolution(int resolution)
        {
            _heightmapResolution = resolution;
            return this;
        }

        public Terrain Build()
        {
            var terrain = Object.Instantiate(_prefab);
            terrain.name = _name ?? _prefab.name;
            terrain.enabled = true;

            terrain.terrainData = new TerrainData
            {
                name = $"{_name} Data",
                alphamapResolution = _alphamapResolution,
                baseMapResolution = _basemapResolution,
                heightmapResolution = _heightmapResolution,
                size = _size,
            };

            var collider = terrain.GetComponent<TerrainCollider>();
            if (collider != null)
            {
                collider.terrainData = terrain.terrainData;
            }

            return terrain;
        }

        public static void ApplySettings(Terrain src, Terrain dst)
        {
            if (dst.allowAutoConnect != src.allowAutoConnect)
            {
                dst.allowAutoConnect = src.allowAutoConnect;
            }
            if (dst.basemapDistance != src.basemapDistance)
            {
                dst.basemapDistance = src.basemapDistance;
            }
            if (dst.detailObjectDensity != src.detailObjectDensity)
            {
                dst.detailObjectDensity = src.detailObjectDensity;
            }
            if (dst.detailObjectDistance != src.detailObjectDistance)
            {
                dst.detailObjectDistance = src.detailObjectDistance;
            }
            if (dst.drawHeightmap != src.drawHeightmap)
            {
                dst.drawHeightmap = src.drawHeightmap;
            }
            if (dst.drawInstanced != src.drawInstanced)
            {
                dst.drawInstanced = src.drawInstanced;
            }
            if (dst.drawTreesAndFoliage != src.drawTreesAndFoliage)
            {
                dst.drawTreesAndFoliage = src.drawTreesAndFoliage;
            }
            if (dst.editorRenderFlags != src.editorRenderFlags)
            {
                dst.editorRenderFlags = src.editorRenderFlags;
            }
            if (dst.freeUnusedRenderingResources != src.freeUnusedRenderingResources)
            {
                dst.freeUnusedRenderingResources = src.freeUnusedRenderingResources;
            }
            if (dst.heightmapMaximumLOD != src.heightmapMaximumLOD)
            {
                dst.heightmapMaximumLOD = src.heightmapMaximumLOD;
            }
            if (dst.heightmapPixelError != src.heightmapPixelError)
            {
                dst.heightmapPixelError = src.heightmapPixelError;
            }
            if (dst.hideFlags != src.hideFlags)
            {
                dst.hideFlags = src.hideFlags;
            }
            if (dst.lightmapIndex != src.lightmapIndex)
            {
                dst.lightmapIndex = src.lightmapIndex;
            }
            if (dst.lightmapScaleOffset != src.lightmapScaleOffset)
            {
                dst.lightmapScaleOffset = src.lightmapScaleOffset;
            }
            if (dst.materialTemplate != src.materialTemplate)
            {
                dst.materialTemplate = src.materialTemplate;
            }
            if (dst.name != src.name)
            {
                dst.name = src.name;
            }
            if (dst.patchBoundsMultiplier != src.patchBoundsMultiplier)
            {
                dst.patchBoundsMultiplier = src.patchBoundsMultiplier;
            }
            if (dst.preserveTreePrototypeLayers != src.preserveTreePrototypeLayers)
            {
                dst.preserveTreePrototypeLayers = src.preserveTreePrototypeLayers;
            }
            if (dst.realtimeLightmapIndex != src.realtimeLightmapIndex)
            {
                dst.realtimeLightmapIndex = src.realtimeLightmapIndex;
            }
            if (dst.reflectionProbeUsage != src.reflectionProbeUsage)
            {
                dst.reflectionProbeUsage = src.reflectionProbeUsage;
            }
            if (dst.renderingLayerMask != src.renderingLayerMask)
            {
                dst.renderingLayerMask = src.renderingLayerMask;
            }
            if (dst.shadowCastingMode != src.shadowCastingMode)
            {
                dst.shadowCastingMode = src.shadowCastingMode;
            }
            if (dst.tag != src.tag)
            {
                dst.tag = src.tag;
            }
            if (dst.treeBillboardDistance != src.treeBillboardDistance)
            {
                dst.treeBillboardDistance = src.treeBillboardDistance;
            }
            if (dst.treeCrossFadeLength != src.treeCrossFadeLength)
            {
                dst.treeCrossFadeLength = src.treeCrossFadeLength;
            }
            if (dst.treeDistance != src.treeDistance)
            {
                dst.treeDistance = src.treeDistance;
            }
            if (dst.treeLODBiasMultiplier != src.treeLODBiasMultiplier)
            {
                dst.treeLODBiasMultiplier = src.treeLODBiasMultiplier;
            }
            if (dst.treeMaximumFullLODCount != src.treeMaximumFullLODCount)
            {
                dst.treeMaximumFullLODCount = src.treeMaximumFullLODCount;
            }

            if (dst.terrainData.baseMapResolution != src.terrainData.baseMapResolution)
            {
                dst.terrainData.baseMapResolution = src.terrainData.baseMapResolution;
            }
            if (dst.terrainData.detailPrototypes != src.terrainData.detailPrototypes)
            {
                dst.terrainData.detailPrototypes = src.terrainData.detailPrototypes;
            }
            if (dst.terrainData.enableHolesTextureCompression != src.terrainData.enableHolesTextureCompression)
            {
                dst.terrainData.enableHolesTextureCompression = src.terrainData.enableHolesTextureCompression;
            }
            if (dst.terrainData.name != src.terrainData.name)
            {
                dst.terrainData.name = src.terrainData.name;
            }
            if (dst.terrainData.treePrototypes != src.terrainData.treePrototypes)
            {
                dst.terrainData.treePrototypes = src.terrainData.treePrototypes;
            }
            if (dst.terrainData.wavingGrassAmount != src.terrainData.wavingGrassAmount)
            {
                dst.terrainData.wavingGrassAmount = src.terrainData.wavingGrassAmount;
            }
            if (dst.terrainData.wavingGrassSpeed != src.terrainData.wavingGrassSpeed)
            {
                dst.terrainData.wavingGrassSpeed = src.terrainData.wavingGrassSpeed;
            }
            if (dst.terrainData.wavingGrassStrength != src.terrainData.wavingGrassStrength)
            {
                dst.terrainData.wavingGrassStrength = src.terrainData.wavingGrassStrength;
            }
            if (dst.terrainData.wavingGrassTint != src.terrainData.wavingGrassTint)
            {
                dst.terrainData.wavingGrassTint = src.terrainData.wavingGrassTint;
            }
        }
    }
}
