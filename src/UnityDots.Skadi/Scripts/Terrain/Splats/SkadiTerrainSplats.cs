using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityDots.Skadi
{
    // [CreateAssetMenu(fileName = "Skadi Terrain Splats", menuName = "Skadi/Splat Data", order = 1)]
    public sealed class SkadiTerrainSplats : ScriptableObject
    {
        [SerializeField]
        private int _splatmapResolution = 512;
        public int SplatmapResolution
        {
            get => _splatmapResolution;
            set
            {
                _splatmapResolution = Mathf.Clamp(Mathf.NextPowerOfTwo(_splatmapResolution), 16, 4092);
                Splatmap.Resize(_splatmapResolution, _splatmapResolution);
                Splatmap.Apply(true, false);
            }
        }

        [SerializeField]
        private List<SkadiTerrainLayer> _layers = null;
        public List<SkadiTerrainLayer> Layers
        {
            get { return _layers; }
        }

        [SerializeField]
        private Texture2DArray _diffuseTextures = null;
        public Texture2DArray DiffuseTextures
        {
            get => _diffuseTextures;
            set => _diffuseTextures = value;
        }

        [SerializeField]
        private Texture2DArray _normalMaps = null;
        public Texture2DArray NormalMaps
        {
            get => _normalMaps;
            set => _normalMaps = value;
        }

        [SerializeField]
        private Texture2DArray _maskMaps = null;
        public Texture2DArray MaskMaps
        {
            get => _maskMaps;
            set => _maskMaps = value;
        }

        [SerializeField]
        private Texture2D _splatmap = null;
        public Texture2D Splatmap
        {
            get
            {
                if (_splatmap != null && !TextureUtility.MatchesResolution(_splatmap, _splatmapResolution))
                {
                    _splatmap.Resize(_splatmapResolution, _splatmapResolution);
                    _splatmap.Apply();
                }

                if (_splatmap == null)
                {
                    _splatmap = new Texture2D(_splatmapResolution, _splatmapResolution, TextureFormat.RGBA32, true, true)
                    {
                        name = "Splatmap",
                        wrapMode = TextureWrapMode.Clamp,
                        filterMode = FilterMode.Bilinear
                    };
                }

#if UNITY_EDITOR
                if (!UnityEditor.AssetDatabase.IsSubAsset(_splatmap))
                {
                    UnityEditor.AssetDatabase.AddObjectToAsset(_splatmap, this);
                    var path = UnityEditor.AssetDatabase.GetAssetPath(_splatmap);
                    UnityEditor.AssetDatabase.ImportAsset(path);
                    _splatmap = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path).First(x => x.name == "Splatmap") as Texture2D;
                }
#endif
                return _splatmap;
            }
        }

        [SerializeField]
        private Texture2D _splatmapIndices = null;
        public Texture2D SplatmapIndices
        {
            get
            {
                if (_splatmapIndices != null && !TextureUtility.MatchesResolution(_splatmapIndices, _splatmapResolution))
                {
                    _splatmapIndices.Resize(_splatmapResolution, _splatmapResolution);
                    _splatmapIndices.Apply();
                }

                if (_splatmapIndices == null)
                {
                    _splatmapIndices = new Texture2D(_splatmapResolution, _splatmapResolution, TextureFormat.RGBA32, false, true)
                    {
                        name = "SplatmapIndices",
                        wrapMode = TextureWrapMode.Clamp,
                        filterMode = FilterMode.Point
                    };
                }

#if UNITY_EDITOR
                if (!UnityEditor.AssetDatabase.IsSubAsset(_splatmapIndices))
                {
                    UnityEditor.AssetDatabase.AddObjectToAsset(_splatmapIndices, this);
                    var path = UnityEditor.AssetDatabase.GetAssetPath(_splatmapIndices);
                    UnityEditor.AssetDatabase.ImportAsset(path);
                    _splatmapIndices = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path).First(x => x.name == "SplatmapIndices") as Texture2D;
                }
#endif
                return _splatmapIndices;
            }
        }
    }
}
