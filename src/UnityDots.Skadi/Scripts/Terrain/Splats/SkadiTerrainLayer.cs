using UnityEngine;

namespace UnityDots.Skadi
{
    public enum SkadiTerrainLayerType
    {
        Overlay = 0,
        Background = 1
    }

    // [CreateAssetMenu(fileName = "Skadi Terrain Layer", menuName = "Skadi/Terrain Layer", order = 1)]
    public sealed class SkadiTerrainLayer : ScriptableObject
    {
        [SerializeField]
        private TerrainLayer _terrainLayer;
        public TerrainLayer TerrainLayer
        {
            get => _terrainLayer;
            set => _terrainLayer = value;
        }

        [SerializeField]
        private SkadiTerrainLayerType _layerType = SkadiTerrainLayerType.Overlay;
        public SkadiTerrainLayerType Type
        {
            get => _layerType;
            set => _layerType = value;
        }

        [SerializeField]
        private Color _tint = Color.white;
        public Color Tint
        {
            get => _tint;
            set => _tint = value;
        }

        [SerializeField]
        private float _blendSharpness = 0.5f;
        public float BlendSharpness
        {
            get => _blendSharpness;
            set => _blendSharpness = value;
        }

        [SerializeField]
        private float _slopeThreshold = 0.5f;
        public float SlopeThreshold
        {
            get => _slopeThreshold;
            set => _slopeThreshold = value;
        }

        [SerializeField]
        private float _slopeBasedDamp = 0.5f;
        public float SlopeBasedDamp
        {
            get => _slopeBasedDamp;
            set => _slopeBasedDamp = value;
        }

        [SerializeField]
        private float _slopeBasedNormalDamp = 0.5f;
        public float SlopeBasedNormalDamp
        {
            get => _slopeBasedNormalDamp;
            set => _slopeBasedNormalDamp = value;
        }
    }
}
