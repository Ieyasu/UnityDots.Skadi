using UnityEngine;

namespace UnityDots.Skadi
{
    [System.Serializable]
    public sealed class ObjectPrototype
    {
        [SerializeField]
        private GameObject _prefab;
        public GameObject Prefab
        {
            get { return _prefab; }
            set { _prefab = value; }
        }

        [SerializeField]
        private float _prevalence;
        public float Prevalence
        {
            get => _prevalence;
            set => _prevalence = value;
        }

        [SerializeField]
        private MinMaxCurve _scale;
        public MinMaxCurve Scale
        {
            get => _scale;
            set => _scale = value;
        }

        [SerializeField]
        private MinMaxCurve _rotation;
        public MinMaxCurve Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }

        [SerializeField]
        private MinMaxCurve _radius;
        public MinMaxCurve Radius
        {
            get => _radius;
            set => _radius = value;
        }

        public static ObjectPrototype Create()
        {
            return new ObjectPrototype
            {
                _prefab = null,
                _prevalence = 1,
                _scale = new MinMaxCurve(0, float.MaxValue),
                _rotation = new MinMaxCurve(0, 360),
                _radius = new MinMaxCurve(0, float.MaxValue),
            };
        }
    }
}
