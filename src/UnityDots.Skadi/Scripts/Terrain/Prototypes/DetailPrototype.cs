using UnityEngine;
using UnityDots.Collections;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [System.Serializable]
    public sealed class DetailPrototype
    {
        private bool _calculatedRadius;

        public bool IsValid => LODs.Length > 0;

        [SerializeField]
        [Tooltip("Material used for the details.")]
        private Material _material = null;
        public Material Material => _material;

        [SerializeField]
        [Tooltip("Layer to render the details to.")]
        private int _layer;
        public int Layer => _layer;

        [SerializeField]
        [Tooltip("Distance at which the details shadows are culled.")]
        private float _shadowCullDistance = 100;
        public float ShadowCullDistance => _shadowCullDistance;

        [SerializeField]
        private float _minScale = 1;
        public float MinScale => _minScale;

        [SerializeField]
        private float _maxScale = 1;
        public float MaxScale => _maxScale;

        [SerializeField]
        [Tooltip("Lods for the detail.")]
        private DetailLod[] _lods = null;
        public DetailLod[] LODs => _lods;

        // Material used for lod transitions.
        private Material _lodMaterial;
        public Material LodMaterial
        {
            get
            {
                if (_lodMaterial == null)
                {
                    _lodMaterial = new Material(Material);
                    _lodMaterial.EnableKeyword("LOD_FADE_CROSSFADE");
                }
                return _lodMaterial;
            }
        }

        // Radius used for culling.
        private float _radius;
        public float Radius
        {
            get
            {
                if (!_calculatedRadius)
                {
                    _radius = CalculateRadius();
                    _calculatedRadius = true;
                }
                return _radius;
            }
        }

        public static DetailPrototype Create()
        {
            return new DetailPrototype
            {
                _layer = LayerMask.NameToLayer("Default"),
                _shadowCullDistance = 100,
                _lods = new DetailLod[] { DetailLod.Create() },
            };
        }

        private float CalculateRadius()
        {
            if (LODs.Length == 0)
            {
                return 0;
            }

            using (var buffer = NonAllocList<Vector3>.Get())
            {
                var distanceSqr = 0.0f;

                foreach (var lod in LODs)
                {
                    lod.Mesh.GetVertices(buffer.Data);
                    foreach (var vertex in buffer)
                    {
                        distanceSqr = max(distanceSqr, vertex.sqrMagnitude);
                    }
                }

                return sqrt(distanceSqr) * MaxScale;
            }
        }
    }
}
