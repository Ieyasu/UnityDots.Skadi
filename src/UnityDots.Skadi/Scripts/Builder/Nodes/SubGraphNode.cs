using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Other/Sub Generator")]
    public sealed class SubGraphNode : NodeData, IGeneratorNode
    {
        [Control, SerializeField]
        private TerrainGenerator _generator = null;

        public bool CanBuild()
        {
            return _generator != null;
        }

        public void Build(TerrainGeneratorBuilder builder)
        {
            builder.BuildSubGenerator(_generator);

            var resources = builder.GetResources(_generator);
            InitializeSockets("Heightmap", resources.Heightmaps);
            InitializeSockets("Watermap", resources.Watermaps);
            InitializeSockets("Alphamap", resources.Alphamaps);
            InitializeSockets("Detailmap", resources.Detailmaps);
        }

        protected override void BuildSockets()
        {
            if (_generator == null)
            {
                return;
            }

            BuildSockets("Heightmap", _generator.Resources.Heightmaps);
            BuildSockets("Watermap", _generator.Resources.Watermaps);
            BuildSockets("Alphamap", _generator.Resources.Alphamaps);
            BuildSockets("Detailmap", _generator.Resources.Detailmaps);
        }

        private void BuildSockets(string name, List<TextureHandle> handles)
        {
            for (var i = 0; i < handles.Count; ++i)
            {
                var socket = new MapSocket { ID = OutputSockets.Count, Capacity = 1, Name = name };
                AddOutputSocket(socket);
            }
        }

        private void InitializeSockets(string name, List<MapHandle> handles)
        {
            var sockets = OutputSockets.Where(x => x.Name == name).ToArray();
            for (var i = 0; i < handles.Count; ++i)
            {
                var socket = sockets[i];
                var handle = handles[i];
                socket.SetValue(handle);
            }
        }
    }
}
