using System;
using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Other/Biome Generator")]
    public sealed class BiomeNode : NodeData, IGeneratorNode, IMultiInputNode
    {
        private const int _minInputSocketCount = 1;
        private const int _maxInputSocketCount = 32;

        [SerializeField]
        private List<Biome> _biomes = null;

        public bool CanBuild()
        {
            return _biomes != null
                    && _biomes.Count > 0
                    && _biomes.All(x => x.Generator != null)
                    && InputSockets.All(x => x.HasValue);
        }

        public void Build(TerrainGeneratorBuilder builder)
        {
            Debug.Assert(InputSockets.Count == _biomes.Count);

            // Build each sub generator
            foreach (var biome in _biomes)
            {
                builder.BuildSubGenerator(biome.Generator);
            }

            // Create combined height and water maps
            var weightsSum = BuildBiomeWeightsSum(builder);
            var heightmapSum = BuildMaps(builder, weightsSum);
            BuildAlphamaps(builder, weightsSum);
            BuildObjects(builder, weightsSum, heightmapSum);
        }

        public bool CanAddInputSocket()
        {
            return InputSockets.Count < _maxInputSocketCount;
        }

        public bool CanRemoveInputSocket()
        {
            return InputSockets.Count > _minInputSocketCount;
        }

        public void OnInputSocketAdd()
        {
            CreateSocket(InputSockets.Count);
            ResizeList();
        }

        public void OnInputSocketRemove()
        {
            var id = InputSockets.Last().ID;
            RemoveInputSocket(id);
            ResizeList();
        }

        private MapHandle BuildMaps(TerrainGeneratorBuilder builder, MapHandle weightsSum)
        {
            var emptyMap = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.ClearTexture(emptyMap.Texture, 0);

            var heightmap = BuildMap(builder, weightsSum, (resources) => resources.Heightmaps.Count > 0 ? resources.Heightmaps[0] : emptyMap);
            var watermap = BuildMap(builder, weightsSum, (resources) => resources.Watermaps.Count > 0 ? resources.Watermaps[0] : emptyMap);

            builder.AddHeightmap(heightmap);
            builder.AddWatermap(watermap);

            return heightmap;
        }

        private void BuildAlphamaps(TerrainGeneratorBuilder builder, MapHandle weightsSum)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/BiomeNode");
            var kernelID = shader.FindKernel("CSAddAlphamap");

            for (var i = 0; i < _biomes.Count; ++i)
            {
                var biome = _biomes[i];
                var resources = builder.GetResources(biome.Generator);
                var weightmap = InputSockets[i].GetValue() as MapHandle;

                for (var j = 0; j < resources.Alphamaps.Count; ++j)
                {
                    var output = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
                    var alphamap = resources.Alphamaps[j];
                    var layer = resources.UnityTerrainLayers[j];

                    builder.SetTextureParam(shader, kernelID, "_InputWeightmap", weightmap);
                    builder.SetTextureParam(shader, kernelID, "_InputWeightmapSum", weightsSum);
                    builder.SetTextureParam(shader, kernelID, "_InputAlphamap", alphamap);
                    builder.SetTextureParam(shader, kernelID, "_OutputAlphamap", output);
                    builder.DispatchSafe(shader, kernelID, output);

                    builder.AddAlphamap(output, layer);
                }
            }
        }

        private void BuildObjects(TerrainGeneratorBuilder builder, MapHandle weightmapSum, MapHandle heightmapSum)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/BiomeNode");
            var kernelID = shader.FindKernel("CSAddObjects");

            // Calculate the combined objects
            for (var i = 0; i < _biomes.Count; ++i)
            {
                var biome = _biomes[i];
                var resources = builder.GetResources(biome.Generator);
                var weightmap = InputSockets[i].GetValue() as MapHandle;
                var heightmap = resources.Heightmaps[0];

                // If there are no objects, no need to add anything
                if (resources.ObjectPrototypes.Count == 0)
                {
                    continue;
                }

                // Add object prototypes of the sub generator to the main generator
                // Keep track of how the indices map so that we can fix the references
                // in the object buffers
                var prototypeIndicesValue = new float[resources.ObjectPrototypes.Count];
                for (var j = 0; j < resources.ObjectPrototypes.Count; ++j)
                {
                    var prototype = resources.ObjectPrototypes[j];
                    var index = builder.AddObjectPrototype(prototype);
                    prototypeIndicesValue[j] = index;
                }

                var prototypeIndices = builder.CreateBuffer(prototypeIndicesValue);
                for (var j = 0; j < resources.ObjectBuffers.Count; ++j)
                {
                    var objects = resources.ObjectBuffers[j];
                    var output = builder.CreateObjects(objects);
                    builder.SetFloatParam(shader, "_PrototypeCount", prototypeIndices.Length);
                    builder.SetBufferParam(shader, kernelID, "_PrototypeIndices", prototypeIndices);
                    builder.SetTextureParam(shader, kernelID, "_InputWeightmap", weightmap);
                    builder.SetTextureParam(shader, kernelID, "_InputWeightmapSum", weightmapSum);
                    builder.SetTextureParam(shader, kernelID, "_InputHeightmap", heightmap);
                    builder.SetTextureParam(shader, kernelID, "_InputHeightmapSum", heightmapSum);
                    builder.SetObjectsParam(shader, kernelID, "_InputObjects", objects);
                    builder.SetObjectsParam(shader, kernelID, "_OutputObjects", output);
                    builder.DispatchSafe(shader, kernelID, output);

                    builder.AddObjectBuffer(output);
                }
            }
        }

        private MapHandle BuildBiomeWeightsSum(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/BiomeNode");
            var kernelID = shader.FindKernel("CSAddWeightmap");

            var output = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            var tempBuffer = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.ClearTexture(tempBuffer.Texture, 0);

            // Calculate sum of biome weights
            for (var i = 0; i < _biomes.Count; ++i)
            {
                var weightmap = InputSockets[i].GetValue() as MapHandle;

                // Swap buffers for ping pong
                if (i > 0)
                {
                    var temp = tempBuffer;
                    tempBuffer = output;
                    output = temp;
                }

                builder.SetTextureParam(shader, kernelID, "_InputWeightmap", weightmap);
                builder.SetTextureParam(shader, kernelID, "_InputWeightmapSum", tempBuffer);
                builder.SetTextureParam(shader, kernelID, "_OutputWeightmapSum", output);
                builder.DispatchSafe(shader, kernelID, output);
            }

            return output;
        }

        private MapHandle BuildMap(TerrainGeneratorBuilder builder, MapHandle weightmapSum, Func<TerrainGeneratorBuilderResources, MapHandle> getMap)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/BiomeNode");
            var kernelID = shader.FindKernel("CSAddHeightmap");

            var output = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            var tempBuffer = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.ClearTexture(tempBuffer.Texture, 0);
            builder.SetSeedParam(shader, "_Seed", 901272230);

            // Calculate the combined heightmap
            for (var i = 0; i < _biomes.Count; ++i)
            {
                var biome = _biomes[i];
                var heightmap = getMap(builder.GetResources(biome.Generator));
                var weightmap = InputSockets[i].GetValue() as MapHandle;

                // Swap buffers for ping pong
                if (i > 0)
                {
                    var temp = tempBuffer;
                    tempBuffer = output;
                    output = temp;
                }

                builder.SetTextureParam(shader, kernelID, "_InputWeightmap", weightmap);
                builder.SetTextureParam(shader, kernelID, "_InputWeightmapSum", weightmapSum);
                builder.SetTextureParam(shader, kernelID, "_InputHeightmap", heightmap);
                builder.SetTextureParam(shader, kernelID, "_InputHeightmapSum", tempBuffer);
                builder.SetTextureParam(shader, kernelID, "_OutputHeightmapSum", output);
                builder.DispatchSafe(shader, kernelID, output);
            }

            return output;
        }

        protected override void BuildSockets()
        {
            if (_biomes == null)
            {
                return;
            }

            var socketCount = max(_biomes.Count, 1);
            for (var i = 0; i < socketCount; ++i)
            {
                CreateSocket(i);
            }
            ResizeList();
        }

        private void CreateSocket(int id)
        {
            var socket = new MapSocket { ID = id, Name = $"Biome {id + 1}", Capacity = 1 };
            socket.ControlPropertyPath = $"_biomes.Array.data[{id}]";
            AddInputSocket(socket);
        }

        private void ResizeList()
        {
            while (_biomes.Count > InputSockets.Count)
            {
                _biomes.RemoveAt(_biomes.Count - 1);
            }

            while (_biomes.Count < InputSockets.Count)
            {
                _biomes.Add(new Biome());
            }
        }
    }
}
