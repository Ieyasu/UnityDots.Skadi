using UnityEngine;

namespace UnityDots.Skadi
{
    [System.Serializable]
    public class Biome
    {
        [SerializeField]
        private TerrainGenerator _generator = null;
        public TerrainGenerator Generator => _generator;
    }
}
