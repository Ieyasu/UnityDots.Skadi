using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Create/Link Objects")]
    public sealed class LinkObjectsNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;

        [Input(1, Label = "Objects")]
        public ObjectsSocket InputObjects
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        [Control, SerializeField]
        private LinkObjectsControls _controls = null;

        public override bool CanBuild()
        {
            return InputObjects.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Create/LinkObjectsNode");
            var clearKernel = shader.FindKernel("CSVoronoiClear");
            var seedKernel = shader.FindKernel("CSVoronoiSeed");
            var vertexKernel = shader.FindKernel("CSVoronoiVertices");
            var voronoiKernel = new int[] { shader.FindKernel("CSVoronoiPing"),
                                            shader.FindKernel("CSVoronoiPong") };
            var triangulateKernel = shader.FindKernel("CSTriangulate");

            // Calculate voronoi using jump flood algorithm
            // https://www.comp.nus.edu.sg/~tants/jfa/i3d06.pdf
            var stepCount = (int)round(log2(builder.MapResolution));
            var voronoi = new MapHandle[] { builder.CreateMap(builder.MapResolution, builder.MapResolution, GraphicsFormat.R32_UInt),
                                            builder.CreateMap(builder.MapResolution, builder.MapResolution, GraphicsFormat.R32_UInt) };


            // Clearing  the JFA
            builder.SetTextureParam(shader, clearKernel, "_OutputVoronoi", voronoi[0]);
            builder.DispatchSafe(shader, clearKernel, voronoi[0]);

            // Initial seeding of the JFA
            // Create triangulation data structures in advance as we need to populate the vertices
            var maxEdgeCount = 6 * InputObjects.Value.Buffer.Length - 12;
            var vertexBufferLength = GetBufferLength(InputObjects.Value.Buffer.Length);
            var edgeBufferLength = GetBufferLength(maxEdgeCount);

            var vertices = builder.CreatePathVertices(vertexBufferLength);
            var edges = builder.CreatePathEdges(edgeBufferLength, ComputeBufferType.Counter | ComputeBufferType.Structured);
            var triangulation = builder.CreatePaths(vertices, edges);

            builder.SetObjectsParam(shader, seedKernel, "_InputObjects", InputObjects.Value);
            builder.SetTextureParam(shader, seedKernel, "_OutputVoronoi", voronoi[0]);
            builder.DispatchSafe(shader, seedKernel, InputObjects.Value);

            // Add vertices that made it to the voronoi diagram
            builder.SetFloatParam(shader, "_PinVertices", _controls.PinVertices ? 1 : 0);
            builder.SetObjectsParam(shader, vertexKernel, "_InputObjects", InputObjects.Value);
            builder.SetTextureParam(shader, vertexKernel, "_InputVoronoi", voronoi[0]);
            builder.SetPathsParam(shader, vertexKernel, "_Output", triangulation);
            builder.DispatchSafe(shader, vertexKernel, voronoi[0]);

            // Do log(N) steps to flood fill the whole texture
            var kernelIndex = 0;
            builder.SetTextureParam(shader, voronoiKernel[0], "_InputVoronoi", voronoi[0]);
            builder.SetTextureParam(shader, voronoiKernel[0], "_OutputVoronoi", voronoi[1]);
            builder.SetPathsParam(shader, voronoiKernel[0], "_Input", triangulation);
            builder.SetTextureParam(shader, voronoiKernel[1], "_InputVoronoi", voronoi[1]);
            builder.SetTextureParam(shader, voronoiKernel[1], "_OutputVoronoi", voronoi[0]);
            builder.SetPathsParam(shader, voronoiKernel[1], "_Input", triangulation);

            for (var i = 0; i < stepCount; ++i)
            {
                // Alternate kernels for pingpong effect
                var stepSize = 1 << (stepCount - i - 1);
                builder.SetFloatParam(shader, "_StepSize", stepSize);
                builder.DispatchSafe(shader, voronoiKernel[kernelIndex], voronoi[kernelIndex]);
                kernelIndex = (kernelIndex + 1) % 2;
            }

            // Execute two additional (JFA + 2) to minimize error
            for (var i = max(stepCount - 2, 0); i < stepCount; ++i)
            {
                // Alternate kernels for pingpong effect
                var stepSize = 1 << (stepCount - i - 1);
                builder.SetFloatParam(shader, "_StepSize", stepSize);
                builder.DispatchSafe(shader, voronoiKernel[kernelIndex], voronoi[kernelIndex]);
                kernelIndex = (kernelIndex + 1) % 2;
            }

            // Triangulate
            var lastVoronoi = voronoi[kernelIndex];
            builder.SetTextureParam(shader, triangulateKernel, "_InputVoronoi", lastVoronoi);
            builder.SetPathsParam(shader, triangulateKernel, "_Output", triangulation);
            builder.DispatchSafe(shader, triangulateKernel, lastVoronoi);

            // Sort and remove duplicate edges
            var sortedPaths = builder.SortAndRemoveDuplicatePaths(triangulation);

            OutputPaths.Value = FilterPaths(builder, sortedPaths);
        }

        private PathsHandle FilterPaths(TerrainGeneratorBuilder builder, PathsHandle triangulation)
        {
            // No need to filter
            if (_controls.Density == PathCount.High)
            {
                return triangulation;
            }

            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Create/LinkObjectsNode");
            var kernel = shader.FindKernel(GetFilterKernel());

            // Collision creation settings have radius influence set to 0 so that we create a collision
            // grid for the object point cloud
            var collisionCreateSettings = new ObjectCollisionControls(0, 0);
            var collisionGrid = builder.CreateObjectCollisions(InputObjects.Value, collisionCreateSettings);

            // For overlap testing the radius influence is set back to normal
            var collisionOverlapSettings = new ObjectCollisionControls(1, 0);
            builder.SetObjectCollisionsParam(shader, kernel, collisionGrid, collisionOverlapSettings);

            builder.SetPathsParam(shader, kernel, "_Input", triangulation);
            builder.SetPathsParam(shader, kernel, "_Output", triangulation);
            builder.DispatchPathEdgesSafe(shader, kernel, triangulation);

            return triangulation;
        }

        private string GetFilterKernel()
        {
            switch (_controls.Density)
            {
                case PathCount.Low:
                    return "CSCreateRNGGraph";
                case PathCount.Medium:
                    return "CSCreateGabrielGraph";
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(_controls.Density.ToString());
            }
        }

        private int GetBufferLength(int count)
        {
            // Make sure buffer size is multiple of thread count
            return count + (TerrainGeneratorBuilder.PathBufferStride - count % TerrainGeneratorBuilder.PathBufferStride) % TerrainGeneratorBuilder.PathBufferStride;
        }
    }
}
