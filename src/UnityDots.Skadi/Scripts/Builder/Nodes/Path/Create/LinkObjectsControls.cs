using System;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class LinkObjectsControls
    {
        [SerializeField]
        private PathCount _density = PathCount.Low;

        [SerializeField]
        private bool _pinVertices = true;

        public PathCount Density
        {
            get => _density;
            set => _density = value;
        }

        public bool PinVertices
        {
            get => _pinVertices;
            set => _pinVertices = value;
        }
    }
}
