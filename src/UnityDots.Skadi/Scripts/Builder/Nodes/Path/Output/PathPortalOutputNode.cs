using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Output/Output Portal")]
    public sealed class PathPortalOutputNode : PathComputeNode, IPortalOutputNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;
        IPortalInputNode IPortalOutputNode.PortalInput => PortalInput;

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        [SerializeField]
        private NodeData _portalInput;
        public IPortalInputNode<PathsHandle> PortalInput
        {
            get => _portalInput as IPortalInputNode<PathsHandle>;
            set => _portalInput = value as NodeData;
        }

        public override bool CanBuild()
        {
            return PortalInput != null;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            OutputPaths.Value = PortalInput.PortalValue;
        }
    }
}
