using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Output/Input Portal")]
    public sealed class PathPortalInputNode : PathComputeNode, IPortalInputNode<PathsHandle>
    {
        public override PathsHandle PathsPreview => PortalValue;
        public PathsHandle PortalValue => InputPaths.HasValue ? InputPaths.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Control, SerializeField]
        private string _name = "Path Portal";
        public string PortalName => _name;
    }
}
