using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Modify/Relax")]
    public sealed class RelaxPathsNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        [Control, SerializeField]
        private RelaxPathsControls _controls = null;

        public override bool CanBuild()
        {
            return InputPaths.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Modify/RelaxPathsNode");
            var kernelID = 0;

            // Copy vertex data from the input paths but use the original edge list as they won't change
            var vertices1 = builder.CreatePathVertices(InputPaths.Value.Vertices.Length);
            var vertices2 = builder.CreatePathVertices(InputPaths.Value.Vertices.Length);
            builder.CopyPathVertices(InputPaths.Value.Vertices, vertices1);
            var buffer1 = builder.CreatePaths(vertices1, InputPaths.Value.Edges);
            var buffer2 = builder.CreatePaths(vertices2, InputPaths.Value.Edges);

            // Create an adjacency list so that we can find the connected vertices efficiently
            var adjacencyList = builder.CreateAdjacencyList(InputPaths.Value);
            builder.SetAdjacencyListParam(shader, kernelID, "_Input", adjacencyList);
            builder.SetFloatParam(shader, "_Strength", _controls.Strength);

            for (var i = 0; i < _controls.Iterations; ++i)
            {
                builder.SetPathsParam(shader, kernelID, "_Input", buffer1);
                builder.SetPathsParam(shader, kernelID, "_Output", buffer2);
                builder.DispatchPathEdgesSafe(shader, kernelID, buffer2);

                var temp = buffer2;
                buffer2 = buffer1;
                buffer1 = temp;
            }

            OutputPaths.Value = buffer1;
        }
    }
}
