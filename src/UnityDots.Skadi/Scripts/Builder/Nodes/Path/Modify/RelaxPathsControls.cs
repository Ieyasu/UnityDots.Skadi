using System;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class RelaxPathsControls
    {
        [SerializeField]
        private int _iterations = 5;

        [SerializeField]
        private float _strength = 0.5f;

        public int Iterations
        {
            get => _iterations;
            set => _iterations = max(value, 0);
        }

        public float Strength
        {
            get => _strength;
            set => _strength = saturate(value);
        }
    }
}
