using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Modify/Pathfind")]
    public sealed class PathfindNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;
        public override MapHandle MapPreview => InputHeightmap.HasValue ? InputHeightmap.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Input(1, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        public override bool CanBuild()
        {
            return InputPaths.HasValue && InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Modify/PathfindNode");
            var pathfindKernel = shader.FindKernel("CSPathfind");

            // OutputPaths.Value = builder.CreatePaths(InputPaths.Value);

            var edges = builder.CreatePathEdges(InputPaths.Value.Edges.Length);
            OutputPaths.Value = builder.CreatePaths(InputPaths.Value.Vertices, edges);

            builder.SetTextureParam(shader, pathfindKernel, "_InputHeightmap", InputHeightmap.Value);
            builder.SetPathsParam(shader, pathfindKernel, "_Input", InputPaths.Value);
            builder.SetPathsParam(shader, pathfindKernel, "_Output", OutputPaths.Value);

            var totalThreadsX = 64 * InputPaths.Value.Edges.Length;
            for (var i = 0; i < totalThreadsX; i += 64 * 65535)
            {
                var threadsX = min(totalThreadsX - i, 64 * 65535);
                builder.SetFloatParam(shader, "_EdgeStartIndex", i / 64);
                builder.DispatchSafe(shader, pathfindKernel, threadsX, 1, 1);
            }
        }
    }
}
