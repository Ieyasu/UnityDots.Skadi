using System;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SubdividePathsControls
    {
        [SerializeField]
        private int _iterations = 3;

        [SerializeField]
        private float _maxLength = 0;

        public int Iterations
        {
            get => _iterations;
            set => _iterations = clamp(value, 0, 10);
        }

        public float MaxLength
        {
            get => _maxLength;
            set => _maxLength = max(value, 0);
        }
    }
}
