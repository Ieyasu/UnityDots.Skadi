using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Modify/Floor")]
    public sealed class FloorPathsNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;
        public override MapHandle MapPreview => InputHeightmap.HasValue ? InputHeightmap.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Input(1, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Input(2, Label = "Intensity")]
        public MapSocket InputIntensity
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        [InputControl(2), SerializeField]
        private float _intensity = 1;

        public override bool CanBuild()
        {
            return InputPaths.HasValue && InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Modify/FloorPathsNode");
            var kernelID = shader.FindKernel(GetKernelName());

            // We don't do any changes to the paths edges so we can reuse the edges from the previous
            // buffer instead of creating a new one.
            var vertices = builder.CreatePathVertices(InputPaths.Value.Vertices.Length);
            var edges = InputPaths.Value.Edges;
            OutputPaths.Value = builder.CreatePaths(vertices, edges);

            if (InputIntensity.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputIntensity", InputIntensity.Value);
            }

            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetTextureParam(shader, kernelID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetPathsParam(shader, kernelID, "_Input", InputPaths.Value);
            builder.SetPathsParam(shader, kernelID, "_Output", OutputPaths.Value);
            builder.DispatchPathEdgesSafe(shader, kernelID, OutputPaths.Value);
        }

        private string GetKernelName()
        {
            return InputIntensity.HasValue ? "CSFloorIntensityMask" : "CSFloor";
        }
    }
}
