using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Modify/Offset")]
    public sealed class OffsetPathsNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Input(1, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        public override bool CanBuild()
        {
            return InputPaths.HasValue && InputOffset.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Modify/OffsetPathsNode");
            var kernelID = 0;

            var vertices = builder.CreatePathVertices(InputPaths.Value.Vertices.Length);
            OutputPaths.Value = builder.CreatePaths(vertices, InputPaths.Value.Edges);
            builder.SetPathsParam(shader, kernelID, "_Input", InputPaths.Value);
            builder.SetTextureParam(shader, kernelID, "_InputOffset", InputOffset.Value);
            builder.SetPathsParam(shader, kernelID, "_Output", OutputPaths.Value);
            builder.DispatchPathVerticesSafe(shader, kernelID, OutputPaths.Value);
        }
    }
}
