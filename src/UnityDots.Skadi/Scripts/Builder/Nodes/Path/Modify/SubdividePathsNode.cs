using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Path/Modify/Subdivide")]
    public sealed class SubdividePathsNode : PathComputeNode
    {
        public override PathsHandle PathsPreview => OutputPaths.HasValue ? OutputPaths.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Output(0, Label = "Paths")]
        public PathsSocket OutputPaths
        {
            get;
            set;
        }

        [Control, SerializeField]
        private SubdividePathsControls _controls = null;

        public override bool CanBuild()
        {
            return InputPaths.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Path/Modify/SubdividePathsNode");
            var kernelID = 0;

            // Calculate the number of edges and vertices we need to allocate for
            var edgeCount = InputPaths.Value.Edges.Length;
            var vertexCount = InputPaths.Value.Vertices.Length;
            for (var i = 0; i < _controls.Iterations; ++i)
            {
                vertexCount += edgeCount;
                edgeCount *= 2;
            }

            // Create the arrays and copy data from the input paths
            OutputPaths.Value = builder.CreatePaths(vertexCount, edgeCount);
            builder.CopyPaths(InputPaths.Value, OutputPaths.Value);

            // // Subdivide in iterations
            builder.SetFloatParam(shader, "_MaxLength", _controls.MaxLength);
            builder.SetPathsParam(shader, kernelID, "_Input", OutputPaths.Value);
            builder.SetPathsParam(shader, kernelID, "_Output", OutputPaths.Value);

            edgeCount = InputPaths.Value.Edges.Length;
            vertexCount = InputPaths.Value.Vertices.Length;
            for (var i = 0; i < _controls.Iterations; ++i)
            {
                builder.SetFloatParam(shader, "_EdgeCount", edgeCount);
                builder.SetFloatParam(shader, "_VertexCount", vertexCount);
                builder.DispatchSafe(shader, kernelID, edgeCount, 1, 1);
                vertexCount += edgeCount;
                edgeCount *= 2;
            }
        }
    }
}
