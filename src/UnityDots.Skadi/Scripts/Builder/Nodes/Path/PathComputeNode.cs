using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    public abstract class PathComputeNode : NodeData, IGeneratorNode, IPreviewNode
    {
        public bool HasPreview => MapPreview != null || ObjectsPreview != null || PathsPreview != null;

        public virtual MapHandle MapPreview => null;
        public virtual ObjectsHandle ObjectsPreview => null;
        public virtual PathsHandle PathsPreview => null;

        public virtual bool CanBuild()
        {
            return true;
        }

        public virtual void Build(TerrainGeneratorBuilder builder) { }

        public virtual void BuildPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            if (MapPreview != null)
            {
                BuildMapPreview(builder, previewTexture);
            }

            if (PathsPreview != null)
            {
                BuildPathPreview(builder, previewTexture);
            }

            if (ObjectsPreview != null)
            {
                BuildObjectPreview(builder, previewTexture);
            }
        }

        public void BuildMapPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            var componentCount = GraphicsFormatUtility.GetComponentCount(MapPreview.Texture.Format);
            var shader = Resources.Load<ComputeShader>("GPU/Preview/PreviewMap" + componentCount);
            builder.SetTextureParam(shader, 0, "_Input", MapPreview);
            builder.SetTextureParam(shader, 0, "_Output", previewTexture);
            builder.DispatchSafe(shader, 0, previewTexture);
        }

        public void BuildObjectPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Preview/PreviewObjects");
            builder.SetObjectsParam(shader, 0, "_Input", ObjectsPreview);
            builder.SetTextureParam(shader, 0, "_Output", previewTexture);
            builder.DispatchSafe(shader, 0, ObjectsPreview);
        }

        public void BuildPathPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Preview/PreviewPaths");
            var edgeKernel = shader.FindKernel("CSDrawEdges");
            var vertexKernel = shader.FindKernel("CSDrawVertices");

            builder.SetPathsParam(shader, edgeKernel, "_Input", PathsPreview);
            builder.SetTextureParam(shader, edgeKernel, "_Output", previewTexture);
            builder.DispatchPathEdgesSafe(shader, edgeKernel, PathsPreview);

            builder.SetPathsParam(shader, vertexKernel, "_Input", PathsPreview);
            builder.SetTextureParam(shader, vertexKernel, "_Output", previewTexture);
            builder.DispatchPathEdgesSafe(shader, vertexKernel, PathsPreview);
        }
    }
}
