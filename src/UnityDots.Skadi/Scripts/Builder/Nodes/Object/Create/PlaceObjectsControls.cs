using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class PlaceObjectsControls
    {
        [SerializeField]
        private ObjectPlacementType _type = ObjectPlacementType.Natural;

        [SerializeField]
        private uint _seed;

        [SerializeField]
        private int _density = 10;

        [SerializeField]
        private float _uniformity = 0;

        [SerializeField]
        private ObjectPrototype[] _prototypes = new ObjectPrototype[0];

        public ObjectPlacementType Type
        {
            get => _type;
            set => _type = value;
        }

        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        public int Density
        {
            get => _density;
            set => _density = value;
        }

        public float Uniformity
        {
            get => _uniformity;
            set => _uniformity = value;
        }

        public ObjectPrototype[] Prototypes
        {
            get => _prototypes;
            set => _prototypes = value;
        }
    }
}
