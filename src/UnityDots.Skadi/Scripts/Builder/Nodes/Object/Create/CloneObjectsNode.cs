using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Create/Clone")]
    public sealed class CloneObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => OutputObjects.HasValue ? OutputObjects.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket InputObjects
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket OutputObjects
        {
            get;
            set;
        }

        [Control, SerializeField]
        private CloneObjectsControls _controls = null;

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_controls == null)
            {
                _controls = new CloneObjectsControls();
            }

            if (_controls.Seed == 0)
            {
                _controls.Seed = (uint)UnityEngine.Random.Range(1, int.MaxValue);
            }
        }

        public override bool CanBuild()
        {
            return InputObjects.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/CloneObjectsNode");
            var kernelName = GetKernelName();
            var kernelID = shader.FindKernel(kernelName);
            var bufferLength = (int)max(ceil(_controls.Count.ConstantMax), 1) * InputObjects.Value.Buffer.Length;

            OutputObjects.Value = builder.CreateObjects(bufferLength, InputObjects.Value);

            // Set the list of prototypes we assign randomly to the clones
            if (_controls.Mode != CloneMode.Self)
            {
                var prototypeIDs = GetPrototypeIDs(builder);
                var prototypeBuffer = builder.CreateBuffer(prototypeIDs);
                builder.SetFloatParam(shader, "_PrototypeCount", prototypeBuffer.Length);
                builder.SetBufferParam(shader, kernelID, "_PrototypeIDs", prototypeBuffer);
            }

            builder.SetSeedParam(shader, "_Seed", _controls.Seed);
            builder.SetFloatParam(shader, "_ScaleInfluence", _controls.ScaleInfluence);
            builder.SetMinMaxCurveParam(shader, kernelID, "_CountCurve", _controls.Count);
            builder.SetMinMaxCurveParam(shader, kernelID, "_DistanceCurve", _controls.Distance);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", InputObjects.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", OutputObjects.Value);
            builder.DispatchSafe(shader, kernelID, InputObjects.Value);
        }

        private string GetKernelName()
        {
            switch (_controls.Mode)
            {
                case CloneMode.Self:
                    return "CSCloneSelf";
                case CloneMode.SelfOrOther:
                    return "CSCloneSelfOrOther";
                case CloneMode.Preselected:
                    return "CSClonePreselected";
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(_controls.Mode.ToString());
            }
        }

        private float[] GetPrototypeIDs(TerrainGeneratorBuilder builder)
        {
            switch (_controls.Mode)
            {
                case CloneMode.SelfOrOther:
                    return InputObjects.Value.Prototypes.Select(x => (float)builder.AddObjectPrototype(x)).ToArray();
                case CloneMode.Preselected:
                    return _controls.Prototypes.Select(x => (float)builder.AddObjectPrototype(x)).ToArray();
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(_controls.Mode.ToString());
            }
        }
    }
}
