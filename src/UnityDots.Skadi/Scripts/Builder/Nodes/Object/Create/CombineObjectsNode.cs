using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Create/Combine")]
    public sealed class CombineObjectsNode : ObjectsComputeNode, IMultiInputNode
    {
        private const int _minSocketCount = 2;
        private const int _maxSocketCount = 20;

        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;

        [Output(0, "Objects")]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [SerializeField]
        private int _socketCount;

        public override bool CanBuild()
        {
            return InputSockets.OfType<ObjectsSocket>().Count(x => x.HasValue) > 0;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/CombineObjectsNode");
            var kernelID = 0;
            var inputs = InputSockets.OfType<ObjectsSocket>().Where(x => x.HasValue).Select(x => x.Value);

            // Find out the parameters of the new objects buffer
            var outputSize = 0;
            var prototypes = new List<ObjectPrototype>();
            var minRadius = float.MaxValue;
            var maxRadius = 0.0f;
            var minScale = float.MaxValue;
            var maxScale = 0.0f;

            foreach (var input in inputs)
            {
                outputSize += input.Buffer.Length;
                prototypes.AddRange(input.Prototypes);
                minRadius = min(minRadius, input.MinRadius);
                maxRadius = max(maxRadius, input.MaxRadius);
                minScale = min(minScale, input.MinScale);
                maxScale = max(maxScale, input.MaxScale);
            }

            Output.Value = builder.CreateObjects(outputSize, prototypes, minRadius, maxRadius, minScale, maxScale);

            // Copy the existing buffers to the new one
            var offset = 0;
            foreach (var input in inputs)
            {
                builder.SetFloatParam(shader, "_OutputOffset", offset);
                builder.SetObjectsParam(shader, kernelID, "_InputObjects", input);
                builder.SetObjectsParam(shader, kernelID, "_OutputObjects", Output.Value);
                builder.DispatchSafe(shader, kernelID, input);
                offset += input.Buffer.Length;
            }
        }

        protected override void BuildSockets()
        {
            _socketCount = max(_socketCount, 2);
            for (var i = 0; i < _socketCount; ++i)
            {
                CreateSocket(i);
            }
        }

        public bool CanAddInputSocket()
        {
            return InputSockets.Count < _maxSocketCount;
        }

        public bool CanRemoveInputSocket()
        {
            return InputSockets.Count > _minSocketCount;
        }

        public void OnInputSocketAdd()
        {
            CreateSocket(InputSockets.Count);
            _socketCount = InputSockets.Count;
        }

        public void OnInputSocketRemove()
        {
            var id = InputSockets.Last().ID;
            RemoveInputSocket(id);
            _socketCount = InputSockets.Count;
        }

        private void CreateSocket(int id)
        {
            var socket = new ObjectsSocket { ID = id, Name = $"Objects {id + 1}", Capacity = 1 };
            AddInputSocket(socket);
        }
    }
}
