using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    public enum CloneMode
    {
        Self,
        SelfOrOther,
        Preselected
    }

    [Serializable]
    public sealed class CloneObjectsControls
    {
        [SerializeField]
        private uint _seed;

        [SerializeField]
        private CloneMode _mode = CloneMode.Self;

        [SerializeField]
        private MinMaxCurve _count = new MinMaxCurve(0, 5);

        [SerializeField]
        private MinMaxCurve _distance = new MinMaxCurve(0, 0.1f);

        [SerializeField]
        private float _scaleInfluence = 0;

        [SerializeField]
        private ObjectPrototype[] _prototypes = new ObjectPrototype[0];

        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        public CloneMode Mode
        {
            get => _mode;
            set => _mode = value;
        }

        public MinMaxCurve Count
        {
            get => _count;
            set => _count = value;
        }

        public MinMaxCurve Distance
        {
            get => _distance;
            set => _distance = value;
        }

        public float ScaleInfluence
        {
            get => _scaleInfluence;
            set => _scaleInfluence = value;
        }

        public ObjectPrototype[] Prototypes
        {
            get => _prototypes;
            set => _prototypes = value;
        }
    }
}
