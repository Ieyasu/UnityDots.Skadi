using System.ComponentModel;
using System.Linq;
using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Create/Place")]
    public sealed class PlaceObjectsNode : ObjectsComputeNode
    {
        // Number of tries to insert a point in to the poisson grid
        private const int _poissonK = 4;
        // The poisson disk grid is filled in levels, inserting points to increasingly tight areas
        private const int _poissonLevels = 3;
        // We randomize the order in which we traverse the poisson disk grid to minimize sampling bias
        private readonly int[] _poissonTraverseOrder = new int[9] { 1, 6, 8, 0, 5, 4, 3, 2, 7 };

        public override ObjectsHandle ObjectsPreview => ObjectsOutput.HasValue ? ObjectsOutput.Value : null;

        [Output(0, Label = "Objects")]
        public ObjectsSocket ObjectsOutput
        {
            get;
            set;
        }

        [Control, SerializeField]
        private PlaceObjectsControls _controls = null;

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_controls == null)
            {
                _controls = new PlaceObjectsControls();
            }

            if (_controls.Seed == 0)
            {
                _controls.Seed = (uint)UnityEngine.Random.Range(1, int.MaxValue);
            }
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            if (_controls.Density == 0)
            {
                PlaceEmpty(builder);
                return;
            }

            switch (_controls.Type)
            {
                case ObjectPlacementType.Random:
                    PlaceRandom(builder);
                    break;
                case ObjectPlacementType.Grid:
                    PlaceGrid(builder);
                    break;
                case ObjectPlacementType.Natural:
                    PlacePoisson(builder);
                    break;
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }

        private void PlaceEmpty(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/PlaceObjectsNode");
            var kernelID = shader.FindKernel("CSPlaceEmpty");

            ObjectsOutput.Value = SetCommonParams(builder, shader, kernelID, _controls.Density);
            builder.DispatchSafe(shader, kernelID, ObjectsOutput.Value);
        }

        private void PlaceRandom(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/PlaceObjectsNode");
            var kernelID = shader.FindKernel("CSPlaceRandom");

            var totalCount = GetTotalCount(builder.TerrainSize);
            ObjectsOutput.Value = SetCommonParams(builder, shader, kernelID, totalCount);
            builder.DispatchSafe(shader, kernelID, ObjectsOutput.Value);
        }

        private void PlaceGrid(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/PlaceObjectsNode");
            var kernelID = shader.FindKernel("CSPlaceGrid");

            // Calculate count
            var totalCount = GetTotalCount(builder.TerrainSize);
            var countSqrt = (int)sqrt(totalCount);
            var count = countSqrt * countSqrt;

            ObjectsOutput.Value = SetCommonParams(builder, shader, kernelID, count);
            builder.SetFloatParam(shader, "_CountSqrt", countSqrt);
            builder.SetFloatParam(shader, "_CellSize", 1.0f / countSqrt);
            builder.SetFloatParam(shader, "_Uniformity", _controls.Uniformity);
            builder.DispatchSafe(shader, kernelID, ObjectsOutput.Value);
        }

        private void PlacePoisson(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Create/PlaceObjectsNode");
            var generateID = shader.FindKernel("CSGeneratePoisson");
            var placeID = shader.FindKernel("CSPlacePoisson");

            // Calculate cell count
            // We multiply with 3 to achieve roughly equal density to other methods
            var totalCount = GetTotalCount(builder.TerrainSize);
            var cellCountSqrt = (int)sqrt(3 * totalCount);
            var cellCount = cellCountSqrt * cellCountSqrt;
            var cellSize = 1.0f / cellCountSqrt;
            var distance = cellSize * sqrt(2);

            // Maximum packing of the plane scaled by a magic constant that won't be exceeded with poisson disk sampling
            // var maximumCount = 1 / (2 * sqrt(3) * distance * distance * 0.25);
            // maximumCount *= 0.8;

            // Common params
            ObjectsOutput.Value = SetCommonParams(builder, shader, placeID, cellCount);
            builder.SetFloatParam(shader, "_CountSqrt", cellCountSqrt);
            builder.SetFloatParam(shader, "_CellSize", cellSize);

            // Generate params
            var poissonGrid = builder.CreateTexture(GraphicsFormat.R16G16_SFloat, cellCountSqrt, cellCountSqrt);
            builder.ClearTexture(poissonGrid, 0);
            builder.SetFloatParam(shader, "_PoissonDistance", distance);
            builder.SetTextureParam(shader, generateID, "_OutputPoissonGrid", poissonGrid);

            var phaseCount = _poissonTraverseOrder.Length;
            var phaseCountSqrt = (int)round(sqrt(phaseCount));

            for (var l = 0; l < _poissonLevels; ++l)
            {
                var level = 1 << (_poissonLevels - l - 1);
                var phaseSizeSqrt = (int)ceil((float)cellCountSqrt / phaseCountSqrt / level);
                builder.SetFloatParam(shader, "_PoissonLevel", level);

                for (var k = 0; k < _poissonK; ++k)
                {
                    builder.SetFloatParam(shader, "_PoissonIteration", k);

                    for (var j = 0; j < _poissonTraverseOrder.Length; ++j)
                    {
                        var order = _poissonTraverseOrder[j];
                        var phaseOffset = new float4(phaseCountSqrt, phaseCountSqrt, order % phaseCountSqrt, order / phaseCountSqrt);

                        builder.SetVectorParam(shader, "_PoissonPhase", phaseOffset);
                        builder.DispatchUnsafe(shader, generateID, phaseSizeSqrt, phaseSizeSqrt, 1);
                    }
                }
            }

            builder.SetTextureParam(shader, placeID, "_InputPoissonGrid", poissonGrid);
            builder.DispatchUnsafe(shader, placeID, ObjectsOutput.Value);
        }

        private int GetTotalCount(float3 terrainSize)
        {
            return (int)(_controls.Density * terrainSize.x * terrainSize.z / 10000);
        }

        private ObjectsHandle SetCommonParams(TerrainGeneratorBuilder builder, ComputeShader shader, int kernelID, int count)
        {
            var prototypeIDs = AddPrototypeIDs(builder);
            var rotationCurves = _controls.Prototypes.Select(x => x.Rotation).ToArray();
            var scaleCurves = _controls.Prototypes.Select(x => x.Scale).ToArray();
            var radiusCurves = _controls.Prototypes.Select(x => x.Radius).ToArray();
            if (prototypeIDs.Length == 0)
            {
                prototypeIDs = new float[] { -1 };
                rotationCurves = new MinMaxCurve[] { new MinMaxCurve(0) };
                scaleCurves = new MinMaxCurve[] { new MinMaxCurve(1) };
                radiusCurves = new MinMaxCurve[] { new MinMaxCurve(1) };
            }

            // Find the minimum and maximum scales for the objects
            var minRadius = float.MaxValue;
            var maxRadius = 0.0f;
            var minScale = float.MaxValue;
            var maxScale = 0.0f;
            foreach (var curve in radiusCurves)
            {
                var bounds = curve.CalculateBounds();
                minRadius = min(minRadius, bounds.yMin);
                maxRadius = max(maxRadius, bounds.yMax);
            }

            foreach (var curve in scaleCurves)
            {
                var bounds = curve.CalculateBounds();
                minScale = min(minScale, bounds.yMin);
                maxScale = max(maxScale, bounds.yMax);
            }

            // Create the compute buffers
            var bufferLength = GetBufferLength(count);
            var prototypeBuffer = builder.CreateBuffer(prototypeIDs);
            var output = builder.CreateObjects(bufferLength, _controls.Prototypes, minRadius, maxRadius, minScale, maxScale);

            // Set shader parameters
            builder.SetSeedParam(shader, "_Seed", _controls.Seed);
            builder.SetFloatParam(shader, "_Count", count);
            builder.SetFloatParam(shader, "_PrototypeCount", _controls.Prototypes.Length);
            builder.SetBufferParam(shader, kernelID, "_PrototypeIDs", prototypeBuffer);
            builder.SetMinMaxCurveParam(shader, kernelID, "_RotationCurve", rotationCurves);
            builder.SetMinMaxCurveParam(shader, kernelID, "_ScaleCurve", scaleCurves);
            builder.SetMinMaxCurveParam(shader, kernelID, "_RadiusCurve", radiusCurves);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", output);

            return output;
        }

        private int GetBufferLength(int count)
        {
            // Make sure buffer size is multiple of thread count
            int length = count + (TerrainGeneratorBuilder.ObjectBufferStride - count % TerrainGeneratorBuilder.ObjectBufferStride) % TerrainGeneratorBuilder.ObjectBufferStride;
            return max(length, TerrainGeneratorBuilder.ObjectBufferStride);
        }

        private float[] AddPrototypeIDs(TerrainGeneratorBuilder builder)
        {
            return _controls.Prototypes.Select(x => (float)builder.AddObjectPrototype(x)).ToArray();
        }
    }
}
