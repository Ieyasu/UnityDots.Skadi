using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Output/Output Portal")]
    public sealed class ObjectPortalOutputNode : ObjectsComputeNode, IPortalOutputNode
    {
        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;
        IPortalInputNode IPortalOutputNode.PortalInput => PortalInput;

        [Output(0)]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [SerializeField]
        private NodeData _portalInput;
        public IPortalInputNode<ObjectsHandle> PortalInput
        {
            get => _portalInput as IPortalInputNode<ObjectsHandle>;
            set => _portalInput = value as NodeData;
        }

        public override bool CanBuild()
        {
            return PortalInput != null;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            Output.Value = PortalInput.PortalValue;
        }
    }
}
