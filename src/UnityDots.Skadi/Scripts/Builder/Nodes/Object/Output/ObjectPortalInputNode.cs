using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Output/Input Portal")]
    public sealed class ObjectPortalInputNode : ObjectsComputeNode, IPortalInputNode<ObjectsHandle>
    {
        public override ObjectsHandle ObjectsPreview => PortalValue;
        public ObjectsHandle PortalValue => Input.HasValue ? Input.Value : null;

        [Input(0)]
        public ObjectsSocket Input
        {
            get;
            set;
        }

        [Control, SerializeField]
        private string _name = "Object Portal";
        public string PortalName => _name;
    }
}
