using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Output/Objects")]
    public sealed class ObjectsOutputNode : ObjectsComputeNode, IPortalInputNode<ObjectsHandle>
    {
        public override ObjectsHandle ObjectsPreview => PortalValue;
        public ObjectsHandle PortalValue => Objects.HasValue ? Objects.Value : null;

        [Input(0)]
        public ObjectsSocket Objects
        {
            get;
            set;
        }

        [SerializeField, Control]
        private string _name = "Objects";
        public string PortalName
        {
            get => _name;
            set => _name = value;
        }

        public override bool CanBuild()
        {
            return Objects.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            builder.AddObjectBuffer(Objects.Value);
        }
    }
}
