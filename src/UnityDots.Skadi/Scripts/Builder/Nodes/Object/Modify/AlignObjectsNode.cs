using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Modify/Align")]
    public sealed class AlignObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;
        public override MapHandle MapPreview => Heightmap.HasValue ? Heightmap.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket Input
        {
            get;
            set;
        }

        [Input(1)]
        public MapSocket Heightmap
        {
            get;
            set;
        }

        [Input(2)]
        public MapSocket Intensity
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [InputControl(2), SerializeField]
        private float _intensity = 1;

        public override bool CanBuild()
        {
            return Input.HasValue && Heightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Modify/AlignObjectsNode");
            var normalsShader = Resources.Load<ComputeShader>("GPU/Util/CalculateNormals");
            var kernelID = shader.FindKernel(GetKernelName());
            var normalsKernelID = 0;

            // Calculate normals
            var normalMap = builder.CreateNormalMap(Heightmap.Value);
            builder.SetTextureParam(normalsShader, normalsKernelID, "_InputHeightmap", Heightmap.Value);
            builder.SetTextureParam(normalsShader, normalsKernelID, "_OutputNormalmap", normalMap);
            builder.DispatchSafe(normalsShader, normalsKernelID, normalMap);

            // Align
            Output.Value = builder.CreateObjects(Input.Value);
            if (Intensity.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputIntensity", Intensity.Value);
            }
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetTextureParam(shader, kernelID, "_InputNormalmap", normalMap);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", Input.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", Output.Value);
            builder.DispatchSafe(shader, kernelID, Output.Value);
        }

        private string GetKernelName()
        {
            return Intensity.HasValue ? "CSAlignIntensityMask" : "CSAlign";
        }
    }
}
