using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SlideObjectsControls
    {
        [SerializeField]
        private int _iterations = 10;

        [SerializeField]
        private float _distancePerIteration = 1;

        public int Iterations
        {
            get => _iterations;
            set => _iterations = value;
        }

        public float DistancePerIteration
        {
            get => _distancePerIteration;
            set => _distancePerIteration = value;
        }
    }
}
