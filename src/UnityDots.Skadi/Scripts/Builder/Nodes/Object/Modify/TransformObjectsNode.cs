using System.ComponentModel;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Modify/Transform")]
    public sealed class TransformObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket Input
        {
            get;
            set;
        }

        [Input(1)]
        public MapSocket Intensity
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private TransformObjectsControls _controls = null;

        [InputControl(1), SerializeField]
        private float _intensity = 1;

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_controls == null)
            {
                _controls = new TransformObjectsControls();
            }

            if (_controls.Seed == 0)
            {
                _controls.Seed = (uint)Random.Range(1, int.MaxValue);
            }
        }

        public override bool CanBuild()
        {
            return Input.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Modify/TransformObjectsNode");
            var kernelID = shader.FindKernel(GetKernelName());

            Output.Value = builder.CreateObjects(Input.Value);
            if (Intensity.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputIntensity", Intensity.Value);
            }

            builder.SetSeedParam(shader, "_Seed", _controls.Seed);
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetMinMaxCurveParam(shader, kernelID, "_HeightCurve", _controls.Height);
            builder.SetMinMaxCurveParam(shader, kernelID, "_RadiusCurve", _controls.Radius);
            builder.SetMinMaxCurveParam(shader, kernelID, "_RotationCurve", _controls.Rotation);
            builder.SetMinMaxCurveParam(shader, kernelID, "_ScaleCurve", _controls.Scale);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", Input.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", Output.Value);
            builder.DispatchSafe(shader, kernelID, Output.Value);
        }

        private string GetKernelName()
        {
            switch (_controls.Space)
            {
                case TransformSpace.Local:
                    return "CSLocal";
                case TransformSpace.WorldAbsolute:
                    return "CSWorldAbsolute";
                case TransformSpace.WorldRelative:
                    return "CSWorldRelative";
                default:
                    throw new InvalidEnumArgumentException(_controls.Space.ToString());
            }
        }
    }
}
