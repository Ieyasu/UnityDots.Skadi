using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class TransformObjectsControls
    {
        [SerializeField]
        private TransformSpace _space = TransformSpace.Local;

        [SerializeField]
        private uint _seed;

        [SerializeField]
        private MinMaxCurve _height = new MinMaxCurve(0, 0);

        [SerializeField]
        private MinMaxCurve _rotation = new MinMaxCurve(0, 0);

        [SerializeField]
        private MinMaxCurve _scale = new MinMaxCurve(1, 1);

        [SerializeField]
        private MinMaxCurve _radius = new MinMaxCurve(1, 1);

        public TransformSpace Space
        {
            get => _space;
            set => _space = value;
        }

        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        public MinMaxCurve Height
        {
            get => _height;
            set => _height = value;
        }

        public MinMaxCurve Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }

        public MinMaxCurve Scale
        {
            get => _scale;
            set => _scale = value;
        }

        public MinMaxCurve Radius
        {
            get => _radius;
            set => _radius = value;
        }
    }
}
