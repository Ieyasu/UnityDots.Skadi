using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Modify/Slide")]
    public sealed class SlideObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;
        public override MapHandle MapPreview => Heightmap.HasValue ? Heightmap.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket Input
        {
            get;
            set;
        }

        [Input(1)]
        public MapSocket Heightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private SlideObjectsControls _controls = null;

        public override bool CanBuild()
        {
            return Input.HasValue && Heightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Modify/SlideObjectsNode");
            var normalsShader = Resources.Load<ComputeShader>("GPU/Util/CalculateNormals");
            var kernelID = 0;
            var normalsKernelID = 0;

            // Calculate normals
            var normalMap = builder.CreateNormalMap(Heightmap.Value);
            builder.SetTextureParam(normalsShader, normalsKernelID, "_InputHeightmap", Heightmap.Value);
            builder.SetTextureParam(normalsShader, normalsKernelID, "_OutputNormalmap", normalMap);
            builder.DispatchSafe(normalsShader, normalsKernelID, normalMap);

            // Slide
            Output.Value = builder.CreateObjects(Input.Value);
            builder.SetFloatParam(shader, "_Iterations", _controls.Iterations);
            builder.SetFloatParam(shader, "_DistancePerIteration", _controls.DistancePerIteration);
            builder.SetTextureParam(shader, kernelID, "_InputNormalMap", normalMap);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", Input.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", Output.Value);
            builder.DispatchSafe(shader, kernelID, Output.Value);
        }
    }
}
