using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Filter/Mask")]
    public sealed class MaskObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => OutputObjects.HasValue ? OutputObjects.Value : null;
        public override MapHandle MapPreview => InputMask.HasValue ? InputMask.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket InputObjects
        {
            get;
            set;
        }

        [Input(1)]
        public MapSocket InputMask
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket OutputObjects
        {
            get;
            set;
        }

        [InputControl(1), SerializeField]
        private float _intensity = 1;

        public override bool CanBuild()
        {
            return InputObjects.HasValue && InputMask.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Filter/MaskObjectsNode");
            var kernelID = 0;

            OutputObjects.Value = builder.CreateObjects(InputObjects.Value);
            builder.SetSeedParam(shader, "_Seed", 172290890);
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetTextureParam(shader, kernelID, "_InputMask", InputMask.Value);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", InputObjects.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", OutputObjects.Value);
            builder.DispatchSafe(shader, kernelID, OutputObjects.Value);
        }
    }
}
