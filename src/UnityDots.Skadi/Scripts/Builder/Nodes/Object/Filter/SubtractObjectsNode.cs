using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Filter/Subtract")]
    public sealed class SubtractObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => OutputObjects.HasValue ? OutputObjects.Value : null;

        [Input(0, Label = "Objects 1")]
        public ObjectsSocket InputObjects1
        {
            get;
            set;
        }

        [Input(1, Label = "Objects 2")]
        public ObjectsSocket InputObjects2
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket OutputObjects
        {
            get;
            set;
        }

        public override bool CanBuild()
        {
            return InputObjects1.HasValue;
        }

        [Control, SerializeField]
        private ObjectCollisionControls _controls = new ObjectCollisionControls(1, 1);

        public override void Build(TerrainGeneratorBuilder builder)
        {
            if (!InputObjects2.HasValue)
            {
                OutputObjects.Value = InputObjects1.Value;
                return;
            }

            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Filter/SubtractObjectsNode");
            var kernelID = 0;

            // Create a collision grid for the objects in Input2
            OutputObjects.Value = builder.CreateObjects(InputObjects1.Value);
            var collisionGrid = builder.CreateObjectCollisions(InputObjects2.Value, _controls);

            // Remove objects from Input1 that collide with Input2
            builder.SetObjectCollisionsParam(shader, kernelID, collisionGrid, _controls);
            builder.SetObjectsParam(shader, kernelID, "_InputObjects", InputObjects1.Value);
            builder.SetObjectsParam(shader, kernelID, "_OutputObjects", OutputObjects.Value);
            builder.DispatchSafe(shader, kernelID, OutputObjects.Value);
        }
    }
}
