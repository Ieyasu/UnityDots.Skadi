using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Object/Filter/Prune")]
    public sealed class PruneObjectsNode : ObjectsComputeNode
    {
        public override ObjectsHandle ObjectsPreview => Output.HasValue ? Output.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket Input
        {
            get;
            set;
        }

        [Output(0, Label = "Objects")]
        public ObjectsSocket Output
        {
            get;
            set;
        }

        [Control, SerializeField]
        private ObjectCollisionControls _controls;

        public override bool CanBuild()
        {
            return Input.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var minScale = lerp(1, Input.Value.MinScale, _controls.ScaleInfluence);
            var minRadius = Input.Value.MinRadius * _controls.RadiusInfluence * minScale;
            if (minRadius < 0.01f)
            {
                Output.Value = Input.Value;
                return;
            }

            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Object/Filter/PruneObjectsNode");
            var initID = shader.FindKernel("CSInitializeCollisionGrid");
            var buildID = shader.FindKernel("CSBuildCollisionGrid");
            var pruneInitialID = shader.FindKernel("CSPruneInitial");
            var pruneID = shader.FindKernel("CSPrune");

            var worldSize = max(builder.TerrainSize.x, builder.TerrainSize.z);
            var maxCellCountSqrt = sqrt(65535 * TerrainGeneratorBuilder.ObjectBufferStride);
            var cellCountSqrt = (int)clamp(ceil(worldSize * sqrt(2) / minRadius), 1, maxCellCountSqrt);

            // Larger than this and Shader dispatch throws a "too many threadgroups" error
            var collisionGrid = builder.CreateTexture(GraphicsFormat.R32_UInt, cellCountSqrt, cellCountSqrt);
            builder.SetFloatParam(shader, "_CollisionRadiusInfluence", _controls.RadiusInfluence);
            builder.SetFloatParam(shader, "_CollisionScaleInfluence", _controls.ScaleInfluence);
            builder.SetVectorParam(shader, "_CollisionGridSize", new float2(cellCountSqrt, cellCountSqrt));

            // Create and build a collision grid for the objects
            builder.SetTextureParam(shader, initID, "_OutputCollisionGrid", collisionGrid);
            builder.DispatchUnsafe(shader, initID, cellCountSqrt, cellCountSqrt, 1);

            builder.SetObjectsParam(shader, buildID, "_InputObjects", Input.Value);
            builder.SetTextureParam(shader, buildID, "_OutputCollisionGrid", collisionGrid);
            builder.DispatchSafe(shader, buildID, Input.Value);

            // Prune objects that did not fit into the collision grid
            Output.Value = builder.CreateObjects(Input.Value);
            builder.SetTextureParam(shader, pruneInitialID, "_InputCollisionGrid", collisionGrid);
            builder.SetObjectsParam(shader, pruneInitialID, "_InputObjects", Input.Value);
            builder.SetObjectsParam(shader, pruneInitialID, "_OutputObjects", Output.Value);
            builder.DispatchSafe(shader, pruneInitialID, Output.Value);

            // Prune objects based on neighbour intersections
            var maxScale = lerp(1, Output.Value.MaxScale, _controls.ScaleInfluence);
            var maxRadius = Output.Value.MaxRadius * _controls.RadiusInfluence * maxScale;
            var cellSize = worldSize / cellCountSqrt;
            var phaseCountSqrt = (int)ceil(2 * maxRadius / cellSize + 1);
            var phaseCount = phaseCountSqrt * phaseCountSqrt;
            var phaseSizeSqrt = (int)ceil((float)cellCountSqrt / phaseCountSqrt);

            builder.SetFloatParam(shader, "_PhaseSizeSqrt", phaseSizeSqrt);
            builder.SetTextureParam(shader, pruneID, "_InputCollisionGrid", collisionGrid);
            builder.SetObjectsParam(shader, pruneID, "_OutputObjects", Output.Value);

            for (var i = 0; i < phaseCount; ++i)
            {
                var phaseOffset = new float4(phaseCountSqrt, phaseCountSqrt, i % phaseCountSqrt, i / phaseCountSqrt);
                builder.SetVectorParam(shader, "_PhaseOffset", phaseOffset);

                builder.DispatchUnsafe(shader, pruneID, phaseSizeSqrt, phaseSizeSqrt, 1);
            }
        }
    }
}
