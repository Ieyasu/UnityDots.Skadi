using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public struct ObjectCollisionControls
    {
        [SerializeField]
        private float _radiusInfluence;

        [SerializeField]
        private float _scaleInfluence;

        public float RadiusInfluence
        {
            get => _radiusInfluence;
            set => _radiusInfluence = value;
        }

        public float ScaleInfluence
        {
            get => _scaleInfluence;
            set => _scaleInfluence = value;
        }

        public ObjectCollisionControls(float radiusInfluence, float scaleInfluence)
        {
            _radiusInfluence = radiusInfluence;
            _scaleInfluence = scaleInfluence;
        }
    }
}
