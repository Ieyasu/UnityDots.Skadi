using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Heightmap")]
    public sealed class HeightmapOutputNode : MapComputeNode, IPortalInputNode<MapHandle>
    {
        public override MapHandle MapPreview => InputHeightmap.HasValue ? InputHeightmap.Value : null;
        public MapHandle PortalValue => InputHeightmap.HasValue ? InputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [SerializeField, Control]
        public HeightmapOutputType _type = HeightmapOutputType.Ground;

        [SerializeField, Control]
        private string _name = "Heightmap";
        public string PortalName
        {
            get => _name;
            set => _name = value;
        }

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            switch (_type)
            {
                case HeightmapOutputType.Ground:
                    builder.AddHeightmap(InputHeightmap.Value);
                    break;
                case HeightmapOutputType.Water:
                    builder.AddWatermap(InputHeightmap.Value);
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(_type.ToString());
            }
        }
    }
}
