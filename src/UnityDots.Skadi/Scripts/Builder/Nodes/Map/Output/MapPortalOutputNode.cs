using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Output Portal")]
    public sealed class MapPortalOutputNode : MapComputeNode, IPortalOutputNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;
        IPortalInputNode IPortalOutputNode.PortalInput => PortalInput;

        [Output(0, Label = "Map")]
        public MapSocket OutputHeightmap
        {
            get;
            set;
        }

        [SerializeField]
        private NodeData _portalInput;
        public IPortalInputNode<MapHandle> PortalInput
        {
            get => _portalInput as IPortalInputNode<MapHandle>;
            set => _portalInput = value as NodeData;
        }

        public override bool CanBuild()
        {
            return PortalInput != null;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            OutputHeightmap.Value = PortalInput.PortalValue;
        }
    }
}
