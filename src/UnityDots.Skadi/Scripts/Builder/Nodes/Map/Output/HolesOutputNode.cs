using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Holes")]
    public sealed class HolesOutputNode : MapComputeNode, IPortalInputNode<MapHandle>
    {
        public override MapHandle MapPreview => PortalValue;
        public MapHandle PortalValue { get; private set; }

        [Input(0, Label = "Holes")]
        public MapSocket InputHoles
        {
            get;
            set;
        }

        [SerializeField, Control]
        private string _name = "Holes";
        public string PortalName
        {
            get => _name;
            set => _name = value;
        }

        public override bool CanBuild()
        {
            return InputHoles.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/BlackAndWhiteMaskNode");

            PortalValue = builder.CreateHeightmap(InputHoles.Value);
            builder.SetFloatParam(shader, "_Cutoff", 0);
            builder.SetTextureParam(shader, 0, "_InputHeightmap", InputHoles.Value);
            builder.SetTextureParam(shader, 0, "_OutputMask", PortalValue);
            builder.DispatchSafe(shader, 0, PortalValue);

            builder.AddHoles(PortalValue);
        }
    }
}
