using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Input Portal")]
    public sealed class MapPortalInputNode : MapComputeNode, IPortalInputNode<MapHandle>
    {
        public override MapHandle MapPreview => InputHeightmap.HasValue ? InputHeightmap.Value : null;
        public MapHandle PortalValue => InputHeightmap.HasValue ? InputHeightmap.Value : null;

        [Input(0, Label = "Map")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Control, SerializeField]
        private string _name = "Map Portal";
        public string PortalName => _name;
    }
}
