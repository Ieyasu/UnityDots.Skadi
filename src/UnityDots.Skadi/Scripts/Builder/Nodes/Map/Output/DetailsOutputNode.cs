using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Details")]
    public sealed class DetailsOutputNode : MapComputeNode, IMultiInputNode
    {
        private const int _minSocketCount = 1;
        private const int _maxSocketCount = 16;

        [SerializeField]
        private List<DetailPrototype> _prototypes = new List<DetailPrototype>();

        public override bool CanBuild()
        {
            return InputSockets.OfType<MapSocket>().Count(x => x.HasValue) > 0;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var maps = InputSockets.OfType<MapSocket>().ToList();
            for (var i = 0; i < min(maps.Count, _prototypes.Count); ++i)
            {
                var map = maps[i];
                if (map.HasValue)
                {
                    builder.AddDetails(map.Value, _prototypes[i]);
                }
            }
        }

        public bool CanAddInputSocket()
        {
            return InputSockets.Count < _maxSocketCount;
        }

        public bool CanRemoveInputSocket()
        {
            return InputSockets.Count > _minSocketCount;
        }

        public void OnInputSocketAdd()
        {
            CreateSocket(InputSockets.Count);
            ResizeList();
        }

        public void OnInputSocketRemove()
        {
            var id = InputSockets.Last().ID;
            RemoveInputSocket(id);
            ResizeList();
        }

        protected override void BuildSockets()
        {
            var socketCount = max(_prototypes.Count, 1);
            for (var i = 0; i < socketCount; ++i)
            {
                CreateSocket(i);
            }
            ResizeList();
        }

        private void CreateSocket(int id)
        {
            var socket = new MapSocket { ID = id, Name = "Input", Capacity = 1 };
            socket.ControlPropertyPath = $"_prototypes.Array.data[{id}]";
            AddInputSocket(socket);
        }

        private void ResizeList()
        {
            while (_prototypes.Count > InputSockets.Count)
            {
                _prototypes.RemoveAt(_prototypes.Count - 1);
            }

            while (_prototypes.Count < InputSockets.Count)
            {
                _prototypes.Add(null);
            }
        }
    }
}
