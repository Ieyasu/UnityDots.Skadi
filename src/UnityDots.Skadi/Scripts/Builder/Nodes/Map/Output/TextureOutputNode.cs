using System.Collections.Generic;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Output/Texture")]
    public sealed class TextureOutputNode : MapComputeNode, IMultiInputNode
    {
        private const int _minSocketCount = 1;
        private const int _maxSocketCount = 16;

        private MapHandle _preview = null;
        public override MapHandle MapPreview => _preview;

        [SerializeField]
        private List<TerrainLayer> _layers = new List<TerrainLayer>();

        [SerializeField]
        private List<SkadiTerrainLayer> _skadiLayers = new List<SkadiTerrainLayer>();

        [SerializeField]
        private TextureLayerType _layerType = TextureLayerType.Unity;

        public override bool CanBuild()
        {
            return InputSockets.OfType<MapSocket>().Count(x => x.HasValue) > 0;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            switch (_layerType)
            {
                case TextureLayerType.Unity:
                    BuildUnityTextures(builder);
                    break;
                case TextureLayerType.Skadi:
                    BuildSkadiTextures(builder);
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException($"Invalid enum {_layerType}");
            }
        }

        private void BuildUnityTextures(TerrainGeneratorBuilder builder)
        {
            var inputs = InputSockets.OfType<MapSocket>().Where(x => x.HasValue).Select(x => x.Value).ToList();
            for (var i = 0; i < inputs.Count; ++i)
            {
                builder.AddAlphamap(inputs[i], _layers[i]);
            }
        }

        private void BuildSkadiTextures(TerrainGeneratorBuilder builder)
        {
            var inputs = InputSockets.OfType<MapSocket>().Where(x => x.HasValue).Select(x => x.Value).ToList();
            for (var i = 0; i < inputs.Count; ++i)
            {
                builder.AddAlphamap(inputs[i], _skadiLayers[i]);
            }
        }

        public bool CanAddInputSocket()
        {
            return InputSockets.Count < _maxSocketCount;
        }

        public bool CanRemoveInputSocket()
        {
            return InputSockets.Count > _minSocketCount;
        }

        public void OnInputSocketAdd()
        {
            CreateSocket(InputSockets.Count);
            ResizeList();
        }

        public void OnInputSocketRemove()
        {
            var id = InputSockets.Last().ID;
            RemoveInputSocket(id);
            ResizeList();
        }

        protected override void BuildSockets()
        {
            var socketCount = max(max(_skadiLayers.Count, _layers.Count), 1);
            for (var i = 0; i < socketCount; ++i)
            {
                CreateSocket(i);
            }
            ResizeList();
        }

        private void CreateSocket(int id)
        {
            var socket = new MapSocket { ID = id, Name = "Input", Capacity = 1 };
            switch (_layerType)
            {
                case TextureLayerType.Unity:
                    socket.ControlPropertyPath = $"_layers.Array.data[{id}]";
                    break;
                case TextureLayerType.Skadi:
                    socket.ControlPropertyPath = $"_skadiLayers.Array.data[{id}]";
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException($"Invalid enum {_layerType}");
            }
            AddInputSocket(socket);
        }

        private void ResizeList()
        {
            while (_layers.Count > InputSockets.Count)
            {
                _layers.RemoveAt(_layers.Count - 1);
            }

            while (_skadiLayers.Count > InputSockets.Count)
            {
                _skadiLayers.RemoveAt(_skadiLayers.Count - 1);
            }

            while (_layers.Count < InputSockets.Count)
            {
                _layers.Add(null);
            }

            while (_skadiLayers.Count < InputSockets.Count)
            {
                _skadiLayers.Add(null);
            }
        }
    }
}
