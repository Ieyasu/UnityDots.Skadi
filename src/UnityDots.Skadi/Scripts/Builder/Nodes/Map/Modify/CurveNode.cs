using System;
using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Curve")]
    public sealed class CurveNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private CurveControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/CurveNode");
            var kernelID = shader.FindKernel(GetKernelName(InputHeightmap.Value.Texture.Format));

            OutputHeightmap.Value = builder.CreateHeightmap(InputHeightmap.Value);

            builder.SetCurveParam(shader, kernelID, "_Curve", 0, 1, _controls.Curve);
            builder.SetTextureParam(shader, kernelID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }

        private string GetKernelName(GraphicsFormat format)
        {
            switch (GraphicsFormatUtility.GetComponentCount(format))
            {
                case 1:
                    return "CSMain1";
                case 2:
                    return "CSMain2";
                case 3:
                    return "CSMain3";
                case 4:
                    return "CSMain4";
                default:
                    throw new ArgumentException($"Invalid graphics format {format}");
            }
        }
    }
}
