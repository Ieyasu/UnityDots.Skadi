using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Blend")]
    public sealed class BlendNode : MapComputeNode, IMultiInputNode
    {
        private const int _minSocketCount = 2;
        private const int _maxSocketCount = 20;

        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [SerializeField]
        private List<float> _opacities = new List<float>();

        [Control(Order = 0), SerializeField]
        private BlendMode _mode = BlendMode.Add;

        public override bool CanBuild()
        {
            return InputSockets.Count > 0 && InputSockets[0].HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/BlendNode");
            var copyKernelID = shader.FindKernel("CSCopy");
            var blendKernelID = shader.FindKernel(GetKernelName());
            var inputs = InputSockets.OfType<MapSocket>().Where(x => x.HasValue).Select(x => x.Value).ToList();

            var input1 = inputs[0];
            var buffer1 = builder.CreateHeightmap(input1);
            var buffer2 = builder.CreateHeightmap(input1);

            // Copy the layer to the first buffer
            builder.SetTextureParam(shader, copyKernelID, "_Input1", input1);
            builder.SetTextureParam(shader, copyKernelID, "_Output", buffer2);
            builder.DispatchSafe(shader, copyKernelID, buffer2);

            // Blend other layers on top of it
            ResizeList();
            for (var i = 1; i < inputs.Count; ++i)
            {
                if (inputs[i] == null)
                {
                    continue;
                }

                var socket = inputs[i];
                var temp = buffer2;
                buffer2 = buffer1;
                buffer1 = temp;

                builder.SetFloatParam(shader, "_Opacity", _opacities[i - 1]);
                builder.SetTextureParam(shader, blendKernelID, "_Input1", buffer1);
                builder.SetTextureParam(shader, blendKernelID, "_Input2", inputs[i]);
                builder.SetTextureParam(shader, blendKernelID, "_Output", buffer2);
                builder.DispatchSafe(shader, blendKernelID, buffer2);
            }

            OutputHeightmap.Value = buffer2;
        }

        public bool CanAddInputSocket()
        {
            return InputSockets.Count < _maxSocketCount;
        }

        public bool CanRemoveInputSocket()
        {
            return InputSockets.Count > _minSocketCount;
        }

        public void OnInputSocketAdd()
        {
            CreateSocket(InputSockets.Count);
            ResizeList();
        }

        public void OnInputSocketRemove()
        {
            var id = InputSockets.Last().ID;
            RemoveInputSocket(id);
            ResizeList();
        }

        protected override void BuildSockets()
        {
            var socketCount = max(_opacities.Count + 1, 2);
            for (var i = 0; i < socketCount; ++i)
            {
                CreateSocket(i);
            }
            ResizeList();
        }

        private void CreateSocket(int id)
        {
            var socket = new MapSocket { ID = id, Name = "Input", Capacity = 1 };
            if (id > 0)
            {
                socket.ControlPropertyPath = $"_opacities.Array.data[{id - 1}]";
            }
            AddInputSocket(socket);
        }

        private void ResizeList()
        {
            var count = max(0, InputSockets.Count - 1);
            while (_opacities.Count > count)
            {
                _opacities.RemoveAt(_opacities.Count - 1);
            }

            while (_opacities.Count < count)
            {
                _opacities.Add(1.0f);
            }
        }

        private string GetKernelName()
        {
            switch (_mode)
            {
                case BlendMode.Add:
                    return "CSAdd";
                case BlendMode.Multiply:
                    return "CSMultiply";
                case BlendMode.Divide:
                    return "CSDivide";
                case BlendMode.Subtract:
                    return "CSSubtract";
                case BlendMode.LinearInterpolation:
                    return "CSLerp";
                case BlendMode.Max:
                    return "CSMax";
                case BlendMode.Min:
                    return "CSMin";
                case BlendMode.BicubicInterpolation:
                    return "CSBicubic";
                default:
                    throw new InvalidEnumArgumentException(_mode.ToString());
            }
        }
    }
}
