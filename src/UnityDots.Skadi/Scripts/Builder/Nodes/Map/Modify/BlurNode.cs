using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Blur")]
    public sealed class BlurNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            private set;
        }

        [Input(1, Label = "Intensity")]
        public MapSocket InputIntensity
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [InputControl(1), SerializeField]
        private float _intensity = 1;

        [Control, SerializeField]
        private BlurControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/BlurNode");
            var horizontalID = shader.FindKernel("CSHorizontal");
            var verticalID = shader.FindKernel("CSVertical");

            var weights = GetWeightValues(_controls.Radius);
            var weightsBuffer = builder.CreateBuffer(weights);
            var buffer1 = builder.CreateHeightmap(InputHeightmap.Value);
            var buffer2 = builder.CreateHeightmap(InputHeightmap.Value);

            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetFloatParam(shader, "_Radius", _controls.Radius);
            builder.SetBufferParam(shader, horizontalID, "_InputWeights", weightsBuffer);
            builder.SetTextureParam(shader, horizontalID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, horizontalID, "_OutputBlurred", buffer1);
            builder.DispatchUnsafe(shader, horizontalID, buffer1);

            builder.SetBufferParam(shader, verticalID, "_InputWeights", weightsBuffer);
            builder.SetTextureParam(shader, verticalID, "_InputBlurred", buffer1);
            builder.SetTextureParam(shader, verticalID, "_OutputBlurred", buffer2);
            builder.DispatchUnsafe(shader, verticalID, buffer2);

            if (InputIntensity.HasValue)
            {
                var intensityID = shader.FindKernel("CSApplyIntensityMask");
                builder.SetTextureParam(shader, intensityID, "_InputIntensity", InputIntensity.Value);
                builder.SetTextureParam(shader, intensityID, "_InputHeightmap", InputHeightmap.Value);
                builder.SetTextureParam(shader, intensityID, "_InputBlurred", buffer2);
                builder.SetTextureParam(shader, intensityID, "_OutputHeightmap", buffer1);
                builder.DispatchSafe(shader, intensityID, buffer1);
            }
            else
            {
                var intensityID = shader.FindKernel("CSApplyIntensity");
                builder.SetTextureParam(shader, intensityID, "_InputHeightmap", InputHeightmap.Value);
                builder.SetTextureParam(shader, intensityID, "_InputBlurred", buffer2);
                builder.SetTextureParam(shader, intensityID, "_OutputHeightmap", buffer1);
                builder.DispatchSafe(shader, intensityID, buffer1);
            }

            OutputHeightmap.Value = buffer1;
        }

        private float2[] GetWeightValues(int radius)
        {
            var weights = new float2[radius + 1];
            var weightSum = 0.0f;
            var sigma = radius / 1.5f;

            for (int i = 0; i <= radius; ++i)
            {
                var weight = 0.39894f * exp(-0.5f * i * i / (sigma * sigma)) / sigma;
                weights[radius - i].x = weight;
                weightSum += weight;
            }

            // We store the partial weight sums for border pixels with the weights themselves.
            // The partial sums are needed, as pixels at the border do not use the whole kernel
            // and thus normalizing using the whole sum would produce incorrect results.
            var cumulativeWeightSum = 0.0f;
            for (int i = 0; i <= radius; ++i)
            {
                weights[i].y = 1.0f / (weightSum + cumulativeWeightSum);
                if (i < radius)
                {
                    cumulativeWeightSum += weights[radius - i - 1].x;
                }
            }

            return weights;
        }
    }
}
