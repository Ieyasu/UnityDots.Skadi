using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class ErosionControls
    {
        [SerializeField]
        private uint _iterations = 10;

        [SerializeField]
        private bool _smoothing = true;

        [SerializeField]
        private float _heightScale = 1.0f;

        [SerializeField]
        private float _sizeScale = 1.0f;

        // Water simulation
        [SerializeField]
        private float _hydraulicTimeStep = 0.5f;

        [SerializeField]
        private float _rain = 0.5f;

        [SerializeField]
        private float _evaporation = 0.5f;

        // Hydraulic erosion
        [SerializeField]
        private float _waterCapacity = 1.0f;

        [SerializeField]
        private float _capacity = 0.1f;

        [SerializeField]
        private float _deposition = 0.5f;

        [SerializeField]
        private float _dissolve = 0.5f;

        // Thermal erosion
        [SerializeField]
        private float _thermalTimeStep = 0.2f;

        [SerializeField]
        private float _talusAngle = 50;

        public uint Iterations
        {
            get => _iterations;
            set => _iterations = value;
        }

        public bool Smoothing
        {
            get => _smoothing;
            set => _smoothing = value;
        }

        public float HeightScale
        {
            get => _heightScale;
            set => _heightScale = value;
        }

        public float SizeScale
        {
            get => _sizeScale;
            set => _sizeScale = value;
        }

        public float HydraulicTimeStep
        {
            get => _hydraulicTimeStep;
            set => _hydraulicTimeStep = value;
        }

        public float Rain
        {
            get => _rain;
            set => _rain = value;
        }

        public float Evaporation
        {
            get => _evaporation;
            set => _evaporation = value;
        }

        public float WaterCapacity
        {
            get => _waterCapacity;
            set => _waterCapacity = value;
        }

        public float Capacity
        {
            get => _capacity;
            set => _capacity = value;
        }

        public float Deposition
        {
            get => _deposition;
            set => _deposition = value;
        }

        public float Dissolve
        {
            get => _dissolve;
            set => _dissolve = value;
        }

        public float ThermalTimeStep
        {
            get => _thermalTimeStep;
            set => _thermalTimeStep = value;
        }

        public float TalusAngle
        {
            get => _talusAngle;
            set => _talusAngle = value;
        }
    }
}
