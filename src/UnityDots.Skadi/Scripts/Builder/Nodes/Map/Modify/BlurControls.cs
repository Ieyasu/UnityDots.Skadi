using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class BlurControls
    {
        [SerializeField]
        private int _radius = 3;

        public int Radius
        {
            get => _radius;
            set => _radius = value;
        }
    }
}
