using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Imprint Objects")]
    public sealed class ImprintObjectsNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Input(1, Label = "Objects")]
        public ObjectsSocket InputObjects
        {
            get;
            set;
        }

        [Input(2, Label = "Intensity")]
        public MapSocket InputIntensity
        {
            get;
            set;
        }

        [Input(3, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [InputControl(2), SerializeField]
        private float _intensity = 1;

        [Control, SerializeField]
        private ImprintControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue && InputObjects.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            if (!InputObjects.HasValue)
            {
                OutputHeightmap.Value = InputHeightmap.Value;
                return;
            }

            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/ImprintObjectsNode");
            var kernelID = shader.FindKernel(GetKernelName());
            var collisionGrid = builder.CreateObjectCollisions(InputObjects.Value, _controls.Collision);

            OutputHeightmap.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetCurveParam(shader, kernelID, "_ProfileCurve", 0, 1, _controls.ProfileCurve);
            builder.SetTextureParam(shader, kernelID, "_InputMap", InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_OutputMap", OutputHeightmap.Value);
            if (InputIntensity.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputIntensity", InputIntensity.Value);
            }
            if (InputOffset.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputOffset", InputOffset.Value);
            }
            builder.SetObjectCollisionsParam(shader, kernelID, collisionGrid, _controls.Collision);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }

        private string GetKernelName()
        {
            var name = "CSImprint";
            if (InputIntensity.HasValue)
            {
                name += "Intensity";
            }
            if (InputOffset.HasValue)
            {
                name += "Offset";
            }
            return name;
        }
    }
}
