using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class TerraceControls
    {
        [SerializeField]
        private int _steps = 10;

        [SerializeField]
        private float _uniformity = 0.5f;

        [SerializeField]
        private float _steepness = 0.5f;

        public int Steps
        {
            get => _steps;
            set => _steps = value;
        }

        public float Uniformity
        {
            get => _uniformity;
            set => _uniformity = value;
        }

        public float Steepness
        {
            get => _steepness;
            set => _steepness = value;
        }
    }
}
