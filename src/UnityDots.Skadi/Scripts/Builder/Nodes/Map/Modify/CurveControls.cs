using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class CurveControls
    {
        [SerializeField]
        private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1, 1);

        public AnimationCurve Curve
        {
            get => _curve;
            set => _curve = value;
        }
    }
}
