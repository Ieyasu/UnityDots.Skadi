using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Erosion")]
    public sealed class ErosionNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Output(1, Label = "Water")]
        public MapSocket OutputWatermap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private ErosionControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/ErosionNode");
            var initKernel = shader.FindKernel("CSInitialize");
            var waterPingKernel = shader.FindKernel("CSUpdateWaterPing");
            var waterPongKernel = shader.FindKernel("CSUpdateWaterPong");
            var erosionPingKernel = shader.FindKernel("CSUpdateErosionPing");
            var erosionPongKernel = shader.FindKernel("CSUpdateErosionPong");
            var normalizeKernel = shader.FindKernel("CSNormalize");

            // Initialize globals
            var output = builder.CreateHeightmap(InputHeightmap.Value);
            var heightmap1 = builder.CreateMap(InputHeightmap.Value, GraphicsFormat.R32_SFloat);
            var heightmap2 = builder.CreateMap(InputHeightmap.Value, GraphicsFormat.R32_SFloat);
            var sedimentMap1 = builder.CreateHeightmap(InputHeightmap.Value);
            var sedimentMap2 = builder.CreateHeightmap(InputHeightmap.Value);
            var watermap1 = builder.CreateHeightmap(InputHeightmap.Value);
            var watermap2 = builder.CreateHeightmap(InputHeightmap.Value);
            var fluxMap1 = builder.CreateMap(InputHeightmap.Value, TextureUtility.HeightmapFormat4);
            var fluxMap2 = builder.CreateMap(InputHeightmap.Value, TextureUtility.HeightmapFormat4);
            var velocityMap = builder.CreateMap(InputHeightmap.Value, TextureUtility.HeightmapFormat2);

            var maxHeightDelta = tan(Mathf.Deg2Rad * _controls.TalusAngle);
            builder.SetFloatParam(shader, "_Smoothing", _controls.Smoothing ? 1 : 0);
            builder.SetFloatParam(shader, "_HydraulicTimeStep", _controls.HydraulicTimeStep);
            builder.SetFloatParam(shader, "_FluxFactor", 9.81f);
            builder.SetFloatParam(shader, "_Rain", _controls.Rain);
            builder.SetFloatParam(shader, "_Evaporation", _controls.Evaporation);
            builder.SetFloatParam(shader, "_Capacity", _controls.Capacity);
            builder.SetFloatParam(shader, "_WaterCapacity", _controls.WaterCapacity);
            builder.SetFloatParam(shader, "_Deposition", _controls.Deposition);
            builder.SetFloatParam(shader, "_Dissolve", _controls.Dissolve);
            builder.SetFloatParam(shader, "_ThermalTimeStep", _controls.ThermalTimeStep);
            builder.SetFloatParam(shader, "_MaxHeightDelta", maxHeightDelta);
            builder.SetFloatParam(shader, "_TerrainHeightScale", _controls.HeightScale);
            builder.SetFloatParam(shader, "_TerrainSizeScale", _controls.SizeScale);

            // Initialize maps
            builder.SetTextureParam(shader, initKernel, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, initKernel, "_OutputHeightmap", heightmap1);
            builder.SetTextureParam(shader, initKernel, "_OutputSediment", sedimentMap1);
            builder.SetTextureParam(shader, initKernel, "_OutputWater", watermap1);
            builder.SetTextureParam(shader, initKernel, "_OutputFlux", fluxMap1);
            builder.SetTextureParam(shader, initKernel, "_OutputVelocity", velocityMap);
            builder.DispatchSafe(shader, initKernel, heightmap1);

            builder.SetTextureParam(shader, waterPingKernel, "_InputFlux", fluxMap1);
            builder.SetTextureParam(shader, waterPingKernel, "_InputWater", watermap1);
            builder.SetTextureParam(shader, waterPingKernel, "_OutputWater", watermap2);
            builder.SetTextureParam(shader, waterPingKernel, "_OutputVelocity", velocityMap);

            builder.SetTextureParam(shader, erosionPingKernel, "_InputVelocity", velocityMap);
            builder.SetTextureParam(shader, erosionPingKernel, "_InputWater", watermap2);
            builder.SetTextureParam(shader, erosionPingKernel, "_InputFlux", fluxMap1);
            builder.SetTextureParam(shader, erosionPingKernel, "_OutputFlux", fluxMap2);
            builder.SetTextureParam(shader, erosionPingKernel, "_InputHeightmap", heightmap1);
            builder.SetTextureParam(shader, erosionPingKernel, "_OutputHeightmap", heightmap2);
            builder.SetTextureParam(shader, erosionPingKernel, "_InputSediment", sedimentMap1);
            builder.SetTextureParam(shader, erosionPingKernel, "_OutputSediment", sedimentMap2);

            builder.SetTextureParam(shader, waterPongKernel, "_InputFlux", fluxMap2);
            builder.SetTextureParam(shader, waterPongKernel, "_InputWater", watermap2);
            builder.SetTextureParam(shader, waterPongKernel, "_OutputWater", watermap1);
            builder.SetTextureParam(shader, waterPongKernel, "_OutputVelocity", velocityMap);

            builder.SetTextureParam(shader, erosionPongKernel, "_InputVelocity", velocityMap);
            builder.SetTextureParam(shader, erosionPongKernel, "_InputWater", watermap1);
            builder.SetTextureParam(shader, erosionPongKernel, "_InputFlux", fluxMap2);
            builder.SetTextureParam(shader, erosionPongKernel, "_OutputFlux", fluxMap1);
            builder.SetTextureParam(shader, erosionPongKernel, "_InputHeightmap", heightmap2);
            builder.SetTextureParam(shader, erosionPongKernel, "_OutputHeightmap", heightmap1);
            builder.SetTextureParam(shader, erosionPongKernel, "_InputSediment", sedimentMap2);
            builder.SetTextureParam(shader, erosionPongKernel, "_OutputSediment", sedimentMap1);

            for (var i = 0; i < _controls.Iterations * 10; ++i)
            {
                builder.DispatchSafe(shader, waterPingKernel, watermap2);
                builder.DispatchSafe(shader, erosionPingKernel, heightmap2);
                builder.DispatchSafe(shader, waterPongKernel, watermap1);
                builder.DispatchSafe(shader, erosionPongKernel, heightmap1);
            }

            builder.SetTextureParam(shader, normalizeKernel, "_InputHeightmap", heightmap1);
            builder.SetTextureParam(shader, normalizeKernel, "_OutputHeightmap", output);
            builder.SetTextureParam(shader, normalizeKernel, "_OutputWater", watermap1);
            builder.DispatchSafe(shader, normalizeKernel, output);

            OutputHeightmap.Value = output;
            OutputWatermap.Value = watermap1;
        }
    }
}
