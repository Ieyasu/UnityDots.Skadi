using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Invert")]
    public sealed class InvertNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            set;
        }

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/InvertNode");
            var kernelID = 0;

            OutputHeightmap.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_Input", InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_Output", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }
    }
}
