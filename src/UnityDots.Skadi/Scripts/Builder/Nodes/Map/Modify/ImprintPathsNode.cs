using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Imprint Paths")]
    public sealed class ImprintPathsNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Input(1, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Input(2, Label = "Intensity")]
        public MapSocket InputIntensity
        {
            get;
            set;
        }

        [Input(3, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [InputControl(2), SerializeField]
        private float _intensity = 1;

        [Control, SerializeField]
        private ImprintControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue && InputPaths.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            if (!InputPaths.HasValue)
            {
                OutputHeightmap.Value = InputHeightmap.Value;
                return;
            }

            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/ImprintPathsNode");
            var initKernel = shader.FindKernel("CSInitialize");
            var calculateKernel = shader.FindKernel(GetKernelName());
            var convertKernel = shader.FindKernel("CSConvertToFloat");

            // Clear mask
            var atomicWeightSum = builder.CreateMap(InputHeightmap.Value, GraphicsFormat.R32_UInt);
            var atomicHeightSum = builder.CreateMap(InputHeightmap.Value, GraphicsFormat.R32_SInt);
            builder.SetTextureParam(shader, initKernel, "_OutputWeightSum", atomicWeightSum);
            builder.SetTextureParam(shader, initKernel, "_OutputHeightSum", atomicHeightSum);
            builder.DispatchSafe(shader, initKernel, atomicHeightSum);

            // Calculate the mask
            builder.SetFloatParam(shader, "_Radius", _controls.Collision.RadiusInfluence);
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetCurveParam(shader, calculateKernel, "_ProfileCurve", 0, 1, _controls.ProfileCurve);
            builder.SetPathsParam(shader, calculateKernel, "_Input", InputPaths.Value);
            builder.SetTextureParam(shader, calculateKernel, "_InputHeightmap", InputHeightmap.Value);
            if (InputIntensity.HasValue)
            {
                builder.SetTextureParam(shader, calculateKernel, "_InputIntensity", InputIntensity.Value);
            }
            if (InputOffset.HasValue)
            {
                builder.SetTextureParam(shader, calculateKernel, "_InputOffset", InputOffset.Value);
            }
            builder.SetTextureParam(shader, calculateKernel, "_OutputWeightSum", atomicWeightSum);
            builder.SetTextureParam(shader, calculateKernel, "_OutputHeightSum", atomicHeightSum);
            builder.DispatchPathEdgesSafe(shader, calculateKernel, InputPaths.Value);

            // Convert to floats
            OutputHeightmap.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetTextureParam(shader, convertKernel, "_InputWeightSum", atomicWeightSum);
            builder.SetTextureParam(shader, convertKernel, "_InputHeightSum", atomicHeightSum);
            builder.SetTextureParam(shader, convertKernel, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, convertKernel, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, convertKernel, OutputHeightmap.Value);
        }

        private string GetKernelName()
        {
            var name = "CSImprint";
            if (InputIntensity.HasValue)
            {
                name += "Intensity";
            }
            if (InputOffset.HasValue)
            {
                name += "Offset";
            }
            return name;
        }
    }
}
