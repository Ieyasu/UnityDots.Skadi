using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Modify/Contrast")]
    public sealed class ContrastNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            set;
        }

        [SerializeField, Control]
        private float _intensity = 1;

        [SerializeField, Control]
        private float _contrast = 1;

        public override bool CanBuild()
        {
            return InputHeightmap.Value != null;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Modify/ContrastNode");
            var kernelID = 0;

            OutputHeightmap.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetFloatParam(shader, "_Intensity", _intensity);
            builder.SetFloatParam(shader, "_Contrast", _contrast);
            builder.SetTextureParam(shader, kernelID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }
    }
}
