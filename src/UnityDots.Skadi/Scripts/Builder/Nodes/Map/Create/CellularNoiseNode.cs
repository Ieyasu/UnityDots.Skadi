using System.ComponentModel;
using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Create/Cellular Noise")]
    public sealed class CellularNoiseNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private CellularNoiseControls _controls = null;

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_controls == null)
            {
                _controls = new CellularNoiseControls();
            }

            if (_controls.Seed == 0)
            {
                _controls.Seed = (uint)UnityEngine.Random.Range(1, int.MaxValue);
            }
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Create/CellularNoiseNode");
            var kernelID = shader.FindKernel(GetKernelName());

            if (InputOffset.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputOffset", InputOffset.Value);
            }

            OutputHeightmap.Value = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.SetSeedParam(shader, "_Seed", _controls.Seed);
            builder.SetFloatParam(shader, "_Frequency", 1 / _controls.Size);
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetFloatParam(shader, "_Uniformity", _controls.Uniformity);
            builder.SetVectorParam(shader, "_Weights", GetWeights());
            builder.SetTextureParam(shader, kernelID, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }

        private float4 GetWeights()
        {
            switch (_controls.Type)
            {
                case CellularNoiseType.Flat:
                    return new float4(0, 0, 0, 0);
                case CellularNoiseType.F1MinusF2:
                    return new float4(-1, 1, 0, 0);
                case CellularNoiseType.F1PlusF2:
                    return new float4(1, 1, 0, 0);
                case CellularNoiseType.F1:
                    return new float4(1, 0, 0, 0);
                case CellularNoiseType.F2:
                    return new float4(0, 1, 0, 0);
                case CellularNoiseType.Custom:
                    return new float4(_controls.Weights.x, _controls.Weights.y, 0, 0);
                default:
                    throw new InvalidEnumArgumentException(_controls.DistanceMetric.ToString());
            }
        }

        private string GetKernelName()
        {
            var flat = (_controls.Type == CellularNoiseType.Flat) ? "Flat" : string.Empty;
            var offset = InputOffset.HasValue ? "Offset" : string.Empty;
            return $"{GetKernelPrefix()}{flat}{offset}";
        }

        private string GetKernelPrefix()
        {
            switch (_controls.DistanceMetric)
            {
                case CellularDistanceMetric.Euclidean:
                    return "CSEuclidean";
                case CellularDistanceMetric.EuclideanSquared:
                    return "CSEuclideanSquared";
                case CellularDistanceMetric.Manhattan:
                    return "CSManhattan";
                default:
                    throw new InvalidEnumArgumentException(_controls.DistanceMetric.ToString());
            }
        }
    }
}
