using System.ComponentModel;
using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Create/Shape")]
    public sealed class ShapeNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private ShapeControls _controls = null;

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Create/ShapeNode");
            var kernelID = shader.FindKernel(GetKernelName());

            if (InputOffset.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputOffset", InputOffset.Value);
            }

            OutputHeightmap.Value = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.SetVectorParam(shader, "_Origin", new float4(_controls.Origin, 0));
            builder.SetVectorParam(shader, "_Scale", new float4(2 / _controls.Scale, 2 / _controls.Scale, 0, 0));
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetTextureParam(shader, kernelID, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }

        private string GetKernelName()
        {
            // No offset or looping needed for simple fill
            if (_controls.Type == ShapeType.Fill)
            {
                return "CSFill";
            }

            var offset = InputOffset.HasValue ? "Offset" : string.Empty;
            return $"{GetKernelPrefix()}{GetKernelSuffix()}{offset}";
        }

        private string GetKernelPrefix()
        {
            switch (_controls.Type)
            {
                case ShapeType.Circle:
                    return "CSCircle";
                case ShapeType.Cone:
                    return "CSCone";
                case ShapeType.Pyramid:
                    return "CSPyramid";
                case ShapeType.LinearGradient:
                    return "CSLinearGradient";
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }

        private string GetKernelSuffix()
        {
            switch (_controls.WrapMode)
            {
                case UVWrapMode.Once:
                    return string.Empty;
                case UVWrapMode.Loop:
                    return "Loop";
                case UVWrapMode.PingPong:
                    return "PingPong";
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }
    }
}
