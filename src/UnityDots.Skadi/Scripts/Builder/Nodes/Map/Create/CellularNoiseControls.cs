using System;
using UnityEngine;
using Unity.Mathematics;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class CellularNoiseControls
    {
        [SerializeField]
        private CellularNoiseType _type = CellularNoiseType.F1MinusF2;

        [SerializeField]
        private CellularDistanceMetric _distanceMetric = CellularDistanceMetric.EuclideanSquared;

        [SerializeField]
        private uint _seed;

        [SerializeField]
        private float _size = 0.25f;

        [SerializeField]
        private float _intensity = 1.0f;

        [SerializeField]
        private float _uniformity = 0;

        [SerializeField]
        private float2 _weights = new float2(-1, 1);

        public CellularNoiseType Type
        {
            get => _type;
            set => _type = value;
        }

        public CellularDistanceMetric DistanceMetric
        {
            get => _distanceMetric;
            set => _distanceMetric = value;
        }

        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        public float Size
        {
            get => _size;
            set => _size = value;
        }

        public float Intensity
        {
            get => _intensity;
            set => _intensity = value;
        }

        public float Uniformity
        {
            get => _uniformity;
            set => _uniformity = value;
        }

        public float2 Weights
        {
            get => _weights;
            set => _weights = value;
        }
    }
}
