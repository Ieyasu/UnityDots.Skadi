using System.ComponentModel;
using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Create/Simplex Noise")]
    public sealed class SimplexNoiseNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Input(0, Label = "Offset")]
        public MapSocket InputOffset
        {
            get;
            set;
        }

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private SimplexNoiseControls _controls = null;

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_controls == null)
            {
                _controls = new SimplexNoiseControls();
            }

            if (_controls.Seed == 0)
            {
                _controls.Seed = (uint)Random.Range(1, int.MaxValue);
            }
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Create/SimplexNoiseNode");
            var kernelID = shader.FindKernel(GetKernelName());

            if (InputOffset.HasValue)
            {
                builder.SetTextureParam(shader, kernelID, "_InputOffset", InputOffset.Value);
            }

            OutputHeightmap.Value = builder.CreateMap(builder.MapResolution, builder.MapResolution, GetGraphicsFormat());
            builder.SetSeedParam(shader, "_Seed", _controls.Seed);
            builder.SetFloatParam(shader, "_Frequency", 1 / _controls.Size);
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetFloatParam(shader, "_Octaves", _controls.Octaves);
            builder.SetFloatParam(shader, "_Lacunarity", _controls.Lacunarity);
            builder.SetFloatParam(shader, "_Persistance", _controls.Persistance);
            builder.SetFloatParam(shader, "_AmplitudeSumInv", 1 / _controls.AmplitudeSum);
            builder.SetTextureParam(shader, kernelID, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, kernelID, OutputHeightmap.Value);
        }

        private string GetKernelName()
        {
            var offset = InputOffset.HasValue ? "Offset" : string.Empty;
            return $"{GetKernelPrefix()}{offset}";
        }

        private string GetKernelPrefix()
        {
            switch (_controls.Type)
            {
                case SimplexNoiseType.Clouds:
                    return "CSCloud";
                case SimplexNoiseType.Warp:
                    return "CSWarp";
                case SimplexNoiseType.Billow:
                    return "CSBillow";
                case SimplexNoiseType.Ridge:
                    return "CSRidge";
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }

        private GraphicsFormat GetGraphicsFormat()
        {
            switch (_controls.Type)
            {
                case SimplexNoiseType.Clouds:
                case SimplexNoiseType.Billow:
                case SimplexNoiseType.Ridge:
                    return TextureUtility.HeightmapFormat;
                case SimplexNoiseType.Warp:
                    return TextureUtility.HeightmapFormat2;
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }
    }
}
