using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Create/Random Noise")]
    public sealed class RandomNoiseNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputHeightmap.HasValue ? OutputHeightmap.Value : null;

        [Output(0, Label = "Heightmap")]
        public MapSocket OutputHeightmap
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private uint _seed;

        protected override void OnEnable()
        {
            if (_seed == 0)
            {
                _seed = (uint)Random.Range(1, int.MaxValue);
            }

            base.OnEnable();
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Create/RandomNoiseNode");

            OutputHeightmap.Value = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.SetSeedParam(shader, "_Seed", _seed);
            builder.SetTextureParam(shader, 0, "_OutputHeightmap", OutputHeightmap.Value);
            builder.DispatchSafe(shader, 0, OutputHeightmap.Value);
        }
    }
}
