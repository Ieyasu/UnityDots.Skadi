using System;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SimplexNoiseControls
    {
        [SerializeField]
        private SimplexNoiseType _type = SimplexNoiseType.Clouds;

        [SerializeField]
        private uint _seed;

        [SerializeField]
        private float _size = 0.5f;

        [SerializeField]
        private float _intensity = 1.0f;

        [SerializeField]
        private int _octaves = 8;

        [SerializeField]
        private float _lacunarity = 2.0f;

        [SerializeField]
        private float _persistance = 0.4f;

        public float AmplitudeSum => (1 - pow(Persistance, Octaves)) / (1 - Persistance);

        public SimplexNoiseType Type
        {
            get => _type;
            set => _type = value;
        }

        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        public float Size
        {
            get => _size;
            set => _size = value;
        }

        public float Intensity
        {
            get => _intensity;
            set => _intensity = value;
        }

        public int Octaves
        {
            get => _octaves;
            set => _octaves = value;
        }

        public float Lacunarity
        {
            get => _lacunarity;
            set => _lacunarity = value;
        }

        public float Persistance
        {
            get => _persistance;
            set => _persistance = value;
        }
    }
}
