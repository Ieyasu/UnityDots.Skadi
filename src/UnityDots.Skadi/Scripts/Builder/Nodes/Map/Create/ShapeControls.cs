using System;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class ShapeControls
    {
        [SerializeField]
        private ShapeType _type = ShapeType.Cone;

        [SerializeField]
        private UVWrapMode _wrapMode = UVWrapMode.Once;

        [SerializeField]
        private float _intensity = 1.0f;

        [SerializeField]
        private float2 _origin = 0.5f;

        [SerializeField]
        private float _scale = 0.5f;

        [SerializeField]
        private float _rotation = 0;

        public ShapeType Type
        {
            get => _type;
            set => _type = value;
        }

        public UVWrapMode WrapMode
        {
            get => _wrapMode;
            set => _wrapMode = value;
        }

        public float Intensity
        {
            get => _intensity;
            set => _intensity = value;
        }

        public float2 Origin
        {
            get => _origin;
            set => _origin = value;
        }

        public float Scale
        {
            get => _scale;
            set => _scale = value;
        }

        public float Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }
    }
}
