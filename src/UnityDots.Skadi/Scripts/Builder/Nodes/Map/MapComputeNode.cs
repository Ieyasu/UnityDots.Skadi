using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    public abstract class MapComputeNode : NodeData, IGeneratorNode, IPreviewNode
    {
        public bool HasPreview => MapPreview != null;

        public virtual MapHandle MapPreview => null;

        public virtual bool CanBuild()
        {
            return true;
        }

        public virtual void Build(TerrainGeneratorBuilder builder) { }

        public virtual void BuildPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            var componentCount = GraphicsFormatUtility.GetComponentCount(MapPreview.Texture.Format);
            var shader = Resources.Load<ComputeShader>("GPU/Preview/PreviewMap" + componentCount);
            builder.SetTextureParam(shader, 0, "_Input", MapPreview);
            builder.SetTextureParam(shader, 0, "_Output", previewTexture);
            builder.DispatchSafe(shader, 0, previewTexture);
        }
    }
}
