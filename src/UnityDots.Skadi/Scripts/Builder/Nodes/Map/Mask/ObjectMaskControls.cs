using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class ObjectMaskControls
    {
        [SerializeField]
        private float _intensity = 1;

        [SerializeField]
        private ObjectCollisionControls _collision = new ObjectCollisionControls();

        [SerializeField]
        private AnimationCurve _profileCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        public float Intensity
        {
            get => _intensity;
            set => _intensity = value;
        }

        public ObjectCollisionControls Collision
        {
            get => _collision;
            set => _collision = value;
        }

        public AnimationCurve ProfileCurve
        {
            get => _profileCurve;
            set => _profileCurve = value;
        }
    }
}
