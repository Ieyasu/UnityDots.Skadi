using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Object Mask")]
    public sealed class ObjectMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Objects")]
        public ObjectsSocket InputObjects
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private ObjectMaskControls _controls = null;

        public override bool CanBuild()
        {
            return InputObjects.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/ObjectMaskNode");
            var kernelID = 0;
            var collisionGrid = builder.CreateObjectCollisions(InputObjects.Value, _controls.Collision);

            OutputMask.Value = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetCurveParam(shader, kernelID, "_ProfileCurve", 0, 1, _controls.ProfileCurve);
            builder.SetObjectCollisionsParam(shader, kernelID, collisionGrid, _controls.Collision);
            builder.SetTextureParam(shader, kernelID, "_OutputMask", OutputMask.Value);
            builder.DispatchSafe(shader, kernelID, OutputMask.Value);
        }
    }
}
