using UnityDots.Graph;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Range")]
    public sealed class RangeMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private RangeMaskControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/RangeMaskNode");

            var minRange = min(_controls.StartRange, _controls.EndRange);
            var maxRange = max(_controls.StartRange, _controls.EndRange);
            var minRangeStart = saturate(minRange);
            var maxRangeEnd = clamp(maxRange, minRangeStart, 1);
            var minRangeEnd = clamp(minRange + _controls.StartTransition, minRangeStart, maxRangeEnd);
            var maxRangeStart = clamp(maxRange - _controls.EndTransition, minRangeEnd, maxRangeEnd);

            OutputMask.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetFloatParam(shader, "_MinRangeStart", minRangeStart);
            builder.SetFloatParam(shader, "_MinRangeEnd", minRangeEnd);
            builder.SetFloatParam(shader, "_MaxRangeStart", maxRangeStart);
            builder.SetFloatParam(shader, "_MaxRangeEnd", maxRangeEnd);
            builder.SetTextureParam(shader, 0, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, 0, "_OutputMask", OutputMask.Value);
            builder.DispatchSafe(shader, 0, OutputMask.Value);
        }
    }
}
