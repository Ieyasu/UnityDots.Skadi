using System.ComponentModel;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Curvature")]
    public sealed class CurvatureMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private CurvatureMaskControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/CurvatureMaskNode");
            var horizontalID = shader.FindKernel("CSHorizontal");
            var verticalID = shader.FindKernel("CSVertical");
            var resultID = shader.FindKernel(GetKernelName());

            var buffer1 = builder.CreateHeightmap(InputHeightmap.Value);
            var buffer2 = builder.CreateHeightmap(InputHeightmap.Value);

            builder.SetFloatParam(shader, "_Radius", _controls.Radius);
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetTextureParam(shader, horizontalID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, horizontalID, "_OutputAverageHeights", buffer1);
            builder.DispatchUnsafe(shader, horizontalID, buffer1);

            builder.SetTextureParam(shader, verticalID, "_InputAverageHeights", buffer1);
            builder.SetTextureParam(shader, verticalID, "_OutputAverageHeights", buffer2);
            builder.DispatchUnsafe(shader, verticalID, buffer2);

            builder.SetTextureParam(shader, resultID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, resultID, "_InputAverageHeights", buffer2);
            builder.SetTextureParam(shader, resultID, "_OutputHeightmap", buffer1);
            builder.DispatchSafe(shader, resultID, buffer1);

            OutputMask.Value = buffer1;
        }

        private string GetKernelName()
        {
            switch (_controls.Type)
            {
                case CurvatureType.Convex:
                    return "CSConvex";
                case CurvatureType.Concave:
                    return "CSConcave";
                case CurvatureType.Both:
                    return "CSCombined";
                default:
                    throw new InvalidEnumArgumentException(_controls.Type.ToString());
            }
        }
    }
}
