using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class RangeMaskControls
    {
        [SerializeField]
        private float _minRange = 0.25f;

        [SerializeField]
        private float _maxRange = 0.75f;

        [SerializeField]
        private float _startTransition = 0.1f;

        [SerializeField]
        private float _endTransition = 0.1f;

        public float StartRange
        {
            get => _minRange;
            set => _minRange = value;
        }

        public float EndRange
        {
            get => _maxRange;
            set => _maxRange = value;
        }

        public float StartTransition
        {
            get => _startTransition;
            set => _startTransition = value;
        }

        public float EndTransition
        {
            get => _endTransition;
            set => _endTransition = value;
        }
    }
}
