using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Black & White")]
    public sealed class BlackAndWhiteMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private float _cutoff = 0;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/BlackAndWhiteMaskNode");

            OutputMask.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetFloatParam(shader, "_Cutoff", _cutoff);
            builder.SetTextureParam(shader, 0, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, 0, "_OutputMask", OutputMask.Value);
            builder.DispatchSafe(shader, 0, OutputMask.Value);
        }
    }
}
