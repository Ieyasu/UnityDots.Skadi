using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Normals")]
    public sealed class NormalMapNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputNormalmap.HasValue ? OutputNormalmap.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Normalmap")]
        public MapSocket OutputNormalmap
        {
            get;
            private set;
        }

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Util/CalculateNormals");
            var kernelID = 0;

            // Calculate normals
            OutputNormalmap.Value = builder.CreateNormalMap(InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, kernelID, "_OutputNormalmap", OutputNormalmap.Value);
            builder.DispatchSafe(shader, kernelID, InputHeightmap.Value);
        }

        public override void BuildPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Preview/PreviewNormalMap");
            builder.SetTextureParam(shader, 0, "_Input", MapPreview.Texture);
            builder.SetTextureParam(shader, 0, "_Output", previewTexture);
            builder.DispatchSafe(shader, 0, previewTexture);
        }
    }
}
