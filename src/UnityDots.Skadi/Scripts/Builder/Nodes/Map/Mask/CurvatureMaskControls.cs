using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class CurvatureMaskControls
    {
        [SerializeField]
        private CurvatureType _type = CurvatureType.Both;

        [SerializeField]
        private int _radius = 10;

        [SerializeField]
        private float _intensity = 10;

        public CurvatureType Type
        {
            get => _type;
            set => _type = value;
        }

        public int Radius
        {
            get => _radius;
            set => _radius = value;
        }

        public float Intensity
        {
            get => _intensity;
            set => _intensity = value;
        }
    }
}
