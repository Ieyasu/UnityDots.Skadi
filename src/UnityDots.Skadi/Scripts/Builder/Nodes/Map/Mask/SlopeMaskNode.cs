using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Slope")]
    public sealed class SlopeMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Heightmap")]
        public MapSocket InputHeightmap
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private SlopeMaskControls _controls = null;

        public override bool CanBuild()
        {
            return InputHeightmap.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/SlopeMaskNode");

            OutputMask.Value = builder.CreateHeightmap(InputHeightmap.Value);
            builder.SetFloatParam(shader, "_MinSlope", _controls.MinSlope);
            builder.SetFloatParam(shader, "_MaxSlope", _controls.MaxSlope);
            builder.SetFloatParam(shader, "_Transition", _controls.Transition);
            builder.SetTextureParam(shader, 0, "_InputHeightmap", InputHeightmap.Value);
            builder.SetTextureParam(shader, 0, "_OutputMask", OutputMask.Value);
            builder.DispatchSafe(shader, 0, OutputMask.Value);
        }
    }
}
