using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SlopeMaskControls
    {
        [SerializeField]
        private float _minSlope = 0;

        [SerializeField]
        private float _maxSlope = 90;

        [SerializeField]
        private float _transition = 10;

        public float MinSlope
        {
            get => _minSlope;
            set => _minSlope = value;
        }

        public float MaxSlope
        {
            get => _maxSlope;
            set => _maxSlope = value;
        }

        public float Transition
        {
            get => _transition;
            set => _transition = value;
        }
    }
}
