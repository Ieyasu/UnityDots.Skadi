using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Node(typeof(TerrainGenerator), "Map/Mask/Path Mask")]
    public sealed class PathMaskNode : MapComputeNode
    {
        public override MapHandle MapPreview => OutputMask.HasValue ? OutputMask.Value : null;

        [Input(0, Label = "Paths")]
        public PathsSocket InputPaths
        {
            get;
            set;
        }

        [Output(0, Label = "Mask")]
        public MapSocket OutputMask
        {
            get;
            private set;
        }

        [Control, SerializeField]
        private PathMaskControls _controls = null;

        [Control, SerializeField]
        private float _radius = 5;

        public override bool CanBuild()
        {
            return InputPaths.HasValue;
        }

        public override void Build(TerrainGeneratorBuilder builder)
        {
            var shader = Resources.Load<ComputeShader>("GPU/Nodes/Map/Mask/PathMaskNode");
            var initKernel = shader.FindKernel("CSInitialize");
            var calculateKernel = shader.FindKernel("CSRasterize");
            var convertKernel = shader.FindKernel("CSConvertToFloat");
            OutputMask.Value = builder.CreateHeightmap(builder.MapResolution, builder.MapResolution);

            // Clear mask
            var atomicMask = builder.CreateMap(OutputMask.Value, GraphicsFormat.R32_UInt);
            builder.SetTextureParam(shader, initKernel, "_OutputMaskAtomic", atomicMask);
            builder.DispatchSafe(shader, initKernel, atomicMask);

            // Calculate the mask
            builder.SetFloatParam(shader, "_Radius", _radius);
            builder.SetFloatParam(shader, "_Intensity", _controls.Intensity);
            builder.SetCurveParam(shader, calculateKernel, "_ProfileCurve", 0, 1, _controls.ProfileCurve);
            builder.SetPathsParam(shader, calculateKernel, "_Input", InputPaths.Value);
            builder.SetTextureParam(shader, calculateKernel, "_OutputMaskAtomic", atomicMask);
            builder.DispatchPathEdgesSafe(shader, calculateKernel, InputPaths.Value);

            // Convert to floats
            builder.SetTextureParam(shader, convertKernel, "_InputMaskAtomic", atomicMask);
            builder.SetTextureParam(shader, convertKernel, "_OutputMask", OutputMask.Value);
            builder.DispatchSafe(shader, convertKernel, OutputMask.Value);
        }
    }
}
