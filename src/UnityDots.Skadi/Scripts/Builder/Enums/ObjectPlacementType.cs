namespace UnityDots.Skadi
{
    public enum ObjectPlacementType
    {
        Random = 0,
        Grid = 1,
        Natural = 2,
    }
}
