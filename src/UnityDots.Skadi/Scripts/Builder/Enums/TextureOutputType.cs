namespace UnityDots.Skadi
{
    public enum TextureLayerType
    {
        Unity = 0,
        Skadi = 1,
    }
}
