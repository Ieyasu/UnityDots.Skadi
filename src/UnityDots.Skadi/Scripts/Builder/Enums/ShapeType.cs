namespace UnityDots.Skadi
{
    public enum ShapeType
    {
        Circle = 0,
        Cone = 1,
        Pyramid = 2,
        LinearGradient = 3,
        Fill = 4,
    }
}
