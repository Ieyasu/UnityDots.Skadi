namespace UnityDots.Skadi
{
    public enum CurvatureType
    {
        Convex = 0,
        Concave = 1,
        Both = 2,
    }
}
