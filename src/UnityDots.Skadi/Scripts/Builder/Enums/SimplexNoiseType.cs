namespace UnityDots.Skadi
{
    public enum SimplexNoiseType
    {
        Clouds = 0,
        Ridge = 1,
        Billow = 2,
        Warp = 3,
    }
}
