namespace UnityDots.Skadi
{
    public enum BlendMode
    {
        Add = 0,
        Subtract = 1,
        Multiply = 2,
        Divide = 3,
        LinearInterpolation = 4,
        BicubicInterpolation = 5,
        Min = 6,
        Max = 7,
    }
}
