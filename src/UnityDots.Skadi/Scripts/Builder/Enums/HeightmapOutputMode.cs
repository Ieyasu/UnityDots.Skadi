namespace UnityDots.Skadi
{
    public enum HeightmapOutputType
    {
        Ground = 0,
        Water = 1,
    }
}
