namespace UnityDots.Skadi
{
    public enum PathCount
    {
        // Minimum = 0,    // Correspons to https://en.wikipedia.org/wiki/Euclidean_minimum_spanning_tree
        Low = 1,        // Corresponds to https://en.wikipedia.org/wiki/Relative_neighborhood_graph
        Medium = 2,     // Corresponds to https://en.wikipedia.org/wiki/Gabriel_graph
        High = 3,       // Corresponds to https://en.wikipedia.org/wiki/Delaunay_triangulation
    }
}
