namespace UnityDots.Skadi
{
    public enum CellularDistanceMetric
    {
        Euclidean = 0,
        EuclideanSquared = 1,
        Manhattan = 2,
    }
}
