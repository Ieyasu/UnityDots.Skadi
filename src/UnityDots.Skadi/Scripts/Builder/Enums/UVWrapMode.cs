namespace UnityDots.Skadi
{
    public enum UVWrapMode
    {
        Once = 0,
        Loop = 1,
        PingPong = 2,
    }
}
