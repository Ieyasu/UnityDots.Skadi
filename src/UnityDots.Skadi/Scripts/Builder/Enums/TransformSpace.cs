namespace UnityDots.Skadi
{
    public enum TransformSpace
    {
        Local = 0,
        WorldAbsolute = 1,
        WorldRelative = 2,
    }
}
