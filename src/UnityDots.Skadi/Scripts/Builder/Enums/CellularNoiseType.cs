namespace UnityDots.Skadi
{
    public enum CellularNoiseType
    {
        Flat = 0,
        F1MinusF2 = 1,
        F1PlusF2 = 2,
        F1 = 3,
        F2 = 4,
        Custom = 5,
    }
}
