using System.Collections.Generic;
using UnityDots.Graph;

namespace UnityDots.Skadi
{
    internal class NodeDependencyGraph
    {
        private NodeData _activeNode;
        private TerrainGenerator _activeGenerator;
        private readonly HashSet<IResourceHandle> _outputResources;
        private readonly Dictionary<IPortalInputNode, HashSet<IPortalOutputNode>> _portalMap;
        private readonly Dictionary<NodeData, HashSet<IResourceHandle>> _nodeResourceDependencies;
        private readonly Dictionary<IResourceHandle, HashSet<NodeData>> _resourceNodeDependencies;

        public NodeDependencyGraph()
        {
            _outputResources = new HashSet<IResourceHandle>();
            _portalMap = new Dictionary<IPortalInputNode, HashSet<IPortalOutputNode>>();
            _nodeResourceDependencies = new Dictionary<NodeData, HashSet<IResourceHandle>>();
            _resourceNodeDependencies = new Dictionary<IResourceHandle, HashSet<NodeData>>();
        }

        public void Clear()
        {
            _activeNode = null;
            _activeGenerator = null;
            _outputResources.Clear();
            _portalMap.Clear();
            _nodeResourceDependencies.Clear();
            _resourceNodeDependencies.Clear();
        }

        public void SetActiveNode(TerrainGenerator generator, NodeData node)
        {
            _activeGenerator = generator;
            _activeNode = node;
        }

        // Marks a resource as an output resource, meaning it no longer gets released automatically
        // even when all the dependencies have been evaluated
        public void MarkAsOutput(IResourceHandle handle)
        {
            _outputResources.Add(handle);
        }

        // Propagates dependencies so that resources depend on the nodes they are inputs for
        public void PropagateDependencies()
        {
            // Go through all outgoing edges for the active node and make sure the
            // resources tied to the edges also depend on the destination nodes
            foreach (var outputEdge in _activeGenerator.GetOutputEdges(_activeNode))
            {
                var endNode = outputEdge.End.Node;
                var outputSocket = _activeNode.OutputSockets[outputEdge.Start.SocketID];
                PropagateDependencies(endNode, outputSocket);
            }

            // For portals, all the inputs for the input portal are also dependencies for the output portal
            if (_activeNode is IPortalInputNode portalInput)
            {
                var inputEdges = _activeGenerator.GetInputEdges(_activeNode);
                foreach (var portalOutput in GetPortalOutputs(portalInput))
                {
                    foreach (var inputEdge in inputEdges)
                    {
                        var endNode = portalOutput as NodeData;
                        var inputSocket = _activeNode.InputSockets[inputEdge.End.SocketID];
                        PropagateDependencies(endNode, inputSocket);
                    }
                }
            }
        }

        // Adds a new resource (texture/buffer) dependency
        public void DependOnActiveNode(IResourceHandle handle)
        {
            DependOnNode(handle, _activeNode);
        }

        // Removes all dependencies from the given nodes
        public void ReleaseActiveNodeDependencies(SerializableCommandBuffer commandBuffer)
        {
            // Remove the node from all dependency lists
            var dependentResources = GetDependentResources(_activeNode);
            foreach (var handle in dependentResources)
            {
                var nodeDependencies = GetDependencies(handle);
                nodeDependencies.Remove(_activeNode);

                // If all dependencies are resolved and the resource is not marked as final output,
                // release the resources for later use
                if (nodeDependencies.Count > 0 || _outputResources.Contains(handle))
                {
                    continue;
                }

                if (handle is BufferHandle buffer)
                {
                    commandBuffer.ReleaseBuffer(buffer);
                }
                else if (handle is TextureHandle texture)
                {
                    commandBuffer.ReleaseTexture(texture);
                }
            }
            dependentResources.Clear();
        }

        private void PropagateDependencies(NodeData endNode, SocketData socket)
        {
            if (socket is MapSocket mapSocket && mapSocket.HasValue && mapSocket.Value != null)
            {
                DependOnNode(mapSocket.Value.Texture, endNode);
            }
            else if (socket is ObjectsSocket objectSocket && objectSocket.HasValue && objectSocket.Value != null)
            {
                DependOnNode(objectSocket.Value.Buffer, endNode);
            }
            else if (socket is PathsSocket pathSocket && pathSocket.HasValue && pathSocket.Value != null)
            {
                DependOnNode(pathSocket.Value.Edges, endNode);
                DependOnNode(pathSocket.Value.Vertices, endNode);
            }
        }

        private void DependOnNode(IResourceHandle handle, NodeData node)
        {
            var nodeDependencies = GetDependencies(handle);
            var resourceDependencies = GetDependentResources(node);
            nodeDependencies.Add(node);
            resourceDependencies.Add(handle);
        }

        // Returns all dependencies for the given resource
        private HashSet<NodeData> GetDependencies(IResourceHandle handle)
        {
            if (!_resourceNodeDependencies.TryGetValue(handle, out HashSet<NodeData> dependencies))
            {
                dependencies = new HashSet<NodeData>();
                _resourceNodeDependencies[handle] = dependencies;
            }
            return dependencies;
        }

        // Returns all resources depending on the given node
        private HashSet<IResourceHandle> GetDependentResources(NodeData node)
        {
            if (!_nodeResourceDependencies.TryGetValue(node, out HashSet<IResourceHandle> dependencies))
            {
                dependencies = new HashSet<IResourceHandle>();
                _nodeResourceDependencies[node] = dependencies;
            }
            return dependencies;
        }

        // Returns all output portals for the given input portal
        private IEnumerable<IPortalOutputNode> GetPortalOutputs(IPortalInputNode portalInput)
        {
            foreach (var node in _activeGenerator.Nodes)
            {
                if (node is IPortalOutputNode portalOutput && portalOutput.PortalInput == portalInput)
                {
                    yield return portalOutput;
                }
            }
        }
    }
}
