using System;
using System.Collections.Generic;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal sealed class NodePreviewTexturePool : IDisposable
    {
        private readonly List<IPreviewNode> _unusedNodes;

        public Dictionary<IPreviewNode, RenderTexture> Textures { get; }
        public bool IsDisposed { get; private set; }
        public int PreviewResolution { get; private set; }

        public NodePreviewTexturePool(int previewResolution)
        {
            _unusedNodes = new List<IPreviewNode>();
            Textures = new Dictionary<IPreviewNode, RenderTexture>();
            IsDisposed = false;
            PreviewResolution = previewResolution;
        }

        ~NodePreviewTexturePool()
        {
            if (!IsDisposed)
            {
                Debug.LogWarning($"{GetType().Name} not disposed properly");
                Dispose();
            }
        }

        public void Dispose()
        {
            IsDisposed = true;

            foreach (var texture in Textures.Values)
            {
                RenderTexture.ReleaseTemporary(texture);
            }

            _unusedNodes.Clear();
            Textures.Clear();
        }

        public void DisposeUnused()
        {
            foreach (var node in _unusedNodes)
            {
                ReleasePreviewTexture(node);
            }

            _unusedNodes.Clear();
            _unusedNodes.AddRange(Textures.Keys);
        }

        public RenderTexture GetTexture(IPreviewNode node)
        {
            // Create and cache the preview texture if its missing or if we deleted it
            if (!Textures.TryGetValue(node, out RenderTexture texture))
            {
                var descriptor = TextureUtility.GetColorTextureDescriptor(PreviewResolution, PreviewResolution);
                texture = RenderTexture.GetTemporary(descriptor);
                texture.Create();
                Textures.Add(node, texture);
            }

            _unusedNodes.Remove(node);
            return texture;
        }

        private void ReleasePreviewTexture(IPreviewNode node)
        {
            if (Textures.TryGetValue(node, out RenderTexture texture))
            {
                RenderTexture.ReleaseTemporary(texture);
                Textures.Remove(node);
            }
        }
    }
}
