using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityDots.Skadi
{
    public sealed class TerrainGeneratorBuilderResources
    {
        public List<DetailPrototype> DetailPrototypes { get; }
        public List<ObjectPrototype> ObjectPrototypes { get; }
        public List<TerrainLayer> UnityTerrainLayers { get; }
        public List<SkadiTerrainLayer> SkadiTerrainLayers { get; }

        public List<MapHandle> Alphamaps { get; }
        public List<MapHandle> Detailmaps { get; }
        public List<MapHandle> Heightmaps { get; }
        public List<MapHandle> HolesTextures { get; }
        public List<MapHandle> Watermaps { get; }
        public List<ObjectsHandle> ObjectBuffers { get; }

        public TerrainGeneratorBuilderResources()
        {
            DetailPrototypes = new List<DetailPrototype>();
            ObjectPrototypes = new List<ObjectPrototype>();
            UnityTerrainLayers = new List<TerrainLayer>();
            SkadiTerrainLayers = new List<SkadiTerrainLayer>();

            Alphamaps = new List<MapHandle>();
            Detailmaps = new List<MapHandle>();
            Heightmaps = new List<MapHandle>();
            HolesTextures = new List<MapHandle>();
            Watermaps = new List<MapHandle>();
            ObjectBuffers = new List<ObjectsHandle>();
        }

        public void Clear()
        {
            DetailPrototypes.Clear();
            ObjectPrototypes.Clear();
            UnityTerrainLayers.Clear();
            SkadiTerrainLayers.Clear();

            Alphamaps.Clear();
            Detailmaps.Clear();
            Heightmaps.Clear();
            HolesTextures.Clear();
            Watermaps.Clear();
            ObjectBuffers.Clear();
        }

        public TerrainGeneratorResources ToSerializable()
        {
            var resources = new TerrainGeneratorResources();
            resources.DetailPrototypes.AddRange(DetailPrototypes);
            resources.ObjectPrototypes.AddRange(ObjectPrototypes);
            resources.UnityTerrainLayers.AddRange(UnityTerrainLayers);
            resources.SkadiTerrainLayers.AddRange(SkadiTerrainLayers);

            resources.Alphamaps.AddRange(Alphamaps.Select(x => x.Texture));
            resources.Detailmaps.AddRange(Detailmaps.Select(x => x.Texture));
            resources.Heightmaps.AddRange(Heightmaps.Select(x => x.Texture));
            resources.HolesTextures.AddRange(HolesTextures.Select(x => x.Texture));
            resources.Watermaps.AddRange(Watermaps.Select(x => x.Texture));
            resources.ObjectBuffers.AddRange(ObjectBuffers.Select(x => x.Buffer));
            return resources;
        }
    }
}
