using UnityDots.Graph;

namespace UnityDots.Skadi
{
    public sealed class ObjectsSocket : SocketData<ObjectsHandle>
    {
        public override bool HasValue => base.HasValue && RawValue != null;
    }
}
