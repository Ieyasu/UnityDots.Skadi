using UnityDots.Graph;

namespace UnityDots.Skadi
{
    public sealed class PathsSocket : SocketData<PathsHandle>
    {
        public override bool HasValue => base.HasValue && RawValue != null;
    }
}
