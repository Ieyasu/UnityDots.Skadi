using UnityDots.Graph;

namespace UnityDots.Skadi
{
    public sealed class MapSocket : SocketData<MapHandle>
    {
        public override bool HasValue => base.HasValue && RawValue != null;
    }
}
