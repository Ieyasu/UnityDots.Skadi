namespace UnityDots.Skadi
{
    public interface IPortalOutputNode
    {
        IPortalInputNode PortalInput { get; }
    }

    public interface IPortalOutputNode<T> : IPortalOutputNode where T : IPortalInputNode
    {
        new T PortalInput { get; }
    }
}
