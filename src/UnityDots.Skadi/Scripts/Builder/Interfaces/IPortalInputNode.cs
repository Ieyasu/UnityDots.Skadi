namespace UnityDots.Skadi
{
    public interface IPortalInputNode
    {
        string PortalName { get; }
    }

    public interface IPortalInputNode<T> : IPortalInputNode
    {
        T PortalValue { get; }
    }
}
