using UnityEngine;

namespace UnityDots.Skadi
{
    public interface IPreviewNode
    {
        bool HasPreview { get; }
        void BuildPreview(TerrainGeneratorBuilder builder, RenderTexture previewTexture);
    }
}
