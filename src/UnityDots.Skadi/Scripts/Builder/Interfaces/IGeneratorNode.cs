namespace UnityDots.Skadi
{
    public interface IGeneratorNode
    {
        bool CanBuild();
        void Build(TerrainGeneratorBuilder builder);
    }
}
