using System;
using System.Collections.Generic;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    public sealed class TerrainGeneratorTraverser
    {
        public readonly Stack<NodeData> _traverseStack;
        public readonly HashSet<NodeData> _processedNodes;
        public readonly HashSet<EdgeData> _processedEdges;
        public readonly HashSet<IPortalInputNode> _processedPortals;

        public TerrainGeneratorTraverser()
        {
            _traverseStack = new Stack<NodeData>();
            _processedNodes = new HashSet<NodeData>();
            _processedEdges = new HashSet<EdgeData>();
            _processedPortals = new HashSet<IPortalInputNode>();
        }

        public void TraverseNodes(TerrainGenerator generator, Action<NodeData> onEvaluateNode)
        {
            foreach (var node in generator.Nodes)
            {
                node.ResetSockets();
            }

            _traverseStack.Clear();
            _processedNodes.Clear();
            _processedEdges.Clear();
            _processedPortals.Clear();

            for (var i = generator.Nodes.Count - 1; i >= 0; --i)
            {
                var node = generator.Nodes[i];
                if (generator.GetInputEdges(node).Count == 0 && !(node is IPortalOutputNode))
                {
                    _traverseStack.Push(node);
                    _processedNodes.Add(node);
                }
            }

            while (_traverseStack.Count != 0)
            {
                var node = _traverseStack.Pop();
                TraverseNode(generator, node, onEvaluateNode);
            }
        }

        private void TraverseNode(TerrainGenerator generator, NodeData node, Action<NodeData> onEvaluateNode)
        {
            onEvaluateNode(node);

            // If the node is an portal input, try adding all linked portal outputs to traverse list
            if (node is IPortalInputNode portalInput)
            {
                _processedPortals.Add(portalInput);

                foreach (var otherNode in generator.Nodes)
                {
                    if (otherNode is IPortalOutputNode portalOutput && portalOutput.PortalInput == portalInput)
                    {
                        TryPushNode(generator, otherNode);
                    }
                }
            }

            // For all nodes we go through their output edges and evaluate them
            var outputEdges = generator.GetOutputEdges(node);
            for (var i = outputEdges.Count - 1; i >= 0; --i)
            {
                var outputEdge = outputEdges[i];
                EvaluateEdge(generator, outputEdge);
                var outputNode = outputEdge.End.Node;
                TryPushNode(generator, outputNode);
            }
        }

        private void TryPushNode(TerrainGenerator generator, NodeData node)
        {
            // Add nodes only if not yet processed
            if (_processedNodes.Contains(node))
            {
                return;
            }

            // Add nodes only if all incoming edges are processed
            foreach (var inputEdge in generator.GetInputEdges(node))
            {
                if (!_processedEdges.Contains(inputEdge))
                {
                    return;
                }
            }

            // Add portal outputs only if the portal input is processed
            if (node is IPortalOutputNode portalOutput && !_processedPortals.Contains(portalOutput.PortalInput))
            {
                return;
            }

            _traverseStack.Push(node);
            _processedNodes.Add(node);
        }

        private void EvaluateEdge(TerrainGenerator generator, EdgeData edge)
        {
            _processedEdges.Add(edge);

            var startSocket = edge.Start.Node.GetOutputSocket(edge.Start.SocketID);
            var endSocket = edge.End.Node.GetInputSocket(edge.End.SocketID);

            if (!startSocket.HasValue)
            {
                return;
            }

            var edgeConverter = generator.GetEdgeConverter(startSocket.Type, endSocket.Type);
            if (edgeConverter == null)
            {
                Debug.LogError($"Can not convert from '{startSocket.Type}' to '{endSocket.Type}'");
                return;
            }

            var value = startSocket.GetValue();
            var convertedValue = edgeConverter.Convert(value);
            endSocket.SetValue(convertedValue);
        }
    }
}
