using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace UnityDots.Skadi
{
    public sealed class ObjectsHandle
    {
        public BufferHandle Buffer { get; }
        public float MinRadius { get; }
        public float MaxRadius { get; }
        public float MinScale { get; }
        public float MaxScale { get; }
        public ReadOnlyCollection<ObjectPrototype> Prototypes { get; }

        internal ObjectsHandle(BufferHandle buffer, IList<ObjectPrototype> prototypes, float minRadius, float maxRadius, float minScale, float maxScale)
        {
            Buffer = buffer;
            MinRadius = minRadius;
            MaxRadius = maxRadius;
            MinScale = minScale;
            MaxScale = maxScale;
            Prototypes = prototypes.Distinct().ToList().AsReadOnly();
        }
    }
}
