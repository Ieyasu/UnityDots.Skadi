namespace UnityDots.Skadi
{
    public struct ObjectCollisionsHandle
    {
        public ObjectsHandle Objects { get; }
        public int CellCount => CellCountSqrt * CellCountSqrt;
        public int CellCountSqrt { get; }

        internal BufferHandle Colliders { get; }
        internal BufferHandle IDList { get; }
        internal BufferHandle IndexList { get; }

        internal ObjectCollisionsHandle(ObjectsHandle objects, BufferHandle colliders, BufferHandle idList, BufferHandle indexList, int cellCountSqrt)
        {
            Objects = objects;
            Colliders = colliders;
            IDList = idList;
            IndexList = indexList;
            CellCountSqrt = cellCountSqrt;
        }
    }
}
