namespace UnityDots.Skadi
{
    public sealed class PathsHandle
    {
        public BufferHandle Vertices { get; }
        public BufferHandle Edges { get; }

        internal PathsHandle(BufferHandle vertices, BufferHandle edges)
        {
            Vertices = vertices;
            Edges = edges;
        }
    }
}
