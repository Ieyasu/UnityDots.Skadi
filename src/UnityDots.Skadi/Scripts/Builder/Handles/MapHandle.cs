namespace UnityDots.Skadi
{
    public sealed class MapHandle
    {
        public TextureHandle Texture { get; }

        internal MapHandle(TextureHandle texture)
        {
            Texture = texture;
        }
    }
}
