namespace UnityDots.Skadi
{
    public sealed class PathsAdjacencyListHandle
    {
        public BufferHandle Edges { get; }
        public BufferHandle IndexList { get; }

        internal PathsAdjacencyListHandle(BufferHandle edges, BufferHandle indexList)
        {
            Edges = edges;
            IndexList = indexList;
        }
    }
}
