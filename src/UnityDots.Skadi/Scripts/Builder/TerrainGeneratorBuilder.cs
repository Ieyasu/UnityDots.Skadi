using System;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Mathematics;
using UnityDots.Graph;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    public sealed class TerrainGeneratorBuilder
    {
        public const int ObjectBufferStride = 128;
        public const int PathBufferStride = 128;

        private bool _buildPreview;
        private readonly SerializableCommandBuffer _commandBuffer;
        private readonly TerrainGeneratorTraverser _graphTraverser;

        // Dependency graph used to track when resources (textures/buffers) should be cleared
        private NodeDependencyGraph _dependencyGraph;

        // Pool of textures for displaying node previews
        private NodePreviewTexturePool _previewTexturePool;

        // Sub graph building
        private TerrainGenerator _mainGenerator;
        private TerrainGenerator _activeSubGenerator;
        private readonly TerrainGeneratorTraverser _subGraphTraverser;
        private readonly Dictionary<TerrainGenerator, TerrainGeneratorBuilderResources> _subGeneratorResources;

        public uint GlobalSeed { get; private set; }
        public int MapResolution { get; private set; }
        public float3 TerrainSize { get; private set; }
        public TerrainGeneratorBuilderResources Resources { get; }

        public TerrainGenerator ActiveGenerator => _activeSubGenerator ?? _mainGenerator;

        public TerrainGeneratorBuilder()
        {
            _commandBuffer = new SerializableCommandBuffer();
            _graphTraverser = new TerrainGeneratorTraverser();
            _dependencyGraph = new NodeDependencyGraph();
            _previewTexturePool = new NodePreviewTexturePool(256);
            _subGraphTraverser = new TerrainGeneratorTraverser();
            _subGeneratorResources = new Dictionary<TerrainGenerator, TerrainGeneratorBuilderResources>();

            Resources = new TerrainGeneratorBuilderResources();
        }

        public void Build(TerrainGenerator generator, bool buildPreview)
        {
            _buildPreview = buildPreview;
            _mainGenerator = generator;
            _activeSubGenerator = null;
            _commandBuffer.Clear();
            _dependencyGraph.Clear();
            _subGeneratorResources.Clear();

            GlobalSeed = generator.Settings.Seed;
            MapResolution = generator.Settings.MapResolution;
            TerrainSize = generator.Settings.TerrainSize;
            Resources.Clear();

            // Set global shader constants
            var terrainSizeVector = TextureUtility.GetSizeVector(TerrainSize.xz);
            var resolutionVector = TextureUtility.GetSizeVector(MapResolution, MapResolution);
            var cellSize = terrainSizeVector * resolutionVector.zwxy;
            _commandBuffer.SetGlobalFloatParam("skadi_TerrainHeight", TerrainSize.y);
            _commandBuffer.SetGlobalVectorParam("skadi_TerrainSize", terrainSizeVector);
            _commandBuffer.SetGlobalVectorParam("skadi_CellSize", cellSize);

            // Build the command buffer and previews, disposing unused preview textures
            _graphTraverser.TraverseNodes(generator, EvaluateNode);

            if (_buildPreview)
            {
                _previewTexturePool.DisposeUnused();
            }
        }

        public void Dispose()
        {
            _previewTexturePool.Dispose();
        }

        public Dictionary<IPreviewNode, RenderTexture> GetPreviewTextures()
        {
            if (!_buildPreview)
            {
                throw new InvalidOperationException("Can't fetch preview textures when preview has not been built");
            }

            return new Dictionary<IPreviewNode, RenderTexture>(_previewTexturePool.Textures);
        }

        public SerializableCommandBuffer GetSerializableCommandBuffer()
        {
            return new SerializableCommandBuffer(_commandBuffer);
        }

        public TerrainGeneratorResources GetSerializableResources()
        {
            return Resources.ToSerializable();
        }

        // Returns the resource list of a sub generator
        public TerrainGeneratorBuilderResources GetResources(TerrainGenerator subGenerator)
        {
            return _subGeneratorResources[subGenerator];
        }

        public void BuildSubGenerator(TerrainGenerator subGenerator)
        {
            _activeSubGenerator = subGenerator;
            _subGraphTraverser.TraverseNodes(subGenerator, EvaluateNode);
            _activeSubGenerator = null;
        }

        public int AddObjectPrototype(ObjectPrototype prototype)
        {
            var activeResources = GetActiveResources();
            activeResources.ObjectPrototypes.Add(prototype);
            return activeResources.ObjectPrototypes.Count - 1;
        }

        public void AddObjectBuffer(ObjectsHandle handle)
        {
            var activeResources = GetActiveResources();
            activeResources.ObjectBuffers.Add(handle);
            _dependencyGraph.MarkAsOutput(handle.Buffer);
        }

        public void AddHeightmap(MapHandle handle)
        {
            var activeResources = GetActiveResources();
            activeResources.Heightmaps.Add(handle);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public void AddHoles(MapHandle handle)
        {
            var activeResources = GetActiveResources();
            activeResources.HolesTextures.Add(handle);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public void AddWatermap(MapHandle handle)
        {
            var activeResources = GetActiveResources();
            activeResources.Watermaps.Add(handle);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public void AddAlphamap(MapHandle handle, TerrainLayer layer)
        {
            var activeResources = GetActiveResources();
            activeResources.Alphamaps.Add(handle);
            activeResources.UnityTerrainLayers.Add(layer);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public void AddAlphamap(MapHandle handle, SkadiTerrainLayer layer)
        {
            var activeResources = GetActiveResources();
            activeResources.Alphamaps.Add(handle);
            activeResources.UnityTerrainLayers.Add(layer.TerrainLayer);
            activeResources.SkadiTerrainLayers.Add(layer);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public void AddDetails(MapHandle handle, DetailPrototype prototype)
        {
            var activeResources = GetActiveResources();
            activeResources.Detailmaps.Add(handle);
            activeResources.DetailPrototypes.Add(prototype);
            _dependencyGraph.MarkAsOutput(handle.Texture);
        }

        public BufferHandle CreateBuffer(int count, int stride, ComputeBufferType type = ComputeBufferType.Default)
        {
            var handle = _commandBuffer.CreateBuffer(count, stride, type);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public BufferHandle CreateBuffer(float[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var handle = _commandBuffer.CreateBuffer(values, type);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public BufferHandle CreateBuffer(float2[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var handle = _commandBuffer.CreateBuffer(values, type);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public BufferHandle CreateBuffer(float3[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var handle = _commandBuffer.CreateBuffer(values, type);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public BufferHandle CreateBuffer(float4[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var handle = _commandBuffer.CreateBuffer(values, type);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public TextureHandle CreateTexture(GraphicsFormat format, int width, int height)
        {
            var handle = _commandBuffer.CreateTexture(format, width, height);
            _dependencyGraph.DependOnActiveNode(handle);
            return handle;
        }

        public MapHandle CreateMap(int width, int height, GraphicsFormat format)
        {
            var texture = CreateTexture(format, width, height);
            return new MapHandle(texture);
        }

        public MapHandle CreateMap(MapHandle template, GraphicsFormat format)
        {
            return CreateMap(template.Texture.Width, template.Texture.Height, format);
        }

        public MapHandle CreateHeightmap(int width, int height)
        {
            return CreateMap(width, height, TextureUtility.HeightmapFormat);
        }

        public MapHandle CreateHeightmap(MapHandle template)
        {
            return CreateMap(template, TextureUtility.HeightmapFormat);
        }

        public MapHandle CreateNormalMap(int width, int height)
        {
            return CreateMap(width, height, TextureUtility.NormalMapFormat);
        }

        public MapHandle CreateNormalMap(MapHandle template)
        {
            return CreateMap(template, TextureUtility.NormalMapFormat);
        }

        public ObjectsHandle CreateObjects(ObjectsHandle template)
        {
            return CreateObjects(template.Buffer.Length, template);
        }

        public ObjectsHandle CreateObjects(int count, ObjectsHandle template)
        {
            return CreateObjects(count, template.Prototypes, template.MinRadius, template.MaxRadius, template.MinScale, template.MaxScale);
        }

        public ObjectsHandle CreateObjects(int count, IList<ObjectPrototype> prototypes, float minRadius, float maxRadius, float minScale, float maxScale)
        {
            if (count % ObjectBufferStride != 0)
            {
                throw new ArgumentException($"Object count must be divisible by {ObjectBufferStride} (the number of threads in the thread group).");
            }

            // Objects are stored in 12 floats (3 for position, 1 for radius, 4 for rotation, 3 for scale, 1 for active)
            var clearObjectsShader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/ClearObjects");
            var objectBuffer = CreateBuffer(count, 12 * sizeof(float));
            SetBufferParam(clearObjectsShader, 0, "_OutputObjects", objectBuffer);
            DispatchSafe(clearObjectsShader, 0, objectBuffer);
            return new ObjectsHandle(objectBuffer, prototypes, minRadius, maxRadius, minScale, maxScale);
        }

        public PathsHandle CreatePaths(BufferHandle vertices, BufferHandle edges)
        {
            return new PathsHandle(vertices, edges);
        }

        public PathsHandle CreatePaths(int vertexCount, int edgeCount)
        {
            var vertices = CreatePathVertices(vertexCount);
            var edges = CreatePathEdges(edgeCount);
            return CreatePaths(vertices, edges);
        }

        public PathsHandle CreatePaths(PathsHandle template)
        {
            return CreatePaths(template.Vertices.Length, template.Edges.Length);
        }

        public BufferHandle CreatePathEdges(int edgeCount, ComputeBufferType bufferType = ComputeBufferType.Default)
        {
            if (edgeCount % PathBufferStride != 0)
            {
                throw new ArgumentException($"Path edge count must be divisible by {PathBufferStride} (the number of threads in the thread group).");
            }

            var clearShader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/ClearPaths");
            var edges = CreateBuffer(edgeCount, 2 * sizeof(float), bufferType);
            var kernel = clearShader.FindKernel("CSClearEdges");
            SetBufferParam(clearShader, kernel, "_OutputEdges", edges);
            DispatchSafe(clearShader, kernel, edges);
            return edges;
        }

        public BufferHandle CreatePathVertices(int vertexCount, ComputeBufferType bufferType = ComputeBufferType.Default)
        {
            if (vertexCount % PathBufferStride != 0)
            {
                throw new ArgumentException($"Path vertex count must be divisible by {PathBufferStride} (the number of threads in the thread group).");
            }

            var clearShader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/ClearPaths");
            var vertices = CreateBuffer(vertexCount, 8 * sizeof(float), bufferType);
            var kernel = clearShader.FindKernel("CSClearVertices");
            SetBufferParam(clearShader, kernel, "_OutputVertices", vertices);
            DispatchSafe(clearShader, kernel, vertices);
            return vertices;
        }

        public void ClearBuffer(BufferHandle texture, float value)
        {
            _commandBuffer.ClearBuffer(texture, value);
        }

        public void ClearTexture(TextureHandle texture, float value)
        {
            ClearTexture(texture, new Color(value, value, value, value));
        }

        public void ClearTexture(TextureHandle texture, Color value)
        {
            _commandBuffer.ClearTexture(texture, value);
        }

        public void ClearTexture(RenderTexture texture, float value)
        {
            ClearTexture(texture, new Color(value, value, value, value));
        }

        public void ClearTexture(RenderTexture texture, Color value)
        {
            _commandBuffer.ClearTexture(texture, value);
        }

        public ObjectsHandle CopyObjects(ObjectsHandle handle)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CopyObjects");
            var kernelID = 0;
            var copy = CreateObjects(handle);
            SetObjectsParam(shader, kernelID, "_InputObjects", handle);
            SetObjectsParam(shader, kernelID, "_OutputObjects", copy);
            DispatchSafe(shader, kernelID, handle);
            return copy;
        }

        public void CopyPathEdges(BufferHandle src, BufferHandle dst)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CopyPaths");
            var copyEdgesKernel = shader.FindKernel("CSCopyEdges");
            SetBufferParam(shader, copyEdgesKernel, "_InputEdges", src);
            SetBufferParam(shader, copyEdgesKernel, "_OutputEdges", dst);
            DispatchSafe(shader, copyEdgesKernel, dst);
        }

        public void CopyPathVertices(BufferHandle src, BufferHandle dst)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CopyPaths");
            var copyVerticesKernel = shader.FindKernel("CSCopyVertices");
            SetBufferParam(shader, copyVerticesKernel, "_InputVertices", src);
            SetBufferParam(shader, copyVerticesKernel, "_OutputVertices", dst);
            DispatchSafe(shader, copyVerticesKernel, dst);
        }

        public void CopyPaths(PathsHandle src, PathsHandle dst)
        {
            CopyPathEdges(src.Edges, dst.Edges);
            CopyPathVertices(src.Vertices, dst.Vertices);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, int threadsX, int threadsY, int threadsZ)
        {
            _commandBuffer.DispatchSafe(shader, kernelIndex, threadsX, threadsY, threadsZ);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, int threadsX, int threadsY, int threadsZ)
        {
            _commandBuffer.DispatchUnsafe(shader, kernelIndex, threadsX, threadsY, threadsZ);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, BufferHandle handle)
        {
            _commandBuffer.DispatchSafe(shader, kernelIndex, handle.Length, 1, 1);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, BufferHandle handle)
        {
            _commandBuffer.DispatchUnsafe(shader, kernelIndex, handle.Length, 1, 1);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, TextureHandle handle)
        {
            _commandBuffer.DispatchSafe(shader, kernelIndex, handle.Width, handle.Height, 1);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, TextureHandle handle)
        {
            _commandBuffer.DispatchUnsafe(shader, kernelIndex, handle.Width, handle.Height, 1);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, Texture texture)
        {
            _commandBuffer.DispatchSafe(shader, kernelIndex, texture.width, texture.height, 1);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, Texture texture)
        {
            _commandBuffer.DispatchUnsafe(shader, kernelIndex, texture.width, texture.height, 1);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, ObjectsHandle handle)
        {
            DispatchSafe(shader, kernelIndex, handle.Buffer);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, ObjectsHandle handle)
        {
            DispatchUnsafe(shader, kernelIndex, handle.Buffer);
        }

        public void DispatchPathEdgesSafe(ComputeShader shader, int kernelIndex, PathsHandle handle)
        {
            DispatchSafe(shader, kernelIndex, handle.Edges);
        }

        public void DispatchPathVerticesSafe(ComputeShader shader, int kernelIndex, PathsHandle handle)
        {
            DispatchSafe(shader, kernelIndex, handle.Vertices);
        }

        public void DispatchPathEdgesUnsafe(ComputeShader shader, int kernelIndex, PathsHandle handle)
        {
            DispatchUnsafe(shader, kernelIndex, handle.Edges);
        }

        public void DispatchPathVerticesUnafe(ComputeShader shader, int kernelIndex, PathsHandle handle)
        {
            DispatchUnsafe(shader, kernelIndex, handle.Vertices);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, MapHandle handle)
        {
            DispatchSafe(shader, kernelIndex, handle.Texture);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, MapHandle handle)
        {
            DispatchUnsafe(shader, kernelIndex, handle.Texture);
        }

        public void SetSeedParam(ComputeShader shader, string name, uint value)
        {
            var combinedSeed = hash(new uint2(value, GlobalSeed));
            _commandBuffer.SetFloatParam(shader, name, combinedSeed);
        }

        public void SetFloatParam(ComputeShader shader, string name, float value)
        {
            _commandBuffer.SetFloatParam(shader, name, value);
        }

        public void SetVectorParam(ComputeShader shader, string name, float4 value)
        {
            _commandBuffer.SetVectorParam(shader, name, value);
        }

        public void SetVectorParam(ComputeShader shader, string name, float2 value)
        {
            _commandBuffer.SetVectorParam(shader, name, new float4(value, 1.0f / value));
        }

        public void SetBufferParam(ComputeShader shader, int kernelIndex, string name, BufferHandle buffer)
        {
            _commandBuffer.SetFloatParam(shader, $"{name}Length", buffer.Length);
            _commandBuffer.SetBufferParam(shader, kernelIndex, name, buffer);
        }

        public void SetTextureParam(ComputeShader shader, int kernelIndex, string name, TextureHandle texture)
        {
            _commandBuffer.SetVectorParam(shader, $"{name}Resolution", TextureUtility.GetSizeVector(texture.Width, texture.Height));
            _commandBuffer.SetTextureParam(shader, kernelIndex, name, texture);
        }

        public void SetTextureParam(ComputeShader shader, int kernelIndex, string name, Texture texture)
        {
            _commandBuffer.SetVectorParam(shader, $"{name}Resolution", TextureUtility.GetSizeVector(texture));
            _commandBuffer.SetTextureParam(shader, kernelIndex, name, texture);
        }

        public void SetTextureParam(ComputeShader shader, int kernelIndex, string name, MapHandle map)
        {
            SetTextureParam(shader, kernelIndex, name, map.Texture);
        }

        public void SetObjectsParam(ComputeShader shader, int kernelIndex, string name, ObjectsHandle objects)
        {
            SetBufferParam(shader, kernelIndex, name, objects.Buffer);
        }

        public void SetPathsParam(ComputeShader shader, int kernelIndex, string name, PathsHandle paths)
        {
            SetBufferParam(shader, kernelIndex, $"{name}Edges", paths.Edges);
            SetBufferParam(shader, kernelIndex, $"{name}Vertices", paths.Vertices);
        }

        public void SetMinMaxCurveParam(ComputeShader shader, int kernelID, string name, MinMaxCurve curve)
        {
            var curveValues = GetCurveValues(curve);
            var curveID = CreateBuffer(curveValues);
            SetBufferParam(shader, kernelID, name, curveID);
        }

        public void SetMinMaxCurveParam(ComputeShader shader, int kernelID, string name, MinMaxCurve[] curves)
        {
            var curveValues = GetCurveValues(curves);
            var curveID = CreateBuffer(curveValues);
            SetBufferParam(shader, kernelID, name, curveID);
        }

        public void SetCurveParam(ComputeShader shader, int kernelID, string name, float min, float max, AnimationCurve curve)
        {
            var curveValues = GetCurveValues(min, max, curve);
            var curveID = CreateBuffer(curveValues);
            SetBufferParam(shader, kernelID, name, curveID);
        }

        public void SetAdjacencyListParam(ComputeShader shader, int kernelID, string name, PathsAdjacencyListHandle adjacencyList)
        {
            SetBufferParam(shader, kernelID, $"{name}AdjacencyEdges", adjacencyList.Edges);
            SetBufferParam(shader, kernelID, $"{name}AdjacencyIndexList", adjacencyList.IndexList);
        }

        public void SetObjectCollisionsParam(ComputeShader shader, int kernelID, ObjectCollisionsHandle collision, ObjectCollisionControls controls)
        {
            var cellCountSqrt = collision.CellCountSqrt;
            var cellSize = 1.0f / cellCountSqrt;
            SetFloatParam(shader, "_CollisionRadiusInfluence", controls.RadiusInfluence);
            SetFloatParam(shader, "_CollisionScaleInflucence", controls.ScaleInfluence);
            SetFloatParam(shader, "_CollisionCellSize", cellSize);
            SetVectorParam(shader, "_CollisionGridSize", new float2(cellCountSqrt, cellCountSqrt));
            SetObjectsParam(shader, kernelID, "_CollisionObjectsInput", collision.Objects);
            SetBufferParam(shader, kernelID, "_CollisionCollidersInput", collision.Colliders);
            SetBufferParam(shader, kernelID, "_CollisionIDListInput", collision.IDList);
            SetBufferParam(shader, kernelID, "_CollisionIndexListInput", collision.IndexList);
        }

        public PathsHandle SortPaths(PathsHandle paths)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CreateAdjacencyList");
            var sortKernel = shader.FindKernel("CSSortEdges");
            var sort1024Kernel = shader.FindKernel("CSSortEdges1024");

            // Create edge buffers
            // Edge count must be divisible by 1024 as that's the minimum number of elements needed by the bitonic sort kernel
            // Also it must be a power of two for bitonic sort to work
            var edgeCount = max(Mathf.NextPowerOfTwo(paths.Edges.Length), 1024);
            var edges1 = CreatePathEdges(edgeCount);
            var edges2 = CreatePathEdges(edgeCount);
            CopyPathEdges(paths.Edges, edges1);
            var buffer1 = CreatePaths(paths.Vertices, edges1);
            var buffer2 = CreatePaths(paths.Vertices, edges2);

            // Sort the edges
            for (uint k = 2; k <= paths.Edges.Length; k *= 2)
            {
                SetFloatParam(shader, "_SortK", k);

                uint j = k >> 1;
                for (; j > 512; j >>= 1)
                {
                    SetFloatParam(shader, "_SortJ", j);
                    SetPathsParam(shader, sortKernel, "_Input", buffer1);
                    SetPathsParam(shader, sortKernel, "_Output", buffer2);
                    DispatchPathEdgesSafe(shader, sortKernel, buffer2);

                    var temp = buffer2;
                    buffer2 = buffer1;
                    buffer1 = temp;
                }

                // For smaller sub lists (<= 1024) we can use a faster kernel that uses groupshared memory for speed up
                if (j > 0)
                {
                    SetFloatParam(shader, "_SortJ", j);
                    SetPathsParam(shader, sort1024Kernel, "_Input", buffer1);
                    SetPathsParam(shader, sort1024Kernel, "_Output", buffer2);
                    DispatchPathEdgesUnsafe(shader, sort1024Kernel, buffer2);

                    var temp = buffer2;
                    buffer2 = buffer1;
                    buffer1 = temp;
                }
            }

            return buffer1;
        }

        public PathsHandle SortAndRemoveDuplicatePaths(PathsHandle paths)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CreateAdjacencyList");
            var removeDuplicatesKernel = shader.FindKernel("CSRemoveDuplicates");

            // Sort the paths as the duplicate removal requires it
            var sortedPaths = SortPaths(paths);
            var edges = CreatePathEdges(sortedPaths.Edges.Length);
            var buffer = CreatePaths(sortedPaths.Vertices, edges);

            // Remove duplicate edges
            SetPathsParam(shader, removeDuplicatesKernel, "_Input", sortedPaths);
            SetPathsParam(shader, removeDuplicatesKernel, "_Output", buffer);
            DispatchPathEdgesSafe(shader, removeDuplicatesKernel, buffer);

            return SortPaths(buffer);
        }

        public PathsAdjacencyListHandle CreateAdjacencyList(PathsHandle paths)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/CreateAdjacencyList");
            var mirrorKernel = shader.FindKernel("CSMirrorEdges");
            var adjacencyListKernel = shader.FindKernel("CSCreateAdjacencyList");

            // Mirror the edges
            var mirroredEdges = CreatePathEdges(2 * paths.Edges.Length);
            var mirroredPaths = CreatePaths(paths.Vertices, mirroredEdges);
            SetPathsParam(shader, mirrorKernel, "_Input", paths);
            SetPathsParam(shader, mirrorKernel, "_Output", mirroredPaths);
            DispatchPathEdgesSafe(shader, mirrorKernel, paths);

            var sortedPaths = SortAndRemoveDuplicatePaths(mirroredPaths);

            // // Create adjacency list
            var adjacencyIndexList = CreateBuffer(paths.Vertices.Length, 2 * sizeof(uint));
            SetPathsParam(shader, adjacencyListKernel, "_Input", sortedPaths);
            SetBufferParam(shader, adjacencyListKernel, "_OutputAdjacencyIndexList", adjacencyIndexList);
            DispatchSafe(shader, adjacencyListKernel, adjacencyIndexList);

            return new PathsAdjacencyListHandle(sortedPaths.Edges, adjacencyIndexList);
        }

        public ObjectCollisionsHandle CreateObjectCollisions(ObjectsHandle objects, ObjectCollisionControls controls)
        {
            var shader = UnityEngine.Resources.Load<ComputeShader>("GPU/Collision/ObjectCollisionGrid");
            var initCellsKernel = shader.FindKernel("CSInitializeCells");
            var initCollidersKernel = shader.FindKernel("CSInitializeColliders");
            var findKernel = shader.FindKernel("CSFindCellIDs");
            var sortKernel = shader.FindKernel("CSSortCellIDs");
            var sort1024Kernel = shader.FindKernel("CSSortCellIDs1024");
            var indexKernel = shader.FindKernel("CSIndexCellIDs");

            // Calculate collision grid size
            var scaleCoefficient = lerp(1, objects.MaxScale, controls.ScaleInfluence);
            var maxRadius = max(objects.MaxRadius * controls.RadiusInfluence * scaleCoefficient, 0.1);

            var worldSize = max(TerrainSize.x, TerrainSize.z);
            var maxCellCountSqrt = sqrt(objects.Buffer.Length);
            var cellCountSqrt = (int)clamp(floor(worldSize / (2 * maxRadius)), 2, maxCellCountSqrt);
            var cellSize = 1.0f / cellCountSqrt;

            // Create collision grid data structures
            // ID list must be divisible by 1024 as that's the minimum number of elements needed by the bitonic sort kernel
            // Also it must be a power of two for bitonic sort to work
            var idListLength = max(Mathf.NextPowerOfTwo(4 * objects.Buffer.Length), 1024);
            var cellCount = cellCountSqrt * cellCountSqrt;
            var idList1 = CreateBuffer(idListLength, 2 * sizeof(uint));
            var idList2 = CreateBuffer(idListLength, 2 * sizeof(uint));
            var indexList = CreateBuffer(cellCount, 2 * sizeof(uint));

            // Initialize parameters
            SetFloatParam(shader, "_CollisionIDListLength", idListLength);
            SetFloatParam(shader, "_CollisionRadiusInfluence", controls.RadiusInfluence);
            SetFloatParam(shader, "_CollisionScaleInflucence", controls.ScaleInfluence);
            SetFloatParam(shader, "_CollisionCellSize", cellSize);
            SetFloatParam(shader, "_CollisionCellSizeInv", 1.0f / cellSize);
            SetVectorParam(shader, "_CollisionGridSize", new float2(cellCountSqrt, cellCountSqrt));

            // Initialize ID list to invalid IDs
            SetBufferParam(shader, initCellsKernel, "_CollisionIDListOutput", idList1);
            DispatchSafe(shader, initCellsKernel, idList1);

            // Create colliders.
            var colliders = CreateBuffer(objects.Buffer.Length, 4 * sizeof(float));
            SetObjectsParam(shader, initCollidersKernel, "_CollisionObjectsInput", objects);
            SetBufferParam(shader, initCollidersKernel, "_CollisionCollidersOutput", colliders);
            DispatchSafe(shader, initCollidersKernel, objects);

            // Create object/cell ID pairs for each object/cell intersection
            SetBufferParam(shader, findKernel, "_CollisionCollidersInput", colliders);
            SetBufferParam(shader, findKernel, "_CollisionIDListOutput", idList1);
            DispatchSafe(shader, findKernel, objects);

            // Sort the object/cell ID pairs so that cell IDs are in increasing order
            for (uint k = 2; k <= idListLength; k *= 2)
            {
                SetFloatParam(shader, "_SortK", k);

                uint j = k >> 1;
                for (; j > 512; j >>= 1)
                {
                    SetFloatParam(shader, "_SortJ", j);
                    SetBufferParam(shader, sortKernel, "_CollisionIDListInput", idList1);
                    SetBufferParam(shader, sortKernel, "_CollisionIDListOutput", idList2);
                    DispatchSafe(shader, sortKernel, idList2);

                    var temp = idList2;
                    idList2 = idList1;
                    idList1 = temp;
                }

                // For smaller sub lists (<= 1024) we can use a faster kernel that uses groupshared memory for speed up
                if (j > 0)
                {
                    SetFloatParam(shader, "_SortJ", j);
                    SetBufferParam(shader, sort1024Kernel, "_CollisionIDListInput", idList1);
                    SetBufferParam(shader, sort1024Kernel, "_CollisionIDListOutput", idList2);
                    DispatchSafe(shader, sort1024Kernel, idList2);

                    var temp = idList2;
                    idList2 = idList1;
                    idList1 = temp;
                }
            }

            // Find the start and end indices for the consecutive cell IDs
            SetBufferParam(shader, indexKernel, "_CollisionIDListInput", idList1);
            SetBufferParam(shader, indexKernel, "_CollisionIndexListOutput", indexList);
            DispatchUnsafe(shader, indexKernel, indexList);

            return new ObjectCollisionsHandle(objects, colliders, idList1, indexList, cellCountSqrt);
        }

        private void EvaluateNode(NodeData node)
        {
            if (node is IGeneratorNode generatorNode && generatorNode.CanBuild())
            {
                _dependencyGraph.SetActiveNode(ActiveGenerator, node);
                generatorNode.Build(this);

                // Build preview if needed
                if (_buildPreview && node is IPreviewNode previewNode && previewNode.HasPreview)
                {
                    var previewTexture = _previewTexturePool.GetTexture(previewNode);
                    ClearTexture(previewTexture, new Color(0.013f, 0.013f, 0.015f, 1.0f));
                    previewNode.BuildPreview(this, previewTexture);
                }

                _dependencyGraph.PropagateDependencies();
                _dependencyGraph.ReleaseActiveNodeDependencies(_commandBuffer);
            }
        }

        private TerrainGeneratorBuilderResources GetActiveResources()
        {
            if (_activeSubGenerator == null)
            {
                return Resources;
            }

            if (!_subGeneratorResources.TryGetValue(_activeSubGenerator, out TerrainGeneratorBuilderResources resources))
            {
                resources = new TerrainGeneratorBuilderResources();
                _subGeneratorResources.Add(_activeSubGenerator, resources);
            }
            return resources;
        }

        private int GetCurveLength(AnimationCurve curve)
        {
            return (curve == null || curve.keys.Length < 2) ? 0 : curve.keys.Length - 1;
        }

        private float[] GetCurveValues(float min, float max, AnimationCurve curve)
        {
            var offset = 1;
            var output = new float[offset + 3 + 5 * GetCurveLength(curve)];
            output[0] = offset;
            GetCurveValues(min, max, curve, offset, output);
            return output;
        }

        private float[] GetCurveValues(MinMaxCurve curve)
        {
            var offset = 1;
            var output = new float[offset + 3 + 5 * GetCurveLength(curve.Curve)];
            output[0] = offset;
            GetCurveValues(curve, offset, output);
            return output;
        }

        private float[] GetCurveValues(MinMaxCurve[] curves)
        {
            var outputLength = curves.Length;
            foreach (var curve in curves)
            {
                outputLength += 3 + 5 * GetCurveLength(curve.Curve);
            }

            var output = new float[outputLength];
            var offset = curves.Length;
            for (var i = 0; i < curves.Length; ++i)
            {
                var curve = curves[i];
                output[i] = offset;
                GetCurveValues(curve, offset, output);
                offset += 3 + 5 * GetCurveLength(curve.Curve);
            }

            return output;
        }

        private void GetCurveValues(MinMaxCurve curve, int offset, float[] output)
        {
            switch (curve.Mode)
            {
                case MinMaxCurveMode.Constant:
                    GetCurveValues(curve.ConstantMax, curve.ConstantMax, null, offset, output);
                    break;
                case MinMaxCurveMode.TwoConstants:
                    GetCurveValues(curve.ConstantMin, curve.ConstantMax, null, offset, output);
                    break;
                case MinMaxCurveMode.Curve:
                    GetCurveValues(curve.ConstantMin, curve.ConstantMax, curve.Curve, offset, output);
                    break;
                default:
                    throw new InvalidEnumArgumentException(curve.Mode.ToString());
            }
        }

        private void GetCurveValues(float min, float max, AnimationCurve curve, int offset, float[] output)
        {
            var curveLength = GetCurveLength(curve);
            var curveSegmentsOffset = offset + 3;
            var curveKeysOffset = curveSegmentsOffset + curveLength;

            // The first section of the curve is constants
            output[offset] = curveLength;
            output[offset + 1] = min;
            output[offset + 2] = max;

            // The second section of the curve buffer is the segment lengths
            for (var i = 0; i < curveLength; ++i)
            {
                output[curveSegmentsOffset + i] = clamp(curve.keys[i + 1].time, 0, 1);
            }

            // The third section of the curve buffer is the keys for each segment
            for (var i = 0; i < curveLength; ++i)
            {
                var key1 = curve.keys[i];
                var key2 = curve.keys[i + 1];
                var p0 = key1.value;
                var p3 = key2.value;
                var delta = (key2.time - key1.time) / 3.0f;
                var p1 = p0 + delta * key1.outTangent;
                var p2 = p3 - delta * key2.inTangent;
                output[curveKeysOffset + 4 * i + 0] = clamp(p0, 0, 1);
                output[curveKeysOffset + 4 * i + 1] = clamp(p1, 0, 1);
                output[curveKeysOffset + 4 * i + 2] = clamp(p2, 0, 1);
                output[curveKeysOffset + 4 * i + 3] = clamp(p3, 0, 1);
            }
        }
    }
}
