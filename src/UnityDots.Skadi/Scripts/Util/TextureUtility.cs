using System;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace UnityDots.Skadi
{
    public static class TextureUtility
    {
        private static Texture2D _barrier;

        public const GraphicsFormat HeightmapFormat = GraphicsFormat.R16_SFloat;
        public const GraphicsFormat HeightmapFormat2 = GraphicsFormat.R16G16_SFloat;
        public const GraphicsFormat HeightmapFormat4 = GraphicsFormat.R16G16B16A16_SFloat;

        public const GraphicsFormat ColorFormat = GraphicsFormat.R8G8B8A8_UNorm;
        public const GraphicsFormat NormalMapFormat = GraphicsFormat.R8G8_UNorm;
        public const GraphicsFormat AlphamapFormat = GraphicsFormat.R8G8B8A8_UNorm;
        public const GraphicsFormat AlphamapIndexFormat = GraphicsFormat.R8G8B8A8_UNorm;

        public static bool MatchesResolution(Texture texture, int resolution)
        {
            return MatchesResolution(texture, resolution, resolution);
        }

        public static bool MatchesResolution(Texture texture, int width, int height)
        {
            return texture.width == width && texture.height == height;
        }

        public static float4 GetSizeVector(Texture texture)
        {
            return GetSizeVector(texture.width, texture.height);
        }

        public static float4 GetSizeVector(float2 resolution)
        {
            return GetSizeVector(resolution.x, resolution.y);
        }

        public static float4 GetSizeVector(float width, float height)
        {
            return new float4(width, height, 1.0f / width, 1.0f / height);
        }

        public static RenderTextureSwapper SwapActiveRenderTexture(RenderTexture texture)
        {
            return new RenderTextureSwapper(texture);
        }

        public static void ClearRenderTexture(RenderTexture texture, float value)
        {
            ClearRenderTexture(texture, new Color(value, value, value, value));
        }

        public static void ClearRenderTexture(RenderTexture texture, Color color)
        {
            var rt = RenderTexture.active;
            RenderTexture.active = texture;
            GL.Clear(true, true, color);
            RenderTexture.active = rt;
        }

        public static Texture2D CreateAlphamapIndexTexture(int width, int height)
        {
            return new Texture2D(width, height, TextureFormat.ARGB32, 1, true)
            {
                wrapMode = TextureWrapMode.Clamp,
                filterMode = FilterMode.Point,
                anisoLevel = 0
            };
        }

        public static RenderTextureDescriptor GetHeightmapTextureDescriptor(int width, int height)
        {
            return GetRenderTextureDescriptor(HeightmapFormat, width, height);
        }

        public static RenderTextureDescriptor GetColorTextureDescriptor(int width, int height)
        {
            var descriptor = GetRenderTextureDescriptor(ColorFormat, width, height);
            descriptor.sRGB = true;
            return descriptor;
        }

        public static RenderTextureDescriptor GetAlphamapTextureDescriptor(int width, int height)
        {
            return GetRenderTextureDescriptor(AlphamapFormat, width, height);
        }

        public static RenderTextureDescriptor GetAlphamapIndexTextureDescriptor(int width, int height)
        {
            return GetRenderTextureDescriptor(AlphamapIndexFormat, width, height);
        }

        public static RenderTextureDescriptor GetRenderTextureDescriptor(RenderTextureFormat format, int width, int height, bool useMipMap = false)
        {
            var descriptor = GetRenderTextureDescriptor(width, height, useMipMap);
            descriptor.colorFormat = format;
            return descriptor;
        }

        public static RenderTextureDescriptor GetRenderTextureDescriptor(GraphicsFormat format, int width, int height, bool useMipMap = false)
        {
            var descriptor = GetRenderTextureDescriptor(width, height, useMipMap);
            descriptor.graphicsFormat = format;
            return descriptor;
        }

        public static RenderTextureDescriptor GetRenderTextureDescriptor(int width, int height, bool useMipMap)
        {
            return new RenderTextureDescriptor
            {
                width = width,
                height = height,
                autoGenerateMips = useMipMap,
                dimension = TextureDimension.Tex2D,
                enableRandomWrite = true,
                useDynamicScale = false,
                memoryless = RenderTextureMemoryless.None,
                sRGB = false,
                useMipMap = useMipMap,
                depthBufferBits = 0,
                volumeDepth = 1,
                msaaSamples = 1
            };
        }

        public static void WaitForGPU()
        {
            if (_barrier == null)
            {
                _barrier = new Texture2D(1, 1, TextureFormat.ARGB32, 0, true);
            }
            _barrier.ReadPixels(new Rect(0, 0, 1, 1), 0, 0, false);
        }

        public static NativeArray<float> ReadTextureAsFloats(RenderTexture texture)
        {
            var result = new NativeArray<float>();
            ReadTextureAsFloatsAsync(texture, (res) => result = res).WaitForCompletion();
            return result;
        }

        public static AsyncGPUReadbackRequest ReadTextureAsFloatsAsync(RenderTexture texture, Action<NativeArray<float>> callback)
        {
            switch (texture.graphicsFormat)
            {
                case GraphicsFormat.R8G8_UNorm:
                    return ReadToCPU_R8G8_UNorm(texture, callback);
                case GraphicsFormat.R16_SFloat:
                    return ReadToCPU_R16_SFloat(texture, callback);
                case GraphicsFormat.R32_SFloat:
                    return ReadToCPU_R32_SFloat(texture, callback);
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(texture.graphicsFormat.ToString());
            }
        }

        public static NativeArray<byte> ReadTextureAsBytes(RenderTexture texture)
        {
            var result = new NativeArray<byte>();
            ReadTextureAsBytesAsync(texture, (res) => result = res).WaitForCompletion();
            return result;
        }

        public static AsyncGPUReadbackRequest ReadTextureAsBytesAsync(RenderTexture texture, Action<NativeArray<byte>> callback)
        {
            return AsyncGPUReadback.Request(texture, 0, (request) =>
            {
                var data = request.GetData<byte>();
                var result = new NativeArray<byte>(data.Length, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
                result.CopyFrom(data);

                callback(result);
            });
        }

        private static AsyncGPUReadbackRequest ReadToCPU_R8G8_UNorm(RenderTexture texture, Action<NativeArray<float>> callback)
        {
            return AsyncGPUReadback.Request(texture, 0, (request) =>
            {
                var data = request.GetData<byte>();
                var result = new NativeArray<float>(data.Length / 2, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
                CopyR8G8ToFloat(data, result);

                callback(result);
            });
        }

        private static AsyncGPUReadbackRequest ReadToCPU_R16_SFloat(RenderTexture texture, Action<NativeArray<float>> callback)
        {
            return AsyncGPUReadback.Request(texture, 0, GraphicsFormat.R32_SFloat, (request) =>
             {
                 var data = request.GetData<float>();
                 var result = new NativeArray<float>(data.Length, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
                 result.CopyFrom(data);

                 callback(result);
             });
        }

        private static AsyncGPUReadbackRequest ReadToCPU_R32_SFloat(RenderTexture texture, Action<NativeArray<float>> callback)
        {
            return AsyncGPUReadback.Request(texture, 0, (request) =>
            {
                var data = request.GetData<float>();
                var result = new NativeArray<float>(data.Length, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
                result.CopyFrom(data);

                callback(result);
            });
        }

        private static void CopyR8G8ToFloat(NativeArray<byte> src, NativeArray<float> dst)
        {
            for (var i = 0; i < src.Length; i += 2)
            {
                var r = src[i];
                var g = src[i + 1];
                var height = (r / 256.0f + g) / 256.0f;
                dst[i / 2] = height;
            }
        }
    }
}
