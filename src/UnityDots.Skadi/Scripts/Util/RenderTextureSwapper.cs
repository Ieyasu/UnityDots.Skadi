using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    public struct RenderTextureSwapper : IDisposable
    {
        private RenderTexture _prev;

        public RenderTextureSwapper(RenderTexture texture)
        {
            _prev = RenderTexture.active;
            RenderTexture.active = texture;
        }

        public void Dispose()
        {
            RenderTexture.active = _prev;
        }
    }
}
