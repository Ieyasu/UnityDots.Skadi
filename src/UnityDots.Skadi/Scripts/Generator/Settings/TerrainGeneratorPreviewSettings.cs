using System;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class TerrainGeneratorPreviewSettings
    {
        [SerializeField]
        private TerrainPreviewRenderMode _renderMode = TerrainPreviewRenderMode.Height;
        public TerrainPreviewRenderMode RenderMode
        {
            get => _renderMode;
            set => _renderMode = value;
        }

        [SerializeField]
        private bool _show = true;
        public bool Show
        {
            get => _show;
            set => _show = value;
        }

        [SerializeField]
        private bool _autoRefresh = true;
        public bool AutoRefresh
        {
            get => _autoRefresh;
            set => _autoRefresh = value;
        }

        [SerializeField]
        private int2 _tiling = new int2(1, 1);
        public int2 Tiling
        {
            get => _tiling;
            set => _tiling = value;
        }
    }
}
