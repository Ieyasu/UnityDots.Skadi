using System;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class TerrainGeneratorSettings
    {
        [SerializeField]
        private uint _seed = 1;
        public uint Seed
        {
            get => _seed;
            set => _seed = value;
        }

        [SerializeField]
        private int _mapResolution = 512;
        public int MapResolution
        {
            get => _mapResolution;
            set => _mapResolution = clamp(Mathf.NextPowerOfTwo(value), 32, 4096);
        }

        [SerializeField]
        private float3 _terrainSize = new float3(512, 128, 512);
        public float3 TerrainSize
        {
            get => _terrainSize;
            set => _terrainSize = clamp(value, 0, 100000);
        }

        [SerializeField]
        private TerrainGeneratorPreviewSettings _preview;
        public TerrainGeneratorPreviewSettings Preview
        {
            get => _preview;
            set => _preview = value;
        }
    }
}
