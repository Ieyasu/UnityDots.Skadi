namespace UnityDots.Skadi
{
    public enum TerrainPreviewRenderMode
    {
        Height,
        StandardShader,
        SkadiShader
    }
}
