using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class SerializableCommandBuffer
    {
        [SerializeField]
        private int _resourceCounter;
        internal int ResourceCounter => _resourceCounter;

        [SerializeField]
        private List<CommandPointer> _commandPointers;
        internal List<CommandPointer> CommandPointers => _commandPointers;

        [SerializeField]
        private List<DispatchCommand> _dispatchCommands;
        internal List<DispatchCommand> DispatchCommands => _dispatchCommands;

        [SerializeField]
        private List<ReleaseResourceCommand> _releaseResourceCommands;
        internal List<ReleaseResourceCommand> ReleaseResourceCommands => _releaseResourceCommands;

        [SerializeField]
        private List<CreateBufferCommand> _createBufferCommands;
        internal List<CreateBufferCommand> CreateBufferCommands => _createBufferCommands;

        [SerializeField]
        private List<CreateTextureCommand> _createTextureCommands;
        internal List<CreateTextureCommand> CreateTextureCommands => _createTextureCommands;

        [SerializeField]
        private List<ClearBufferCommand> _clearBufferCommands;
        internal List<ClearBufferCommand> ClearBufferCommands => _clearBufferCommands;

        [SerializeField]
        private List<ClearTextureCommand> _clearTextureCommands;
        internal List<ClearTextureCommand> ClearTextureCommands => _clearTextureCommands;

        [SerializeField]
        private List<SetFloatCommand> _setFloatCommands;
        internal List<SetFloatCommand> SetFloatCommands => _setFloatCommands;

        [SerializeField]
        private List<SetVectorCommand> _setVectorCommands;
        internal List<SetVectorCommand> SetVectorCommands => _setVectorCommands;

        [SerializeField]
        private List<SetBufferCommand> _setBufferCommands;
        internal List<SetBufferCommand> SetBufferCommands => _setBufferCommands;

        [SerializeField]
        private List<SetTextureCommand> _setTextureCommands;
        internal List<SetTextureCommand> SetTextureCommands => _setTextureCommands;

        [SerializeField]
        private List<SetFloatCommand> _setGlobalFloatCommands;
        internal List<SetFloatCommand> SetGlobalFloatCommands => _setGlobalFloatCommands;

        [SerializeField]
        private List<SetVectorCommand> _setGlobalVectorCommands;
        internal List<SetVectorCommand> SetGlobalVectorCommands => _setGlobalVectorCommands;

        public SerializableCommandBuffer()
        {
            _resourceCounter = 0;
            _commandPointers = new List<CommandPointer>();
            _dispatchCommands = new List<DispatchCommand>();
            _releaseResourceCommands = new List<ReleaseResourceCommand>();

            _createBufferCommands = new List<CreateBufferCommand>();
            _createTextureCommands = new List<CreateTextureCommand>();
            _clearBufferCommands = new List<ClearBufferCommand>();
            _clearTextureCommands = new List<ClearTextureCommand>();

            _setFloatCommands = new List<SetFloatCommand>();
            _setVectorCommands = new List<SetVectorCommand>();
            _setBufferCommands = new List<SetBufferCommand>();
            _setTextureCommands = new List<SetTextureCommand>();
            _setGlobalFloatCommands = new List<SetFloatCommand>();
            _setGlobalVectorCommands = new List<SetVectorCommand>();
        }

        public SerializableCommandBuffer(SerializableCommandBuffer other)
            : this()
        {
            _resourceCounter = other._resourceCounter;
            _commandPointers.AddRange(other._commandPointers);
            _dispatchCommands.AddRange(other._dispatchCommands);
            _releaseResourceCommands.AddRange(other._releaseResourceCommands);

            _createBufferCommands.AddRange(other._createBufferCommands);
            _createTextureCommands.AddRange(other._createTextureCommands);
            _clearBufferCommands.AddRange(other._clearBufferCommands);
            _clearTextureCommands.AddRange(other._clearTextureCommands);

            _setFloatCommands.AddRange(other._setFloatCommands);
            _setVectorCommands.AddRange(other._setVectorCommands);
            _setBufferCommands.AddRange(other._setBufferCommands);
            _setTextureCommands.AddRange(other._setTextureCommands);
            _setGlobalFloatCommands.AddRange(other._setGlobalFloatCommands);
            _setGlobalVectorCommands.AddRange(other._setGlobalVectorCommands);
        }

        public void Clear()
        {
            _resourceCounter = 0;
            _commandPointers.Clear();
            _dispatchCommands.Clear();
            _releaseResourceCommands.Clear();

            _createBufferCommands.Clear();
            _createTextureCommands.Clear();
            _clearBufferCommands.Clear();
            _clearTextureCommands.Clear();

            _setFloatCommands.Clear();
            _setVectorCommands.Clear();
            _setBufferCommands.Clear();
            _setTextureCommands.Clear();
            _setGlobalFloatCommands.Clear();
            _setGlobalVectorCommands.Clear();
        }

        public bool SequenceEquals(SerializableCommandBuffer other)
        {
            return _resourceCounter == other._resourceCounter
                    && _commandPointers.SequenceEquals(other._commandPointers)
                    && _dispatchCommands.SequenceEquals(other._dispatchCommands)
                    && _releaseResourceCommands.SequenceEquals(other._releaseResourceCommands)
                    && _createBufferCommands.SequenceEquals(other._createBufferCommands)
                    && _createTextureCommands.SequenceEquals(other._createTextureCommands)
                    && _clearBufferCommands.SequenceEquals(other._clearBufferCommands)
                    && _clearTextureCommands.SequenceEquals(other._clearTextureCommands)
                    && _setFloatCommands.SequenceEquals(other._setFloatCommands)
                    && _setVectorCommands.SequenceEquals(other._setVectorCommands)
                    && _setBufferCommands.SequenceEquals(other._setBufferCommands)
                    && _setTextureCommands.SequenceEquals(other._setTextureCommands)
                    && _setGlobalFloatCommands.SequenceEquals(other._setGlobalFloatCommands)
                    && _setGlobalVectorCommands.SequenceEquals(other._setGlobalVectorCommands);
        }

        public BufferHandle CreateBuffer(int count, int stride, ComputeBufferType type = ComputeBufferType.Default)
        {
            var command = new CreateBufferCommand(GetNextResourceID(), count, stride, type);
            return CreateBuffer(command);
        }

        public BufferHandle CreateBuffer(float[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var command = new CreateBufferCommand(GetNextResourceID(), values, type);
            return CreateBuffer(command);
        }

        public BufferHandle CreateBuffer(float2[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var command = new CreateBufferCommand(GetNextResourceID(), values, type);
            return CreateBuffer(command);
        }

        public BufferHandle CreateBuffer(float3[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var command = new CreateBufferCommand(GetNextResourceID(), values, type);
            return CreateBuffer(command);
        }

        public BufferHandle CreateBuffer(float4[] values, ComputeBufferType type = ComputeBufferType.Default)
        {
            var command = new CreateBufferCommand(GetNextResourceID(), values, type);
            return CreateBuffer(command);
        }

        public TextureHandle CreateTexture(GraphicsFormat format, int width, int height)
        {
            var command = new CreateTextureCommand(GetNextResourceID(), format, width, height);
            return CreateTexture(command);
        }

        public void ClearBuffer(BufferHandle buffer, float value)
        {
            var command = new ClearBufferCommand(buffer.ResourceID, value);
            AddCommand(CommandType.ClearBuffer, _clearBufferCommands, command);
        }

        public void ClearTexture(TextureHandle texture, Color value)
        {
            var command = new ClearTextureCommand(texture.ResourceID, value);
            AddCommand(CommandType.ClearTexture, _clearTextureCommands, command);
        }

        public void ClearTexture(RenderTexture texture, Color value)
        {
            var command = new ClearTextureCommand(texture, value);
            AddCommand(CommandType.ClearTexture, _clearTextureCommands, command);
        }

        public void DispatchUnsafe(ComputeShader shader, int kernelIndex, int width, int height, int depth)
        {
            var dispatch = new DispatchCommand(shader, kernelIndex, width, height, depth, false);
            var commandIndex = new CommandPointer(CommandType.Dispatch, _dispatchCommands.Count);
            _commandPointers.Add(commandIndex);
            _dispatchCommands.Add(dispatch);
        }

        public void DispatchSafe(ComputeShader shader, int kernelIndex, int width, int height, int depth)
        {
            var dispatch = new DispatchCommand(shader, kernelIndex, width, height, depth, true);
            var commandIndex = new CommandPointer(CommandType.Dispatch, _dispatchCommands.Count);
            _commandPointers.Add(commandIndex);
            _dispatchCommands.Add(dispatch);
        }

        public void ReleaseBuffer(BufferHandle handle)
        {
            var command = new ReleaseResourceCommand(handle.ResourceID, ResourceCommandType.Buffer);
            AddCommand(CommandType.ReleaseResource, _releaseResourceCommands, command);
        }

        public void ReleaseTexture(TextureHandle handle)
        {
            var command = new ReleaseResourceCommand(handle.ResourceID, ResourceCommandType.Texture);
            AddCommand(CommandType.ReleaseResource, _releaseResourceCommands, command);
        }

        public void SetFloatParam(ComputeShader shader, string name, float value)
        {
            ValidateSetParam(shader, name);
            var command = new SetFloatCommand(shader, name, value);
            AddCommand(CommandType.SetFloat, _setFloatCommands, command);
        }

        public void SetGlobalFloatParam(string name, float value)
        {
            var command = new SetFloatCommand(name, value);
            _setGlobalFloatCommands.Add(command);
        }

        public void SetVectorParam(ComputeShader shader, string name, float4 value)
        {
            ValidateSetParam(shader, name);
            var command = new SetVectorCommand(shader, name, value);
            AddCommand(CommandType.SetVector, _setVectorCommands, command);
        }

        public void SetGlobalVectorParam(string name, float4 value)
        {
            var command = new SetVectorCommand(name, value);
            _setGlobalVectorCommands.Add(command);
        }

        public void SetBufferParam(ComputeShader shader, int kernelIndex, string name, BufferHandle buffer)
        {
            ValidateSetParam(shader, name);
            var command = new SetBufferCommand(shader, kernelIndex, name, buffer.ResourceID);
            AddCommand(CommandType.SetBuffer, _setBufferCommands, command);
        }

        public void SetTextureParam(ComputeShader shader, int kernelIndex, string name, TextureHandle texture)
        {
            ValidateSetParam(shader, name);
            var command = new SetTextureCommand(shader, kernelIndex, name, texture.ResourceID);
            AddCommand(CommandType.SetTexture, _setTextureCommands, command);
        }

        public void SetTextureParam(ComputeShader shader, int kernelIndex, string name, Texture texture)
        {
            ValidateSetParam(shader, name);
            var command = new SetTextureCommand(shader, kernelIndex, name, texture);
            AddCommand(CommandType.SetTexture, _setTextureCommands, command);
        }

        private BufferHandle CreateBuffer(CreateBufferCommand command)
        {
            AddCommand(CommandType.CreateBuffer, _createBufferCommands, command);
            return new BufferHandle(command.ResourceID, command.Length);
        }

        private TextureHandle CreateTexture(CreateTextureCommand command)
        {
            AddCommand(CommandType.CreateTexture, _createTextureCommands, command);
            return new TextureHandle(command.ResourceID, command.Format, command.Width, command.Height);
        }

        private void AddCommand<T>(CommandType commandType, List<T> commands, T command)
        {
            var commandPointer = new CommandPointer(commandType, commands.Count);
            _commandPointers.Add(commandPointer);
            commands.Add(command);
        }

        private int GetNextResourceID()
        {
            return _resourceCounter++;
        }

        private void ValidateSetParam(ComputeShader shader, string name)
        {
            if (shader == null)
            {
                throw new ArgumentException("Shader is null");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException($"Name {0} is invalid");
            }
        }
    }
}
