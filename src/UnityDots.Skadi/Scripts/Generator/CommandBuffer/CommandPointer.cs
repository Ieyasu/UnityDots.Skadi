using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal enum CommandType
    {
        Dispatch,
        ReleaseResource,
        CreateBuffer,
        ClearBuffer,
        CreateTexture,
        ClearTexture,
        SetFloat,
        SetVector,
        SetBuffer,
        SetTexture,
    }

    [Serializable]
    internal struct CommandPointer : IEquatable<CommandPointer>
    {
        [SerializeField]
        private CommandType _commandType;
        public CommandType CommandType => _commandType;

        [SerializeField]
        private int _commandIndex;
        public int CommandIndex => _commandIndex;

        public CommandPointer(CommandType type, int index)
        {
            _commandType = type;
            _commandIndex = index;
        }

        public override string ToString()
        {
            return $"CommandPointer({_commandType}, {_commandIndex})";
        }

        public override bool Equals(object obj)
        {
            return obj is CommandPointer other && Equals(other);
        }

        public bool Equals(CommandPointer other)
        {
            return CommandType == other.CommandType
                   && CommandIndex == other.CommandIndex;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CommandType, CommandIndex);
        }
    }
}
