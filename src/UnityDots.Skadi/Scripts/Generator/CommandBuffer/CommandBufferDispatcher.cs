using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi
{
    /// <summary>
    /// ComputeDispatcher handles dispatching a generated ComputeShader.
    ///
    /// The dispatcher does the following:
    /// 1. Creates resources needed by the ComputeShader (such as ComputeBuffers and RenderTextures)
    /// 2. Sets variables for the ComputeShader kernels
    /// 3. Dispatches the ComputeShader kernels
    /// </summary>
    internal sealed class CommandBufferDispatcher
    {
        private readonly ComputeShader _clearBufferShader;

        public ComputeResourcePool Resources { get; }

        public CommandBufferDispatcher()
        {
            Resources = new ComputeResourcePool();
            _clearBufferShader = UnityEngine.Resources.Load<ComputeShader>("GPU/Util/ClearBuffer");
        }

        ~CommandBufferDispatcher()
        {
            TryRelease();
        }

        public void Release()
        {
            Resources.Release();
        }

        public void Execute(SerializableCommandBuffer commandBuffer)
        {
            TryRelease();

            Execute(commandBuffer, 0, commandBuffer.CommandPointers.Count);
        }

        public IEnumerator ExecuteAsync(SerializableCommandBuffer commandBuffer)
        {
            TryRelease();

            var dispatchCommandsPerFrame = 20;
            for (var startCommand = 0; startCommand < commandBuffer.CommandPointers.Count;)
            {
                var dispatchCommands = 0;
                for (var endCommand = startCommand; endCommand < commandBuffer.CommandPointers.Count; ++endCommand)
                {
                    if (commandBuffer.CommandPointers[endCommand].CommandType == CommandType.Dispatch)
                    {
                        ++dispatchCommands;
                    }

                    if (dispatchCommands >= dispatchCommandsPerFrame || endCommand >= commandBuffer.CommandPointers.Count - 1)
                    {
                        Execute(commandBuffer, startCommand, endCommand + 1);
                        startCommand = endCommand + 1;
                        break;
                    }
                }
                yield return null;
            }
        }

        public ComputeBuffer FindBufferWithID(int resourceID)
        {
            for (var i = Resources.Buffers.Count - 1; i >= 0; --i)
            {
                var resource = Resources.Buffers[i];
                if (resource.ResourceID == resourceID)
                {
                    return resource.Buffer;
                }
            }
            return null;
        }

        public RenderTexture FindTextureWithID(int resourceID)
        {
            for (var i = Resources.Textures.Count - 1; i >= 0; --i)
            {
                var resource = Resources.Textures[i];
                if (resource.ResourceID == resourceID)
                {
                    return resource.Texture;
                }
            }
            return null;
        }

        private void TryRelease()
        {
            if (Resources.Buffers.Count > 0 || Resources.Textures.Count > 0)
            {
                Debug.LogWarning("CommandBufferDispatcher not released properly");
                Release();
            }
        }

        private void Execute(SerializableCommandBuffer commandBuffer, int startCommand, int endCommand)
        {
            var i = startCommand;
            try
            {
                for (; i < endCommand; ++i)
                {
                    ExecuteCommand(commandBuffer, commandBuffer.CommandPointers[i]);
                }
            }
            catch (Exception e)
            {
                // Make sure we don't leak if an exception is thrown
                Debug.LogError("Failed to execute compute shader asynchronously using command " + commandBuffer.CommandPointers[i]);
                Debug.LogException(e);
                Release();
            }
        }
        private void ExecuteCommand(SerializableCommandBuffer commandBuffer, CommandPointer pointer)
        {
            switch (pointer.CommandType)
            {
                case CommandType.ClearBuffer:
                    ExecuteClearBufferCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.ClearTexture:
                    ExecuteClearTextureCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.CreateBuffer:
                    ExecuteCreateBufferCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.CreateTexture:
                    ExecuteCreateTextureCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.Dispatch:
                    ExecuteDispatchCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.ReleaseResource:
                    ExecuteReleaseResourceCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.SetFloat:
                    ExecuteSetFloatCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.SetVector:
                    ExecuteSetVectorCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.SetBuffer:
                    ExecuteSetBufferCommand(commandBuffer, pointer.CommandIndex);
                    break;
                case CommandType.SetTexture:
                    ExecuteSetTextureCommand(commandBuffer, pointer.CommandIndex);
                    break;
                default:
                    throw new InvalidEnumArgumentException(pointer.CommandType.ToString());
            }
        }

        private void ExecuteClearBufferCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.ClearBufferCommands[commandIndex];
            var buffer = Resources.GetBuffer(command.ResourceID).Buffer;
            var bufferLength = buffer.count * (buffer.stride / 4);
            _clearBufferShader.SetFloat("_ClearValue", command.ClearValue);
            _clearBufferShader.SetFloat("_BufferLength", bufferLength);
            _clearBufferShader.SetBuffer(0, "_Buffer", buffer);
            _clearBufferShader.DispatchUnsafe(0, bufferLength, 1, 1);
        }

        private void ExecuteClearTextureCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.ClearTextureCommands[commandIndex];
            var texture = command.Texture ?? Resources.GetTexture(command.ResourceID).Texture;
            TextureUtility.ClearRenderTexture(texture, command.ClearValue);
        }

        private void ExecuteCreateBufferCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.CreateBufferCommands[commandIndex];
            Resources.CreateBuffer(command.ResourceID, command.Length, command.Stride, command.BufferType, command.Array);
        }

        private void ExecuteCreateTextureCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.CreateTextureCommands[commandIndex];
            Resources.CreateTexture(command.ResourceID, command.Format, command.Width, command.Height);
        }

        private void ExecuteDispatchCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.DispatchCommands[commandIndex];

            // Set global shader variables for the shader
            foreach (var globalCommand in commandBuffer.SetGlobalFloatCommands)
            {
                command.Shader.SetFloat(globalCommand.Name, globalCommand.Value);
            }
            foreach (var globalCommand in commandBuffer.SetGlobalVectorCommands)
            {
                command.Shader.SetVector(globalCommand.Name, globalCommand.Value);
            }

            var dimensions = new int3(command.Width, command.Height, command.Depth);
            if (command.Safe)
            {
                command.Shader.DispatchSafe(command.KernelIndex, dimensions.x, dimensions.y, dimensions.z);
            }
            else
            {
                command.Shader.DispatchUnsafe(command.KernelIndex, dimensions.x, dimensions.y, dimensions.z);
            }
        }

        private void ExecuteReleaseResourceCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.ReleaseResourceCommands[commandIndex];
            switch (command.Type)
            {
                case ResourceCommandType.Buffer:
                    Resources.ReleaseBuffer(command.ResourceID);
                    break;
                case ResourceCommandType.Texture:
                    Resources.ReleaseTexture(command.ResourceID);
                    break;
                default:
                    throw new InvalidEnumArgumentException(command.Type.ToString());
            }
        }

        private void ExecuteSetFloatCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.SetFloatCommands[commandIndex];
            command.Shader.SetFloat(command.Name, command.Value);
        }

        private void ExecuteSetVectorCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.SetVectorCommands[commandIndex];
            command.Shader.SetVector(command.Name, command.Value);
        }

        private void ExecuteSetBufferCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.SetBufferCommands[commandIndex];
            var resource = Resources.GetBuffer(command.ResourceID).Buffer;
            command.Shader.SetBuffer(command.KernelIndex, command.Name, resource);
        }

        private void ExecuteSetTextureCommand(SerializableCommandBuffer commandBuffer, int commandIndex)
        {
            var command = commandBuffer.SetTextureCommands[commandIndex];
            switch (command.Type)
            {
                case SetTextureCommandType.ResourceID:
                    var resource = Resources.GetTexture(command.ResourceID).Texture;
                    command.Shader.SetTexture(command.KernelIndex, command.Name, resource);
                    break;
                case SetTextureCommandType.Texture:
                    command.Shader.SetTexture(command.KernelIndex, command.Name, command.Texture);
                    break;
                default:
                    throw new InvalidEnumArgumentException(command.Type.ToString());
            }
        }
    }
}
