using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    internal sealed class ComputeResourcePool
    {
        public List<RenderTextureResource> Textures { get; }
        public List<ComputeBufferResource> Buffers { get; }

        public ComputeResourcePool()
        {
            Buffers = new List<ComputeBufferResource>();
            Textures = new List<RenderTextureResource>();
        }

        public void Release()
        {
            foreach (var resource in Buffers)
            {
                if (resource.Buffer != null)
                {
                    resource.Buffer.Dispose();
                }
            }
            Buffers.Clear();

            foreach (var resource in Textures)
            {
                if (resource.Texture != null)
                {
                    RenderTexture.ReleaseTemporary(resource.Texture);
                }
            }
            Textures.Clear();
        }

        public ComputeBufferResource ClaimBuffer(int resourceID)
        {
            var resource = GetBuffer(resourceID);
            Buffers.Remove(resource);
            return resource;
        }

        public RenderTextureResource ClaimTexture(int resourceID)
        {
            var resource = GetTexture(resourceID);
            Textures.Remove(resource);
            return resource;
        }

        public ComputeBuffer CreateBuffer(int resourceID, int count, int stride, ComputeBufferType type, float[] data)
        {
            var buffer = new ComputeBuffer(count, stride, type);
            if ((type & ComputeBufferType.Append) != 0 || (type & ComputeBufferType.Counter) != 0)
            {
                buffer.SetCounterValue(0);
            }
            if (data != null)
            {
                buffer.SetData(data);
            }
            Buffers.Add(new ComputeBufferResource(buffer, resourceID));
            return buffer;
        }

        public Texture CreateTexture(int resourceID, GraphicsFormat format, int width, int height)
        {
            var descriptor = TextureUtility.GetRenderTextureDescriptor(format, width, height);
            var texture = RenderTexture.GetTemporary(descriptor);
            texture.Create();
            Textures.Add(new RenderTextureResource(texture, resourceID));
            return texture;
        }

        public void ReleaseBuffer(int resourceID)
        {
            var resource = GetBuffer(resourceID);
            Buffers.Remove(resource);
            resource.Buffer.Dispose();
        }

        public void ReleaseTexture(int resourceID)
        {
            var resource = GetTexture(resourceID);
            Textures.Remove(resource);
            RenderTexture.ReleaseTemporary(resource.Texture);
        }

        public ComputeBufferResource GetBuffer(int resourceID)
        {
            foreach (var resource in Buffers)
            {
                if (resource.ResourceID == resourceID)
                {
                    return resource;
                }
            }

            throw new ArgumentException($"No buffer with resource ID {resourceID}");
        }

        public RenderTextureResource GetTexture(int resourceID)
        {
            foreach (var resource in Textures)
            {
                if (resource.ResourceID == resourceID)
                {
                    return resource;
                }
            }

            throw new ArgumentException($"No texture with resource ID {resourceID}");
        }
    }
}
