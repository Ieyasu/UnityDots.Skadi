using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal struct RenderTextureResource : IEquatable<RenderTextureResource>
    {
        public RenderTexture Texture { get; }
        public int ResourceID { get; }

        public RenderTextureResource(RenderTexture texture, int resourceID)
        {
            Texture = texture;
            ResourceID = resourceID;
        }

        public override bool Equals(object obj)
        {
            return obj is RenderTextureResource other && Equals(other);
        }

        public bool Equals(RenderTextureResource other)
        {
            return EqualityComparer<RenderTexture>.Default.Equals(Texture, other.Texture)
                   && ResourceID == other.ResourceID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Texture, ResourceID);
        }
    }
}
