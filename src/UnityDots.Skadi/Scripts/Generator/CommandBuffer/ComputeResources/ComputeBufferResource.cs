using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    internal struct ComputeBufferResource : IEquatable<ComputeBufferResource>
    {
        public ComputeBuffer Buffer { get; }
        public int ResourceID { get; }

        public ComputeBufferResource(ComputeBuffer buffer, int resourceID)
        {
            Buffer = buffer;
            ResourceID = resourceID;
        }

        public override bool Equals(object obj)
        {
            return obj is ComputeBufferResource other && Equals(other);
        }

        public bool Equals(ComputeBufferResource other)
        {
            return EqualityComparer<ComputeBuffer>.Default.Equals(Buffer, other.Buffer)
                   && ResourceID == other.ResourceID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Buffer, ResourceID);
        }
    }
}
