using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct CreateBufferCommand : IEquatable<CreateBufferCommand>
    {
        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        [SerializeField]
        private float[] _array;
        public float[] Array => _array;

        [SerializeField]
        private int _size;
        public int Length => _size;

        [SerializeField]
        private int _stride;
        public int Stride => _stride;

        [SerializeField]
        private ComputeBufferType _bufferType;
        public ComputeBufferType BufferType => _bufferType;

        public CreateBufferCommand(int resourceID, int count, int stride, ComputeBufferType bufferType)
        {
            _resourceID = resourceID;
            _array = null;
            _size = count;
            _stride = stride;
            _bufferType = bufferType;
        }

        public CreateBufferCommand(int resourceID, float[] array, ComputeBufferType bufferType)
        {
            _resourceID = resourceID;
            _array = array;
            _size = _array.Length;
            _stride = 4;
            _bufferType = bufferType;
        }

        public CreateBufferCommand(int resourceID, float2[] array, ComputeBufferType bufferType)
        {
            _resourceID = resourceID;
            _array = new float[2 * array.Length];
            _size = _array.Length;
            _stride = 4;
            _bufferType = bufferType;

            for (var i = 0; i < array.Length; ++i)
            {
                _array[2 * i] = array[i].x;
                _array[2 * i + 1] = array[i].y;
            }
        }

        public CreateBufferCommand(int resourceID, float3[] array, ComputeBufferType bufferType)
        {
            _resourceID = resourceID;
            _array = new float[3 * array.Length];
            _size = _array.Length;
            _stride = 4;
            _bufferType = bufferType;

            for (var i = 0; i < array.Length; ++i)
            {
                _array[3 * i] = array[i].x;
                _array[3 * i + 1] = array[i].y;
                _array[3 * i + 2] = array[i].z;
            }
        }

        public CreateBufferCommand(int resourceID, float4[] array, ComputeBufferType bufferType)
        {
            _resourceID = resourceID;
            _array = new float[4 * array.Length];
            _size = _array.Length;
            _stride = 4;
            _bufferType = bufferType;

            for (var i = 0; i < array.Length; ++i)
            {
                _array[4 * i] = array[i].x;
                _array[4 * i + 1] = array[i].y;
                _array[4 * i + 2] = array[i].z;
                _array[4 * i + 3] = array[i].w;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is CreateBufferCommand other && Equals(other);
        }

        public bool Equals(CreateBufferCommand other)
        {
            return ResourceID == other.ResourceID
                   && Length == other.Length
                   && Stride == other.Stride
                   && EqualityComparer<ComputeBufferType>.Default.Equals(BufferType, other.BufferType)
                   && ArrayExtensions.SequenceEquals(Array, other.Array);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ResourceID, Array, Length, Stride, BufferType);
        }
    }
}
