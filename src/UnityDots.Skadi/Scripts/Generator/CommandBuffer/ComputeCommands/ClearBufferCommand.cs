using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct ClearBufferCommand : IEquatable<ClearBufferCommand>
    {
        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        [SerializeField]
        private float _clearValue;
        public float ClearValue => _clearValue;

        public ClearBufferCommand(int resourceID, float clearValue)
        {
            _resourceID = resourceID;
            _clearValue = clearValue;
        }

        public override bool Equals(object obj)
        {
            return obj is ClearBufferCommand other && Equals(other);
        }

        public bool Equals(ClearBufferCommand other)
        {
            return ResourceID == other.ResourceID
                   && ClearValue == other.ClearValue;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ResourceID, ClearValue);
        }
    }
}
