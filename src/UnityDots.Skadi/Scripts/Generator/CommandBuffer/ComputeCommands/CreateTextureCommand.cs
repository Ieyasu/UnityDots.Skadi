using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct CreateTextureCommand : IEquatable<CreateTextureCommand>
    {
        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        [SerializeField]
        private GraphicsFormat _format;
        public GraphicsFormat Format => _format;

        [SerializeField]
        private int _width;
        public int Width => _width;

        [SerializeField]
        private int _height;
        public int Height => _height;

        public CreateTextureCommand(int resourceID, GraphicsFormat format, int width, int height)
        {
            _resourceID = resourceID;
            _format = format;
            _width = width;
            _height = height;
        }

        public override bool Equals(object obj)
        {
            return obj is CreateTextureCommand other && Equals(other);
        }

        public bool Equals(CreateTextureCommand other)
        {
            return ResourceID == other.ResourceID
                   && EqualityComparer<GraphicsFormat>.Default.Equals(Format, other.Format)
                   && Width == other.Width
                   && Height == other.Height;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ResourceID, Format, Width, Height);
        }
    }
}
