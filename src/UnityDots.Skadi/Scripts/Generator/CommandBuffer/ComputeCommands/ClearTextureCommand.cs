using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct ClearTextureCommand : IEquatable<ClearTextureCommand>
    {
        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        [SerializeField]
        private RenderTexture _texture;
        public RenderTexture Texture => _texture;

        [SerializeField]
        private Color _clearValue;
        public Color ClearValue => _clearValue;

        public ClearTextureCommand(int resourceID, Color clearValue)
        {
            _texture = null;
            _resourceID = resourceID;
            _clearValue = clearValue;
        }

        public ClearTextureCommand(RenderTexture texture, Color clearValue)
        {
            _texture = texture;
            _resourceID = 0;
            _clearValue = clearValue;
        }

        public override bool Equals(object obj)
        {
            return obj is ClearTextureCommand other && Equals(other);
        }

        public bool Equals(ClearTextureCommand other)
        {
            return ResourceID == other.ResourceID
                   && EqualityComparer<RenderTexture>.Default.Equals(Texture, other.Texture)
                   && EqualityComparer<Color>.Default.Equals(ClearValue, other.ClearValue);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ResourceID, Texture, ClearValue);
        }
    }
}
