using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct SetBufferCommand : IEquatable<SetBufferCommand>
    {
        [SerializeField]
        private ComputeShader _shader;
        public ComputeShader Shader => _shader;

        [SerializeField]
        private int _kernelIndex;
        public int KernelIndex => _kernelIndex;

        [SerializeField]
        private string _name;
        public string Name => _name;

        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        public SetBufferCommand(ComputeShader shader, int kernelIndex, string name, int resourceID)
        {
            _shader = shader;
            _kernelIndex = kernelIndex;
            _name = name;
            _resourceID = resourceID;
        }

        public override bool Equals(object obj)
        {
            return obj is SetBufferCommand other && Equals(other);
        }

        public bool Equals(SetBufferCommand other)
        {
            return EqualityComparer<ComputeShader>.Default.Equals(Shader, other.Shader)
                   && KernelIndex == other.KernelIndex
                   && Name == other.Name
                   && ResourceID == other.ResourceID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Shader, KernelIndex, Name, ResourceID);
        }
    }
}
