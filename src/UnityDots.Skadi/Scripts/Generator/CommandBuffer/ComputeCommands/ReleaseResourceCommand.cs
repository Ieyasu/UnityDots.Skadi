using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    public enum ResourceCommandType
    {
        Buffer = 0,
        Texture = 1,
    }

    [Serializable]
    internal struct ReleaseResourceCommand : IEquatable<ReleaseResourceCommand>
    {
        [SerializeField]
        private ResourceCommandType _type;
        public ResourceCommandType Type => _type;

        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        public ReleaseResourceCommand(int resourceID, ResourceCommandType type)
        {
            _resourceID = resourceID;
            _type = type;
        }

        public override bool Equals(object obj)
        {
            return obj is ReleaseResourceCommand other && Equals(other);
        }

        public bool Equals(ReleaseResourceCommand other)
        {
            return Type == other.Type && ResourceID == other.ResourceID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Type, ResourceID);
        }
    }
}
