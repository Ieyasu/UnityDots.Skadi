using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    public enum SetTextureCommandType
    {
        ResourceID = 0,
        Texture = 1,
    }

    [Serializable]
    internal struct SetTextureCommand : IEquatable<SetTextureCommand>
    {
        [SerializeField]
        private SetTextureCommandType _type;
        public SetTextureCommandType Type => _type;

        [SerializeField]
        private ComputeShader _shader;
        public ComputeShader Shader => _shader;

        [SerializeField]
        private int _kernelIndex;
        public int KernelIndex => _kernelIndex;

        [SerializeField]
        private string _name;
        public string Name => _name;

        [SerializeField]
        private Texture _texture;
        public Texture Texture => _texture;

        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        public SetTextureCommand(ComputeShader shader, int kernelIndex, string name, int resourceID)
        {
            _type = SetTextureCommandType.ResourceID;
            _shader = shader;
            _kernelIndex = kernelIndex;
            _name = name;
            _resourceID = resourceID;
            _texture = null;
        }

        public SetTextureCommand(ComputeShader shader, int kernelIndex, string name, Texture texture)
        {
            _type = SetTextureCommandType.Texture;
            _shader = shader;
            _kernelIndex = kernelIndex;
            _name = name;
            _resourceID = 0;
            _texture = texture;
        }

        public override bool Equals(object obj)
        {
            return obj is SetTextureCommand other && Equals(other);
        }

        public bool Equals(SetTextureCommand other)
        {
            return Type == other.Type
                    && EqualityComparer<ComputeShader>.Default.Equals(Shader, other.Shader)
                    && KernelIndex == other.KernelIndex
                    && Name == other.Name
                    && EqualityComparer<Texture>.Default.Equals(Texture, other.Texture)
                    && ResourceID == other.ResourceID;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Type, Shader, KernelIndex, Name, Texture, ResourceID);
        }
    }
}
