using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct SetFloatCommand : IEquatable<SetFloatCommand>
    {
        [SerializeField]
        private ComputeShader _shader;
        public ComputeShader Shader => _shader;

        [SerializeField]
        private string _name;
        public string Name => _name;

        [SerializeField]
        private float _value;
        public float Value => _value;

        public bool IsGlobal => _shader == null;

        public SetFloatCommand(string name, float value)
            : this(null, name, value)
        {
        }

        public SetFloatCommand(ComputeShader shader, string name, float value)
        {
            _shader = shader;
            _name = name;
            _value = value;
        }

        public override bool Equals(object obj)
        {
            return obj is SetFloatCommand other && Equals(other);
        }

        public bool Equals(SetFloatCommand other)
        {
            return EqualityComparer<ComputeShader>.Default.Equals(Shader, other.Shader)
                   && Name == other.Name
                   && Value == other.Value
                   && IsGlobal == other.IsGlobal;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Shader, Name, Value, IsGlobal);
        }
    }
}
