using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct SetVectorCommand : IEquatable<SetVectorCommand>
    {
        [SerializeField]
        private ComputeShader _shader;
        public ComputeShader Shader => _shader;

        [SerializeField]
        private string _name;
        public string Name => _name;

        [SerializeField]
        private float4 _value;
        public float4 Value => _value;

        public bool IsGlobal => _shader == null;

        public SetVectorCommand(string name, float4 value)
            : this(null, name, value)
        {
        }

        public SetVectorCommand(ComputeShader shader, string name, float4 value)
        {
            _shader = shader;
            _name = name;
            _value = value;
        }

        public override bool Equals(object obj)
        {
            return obj is SetVectorCommand other && Equals(other);
        }

        public bool Equals(SetVectorCommand other)
        {
            return EqualityComparer<ComputeShader>.Default.Equals(Shader, other.Shader)
                   && Name == other.Name
                   && EqualityComparer<float4>.Default.Equals(Value, other.Value)
                   && IsGlobal == other.IsGlobal;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Shader, Name, Value, IsGlobal);
        }
    }
}
