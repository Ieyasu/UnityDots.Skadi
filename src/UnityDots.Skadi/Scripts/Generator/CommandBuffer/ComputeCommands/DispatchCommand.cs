using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    internal struct DispatchCommand : IEquatable<DispatchCommand>
    {
        [SerializeField]
        private ComputeShader _shader;
        public ComputeShader Shader => _shader;

        [SerializeField]
        private int _kernelIndex;
        public int KernelIndex => _kernelIndex;

        [SerializeField]
        private int _width;
        public int Width => _width;

        [SerializeField]
        private int _height;
        public int Height => _height;

        [SerializeField]
        private int _depth;
        public int Depth => _depth;

        [SerializeField]
        private bool _safe;
        public bool Safe => _safe;

        public DispatchCommand(ComputeShader shader, int kernelIndex, int width, int height, int depth, bool safe)
        {
            _shader = shader;
            _kernelIndex = kernelIndex;
            _width = width;
            _height = height;
            _depth = depth;
            _safe = safe;
        }

        public override bool Equals(object obj)
        {
            return obj is DispatchCommand other && Equals(other);
        }

        public bool Equals(DispatchCommand other)
        {
            return EqualityComparer<ComputeShader>.Default.Equals(Shader, other.Shader)
                   && KernelIndex == other.KernelIndex
                   && Width == other.Width
                   && Height == other.Height
                   && Depth == other.Depth
                   && Safe == other.Safe;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Shader, KernelIndex, Width, Height, Depth, Safe);
        }
    }
}
