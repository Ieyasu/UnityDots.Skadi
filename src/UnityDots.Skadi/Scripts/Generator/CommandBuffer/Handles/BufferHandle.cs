using System;
using UnityEngine;

namespace UnityDots.Skadi
{
    [System.Serializable]
    public struct BufferHandle : IResourceHandle, IEquatable<BufferHandle>
    {
        [SerializeField]
        private int _resourceID;
        public int ResourceID => _resourceID;

        [SerializeField]
        private int _length;
        public int Length => _length;

        public BufferHandle(int resourceID, int length)
        {
            _resourceID = resourceID;
            _length = length;
        }

        public override bool Equals(object obj)
        {
            return obj is BufferHandle other && Equals(other);
        }

        public bool Equals(BufferHandle other)
        {
            return ResourceID == other.ResourceID && Length == other.Length;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ResourceID, Length);
        }
    }
}
