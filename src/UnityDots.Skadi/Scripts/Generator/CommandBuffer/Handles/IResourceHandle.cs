namespace UnityDots.Skadi
{
    public interface IResourceHandle
    {
        int ResourceID { get; }
    }
}
