using System;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    public sealed class RawTerrainData : IDisposable
    {
        public bool IsDisposed { get; private set; }
        public float3 TerrainSize { get; internal set; }
        public int AlphamapResolution { get; internal set; }
        public int DetailmapResolution { get; internal set; }
        public int HeightmapResolution { get; internal set; }
        public int HolesTextureResolution => HeightmapResolution;

        public int AlphamapCount => Alphamaps.Length;
        public int DetailmapCount => Detailmaps.Length;
        public int HeightmapCount => Heightmaps.Length;
        public int HolesTexturesCount => HolesTextures.Length;
        public int WatermapCount => Watermaps.Length;
        public int ObjectBufferCount => ObjectBuffers.Length;
        public int DetailPrototypeCount => DetailPrototypes.Length;
        public int ObjectPrototypeCount => ObjectPrototypes.Length;
        public int UnityTerrainLayerCount => UnityTerrainLayers.Length;
        public int SkadiTerrainLayerCount => SkadiTerrainLayers.Length;

        internal DetailPrototype[] DetailPrototypes { get; set; }
        internal ObjectPrototype[] ObjectPrototypes { get; set; }
        internal TerrainLayer[] UnityTerrainLayers { get; set; }
        internal SkadiTerrainLayer[] SkadiTerrainLayers { get; set; }

        internal RenderTexture[] Alphamaps { get; set; }
        internal RenderTexture[] Detailmaps { get; set; }
        internal RenderTexture[] Heightmaps { get; set; }
        internal RenderTexture[] Watermaps { get; set; }
        internal RenderTexture[] HolesTextures { get; set; }
        internal ComputeBuffer[] ObjectBuffers { get; set; }

        ~RawTerrainData()
        {
            if (!IsDisposed)
            {
                Debug.LogWarning("RawTerrainData not disposed properly");
                Dispose();
            }
        }

        public void Dispose()
        {
            DisposeMaps(Alphamaps);
            DisposeMaps(Detailmaps);
            DisposeMaps(Heightmaps);
            DisposeMaps(HolesTextures);
            DisposeMaps(Watermaps);
            DisposeBuffers(ObjectBuffers);

            Alphamaps = null;
            Detailmaps = null;
            Heightmaps = null;
            HolesTextures = null;
            Watermaps = null;
            ObjectBuffers = null;
            DetailPrototypes = null;
            ObjectPrototypes = null;
            UnityTerrainLayers = null;
            SkadiTerrainLayers = null;

            IsDisposed = true;
        }

        public RenderTexture GetAlphamap(int index)
        {
            return Alphamaps[index];
        }

        public RenderTexture GetDetailmap(int index)
        {
            return Detailmaps[index];
        }

        public RenderTexture GetHeightmap(int index)
        {
            return Heightmaps[index];
        }

        public RenderTexture GetHolesTextures(int index)
        {
            return HolesTextures[index];
        }

        public RenderTexture GetWatermap(int index)
        {
            return Watermaps[index];
        }

        public ComputeBuffer GetObjectBuffer(int index)
        {
            return ObjectBuffers[index];
        }

        private void DisposeMaps(RenderTexture[] maps)
        {
            if (maps == null)
            {
                return;
            }

            foreach (var map in maps)
            {
                map.Release();
            }
        }

        private void DisposeBuffers(ComputeBuffer[] buffers)
        {
            if (buffers == null)
            {
                return;
            }

            foreach (var buffer in buffers)
            {
                buffer.Dispose();
            }
        }
    }
}
