using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public struct RawObjectData : IEquatable<RawObjectData>
    {
        [SerializeField]
        private int _prototypeID;
        public int PrototypeID
        {
            get => _prototypeID;
            set => _prototypeID = value;
        }

        [SerializeField]
        private float3 _position;
        public float3 Position
        {
            get => _position;
            set => _position = value;
        }

        [SerializeField]
        private quaternion _rotation;
        public quaternion Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }

        [SerializeField]
        private float _scale;
        public float Scale
        {
            get => _scale;
            set => _scale = value;
        }

        public override bool Equals(object obj)
        {
            return obj is RawObjectData other && Equals(other);
        }

        public bool Equals(RawObjectData other)
        {
            return PrototypeID == other.PrototypeID
                    && EqualityComparer<float3>.Default.Equals(Position, other.Position)
                    && EqualityComparer<quaternion>.Default.Equals(Rotation, other.Rotation)
                    && Scale == other.Scale;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PrototypeID, Position, Rotation, Scale);
        }
    }
}
