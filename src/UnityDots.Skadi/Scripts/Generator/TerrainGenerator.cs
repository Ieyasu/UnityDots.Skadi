using System;
using System.Collections;
using System.Collections.Generic;
using UnityDots.Collections;
using UnityDots.Graph;
using UnityEngine;

namespace UnityDots.Skadi
{
    public sealed class TerrainGenerator : GraphData
    {
        private CommandBufferDispatcher _dispatcher;
        private CommandBufferDispatcher Dispatcher => _dispatcher ?? (_dispatcher = new CommandBufferDispatcher());

        [SerializeField]
        private TerrainGeneratorSettings _settings = null;
        public TerrainGeneratorSettings Settings => _settings;

        [SerializeField]
        private TerrainGeneratorResources _resources = null;
        public TerrainGeneratorResources Resources
        {
            get => _resources;
            set => _resources = value;
        }

        [SerializeField]
        private SerializableCommandBuffer _commandBuffer = null;
        public SerializableCommandBuffer CommandBuffer
        {
            get => _commandBuffer;
            set => _commandBuffer = value;
        }

        public void Generate()
        {
            Generate(CommandBuffer);
        }

        public IEnumerator GenerateAsync()
        {
            yield return GenerateAsync(CommandBuffer);
        }

        public void Generate(SerializableCommandBuffer commandBuffer)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Dispatcher.Execute(commandBuffer);
            // TextureUtility.WaitForGPU();
            Debug.Log($"Generate data in {watch.ElapsedMilliseconds} ms");
        }

        public IEnumerator GenerateAsync(SerializableCommandBuffer commandBuffer)
        {
            yield return Dispatcher.ExecuteAsync(commandBuffer);
        }

        public RawTerrainData GetRawData()
        {
            try
            {
                return GetRawDataInternal(Resources);
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to get terrain generator output");
                Debug.LogException(e);
                return new RawTerrainData();
            }
            finally
            {
                Dispatcher.Release();
            }
        }

        public RawTerrainData GetRawData(TerrainGeneratorResources resources)
        {
            try
            {
                return GetRawDataInternal(resources);
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to get terrain generator output");
                Debug.LogException(e);
                return new RawTerrainData();
            }
            finally
            {
                Dispatcher.Release();
            }
        }

        private RawTerrainData GetRawDataInternal(TerrainGeneratorResources resources)
        {
            var data = new RawTerrainData
            {
                TerrainSize = Settings.TerrainSize,
                AlphamapResolution = Settings.MapResolution,
                DetailmapResolution = Settings.MapResolution,
                HeightmapResolution = Settings.MapResolution + 1,

                DetailPrototypes = resources.DetailPrototypes.ToArray(),
                ObjectPrototypes = resources.ObjectPrototypes.ToArray(),
                UnityTerrainLayers = resources.UnityTerrainLayers.ToArray(),
                SkadiTerrainLayers = resources.SkadiTerrainLayers.ToArray(),

                Alphamaps = ClaimTextures(resources.Alphamaps),
                Detailmaps = ClaimTextures(resources.Detailmaps),
                Heightmaps = ClaimTextures(resources.Heightmaps),
                HolesTextures = ClaimTextures(resources.HolesTextures),
                Watermaps = ClaimTextures(resources.Watermaps),
                ObjectBuffers = ClaimBuffers(resources.ObjectBuffers),
            };

            return data;
        }

        private RenderTexture[] ClaimTextures(List<TextureHandle> handles)
        {
            var data = new RenderTexture[handles.Count];
            for (var i = 0; i < handles.Count; ++i)
            {
                if (data[i] != null)
                {
                    continue;
                }

                data[i] = Dispatcher.Resources.ClaimTexture(handles[i].ResourceID).Texture;

                // Handle multiple handles pointing to the same texture
                for (var j = i + 1; j < handles.Count; ++j)
                {
                    if (handles[i].ResourceID == handles[j].ResourceID)
                    {
                        data[j] = data[i];
                    }
                }
            }
            return data;
        }

        private ComputeBuffer[] ClaimBuffers(List<BufferHandle> handles)
        {
            var data = new ComputeBuffer[handles.Count];
            for (var i = 0; i < handles.Count; ++i)
            {
                if (data[i] != null)
                {
                    continue;
                }

                data[i] = Dispatcher.Resources.ClaimBuffer(handles[i].ResourceID).Buffer;

                // Handle multiple handles pointing to the same texture
                for (var j = i + 1; j < handles.Count; ++j)
                {
                    if (handles[i].ResourceID == handles[j].ResourceID)
                    {
                        data[j] = data[i];
                    }
                }
            }
            return data;
        }
    }
}
