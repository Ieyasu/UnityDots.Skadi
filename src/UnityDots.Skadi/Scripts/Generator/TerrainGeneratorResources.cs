using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Skadi
{
    [Serializable]
    public sealed class TerrainGeneratorResources
    {
        [SerializeField]
        private List<DetailPrototype> _detailPrototypes = null;
        public List<DetailPrototype> DetailPrototypes => _detailPrototypes;

        [SerializeField]
        private List<ObjectPrototype> _objectPrototypes = null;
        public List<ObjectPrototype> ObjectPrototypes => _objectPrototypes;

        [SerializeField]
        private List<TerrainLayer> _unityTerrainLayers = null;
        public List<TerrainLayer> UnityTerrainLayers => _unityTerrainLayers;

        [SerializeField]
        private List<SkadiTerrainLayer> _skadiTerrainLayers = null;
        public List<SkadiTerrainLayer> SkadiTerrainLayers => _skadiTerrainLayers;

        [SerializeField]
        private List<TextureHandle> _alphamaps = null;
        public List<TextureHandle> Alphamaps => _alphamaps;

        [SerializeField]
        private List<TextureHandle> _detailmaps = null;
        public List<TextureHandle> Detailmaps => _detailmaps;

        [SerializeField]
        private List<TextureHandle> _heightmaps = null;
        public List<TextureHandle> Heightmaps => _heightmaps;

        [SerializeField]
        private List<TextureHandle> _holesTextures = null;
        public List<TextureHandle> HolesTextures => _holesTextures;

        [SerializeField]
        private List<TextureHandle> _watermaps = null;
        public List<TextureHandle> Watermaps => _watermaps;

        [SerializeField]
        private List<BufferHandle> _objectBuffers = null;
        public List<BufferHandle> ObjectBuffers => _objectBuffers;

        public TerrainGeneratorResources()
        {
            _detailPrototypes = new List<DetailPrototype>();
            _objectPrototypes = new List<ObjectPrototype>();
            _unityTerrainLayers = new List<TerrainLayer>();
            _skadiTerrainLayers = new List<SkadiTerrainLayer>();

            _alphamaps = new List<TextureHandle>();
            _detailmaps = new List<TextureHandle>();
            _heightmaps = new List<TextureHandle>();
            _holesTextures = new List<TextureHandle>();
            _watermaps = new List<TextureHandle>();
            _objectBuffers = new List<BufferHandle>();
        }

        public TerrainGeneratorResources(TerrainGeneratorResources other)
            : this()
        {
            _detailPrototypes.AddRange(other._detailPrototypes);
            _objectPrototypes.AddRange(other._objectPrototypes);
            _unityTerrainLayers.AddRange(other._unityTerrainLayers);
            _skadiTerrainLayers.AddRange(other._skadiTerrainLayers);

            _alphamaps.AddRange(other._alphamaps);
            _detailmaps.AddRange(other._detailmaps);
            _heightmaps.AddRange(other._heightmaps);
            _holesTextures.AddRange(other._holesTextures);
            _watermaps.AddRange(other._watermaps);
            _objectBuffers.AddRange(other._objectBuffers);
        }

        public void Clear()
        {
            _detailPrototypes.Clear();
            _objectPrototypes.Clear();
            _unityTerrainLayers.Clear();
            _skadiTerrainLayers.Clear();

            _alphamaps.Clear();
            _detailmaps.Clear();
            _heightmaps.Clear();
            _holesTextures.Clear();
            _watermaps.Clear();
            _objectBuffers.Clear();
        }

        public bool SequenceEquals(TerrainGeneratorResources other)
        {
            return _detailPrototypes.SequenceEquals(other._detailPrototypes)
                    && _objectPrototypes.SequenceEquals(other._objectPrototypes)
                    && _unityTerrainLayers.SequenceEquals(other._unityTerrainLayers)
                    && _skadiTerrainLayers.SequenceEquals(other._skadiTerrainLayers)
                    && _alphamaps.SequenceEquals(other._alphamaps)
                    && _detailmaps.SequenceEquals(other._detailmaps)
                    && _heightmaps.SequenceEquals(other._heightmaps)
                    && _holesTextures.SequenceEquals(other._holesTextures)
                    && _watermaps.SequenceEquals(other._watermaps)
                    && _objectBuffers.SequenceEquals(other._objectBuffers);
        }
    }
}
