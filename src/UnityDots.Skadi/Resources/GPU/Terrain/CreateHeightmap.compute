#pragma kernel CS_R16 OUTPUT_R16
#pragma kernel CS_R8G8 OUTPUT_R8G8

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Common.hlsl"
#include "UnityCG.cginc"

float4 _OutputResolution;
Texture2D<float> _Input;

#ifdef OUTPUT_R16
    RWTexture2D<float> _Output;
#elif OUTPUT_R8G8
    RWTexture2D<float4> _Output;
#endif

void Main(uint2 id)
{
    if (id.x < (uint)_OutputResolution.x && id.y < (uint)_OutputResolution.y)
    {
        float2 uv = id.xy / (_OutputResolution.xy - 1);
        float height = saturate(_Input.SampleLevel(_SkadiLinearClamp, uv, 0));

        #ifdef OUTPUT_R16
            _Output[id.xy] = height;
        #elif OUTPUT_R8G8
            // TODO: Find out why we have to multiply with 0.5 ???
            _Output[id.xy] = PackHeightmap(0.5f * height);
        #endif
    }
}

SKADI_MAP_KERNEL(CS_R16)
{
    Main(id.xy);
}

SKADI_MAP_KERNEL(CS_R8G8)
{
    Main(id.xy);
}
