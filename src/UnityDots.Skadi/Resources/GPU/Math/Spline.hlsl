#ifndef SPLINE_HLSL_INCLUDED
#define SPLINE_HLSL_INCLUDED

float _SplineEvaluateSegment(float t, float p0, float p1, float p2, float p3)
{
    float tt = t * t;
    float tinv = 1 - t;
    float ttinv = tinv * tinv;
    return saturate((p0 * (ttinv * tinv)) +
            (p1 * (3.0f * ttinv * t)) +
            (p2 * (3.0f * tinv * tt)) +
            (p3 * (tt * t)));
}

float3 _SplineFindSegment(float t, uint curveStart, uint curveEnd, StructuredBuffer<float> curve)
{
    float minT = 0;
    float maxT = 0;
    for (uint i = curveStart; i < curveEnd; ++i)
    {
        minT = maxT;
        maxT = curve[i];

        if (t <= maxT)
        {
            break;
        }
    }
    return float3(min(i - curveStart, curveEnd - 1), minT, maxT);
}

float SplineMin(uint curveIndex, StructuredBuffer<float> curves)
{
    uint curveOffset = curves[curveIndex];
    return curves[curveOffset + 1];
}

float SplineMin(StructuredBuffer<float> curves)
{
    return SplineMin(0, curves);
}

float SplineMax(uint curveIndex, StructuredBuffer<float> curves)
{
    uint curveOffset = curves[curveIndex];
    return curves[curveOffset + 2];
}

float SplineMax(StructuredBuffer<float> curves)
{
    return SplineMax(0, curves);
}

float SplineEvaluate(float t, uint curveIndex, StructuredBuffer<float> curves)
{
    uint curveOffset = uint(curves[curveIndex]);
    uint curveLength = uint(curves[curveOffset]);
    float curveMin = curves[curveOffset + 1];
    float curveMax = curves[curveOffset + 2];

    if (curveLength == 0)
    {
        return lerp(curveMin, curveMax, t);
    }
    else
    {
        uint curveSegmentsOffset = curveOffset + 3;
        uint curveKeysOffset = curveSegmentsOffset + curveLength;
        float3 segment = _SplineFindSegment(t, curveSegmentsOffset, curveKeysOffset, curves);
        float scaledT = saturate((t - segment.y) / max(segment.z - segment.y, 0.00001f));
        float p0 = curves[curveKeysOffset + 4 * segment.x];
        float p1 = curves[curveKeysOffset + 4 * segment.x + 1];
        float p2 = curves[curveKeysOffset + 4 * segment.x + 2];
        float p3 = curves[curveKeysOffset + 4 * segment.x + 3];
        float value = _SplineEvaluateSegment(scaledT, p0, p1, p2, p3);
        return lerp(curveMin, curveMax, value);
    }
}

float SplineEvaluate(float t, StructuredBuffer<float> curve)
{
    return SplineEvaluate(t, 0, curve);
}

#endif //SPLINE_HLSL_INCLUDED
