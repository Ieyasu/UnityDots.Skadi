#ifndef MATH_HLSL_INCLUDED
#define MATH_HLSL_INCLUDED

#define PI 3.1415926538

float3 Forward()
{
    return float3(1, 0, 0);
}

float3 Right()
{
    return float3(0, 0, 1);
}

float3 Up()
{
    return float3(0, 1, 0);
}

float deg_to_rad(float x)
{
    return x * (PI / 180.0);
}

float rad_to_deg(float x)
{
    return x * (180.0 / PI);
}

float glsl_mod(float x, float y)
{
    return x - floor(x / y) * y;
}

float2 glsl_mod(float2 x, float y)
{
    return x - floor(x / y) * y;
}

float3 glsl_mod(float3 x, float y)
{
    return x - floor(x / y) * y;
}

float4 glsl_mod(float4 x, float y)
{
    return x - floor(x / y) * y;
}

float distance_sqr(float2 x, float2 y)
{
    float2 delta = x - y;
    return dot(delta, delta);
}

float distance_sqr(float3 x, float3 y)
{
    float3 delta = x - y;
    return dot(delta, delta);
}

float distance_sqr(float4 x, float4 y)
{
    float4 delta = x - y;
    return dot(delta, delta);
}


float3 mod7(float3 x)
{
    return x - floor(x * (1.0 / 7.0)) * 7.0;
}

float4 mod7(float4 x)
{
  return x - floor(x * (1.0 / 7.0)) * 7.0;
}

float mod289(float x)
{
    return x - floor(x / 289.0) * 289.0;
}

float2 mod289(float2 x)
{
    return x - floor(x / 289.0) * 289.0;
}

float3 mod289(float3 x)
{
    return x - floor(x / 289.0) * 289.0;
}

float4 mod289(float4 x)
{
    return x - floor(x / 289.0) * 289.0;
}

float permute(float x)
{
    return mod289((x * 34.0 + 1.0) * x);
}

float3 permute(float3 x)
{
    return mod289((x * 34.0 + 1.0) * x);
}

float4 permute(float4 x)
{
    return mod289((x * 34.0 + 1.0) * x);
}

float taylorInvSqrt(float r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}

float3 taylorInvSqrt(float3 r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}

float4 taylorInvSqrt(float4 r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}

#endif // MATH_HLSL_INCLUDED
