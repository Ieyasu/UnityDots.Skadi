#ifndef GEOMETRY_HLSL_INCLUDED
#define GEOMETRY_HLSL_INCLUDED

float PointToLineSegmentDistanceSqr(float2 p, float2 start, float2 end)
{
    float2 pa = p - start;
    float2 ba = end - start;
    float h = saturate(dot(pa, ba) / dot(ba, ba));
    float2 delta = pa - ba * h;
    return dot(delta, delta);
}

float PointToLineSegmentDistance(float2 p, float2 start, float2 end)
{
    return sqrt(PointToLineSegmentDistanceSqr(p, start, end));
}

float PointToAABBDistanceSqr(float2 p, float2 rectMin, float2 rectMax)
{
    float2 center = 0.5f * (rectMax + rectMin);
    float2 halfSize = 0.5f * (rectMax - rectMin);
    float2 delta = max(abs(center - p) - halfSize, 0);
    return dot(delta, delta);
}

float PointToAABBDistance(float2 p, float2 rectMin, float2 rectMax)
{
    return sqrt(PointToAABBDistanceSqr(p, rectMin, rectMax));
}

float2 ClosestPointOnLineSegment(float2 p, float2 start, float2 end, out float t)
{
    float2 delta = end - start;
    t = saturate(dot(p - start, delta) / dot(delta, delta));
    return start + t * delta;
}

float2 ClosestPointOnLineSegment(float2 p, float2 start, float2 end)
{
    float t;
    return ClosestPointOnLineSegment(p, start, end, t);
}

#endif // GEOMETRY_HLSL_INCLUDED
