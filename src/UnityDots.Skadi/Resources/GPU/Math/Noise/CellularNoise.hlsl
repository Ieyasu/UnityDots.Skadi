#ifndef CELLULAR_NOISE_HLSL_INCLUDED
#define CELLULAR_NOISE_HLSL_INCLUDED

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Random.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Noise/NoiseCommon.hlsl"

void CellularNoiseEuclideanSquared(float2 v, float jitter, out float2 cd, out float2 cp)
{
    const float K = 0.142857142857;
    const float Ko = 0.428571428571;
    const float3 oi = float3(-1.0, 0.0, 1.0);
    const float3 of = float3(-0.5, 0.5, 1.5);

    float2 pi = floor(v);
    float2 pf = frac(v);
    float3 px = permute(pi.x + oi);

    float3 p, ox, oy, dx, dy;
    cd = 1e6;
	cp = 0;

    [unroll]
    for (uint i = 0; i < 3; ++i)
    {
        p = permute(px[i] + pi.y + oi); // pi1, pi2, pi3
        ox = frac(p * K) - Ko;
        oy = mod7(floor(p * K)) * K - Ko;
        dx = pf.x - of[i] + jitter * ox;
        dy = pf.y - of + jitter * oy;

        float3 d = dx * dx + dy * dy;

        // Find the lowest and second lowest distances.
        [unroll]
        for(uint n = 0; n < 3; ++n)
        {
            if(d[n] < cd.x)
            {
                cd.y = cd.x;
                cd.x = d[n];
				cp = float2(ox[n], oy[n]);
            }
            else if(d[n] < cd.y)
            {
                cd.y = d[n];
            }
        }
    }
}

void CellularNoiseEuclidean(float2 v, float jitter, out float2 cd, out float2 cp)
{
	CellularNoiseEuclideanSquared(v, jitter, cd, cp);
    cd = sqrt(cd);
}

void CellularNoiseManhattan(float2 v, float jitter, out float2 cd, out float2 cp)
{
    const float K = 0.142857142857;
    const float Ko = 0.428571428571;
    const float3 oi = float3(-1.0, 0.0, 1.0);
    const float3 of = float3(-0.5, 0.5, 1.5);

    float2 pi = floor(v);
    float2 pf = frac(v);
    float3 px = permute(pi.x + oi);

    float3 p, ox, oy, dx, dy;
    cd = 1e6;
	cp = 0;

    [unroll]
    for (uint i = 0; i < 3; ++i)
    {
        p = permute(px[i] + pi.y + oi); // pi1, pi2, pi3
        ox = frac(p * K) - Ko;
        oy = mod7(floor(p * K)) * K - Ko;
        dx = pf.x - of[i] + jitter * ox;
        dy = pf.y - of + jitter * oy;

        float3 d = abs(dx) + abs(dy);

        // Find the lowest and second lowest distances.
        [unroll]
        for(uint n = 0; n < 3; ++n)
        {
            if(d[n] < cd.x)
            {
                cd.y = cd.x;
                cd.x = d[n];
				cp = float2(ox[n], oy[n]);
            }
            else if(d[n] < cd.y)
            {
                cd.y = d[n];
            }
        }
    }
}

#endif // CELLULAR_NOISE_HLSL_INCLUDED
