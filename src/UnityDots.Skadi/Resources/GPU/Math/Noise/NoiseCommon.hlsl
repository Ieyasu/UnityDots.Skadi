#ifndef NOISE_COMMON_HLSL_INCLUDED
#define NOISE_COMMON_HLSL_INCLUDED

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Math.hlsl"

float2 NoisePosition(float2 uv, float2 offset, float2 frequency, uint seed)
{
    return offset + frequency *  uv + float2(seed, seed / 256.0) % 256;
}

float3 NoisePosition(float3 uv, float3 offset, float3 frequency, uint seed)
{
    return offset + frequency * uv + float3(seed, seed, seed / 65536.0) % 256;
}

float4 NoisePosition(float4 uv, float4 offset, float4 frequency, uint seed)
{
    return offset + frequency * uv + float4(seed, seed / 256.0, seed / 65536.0, seed / 16777216.0) % 256;
}

#endif //NOISE_COMMON_HLSL_INCLUDED
