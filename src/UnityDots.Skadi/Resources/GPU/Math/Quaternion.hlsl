#ifndef QUATERNION_HLSL_INCLUDED
#define QUATERNION_HLSL_INCLUDED

// https://gist.github.com/mattatz/40a91588d5fb38240403f198a938a593

#define QUATERNION_IDENTITY float4(0, 0, 0, 1)

float4 q_normalize(float4 q)
{
    return q / length(q);
}

float4 q_mul(float4 q1, float4 q2)
{
    float4 q;
    q.xyz = q2.xyz * q1.w + q1.xyz * q2.w + cross(q1.xyz, q2.xyz);
    q.w = q1.w * q2.w - dot(q1.xyz, q2.xyz);
    return q_normalize(q);
}

float3 q_rotate(float4 q, float3 v)
{
    // float4 qc = q * float4(-1, -1, -1, 1);
    // return q_mul(q, q_mul(float4(v, 0), qc)).xyz;
    // float3 t = 2 * cross(q.xyz, v);
    // return v + q.w * t + cross(q.xyz, t);
    float4 _qv = float4(+ v.x *  q.w + v.y * -q.z - v.z * -q.y,
				        - v.x * -q.z + v.y *  q.w + v.z * -q.x,
				        + v.x * -q.y - v.y * -q.x + v.z *  q.w,
				        - v.x * -q.x - v.y * -q.y - v.z * -q.z );
	float3 r = float3(q.w * _qv.x + q.x * _qv.w + q.y * _qv.z - q.z * _qv.y,
				      q.w * _qv.y - q.x * _qv.z + q.y * _qv.w + q.z * _qv.x,
				      q.w * _qv.z + q.x * _qv.y - q.y * _qv.x + q.z * _qv.w);
	return r;
}

float4 q_from_angle_axis(float angle, float3 axis)
{
    float sn = sin(angle * 0.5);
    float cs = cos(angle * 0.5);
    return q_normalize(float4(axis * sn, cs));
}

float4 q_from_euler(float3 euler)
{
    // https://docs.unity3d.com/ScriptReference/Transform-eulerAngles.html
    // Unity euler angles are performed in z, x, y order
    float cy = cos(euler.z * 0.5);
    float sy = sin(euler.z * 0.5);
    float cp = cos(euler.x * 0.5);
    float sp = sin(euler.x * 0.5);
    float cr = cos(euler.y * 0.5);
    float sr = sin(euler.y * 0.5);

    float4 q;
    q.w = cr * cp * cy + sr * sp * sy;
    q.x = sr * cp * cy - cr * sp * sy;
    q.y = cr * sp * cy + sr * cp * sy;
    q.z = cr * cp * sy - sr * sp * cy;
    return q_normalize(q);
}

float4 q_slerp(float4 q1, float4 q2, float t)
{
    // Compute the cosine of the angle between the two vectors.
    float d = dot(q1, q2);

    // If the dot product is negative, slerp won't take
    // the shorter path. Note that q2 and -q2 are equivalent when
    // the negation is applied to all four components. Fix by
    // reversing one quaternion.
    if (d < 0.0f) {
        q2 = -q2;
        d = -d;
    }

    float4 result = 0;
    if (d > 0.9995) {
        // If the inputs are too close for comfort, linearly interpolate
        // and normalize the result.

        result = q1 + t * (q2 - q1);
    }
    else
    {
        // Since dot is in range [0, 0.9995], acos is safe
        float theta_0 = acos(d);        // theta_0 = angle between input vectors
        float theta = theta_0 * t;        // theta = angle between q1 and result
        float sin_theta = sin(theta);     // compute this value only once
        float sin_theta_0 = 1 / sin(theta_0); // compute this value only once

        float s0 = cos(theta) - d * sin_theta * sin_theta_0;  // == sin(theta_0 - theta) / sin(theta_0)
        float s1 = sin_theta * sin_theta_0;

        result = (s0 * q1) + (s1 * q2);
    }

    return q_normalize(result);
}

float4 q_look_at(float3 forward, float3 up)
{
    float3 right = normalize(cross(forward, up));
    up = normalize(cross(forward, right));

    float m00 = right.x;
    float m01 = right.y;
    float m02 = right.z;
    float m10 = up.x;
    float m11 = up.y;
    float m12 = up.z;
    float m20 = forward.x;
    float m21 = forward.y;
    float m22 = forward.z;

    float num8 = (m00 + m11) + m22;
    float4 q = QUATERNION_IDENTITY;

    if (num8 > 0.0)
    {
        float num = sqrt(num8 + 1.0);
        q.w = num * 0.5;
        num = 0.5 / num;
        q.x = (m12 - m21) * num;
        q.y = (m20 - m02) * num;
        q.z = (m01 - m10) * num;
    }
    else if ((m00 >= m11) && (m00 >= m22))
    {
        float num7 = sqrt(((1.0 + m00) - m11) - m22);
        float num4 = 0.5 / num7;
        q.x = 0.5 * num7;
        q.y = (m01 + m10) * num4;
        q.z = (m02 + m20) * num4;
        q.w = (m12 - m21) * num4;
    }
    else if (m11 > m22)
    {
        float num6 = sqrt(((1.0 + m11) - m00) - m22);
        float num3 = 0.5 / num6;
        q.x = (m10 + m01) * num3;
        q.y = 0.5 * num6;
        q.z = (m21 + m12) * num3;
        q.w = (m20 - m02) * num3;
    }
    else
    {
        float num5 = sqrt(((1.0 + m22) - m00) - m11);
        float num2 = 0.5 / num5;
        q.x = (m20 + m02) * num2;
        q.y = (m21 + m12) * num2;
        q.z = 0.5 * num5;
        q.w = (m01 - m10) * num2;
    }

    return q_normalize(q);
}

#endif // QUATERNION_HLSL_INCLUDED
