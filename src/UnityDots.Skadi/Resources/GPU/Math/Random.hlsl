#ifndef RANDOM_HLSL_INCLUDED
#define RANDOM_HLSL_INCLUDED

//https://www.shadertoy.com/view/4djSRW
//https://www.shadertoy.com/view/Xt3cDn

// Largest integer that can be expressed by floating points before losing accuracy.
// Use this to scale floats to use them as inputs for the random functions.
#define RANDOM_SCALE 16777216

uint _RandomBaseHash(uint p)
{
    p = 1103515245U*((p >> 1U)^(p));
    uint h32 = 1103515245U*((p)^(p>>3U));
    return h32^(h32 >> 16);
}

uint _RandomBaseHash(uint2 p)
{
    p = 1103515245U*((p >> 1U)^(p.yx));
    uint h32 = 1103515245U*((p.x)^(p.y>>3U));
    return h32^(h32 >> 16);
}

uint _RandomBaseHash(uint3 p)
{
    p = 1103515245U*((p.xyz >> 1U)^(p.yzx));
    uint h32 = 1103515245U*((p.x^p.z)^(p.y>>3U));
    return h32^(h32 >> 16);
}

float Random(uint x)
{
    uint n = _RandomBaseHash(x);
    return float(n)*(1.0/float(0xffffffffU));
}

float Random(uint2 x)
{
    uint n = _RandomBaseHash(x);
    return float(n)*(1.0/float(0xffffffffU));
}

float Random(uint3 x)
{
    uint n = _RandomBaseHash(x);
    return float(n)*(1.0/float(0xffffffffU));
}

float2 Random2(uint x)
{
    uint n = _RandomBaseHash(x);
    uint2 rz = uint2(n, n*48271U);
    return float2((rz.xy >> 1) & uint2(0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float2 Random2(uint2 x)
{
    uint n = _RandomBaseHash(x);
    uint2 rz = uint2(n, n*48271U);
    return float2((rz.xy >> 1) & uint2(0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float2 Random2(uint3 x)
{
    uint n = _RandomBaseHash(x);
    uint2 rz = uint2(n, n*48271U);
    return float2((rz.xy >> 1) & uint2(0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float3 Random3(uint x)
{
    uint n = _RandomBaseHash(x);
    uint3 rz = uint3(n, n*16807U, n*48271U);
    return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float3 Random3(uint2 x)
{
    uint n = _RandomBaseHash(x);
    uint3 rz = uint3(n, n*16807U, n*48271U);
    return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float3 Random3(uint3 x)
{
    uint n = _RandomBaseHash(x);
    uint3 rz = uint3(n, n*16807U, n*48271U);
    return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float4 Random4(uint x)
{
    uint n = _RandomBaseHash(x);
    uint4 rz = uint4(n, n*16807U, n*48271U, n*69621U);
    return float4((rz >> 1) & uint4(0x7fffffffU, 0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float4 Random4(uint2 x)
{
    uint n = _RandomBaseHash(x);
    uint4 rz = uint4(n, n*16807U, n*48271U, n*69621U);
    return float4((rz >> 1) & uint4(0x7fffffffU, 0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

float4 Random4(uint3 x)
{
    uint n = _RandomBaseHash(x);
    uint4 rz = uint4(n, n*16807U, n*48271U, n*69621U);
    return float4((rz >> 1) & uint4(0x7fffffffU, 0x7fffffffU, 0x7fffffffU, 0x7fffffffU))/float(0x7fffffff);
}

uint RandomSeed(uint x)
{
    return (uint)(Random(x) * RANDOM_SCALE);
}

uint RandomSeed(uint2 x)
{
    return (uint)(Random(x) * RANDOM_SCALE);
}

uint RandomSeed(uint3 x)
{
    return (uint)(Random(x) * RANDOM_SCALE);
}

uint2 RandomSeed2(uint x)
{
    return (uint2)(Random2(x) * RANDOM_SCALE);
}

uint2 RandomSeed2(uint2 x)
{
    return (uint2)(Random2(x) * RANDOM_SCALE);
}

uint2 RandomSeed2(uint3 x)
{
    return (uint2)(Random2(x) * RANDOM_SCALE);
}

uint3 RandomSeed3(uint x)
{
    return (uint3)(Random3(x) * RANDOM_SCALE);
}

uint3 RandomSeed3(uint2 x)
{
    return (uint3)(Random3(x) * RANDOM_SCALE);
}

uint3 RandomSeed3(uint3 x)
{
    return (uint3)(Random3(x) * RANDOM_SCALE);
}

uint4 RandomSeed4(uint x)
{
    return (uint4)(Random4(x) * RANDOM_SCALE);
}

uint4 RandomSeed4(uint2 x)
{
    return (uint4)(Random4(x) * RANDOM_SCALE);
}

uint4 RandomSeed4(uint3 x)
{
    return (uint4)(Random4(x) * RANDOM_SCALE);
}

#endif //RANDOM_HLSL_INCLUDED
