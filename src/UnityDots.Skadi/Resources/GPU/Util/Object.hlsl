#ifndef OBJECT_HLSL_INCLUDED
#define OBJECT_HLSL_INCLUDED

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Common.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Math.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Quaternion.hlsl"

struct Object
{
    float PrototypeID;
	float3 Position;
	float Scale;
	float4 Rotation;
    float Priority;
	float Radius;
    float IsActive;
};

bool _ObjectHasInvalidPosition(float3 position)
{
    return position.x < 0 || position.x > skadi_TerrainSize.x || position.z < 0 || position.z > skadi_TerrainSize.y;
}

bool IsActive(Object obj)
{
    return obj.IsActive > 0;
}

float3 Forward(Object obj)
{
    return q_rotate(obj.Rotation, Forward());
}

float3 Right(Object obj)
{
    return q_rotate(obj.Rotation, Right());
}

float3 Up(Object obj)
{
    return q_rotate(obj.Rotation, Up());
}

bool HasInvalidPosition(Object obj)
{
    return _ObjectHasInvalidPosition(obj.Position);
}

void SetInactive(inout Object obj)
{
    obj.IsActive = 0;
}

void SetPosition(inout Object obj, float3 position)
{
    obj.Position = position;
    obj.IsActive = _ObjectHasInvalidPosition(position) ? 0 : obj.IsActive;
}

Object CreateObject(uint id, float3 position, float priority)
{
    Object obj;
    obj.Scale = 1;
    obj.Rotation = float4(0, 0, 0, 1);
    obj.Radius = 1;
    obj.Priority = priority;
    obj.PrototypeID = id;
    obj.IsActive = 1;
    SetPosition(obj, position);
    return obj;
}

Object CloneObject(Object other)
{
    Object obj;
    obj.Position = other.Position;
    obj.Scale = other.Scale;
    obj.Rotation = other.Rotation;
    obj.Radius = other.Radius;
    obj.Priority = other.Priority;
    obj.PrototypeID = other.PrototypeID;
    obj.IsActive = other.IsActive;
    return obj;
}

Object CreateEmptyObject()
{
    Object obj;
    obj.Position = 0;
    obj.Scale = 0;
    obj.Rotation = 0;
    obj.Radius = 0;
    obj.Priority = 0;
    obj.PrototypeID = -1;
    obj.IsActive = 0;
    return obj;
}

#endif // OBJECT_HLSL_INCLUDED
