#ifndef COMMON_HLSL_INCLUDED
#define COMMON_HLSL_INCLUDED

#define SKADI_MAP_KERNEL_THREADS_X 32
#define SKADI_MAP_KERNEL_THREADS_Y 32
#define SKADI_MAP_KERNEL_THREADS_Z 1
#define SKADI_MAP_KERNEL(X) \
    [numthreads(SKADI_MAP_KERNEL_THREADS_X, SKADI_MAP_KERNEL_THREADS_Y, SKADI_MAP_KERNEL_THREADS_Z)] \
    void X(uint3 id : SV_DispatchThreadID)

#define SKADI_OBJECT_KERNEL_THREADS_X 128
#define SKADI_OBJECT_KERNEL_THREADS_Y 1
#define SKADI_OBJECT_KERNEL_THREADS_Z 1
#define SKADI_OBJECT_KERNEL(X) \
    [numthreads(SKADI_OBJECT_KERNEL_THREADS_X, SKADI_OBJECT_KERNEL_THREADS_Y, SKADI_OBJECT_KERNEL_THREADS_Z)] \
    void X(uint3 id : SV_DispatchThreadID)

#define SKADI_PATH_EDGE_KERNEL_THREADS_X 128
#define SKADI_PATH_EDGE_KERNEL_THREADS_Y 1
#define SKADI_PATH_EDGE_KERNEL_THREADS_Z 1
#define SKADI_PATH_EDGE_KERNEL(X) \
    [numthreads(SKADI_PATH_EDGE_KERNEL_THREADS_X, SKADI_PATH_EDGE_KERNEL_THREADS_Y, SKADI_PATH_EDGE_KERNEL_THREADS_Z)] \
    void X(uint3 id : SV_DispatchThreadID)

#define SKADI_PATH_VERTEX_KERNEL_THREADS_X 128
#define SKADI_PATH_VERTEX_KERNEL_THREADS_Y 1
#define SKADI_PATH_VERTEX_KERNEL_THREADS_Z 1
#define SKADI_PATH_VERTEX_KERNEL(X) \
    [numthreads(SKADI_PATH_VERTEX_KERNEL_THREADS_X, SKADI_PATH_VERTEX_KERNEL_THREADS_Y, SKADI_PATH_VERTEX_KERNEL_THREADS_Z)] \
    void X(uint3 id : SV_DispatchThreadID)

float skadi_TerrainHeight;
float4 skadi_CellSize;
float4 skadi_TerrainSize;

// Largest integer that can be expressed by floating points before losing accuracy.
#define SKADI_MAX_FLOAT_INTEGER 16777216

// Maximum amount of supported groupshared memory (in bytes)
#define SKADI_MAX_GROUPSHARED_MEMORY 32768

SamplerState _SkadiLinearClamp;

float Sample(Texture2D<float> src, float4 dstResolution, uint2 dstID)
{
    float2 uv = (dstID.xy + 0.5) * dstResolution.zw;
    return src.SampleLevel(_SkadiLinearClamp, uv, 0);
}

float2 Sample(Texture2D<float2> src, float4 dstResolution, uint2 dstID)
{
    float2 uv = (dstID.xy + 0.5) * dstResolution.zw;
    return src.SampleLevel(_SkadiLinearClamp, uv, 0);
}

float4 Sample(Texture2D<float4> src, float4 dstResolution, uint2 dstID)
{
    float2 uv = (dstID.xy + 0.5) * dstResolution.zw;
    return src.SampleLevel(_SkadiLinearClamp, uv, 0);
}

#endif // COMMON_HLSL_INCLUDED
