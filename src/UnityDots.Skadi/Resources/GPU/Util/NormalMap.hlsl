#ifndef NORMAL_MAP_HLSL_INCLUDED
#define NORMAL_MAP_HLSL_INCLUDED

//https://aras-p.info/texts/CompactNormalStorage.html

float2 NormalEncode(float3 normal)
{
    float p = sqrt(normal.z * 8 + 8);
    return normal.xy / p + 0.5;
}

float3 NormalDecode(float2 encoded)
{
    float2 fenc = encoded * 4 - 2;
    float f = dot(fenc, fenc);
    float3 normal;
    normal.xy = fenc * sqrt(1 - 0.25 * f);
    normal.z = 1 - 0.5 * f;
    return normal;
}

#endif //NORMAL_MAP_HLSL_INCLUDED
