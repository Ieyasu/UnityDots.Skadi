#ifndef PATH_HLSL_INCLUDED
#define PATH_HLSL_INCLUDED

#define SKADI_INVALID_VERTEX_ID 0xFFFFFFFF

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Object.hlsl"

struct PathVertex
{
    float3 Position;
    float Radius;
    float3 _Padding;
    float IsPinned;
};

struct PathEdge
{
    uint StartVertex;
    uint EndVertex;
};

bool IsValid(PathVertex vertex)
{
    return vertex.Radius >= 0;
}

bool IsValid(PathEdge edge)
{
    return edge.StartVertex != SKADI_INVALID_VERTEX_ID && edge.EndVertex != SKADI_INVALID_VERTEX_ID;
}

bool IsPinned(PathVertex vertex)
{
    return vertex.IsPinned != 0;
}

PathVertex CreatePathVertex(float3 position, float radius, bool isPinned)
{
    PathVertex vertex;
    vertex.Position = position;
    vertex.Radius = radius;
    vertex._Padding = 0;
    vertex.IsPinned = (float)isPinned;
    return vertex;
}

PathVertex CreatePathVertex(Object obj, bool isPinned)
{
    float radius = IsActive(obj) ? obj.Radius : -1;
    return CreatePathVertex(obj.Position, radius, isPinned);
}

PathVertex CreateEmptyPathVertex()
{
    return CreatePathVertex(0, -1, 0);
}

PathEdge CreatePathEdge(uint start, uint end)
{
    PathEdge edge;
    edge.StartVertex = start;
    edge.EndVertex = end;
    return edge;
}

PathEdge CreateEmptyPathEdge()
{
    return CreatePathEdge(SKADI_INVALID_VERTEX_ID, SKADI_INVALID_VERTEX_ID);
}

#endif // PATH_HLSL_INCLUDED
