#ifndef OBJECT_COLLISION_HLSL_INCLUDED
#define OBJECT_COLLISION_HLSL_INCLUDED

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Object.hlsl"

struct Collider
{
    float2 Position;
    float Radius;
    float IsActive;
};

// ObjectCollision.hlsl
// A collection of helper functions for detecting collisions between objects, as well as defintion of mandatory
// variables needed for the detection.

// Value assigned to object/cell ID pairs that do not point to an object.
#define COLLISION_INVALID_ID 0xffffffff

// Limit the number of collisions to check so that the GPU won't choke on pathological cases.
#define COLLISION_MAX_CHECKS 512

// The value used to scale radii of the colliding objects when checking collisions.
float _CollisionRadiusInfluence;

// The normalized size (in range [0, 1]) of each cell in the collision grid.
float _CollisionCellSize;

// Size of the collision grid measured in number of cells.
float4 _CollisionGridSize;

// List of object/cell ID pairs indicating which objects reside in which cells.
StructuredBuffer<uint2> _CollisionIDListInput;

// A map that is indexed by cell ID's and points to a sorted range of object/cell ID pairs in the ID list.
StructuredBuffer<uint2> _CollisionIndexListInput;

// The position (x, y), radius (z) and alive state (w) of the inserted objects.
StructuredBuffer<Collider> _CollisionCollidersInput;

// The objects that are inserted into the collision grid and consequently to the ID list.
StructuredBuffer<Object> _CollisionObjectsInput;

uint _CalculateCollisionCellID(uint2 cell)
{
    return _CollisionGridSize.x * cell.y + cell.x;
}

bool _Intersects(float2 origin, float radius, Collider collider)
{
    float2 delta = origin - collider.Position;
    float distanceSqr = dot(delta, delta);
    float radiusSum = _CollisionRadiusInfluence * radius + collider.Radius;
    return distanceSqr < radiusSum * radiusSum;
}

// Returns the object ID component of an object/cell ID pair.
uint GetCollisionObjectID(uint2 ids)
{
    return ids.x;
}

// Returns the collision grid cell ID component of an object/cell ID pair.
uint GetCollisionCellID(uint2 ids)
{
    return ids.y;
}

bool IsActive(Collider collider)
{
    return collider.IsActive > 0;
}

Collider CreateCollider(Object obj)
{
    Collider collider;
    collider.Position = obj.Position.xz;
    collider.Radius = _CollisionRadiusInfluence * obj.Radius;
    collider.IsActive = obj.IsActive;
    return collider;
}

// Returns the collider corresponding to the given ID list index.
Collider GetCollider(uint idListIndex)
{
    uint2 idPair = _CollisionIDListInput[idListIndex];
    uint objID = GetCollisionObjectID(idPair);
    return _CollisionCollidersInput[objID];
}

// Returns the collision object corresponding to the given ID list index.
Object GetCollisionObject(uint idListIndex)
{
    uint2 idPair = _CollisionIDListInput[idListIndex];
    uint objID = GetCollisionObjectID(idPair);
    return _CollisionObjectsInput[objID];
}

// Returns a start and end index pointing to the ID list with object/cell ID pairs. The indices can be used to iterate
// over object IDs belonging to the given cell.
uint2 GetCollisionCellIndices(uint2 cell)
{
    uint cellID = _CalculateCollisionCellID(cell);
    uint2 index = _CollisionIndexListInput[cellID];
    uint maxLimit = min(index.x + COLLISION_MAX_CHECKS, index.y);
    return uint2(index.x, index.y);
}

// Returns the start and end cells the given cirlce overlaps on the collision grid. This should be used to get
// overlapping cells when the radius of the circle can be larger than the size of a single collision cell.
void GetOverlappingCells(float2 origin, float radius, out uint2 startCell, out uint2 endCell)
{
    float2 normPos = origin * skadi_TerrainSize.zw;
    float normRadius = radius * _CollisionRadiusInfluence * min(skadi_TerrainSize.z, skadi_TerrainSize.w);
    float2 startPos = normPos - normRadius;
    float2 endPos = normPos + normRadius;
    startCell = (uint2)clamp(startPos * _CollisionGridSize.xy, 0, _CollisionGridSize.xy - 1);
    endCell = (uint2)clamp(endPos * _CollisionGridSize.xy, 0, _CollisionGridSize.xy - 1);
}

void GetOverlappingCells(Collider collider, out uint2 startCell, out uint2 endCell)
{
    GetOverlappingCells(collider.Position, collider.Radius, startCell, endCell);
}

void GetOverlappingCells(Object obj, out uint2 startCell, out uint2 endCell)
{
    GetOverlappingCells(obj.Position.xz, obj.Radius, startCell, endCell);
}

// Returns four of the closest cells to the origin. This should be used to get overlapping cells when the radius of the
// object is guaranteed to be smaller or equal to the size of a single collision cell.
void GetOverlappingCells(float2 origin, out int2 cell1, out int2 cell2, out int2 cell3, out int2 cell4)
{
    float2 normPos = origin * skadi_TerrainSize.zw;
    cell1 = (int2)(normPos * _CollisionGridSize.xy);
    float2 cell1Center = (cell1 + 0.5) * _CollisionCellSize;
    float2 centerDelta = normPos - cell1Center;
    cell2 = cell1 + int2(sign(centerDelta.x), sign(centerDelta.y));
    cell3 = int2(cell2.x, cell1.y);
    cell4 = int2(cell1.x, cell2.y);
}

void GetOverlappingCells(Collider collider, out int2 cell1, out int2 cell2, out int2 cell3, out int2 cell4)
{
    GetOverlappingCells(collider.Position, cell1, cell2, cell3, cell4);
}

void GetOverlappingCells(Object obj, out int2 cell1, out int2 cell2, out int2 cell3, out int2 cell4)
{
    GetOverlappingCells(obj.Position.xz, cell1, cell2, cell3, cell4);
}

// Returns true if the point collides with any collider in the collision grid.
bool IntersectsAny(float2 origin, float radius)
{
    uint2 startCell, endCell;
    GetOverlappingCells(origin, radius, startCell, endCell);

    for (uint y = startCell.y; y <= endCell.y; ++y)
    {
        for (uint x = startCell.x; x <= endCell.x; ++x)
        {
            uint2 cell = uint2(x, y);
            uint2 range = GetCollisionCellIndices(cell);
            for (uint i = range.x; i < range.y; ++i)
            {
                Collider collider = GetCollider(i);
                if (_Intersects(origin, radius, collider))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

// Returns true if both points collide with any collider in the collision grid.
bool IntersectsAny(float2 origin1, float radius1, float2 origin2, float radius2)
{
    uint2 startCell, endCell;
    GetOverlappingCells(origin1, radius1, startCell, endCell);

    for (uint y = startCell.y; y <= endCell.y; ++y)
    {
        for (uint x = startCell.x; x <= endCell.x; ++x)
        {
            uint2 cell = uint2(x, y);
            uint2 range = GetCollisionCellIndices(cell);
            for (uint i = range.x; i < range.y; ++i)
            {
                Collider collider = GetCollider(i);
                if (_Intersects(origin1, radius1, collider) && _Intersects(origin2, radius2, collider))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

// Returns true if the object collides with any collider in the collision grid.
bool IntersectsAny(Object obj)
{
    return IntersectsAny(obj.Position.xz, obj.Radius);
}

#endif // OBJECT_COLLISION_HLSL_INCLUDED
