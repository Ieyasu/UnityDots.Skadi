#pragma kernel CSInitializeCollisionGrid
#pragma kernel CSBuildCollisionGrid
#pragma kernel CSPruneInitial
#pragma kernel CSPrune

#define EMPTY_CELL 0

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Object.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Collision/ObjectCollision.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Random.hlsl"

float _PhaseSizeSqrt;
float4 _PhaseOffset;
Texture2D<uint> _InputCollisionGrid;
RWTexture2D<uint> _OutputCollisionGrid;

StructuredBuffer<Object> _InputObjects;
RWStructuredBuffer<Object> _OutputObjects;

uint PackID(uint id, float priority)
{
    uint byte = 255 * priority;
    return (byte << 24) | id;
}

uint UnpackID(uint packedID)
{
    return 0x00FFFFFF & packedID;
}

[numthreads(8, 8, 1)]
void CSInitializeCollisionGrid(uint3 id : SV_DispatchThreadID)
{
    if (id.x >= (uint)_CollisionGridSize.x || id.y > (uint)_CollisionGridSize.y)
    {
        return;
    }

    _OutputCollisionGrid[id.xy] = EMPTY_CELL;
}

SKADI_OBJECT_KERNEL(CSBuildCollisionGrid)
{
    // Calculate the cell the object is in
    Object obj = _InputObjects[id.x];
    int2 cell = obj.Position.xz * skadi_TerrainSize.zw * _CollisionGridSize.xy;
    if (cell.x < 0 || cell.y < 0 || cell.x >= _CollisionGridSize.x || cell.y >= _CollisionGridSize.y)
    {
        return;
    }

    // The priority of the object is encoded as 8 most significant bits of the id. This leaves ~16 million IDs free.
    uint packedID = PackID(id.x, obj.Priority);

    // Try to put the object in the grid cell (the cells are sized so that only one object can be in one cell)
    InterlockedMax(_OutputCollisionGrid[cell], packedID);
}

SKADI_OBJECT_KERNEL(CSPruneInitial)
{
    // Calculate the cell the object is in
    Object obj = _InputObjects[id.x];
    int2 cell = obj.Position.xz * skadi_TerrainSize.zw * _CollisionGridSize.xy;
    if (cell.x < 0 || cell.y < 0 || cell.x >= _CollisionGridSize.x || cell.y >= _CollisionGridSize.y)
    {
        return;
    }

    if (UnpackID(_InputCollisionGrid[cell]) != id.x)
    {
        SetInactive(obj);
    }

    _OutputObjects[id.x] = obj;
}

[numthreads(8, 8, 1)]
void CSPrune(uint3 id : SV_DispatchThreadID)
{
    uint2 cell = _PhaseOffset.xy * id.xy + (uint2)_PhaseOffset.zw;
    if (cell.x >= (uint)_CollisionGridSize.x || cell.y >= (uint)_CollisionGridSize.y)
    {
        return;
    }

    uint objID = UnpackID(_InputCollisionGrid[cell]);
    Object obj = _OutputObjects[objID];

    uint2 startCell, endCell;
    GetOverlappingCells(obj, startCell, endCell);

    for (uint y = startCell.y; y <= endCell.y; ++y)
    {
        for (uint x = startCell.x; x <= endCell.x; ++x)
        {
            uint2 otherCell = uint2(x, y);
            uint otherID = UnpackID(_InputCollisionGrid[otherCell]);
            Object otherObj = _OutputObjects[otherID];

            if (!IsActive(otherObj) || obj.Priority > otherObj.Priority || otherID == objID)
            {
                continue;
            }

            // Check for intersection
            float2 delta = otherObj.Position.xz - obj.Position.xz;
            float distanceSqr = dot(delta, delta);
            float radiusSum = _CollisionRadiusInfluence * (otherObj.Radius + obj.Radius);
            if (distanceSqr < radiusSum * radiusSum)
            {
                SetInactive(obj);
                break;
            }
        }
    }

    _OutputObjects[objID] = obj;
}
