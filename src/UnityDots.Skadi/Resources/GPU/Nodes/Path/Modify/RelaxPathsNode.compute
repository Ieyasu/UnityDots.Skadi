#pragma kernel CSMain

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Random.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Path.hlsl"

float _Strength;
StructuredBuffer<PathEdge> _InputAdjacencyEdges;
StructuredBuffer<uint2> _InputAdjacencyIndexList;
StructuredBuffer<PathVertex> _InputVertices;
RWStructuredBuffer<PathVertex> _OutputVertices;

SKADI_PATH_VERTEX_KERNEL(CSMain)
{
    PathVertex vertex = _InputVertices[id.x];

    if (IsValid(vertex) && !IsPinned(vertex))
    {
        // Calculate the weighted centreid of neighbouring vertices
        uint vertexCount = 1;
        uint2 range = _InputAdjacencyIndexList[id.x];
        float3 position = vertex.Position;
        for (uint i = range.x; i < range.y; ++i)
        {
            PathEdge edge = _InputAdjacencyEdges[i];
            PathVertex neighbour = _InputVertices[edge.EndVertex];
            position += neighbour.Position;
            vertexCount += 1;
        }
        float3 vertexCentroid = position / vertexCount;

        vertex.Position = lerp(vertex.Position, vertexCentroid, saturate(_Strength));
    }

    _OutputVertices[id.x] = vertex;
}
