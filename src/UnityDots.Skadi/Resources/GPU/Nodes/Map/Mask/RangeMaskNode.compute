#pragma kernel CSMain

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Common.hlsl"

float _MinRangeStart;
float _MinRangeEnd;
float _MaxRangeStart;
float _MaxRangeEnd;

Texture2D<float> _InputHeightmap;
RWTexture2D<float> _OutputMask;

float GetHeight(uint3 id)
{
    return _InputHeightmap[id.xy];
}

float GetMask(float height)
{
    // Avoid division by zero
    float epsilon = 0.00001;

    float minDist = (height - _MinRangeStart) / max(_MinRangeEnd - _MinRangeStart, epsilon);
    float maxDist = (_MaxRangeEnd - height) / max(_MaxRangeEnd - _MaxRangeStart, epsilon);
    return min(minDist, maxDist);
}

SKADI_MAP_KERNEL(CSMain)
{
    float height = GetHeight(id);
    float mask = GetMask(height);
    _OutputMask[id.xy] = saturate(mask);
}
