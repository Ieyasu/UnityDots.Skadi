#pragma kernel CSConvex
#pragma kernel CSConcave
#pragma kernel CSCombined
#pragma kernel CSHorizontal
#pragma kernel CSVertical

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Common.hlsl"

#define THREAD_COUNT 1024
#define MAX_RADIUS 100
#define CACHE_SIZE (THREAD_COUNT + 2 * MAX_RADIUS)

groupshared float _CurvatureCache[CACHE_SIZE];

float _Intensity;
float _Radius;
Texture2D<float> _InputHeightmap;
Texture2D<float> _InputAverageHeights;

float4 _OutputAverageHeightsResolution;
RWTexture2D<float> _OutputAverageHeights;
RWTexture2D<float> _OutputHeightmap;

float Convex(float height, float averageHeight)
{
    return saturate((height - averageHeight) * _Intensity);
}

float Concave(float height, float averageHeight)
{
    return saturate((averageHeight - height) * _Intensity);
}

float CurvatureSum(uint index)
{
    float result = 0;
    for (int i = 0; i <= 2 * _Radius; ++i)
    {
        result += _CurvatureCache[index + i];
    }
    return result;
}

SKADI_MAP_KERNEL(CSConvex)
{
    float height = _InputHeightmap[id.xy];
    float averageHeight = _InputAverageHeights[id.xy];
    _OutputHeightmap[id.xy] = Convex(height, averageHeight);
}

SKADI_MAP_KERNEL(CSConcave)
{
    float height = _InputHeightmap[id.xy];
    float averageHeight = _InputAverageHeights[id.xy];
    _OutputHeightmap[id.xy] = Concave(height, averageHeight);
}


SKADI_MAP_KERNEL(CSCombined)
{
    float height = _InputHeightmap[id.xy];
    float averageHeight = _InputAverageHeights[id.xy];
    float convex = Convex(height, averageHeight);
    float concave = Concave(height, averageHeight);
    _OutputHeightmap[id.xy] = 0.5 * (convex - concave + 1);
}

[numthreads(1024, 1, 1)]
void CSHorizontal(uint3 id : SV_DispatchThreadID, uint3 groupID : SV_GroupThreadID)
{
    int index = groupID.x;

    _CurvatureCache[index + _Radius] = (id.x < (uint)_OutputAverageHeightsResolution.x) ? _InputHeightmap[id.xy] : 0;
    if (index < _Radius)
    {
        int2 pos = int2(id.x - _Radius, id.y);
        _CurvatureCache[index] = pos.x >= 0 ? _InputHeightmap[pos] : 0;
    }
    else if (index >= THREAD_COUNT - _Radius)
    {
        int2 pos = int2(id.x + _Radius, id.y);
        _CurvatureCache[index + 2 * _Radius] = pos.x < _OutputAverageHeightsResolution.x ? _InputHeightmap[pos] : 0;
    }

    GroupMemoryBarrierWithGroupSync();

    if (id.x < (uint)_OutputAverageHeightsResolution.x && id.y < (uint)_OutputAverageHeightsResolution.y)
    {
        _OutputAverageHeights[id.xy] = CurvatureSum(index);
    }
}

[numthreads(1, 1024, 1)]
void CSVertical(uint3 id : SV_DispatchThreadID, uint3 groupID : SV_GroupThreadID)
{
    int index = groupID.y;

    _CurvatureCache[index + _Radius] = (id.y < (uint)_OutputAverageHeightsResolution.y) ? _InputAverageHeights[id.xy] : 0;
    if (index < _Radius)
    {
        int2 pos = int2(id.x, id.y - _Radius);
        _CurvatureCache[index] = pos.y >= 0 ? _InputAverageHeights[pos] : 0;
    }
    else if (index >= THREAD_COUNT - _Radius)
    {
        int2 pos = int2(id.x, id.y + _Radius);
        _CurvatureCache[index + 2 * _Radius] = pos.y < _OutputAverageHeightsResolution.y ? _InputAverageHeights[pos] : 0;
    }

    GroupMemoryBarrierWithGroupSync();

    if (id.x < (uint)_OutputAverageHeightsResolution.x && id.y < (uint)_OutputAverageHeightsResolution.y)
    {
        // The sum doesn't include values that go over borders so we need to take that into account when normalizing it
        uint2 distanceToBorder = min(id.xy, _OutputAverageHeightsResolution.xy - id.xy - 1);
        uint2 length = min(_Radius, distanceToBorder) + _Radius + 1;

        _OutputAverageHeights[id.xy] = CurvatureSum(index) / (length.x * length.y);
    }
}
