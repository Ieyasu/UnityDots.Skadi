#pragma kernel CSImprint
#pragma kernel CSImprintIntensity INTENSITY_MASK
#pragma kernel CSImprintOffset OFFSET_MASK
#pragma kernel CSImprintIntensityOffset INTENSITY_MASK OFFSET_MASK

#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Collision/ObjectCollision.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Math/Spline.hlsl"
#include "Packages/com.unitydots.skadi/UnityDots.Skadi/Resources/GPU/Util/Common.hlsl"

float _Intensity;
StructuredBuffer<float> _ProfileCurve;

Texture2D<float> _InputIntensity;
Texture2D<float> _InputOffset;
Texture2D<float> _InputMap;
RWTexture2D<float> _OutputMap;

float GetHeight(uint2 id)
{
    return _InputMap[id.xy];
}

float GetIntensity(uint2 id)
{
    #if INTENSITY_MASK
        return saturate(_Intensity * _InputIntensity[id.xy]);
    #else
        return saturate(_Intensity);
    #endif
}

float2 GetOffset(uint2 id)
{
    #if OFFSET_MASK
        return _InputOffset[id.xy];
    #else
        return 0;
    #endif
}

uint2 GetCollisionCellIndices(float2 pos)
{
    float2 uv = pos * skadi_TerrainSize.zw;
    uint2 cell = min(uv * _CollisionGridSize.xy, _CollisionGridSize.xy - 1);
    return GetCollisionCellIndices(cell);
}

void Imprint(uint3 id)
{
    // Get the objects we collide with
    float2 offset = GetOffset(id.xy);
    float2 pos = (id.xy + 0.5) * skadi_CellSize.xy + offset * 100;
    uint2 range = GetCollisionCellIndices(pos);

    // Sample local height and imprint intensity
    float height = GetHeight(id.xy);
    float intensity = GetIntensity(id.xy);

    // Calculate the sum of new heights caused by overlapping objects
    float heightSum = 0;
    float weightSum = 0;
    for (uint i = range.x; i < range.y; ++i)
    {
        Collider collider = GetCollider(i);
        float dist = distance(pos, collider.Position);

        if (dist <= collider.Radius)
        {
            // Interpolate between the value below the object and the value of this pixel to get the new height
            float weight = (collider.Radius - dist) / collider.Radius;
            float t = SplineEvaluate(weight, _ProfileCurve);

            Object obj = GetCollisionObject(i);
            float objHeight = obj.Position.y / skadi_TerrainHeight;
            heightSum += weight * lerp(height, objHeight, t * intensity);
            weightSum += weight;
        }
    }

    _OutputMap[id.xy] = weightSum > 0 ? heightSum / weightSum : height;
}

SKADI_MAP_KERNEL(CSImprint)
{
    Imprint(id);
}

SKADI_MAP_KERNEL(CSImprintIntensity)
{
    Imprint(id);
}

SKADI_MAP_KERNEL(CSImprintOffset)
{
    Imprint(id);
}

SKADI_MAP_KERNEL(CSImprintIntensityOffset)
{
    Imprint(id);
}
