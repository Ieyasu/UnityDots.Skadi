#ifndef WATER_CAUSTICS_CGINC
#define WATER_CAUSTICS_CGINC

sampler2D _CausticsTex;
float4 _CausticsColor;
float _CausticsSpeed;
float _CausticsScale;
float _CausticsStartDepth;
float _CausticsEndDepth;
float _CausticsFadeDistance;

float4 _CalculateCaustics(WaterParams params)
{
    // Calculate animation
    float scale = 1 - _CausticsScale;
    float4 causticsScale = float4(scale, scale, scale * 0.9, scale * 0.95);
    float2 causticsDirectionXY = normalize(float2(_WaveDirection.x, _WaveDirection.y));
    float2 causticsDirectionZW = normalize(float2(_WaveDirection.z, _WaveDirection.w));
    float4 causticsDirection = float4(causticsDirectionXY.x, causticsDirectionXY.y, causticsDirectionZW.x, causticsDirectionZW.y);
    float4 causticsOffset = frac(_CausticsSpeed * causticsDirection * causticsScale * _Time.x);

    // Calculate uv based on world position
    // https://forum.unity.com/threads/computing-world-position-from-depth.760421/
    float3 cameraForward = mul((float3x3)unity_CameraToWorld, float3(0, 0, -1));
    float flattenDot = dot(cameraForward, params.viewDir);
    float3 worldPos = _WorldSpaceCameraPos - params.viewDir * params.eyeDepth / flattenDot;
    float4 uv = worldPos.xzxz * causticsScale + causticsOffset;
    uv = worldPos.xzxz * causticsScale + causticsOffset;

    // Sample textures
    float4 caustics1 = tex2D(_CausticsTex, uv.xy) * _CausticsColor;
    float4 caustics2 = tex2D(_CausticsTex, uv.zw) * _CausticsColor;

    // Calculate attenuation
    float depth = (params.eyeDepth + params.viewPos.z);
    float waterAtten = 1 - saturate(depth / _AttenuationDepth);
    float deltaDepth = min(_CausticsEndDepth - depth, depth - _CausticsStartDepth);
    float causticsAtten = saturate(deltaDepth / _CausticsFadeDistance);

    return min(caustics1, caustics2) * (1 - params.fresnel) * waterAtten * causticsAtten;
}

#endif //WATER_CAUSTICS_CGINC
