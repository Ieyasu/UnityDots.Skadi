#ifndef WATER_WAVES_CGINC
#define WATER_WAVES_CGINC

float _WaveAmplitude;
float _WaveScale;
float _WaveSpeed;
float _BumpScale;
sampler2D _BumpMap;

float3 _CalculateGerstnerWave(float4 wave, float3 vertex, inout float3 tangent, inout float3 binormal)
{
    float steepness = wave.z;
    float wavelength = wave.w;
    float k = 2 * UNITY_PI / wavelength;
    float c = sqrt(9.8 / k);
    float2 direction = normalize(wave.xy);
    float f = k * (dot(direction, vertex.xz) - c * _Time.y);
    float a = steepness / k;

    tangent += float3(
        -direction.x * direction.x * (steepness * sin(f)),
        direction.x * (steepness * cos(f)),
        -direction.x * direction.y * (steepness * sin(f))
    );
    binormal += float3(
        -direction.x * direction.y * (steepness * sin(f)),
        direction.y * (steepness * cos(f)),
        -direction.y * direction.y * (steepness * sin(f))
    );

    return float3(
        direction.x * (a * cos(f)),
        a * sin(f),
        direction.y * (a * cos(f))
    );
}

float4 CalculateWaveBumpUV(float3 vertex)
{
    float scale = 1 - _WaveScale;
    float4 waveScale = float4(scale, scale, scale * 0.4f, scale * 0.45f);
    float2 waveDirectionXY = normalize(float2(_WaveDirection.x, _WaveDirection.y));
    float2 waveDirectionZW = normalize(float2(_WaveDirection.z, _WaveDirection.w));
    float4 waveDirection = float4(waveDirectionXY.x, waveDirectionXY.y, waveDirectionZW.x, waveDirectionZW.y);
    float4 waveOffset = frac(_WaveSpeed * waveDirection * waveScale * _Time.x);
    float4 wpos = mul(unity_ObjectToWorld, vertex);
    return wpos.xzxz * waveScale + waveOffset;
}

void CalculateWaveDisplacement(inout float3 vertex, out float3 normal)
{
    float3 tangent = 0;
    float3 binormal = 0;
    float3 result = vertex;

    float persistance = 0.5;
    float waveLength = 20;
    float radians = 0.5;

    float2 direction = _WaveDirection.xy;
    float crot = cos(radians);
    float srot = sin(radians);
    // for (int i = 0; i < 1; ++i)
    // {
        float4 wave = float4(direction, 0.15, waveLength);
        result += _CalculateGerstnerWave(wave, vertex, tangent, binormal);

    //     waveLength *= persistance;
    //     direction = float2(crot * direction.x - srot * direction.y, srot * direction.x + crot * direction.y);
    // }

    vertex = result;
    normal = normalize(cross(binormal, tangent));
}

#endif //WATER_WAVES_CGINC
