#ifndef WATER_COMMON_CGINC
#define WATER_COMMON_CGINC

struct WaterParams
{
    float4 screenPos;
    float3 viewPos;
    float3 viewDir;
    float3 waveNormal;
    float3 meshNormal;
    float3 worldNormal;
    float eyeDepth;
    float fresnel;
    bool isUnderSurface;
};

// Grab texture
sampler2D _GrabTexture;
sampler2D _CameraDepthTexture;
float4 _GrabTexture_TexelSize;

// Attenuation
float _AttenuationDepth;
float4 _AttenuationColor;

// Smoothing
float _EdgeSmooth;
float4 _WaveDirection;

#include "WaterCaustics.cginc"
#include "WaterRefraction.cginc"
#include "WaterReflection.cginc"
#include "WaterWaves.cginc"

// https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
float _CalculateFresnel(float3 viewDir, float3 worldNormal)
{
    float ior1 = 1.0;
    float ior2 = 1.33;

    // Calculate the incoming and outgoing rays
    float cosIn = dot(viewDir, worldNormal);

    // Under the surface, flip ior's
    if (cosIn < 0)
    {
        float temp = ior1;
        ior1 = ior2;
        ior2 = temp;
        cosIn = -cosIn;
    }

    float k = ior1 / ior2 * sqrt(1 - cosIn * cosIn);
    float cosOut = sqrt(1 - k * k);

    // Calculate the fresnel coefficient
    float fres1 = (ior1 * cosIn - ior2 * cosOut) / (ior1 * cosIn + ior2 * cosOut);
    float fres2 = (ior1 * cosOut - ior2 * cosIn) / (ior1 * cosOut + ior2 * cosIn);
    float fresnel = 0.5 * (fres1 * fres1 + fres2 * fres2);
    return saturate(fresnel);

}

float4 _CalculateEdgeColor(WaterParams params)
{
    // Smooth edges
    float4 grabPos = params.screenPos;
    #if UNITY_SINGLE_PASS_STEREO
        grabPos.xy = TransformStereoScreenSpaceTex(grabPos.xy, grabPos.w);
    #endif

    float4 grabCol = float4(tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(grabPos)).xyz, 0);
    return grabCol;
}

void WaterVertex(inout appdata_full v, inout WATER_VERTEX_OUTPUT data)
{
    // CalculateWaveDisplacement(v.vertex.xyz, data.waveNormal);

    data.bumpuv = CalculateWaveBumpUV(v.vertex.xyz);
    data.viewPos = UnityObjectToViewPos(v.vertex);
    float4 clipPos = UnityViewToClipPos(data.viewPos);
    data.screenPos = ComputeScreenPos(clipPos);
    float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
    data.worldViewDir = UnityWorldSpaceViewDir(worldPos);
}

WaterParams CreateWaterParams(float4 screenPos, float4 uv, float3 viewPos, float3 viewDir, float3 worldNormal, float waveNormal)
{
    WaterParams params;
    params.screenPos = screenPos;
    params.viewPos = viewPos;
    params.viewDir = viewDir;

    float3 waveBump1 = UnpackNormalWithScale(tex2D(_BumpMap, uv.xy), _BumpScale).xyz;
    float3 waveBump2 = UnpackNormalWithScale(tex2D(_BumpMap, uv.zw), _BumpScale).xyz;
    params.waveNormal = normalize(0.5 * (waveBump1 + waveBump2));
    params.meshNormal = worldNormal /* + waveNormal */;
    params.worldNormal = normalize(float3(params.waveNormal.xy + params.meshNormal.xz, abs(params.waveNormal.z) * params.meshNormal.y).xzy);

    float depth = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(params.screenPos));
    params.eyeDepth = LinearEyeDepth(depth);
    params.fresnel = _CalculateFresnel(params.viewDir, params.worldNormal);
    params.isUnderSurface = dot(params.worldNormal, params.viewDir) < 0;
    return params;
}

void CalculateWaterColor(WaterParams params, inout WATER_SURFACE_OUTPUT o)
{
    // Calculate all different colors the water can have
    float4 refrCol = _CalculateRefraction(params);
    float4 reflCol = _CalculateReflection(params);
    float4 causCol = _CalculateCaustics(params);
    float4 edgeCol = _CalculateEdgeColor(params);

    // Blend the colors based on fresnel and water depth
    float edgeT = saturate((params.eyeDepth + params.viewPos.z) / _EdgeSmooth);
    float4 waterCol = lerp(refrCol, reflCol, params.fresnel);
    waterCol = lerp(edgeCol, waterCol, edgeT);

    o.Albedo = edgeT * waterCol.rgb;
    o.Emission = waterCol.rgb + causCol * edgeT;
    o.Gloss = edgeT * params.fresnel * waterCol.a;
    o.Alpha = 1;
    o.Normal = params.isUnderSurface ? -params.waveNormal : params.waveNormal;
}

#endif //WATER_COMMON_CGINC
