// Shader "Custom/Nature/Water"
// {
//     Properties
//     {
//         _EdgeSmooth ("Edge smoothing", Float) = 0.2

//         [Header(Refraction)]
//         _RefractionColor ("Refraction color", Color)  = (1.0, 1.0, 1.0, 1.0)
//         _RefractionDistort ("Refraction distort", Range (0, 1)) = 0.2
//         _RefractionFadeDepth ("Refraction fade depth", Float) = 1.0
//         _RefractionFadeColor ("Refraction fade color", Color) = (0.0, 0.0, 0.0, 1.0)

//         [Header(Reflections)]
//         _SkyboxFog ("Skybox fog", Color) = (1.0, 1.0, 1.0, 0.0)
//         _ReflectionColor ("Reflection color", Color)  = (1.0, 1.0, 1.0, 1.0)
//         _ReflectionDistort ("Reflection distort", Range (0, 1)) = 0.2

//         [Header(Reflections (SSR))]
//         _SSRStride ("SSR sampling stride", Float) = 16.0
//         _SSRStrideDistance ("SSR stride convergence distance", Float) = 200.0
//         _SSRIterations ("SSR sampling iterations", Int) = 64
//         _SSRMaxDistance ("SSR sampling distance", Float) = 1000.0
//         _SSRScreenEdgeFade ("SSR screen edge fade ", Range(0, 1)) = 0.5
//         _SSRSteepnessFade ("SSR eye fade start", Range(0, 1)) = 0.2

//         [Header(Waves)]
//         _WaveScale ("Wave scale", Range (0.01, 1.0)) = 0.1
//         _WaveSpeed ("Wave speed", Float) = 1.0
//         _WaveDirection ("Wave direction", Vector) = (1, 0.5, -1, -0.4)
//         [NoScaleOffset] _BumpMap ("Normalmap ", 2D) = "bump" {}
//         _BumpScale ("Normalmap scale", Float) = 1.0
//     }

//     Subshader
//     {
//         Tags
//         {
//             "Queue" = "AlphaTest+100"
//         }

//         GrabPass
//         {
//             Name "SkadiWater"
//         }

//         Pass
//         {
//             Cull Off

//             CGPROGRAM
//             #pragma vertex vert
//             #pragma fragment frag
//             #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
//             #pragma multi_compile_fog
//             #pragma multi_compile_instancing

//             #include "UnityCG.cginc"

//             struct v2f
//             {
//                 float4 pos : SV_POSITION;
//                 float4 screenPos : TEXCOORD0;
//                 float4 bumpuv : TEXCOORD1;
//                 float3 viewPos : TEXCOORD2;
//                 float3 worldViewDir: TEXCOORD3;
//                 float3 tspace0 : TEXCOORD4;
//                 float3 tspace1 : TEXCOORD5;
//                 float3 tspace2 : TEXCOORD6;
//                 UNITY_FOG_COORDS(7)
//             };

//             #define WATER_VERTEX_OUTPUT v2f
//             #include "WaterCommon.cginc"

//             struct appdata_t
//             {
//                 float4 vertex : POSITION;
//                 float4 tangent : TANGENT;
//                 float3 normal : NORMAL;
//             };

//             v2f vert(appdata_t v)
//             {
//                 v2f o;
//                 o.pos = UnityObjectToClipPos(v.vertex);
//                 o.screenPos = ComputeScreenPos(o.pos);
//                 o.worldViewDir = WorldSpaceViewDir(v.vertex);
//                 o.viewPos = UnityObjectToViewPos(v.vertex);
//                 o.bumpuv = CalculateWaveBumpUV(v.vertex.xyz);

//                 // Calculate tangent space matrix.
//                 float3 normal = UnityObjectToWorldNormal(v.normal);
//                 float3 tangent = UnityObjectToWorldDir(v.tangent.xyz);
//                 float tangentSign = v.tangent.w * unity_WorldTransformParams.w;
//                 float3 bitangent = cross(normal, tangent) * tangentSign;
//                 o.tspace0 = float3(tangent.x, bitangent.x, normal.x);
//                 o.tspace1 = float3(tangent.y, bitangent.y, normal.y);
//                 o.tspace2 = float3(tangent.z, bitangent.z, normal.z);

//                 UNITY_TRANSFER_FOG(o, o.pos);
//                 return o;
//             }

//             float4 frag(v2f i) : SV_Target
//             {
//                 WaterData data;
//                 data.screenPos = i.screenPos;
//                 data.viewPos = i.viewPos;
//                 data.worldViewDir = normalize(i.worldViewDir);
//                 data.eyeDepth = 0;

//                 // Calculate world normal
//                 float3 waveBump1 = UnpackNormalWithScale(tex2D(_BumpMap, i.bumpuv.xy), _BumpScale).rgb;
//                 float3 waveBump2 = UnpackNormalWithScale(tex2D(_BumpMap, i.bumpuv.zw), _BumpScale).rgb;
//                 float3 waveNormal = 0.5 * (waveBump1 + waveBump2);
//                 data.worldNormalMesh.x = i.tspace0.z;
//                 data.worldNormalMesh.y = i.tspace1.z;
//                 data.worldNormalMesh.z = i.tspace2.z;
//                 data.worldNormal.x = dot(i.tspace0, waveNormal);
//                 data.worldNormal.y = dot(i.tspace1, waveNormal);
//                 data.worldNormal.z = dot(i.tspace2, waveNormal);
//                 data.fresnel = CalculateFresnel(data.worldViewDir, data.worldNormal);

//                 float4 col = CalculateWaterColor(data);
//                 UNITY_APPLY_FOG(i.fogCoord, col);
//                 return col;
//             }
//             ENDCG
//         }
//     }
// }
