#ifndef WATER_REFRACTION_CGINC
#define WATER_REFRACTION_CGINC

float _RefractionDistort;
float4 _RefractionColor;

float4 _CalculateRefraction(WaterParams params)
{
    float2 offset = 300 * params.worldNormal.xz * _RefractionDistort * _GrabTexture_TexelSize.xy;
    float4 grabPos = params.screenPos;
    #if UNITY_SINGLE_PASS_STEREO
        grabPos.xy = TransformStereoScreenSpaceTex(grabPos.xy, grabPos.w);
    #endif
    #ifdef UNITY_Z_0_FAR_FROM_CLIPSPACE
        grabPos.xy = offset * UNITY_Z_0_FAR_FROM_CLIPSPACE(grabPos.z) + grabPos.xy;
    #else
        grabPos.xy = offset * grabPos.z + grabPos.xy;
    #endif
    float4 grabCol = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(grabPos));
    float4 refrCol = grabCol * _RefractionColor;

    float attenuation = saturate((params.eyeDepth + params.viewPos.z) / _AttenuationDepth);
    return lerp(refrCol, _AttenuationColor, attenuation);
}

#endif //WATER_REFRACTION_CGINC
