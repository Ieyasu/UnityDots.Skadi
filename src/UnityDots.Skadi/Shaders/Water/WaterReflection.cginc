#ifndef WATER_REFLECTION_CGINC
#define WATER_REFLECTION_CGINC

// http://casual-effects.blogspot.com/2014/08/screen-space-ray-tracing.html

#include "UnityCG.cginc"

float4 _SkyboxFog;
float4 _ReflectionColor;
float _ReflectionDistort;
float _SSRMaxDistance;
float _SSRStride;
float _SSRStrideDistance;
float _SSRScreenEdgeFade;
float _SSRSteepnessFade;
float _SSRMaxPenetration;
float _SSRPenetrationFade;
int _SSRIterations;

struct Ray
{
    float3 origin;
    float3 direction;
};

struct Segment
{
    float3 start;
    float3 end;
    float3 direction;
};

struct Result
{
    bool isHit;
    float2 uv;
    float depth;
    float penetrationDepth;
    int iterationCount;
};

void SwapIfBigger(inout float a, inout float b)
{
    if (a > b)
    {
        float tmp = a;
        a = b;
        b = tmp;
    }
}

float GetSquaredDistance(float2 first, float2 second)
{
    first -= second;
    return dot(first, first);
}


float4 ProjectToScreenSpace(float4x4 projectionMatrix, float3 position)
{
    return float4(
        projectionMatrix[0][0] * position.x + projectionMatrix[0][2] * position.z,
        projectionMatrix[1][1] * position.y + projectionMatrix[1][2] * position.z,
        projectionMatrix[2][2] * position.z + projectionMatrix[2][3],
        projectionMatrix[3][2] * position.z
    );
}

Result March(Ray ray, float jitter)
{
    Result result;
    result.isHit = false;
    result.uv = 0;
    result.depth = 0;
    result.penetrationDepth = 0;
    result.iterationCount = 0;

    // Clip to the near plane
    float distanceToEnd = ray.origin.z + ray.direction.z * _SSRMaxDistance;
    float rayLength = (distanceToEnd > -_ProjectionParams.y) ? (-_ProjectionParams.y - ray.origin.z) / ray.direction.z : _SSRMaxDistance;
    Segment segment;
    segment.start = ray.origin;
    segment.end = ray.origin + ray.direction * rayLength;

    // Project into homogeneous clip space
    float4x4 screenSpaceProjectionMatrix = {
        0.5 * _ScreenParams.x, 0, 0, 0.5 * _ScreenParams.x,
        0, 0.5 * _ScreenParams.y, 0, 0.5 * _ScreenParams.y,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    screenSpaceProjectionMatrix = mul(screenSpaceProjectionMatrix, unity_CameraProjection);
    float4 r = ProjectToScreenSpace(screenSpaceProjectionMatrix, segment.start);
    float4 q = ProjectToScreenSpace(screenSpaceProjectionMatrix, segment.end);

    // The interpolated homogeneous version of the camera-space points
    float2 homogenizers = rcp(float2(r.w, q.w));
    segment.start *= homogenizers.x;
    segment.end *= homogenizers.y;

    // Screen-space endpoints
    // If the line is degenerate, make it cover at least one pixel
    // to avoid handling zero-pixel extent as a special case later
    float4 endPoints = float4(r.xy, q.xy) * homogenizers.xxyy;
    endPoints.zw += step(GetSquaredDistance(endPoints.xy, endPoints.zw), 0.0001) * 0.01;

    // Permute so that the primary iteration is in x to collapse
    // all quadrant-specific DDA cases later
    bool isPermuted = false;
    float2 displacement = endPoints.zw - endPoints.xy;
    if (abs(displacement.x) < abs(displacement.y))
    {
        // This is a more-vertical line
        isPermuted = true;
        displacement = displacement.yx;
        endPoints.xyzw = endPoints.yxwz;
    }

    float direction = sign(displacement.x);
    float normalizer = direction / displacement.x;

    // Track the derivatives of Q and k
    float3  dQ = (segment.end - segment.start) * normalizer;
    float dk = (homogenizers.y - homogenizers.x) * normalizer;
    float2  dP = float2(direction, displacement.y * normalizer);

    // Calculate pixel stride based on distance of ray origin from camera.
    // Since perspective means distant objects will be smaller in screen space
    // we can use this to have higher quality reflections for far away objects
    // while still using a large pixel stride for near objects (and increase performance)
    // this also helps mitigate artifacts on distant reflections when we use a large
    // pixel stride.
    float strideScaler = 1.0 - min(1.0, -ray.origin.z / _SSRStrideDistance);
    float pixelStride = 1.0 + strideScaler * _SSRStride;

    // Scale derivatives by the desired pixel stride and then
    // offset the starting values by the jitter fraction
    dP *= pixelStride; dQ *= pixelStride; dk *= pixelStride;
    endPoints.xy += dP * jitter; segment.start += dQ * jitter; homogenizers.x += dk * jitter;

    // Track ray step and derivatives in a float4 to parallelize
    float4 pqk = float4(endPoints.xy, segment.start.z, homogenizers.x);
    float4 dPQK = float4(dP, dQ.z, dk);
    float i = 0;
    float depth = 0;
    float2 z = 0;
    float2 uv = 0;
    for (i = 0; i < _SSRIterations; ++i)
    {
        pqk += dPQK;

        z.x = z.y;
        z.y = (dPQK.z * 0.5 + pqk.z) / (dPQK.w * 0.5 + pqk.w);
        SwapIfBigger(z.y, z.x);

        uv = isPermuted ? pqk.yx : pqk.xy;
        uv *= _ScreenParams.zw - 1;

        float4 uv4 = float4(uv, 0.0, 0.0);
        float depthRaw = tex2Dlod(_CameraDepthTexture, uv4).r;
        float depth = -LinearEyeDepth(depthRaw);
        float penetrationDepth = depth - z.y;

        if (z.y <= depth && penetrationDepth < _SSRMaxPenetration)
        {
            result.isHit = true;
            result.uv = uv;
            result.depth = -z.y;
            result.penetrationDepth = penetrationDepth;
            result.iterationCount = i;
            return result;
        }
    }

    return result;
}

float _CalculateConfidence(Result result, Ray ray, WaterParams params)
{
    float confidence = 1.0 - ((float)result.iterationCount / _SSRIterations);

    // Fade result based on distance from screen edge
    // This removes artifacts when trying to sample out of screen bounds
    float offset = min(1.0 - max(result.uv.x, result.uv.y), min(result.uv.x, result.uv.y));
    confidence *= sqrt(saturate(offset / _SSRScreenEdgeFade));

    // Fade result based on how much the bounced ray faces the camera.
    // This removes the sharp transition when rays start to bounce the wrong way
    confidence *= saturate(-ray.direction.z / _SSRSteepnessFade);

    // Fade result based on distance from ray origin
    confidence *= 1.0 - saturate((result.depth - result.penetrationDepth) / _SSRMaxDistance);

    // Fade results based on how deep inside the object they penetrate
    confidence *= saturate((_SSRMaxPenetration - result.penetrationDepth) / (_SSRMaxPenetration * _SSRPenetrationFade));

    return confidence;
}

float4 _CalculateReflection(WaterParams params)
{
    // Calculate skybox reflection.
    float3 reflNormal = normalize(lerp(params.meshNormal, params.worldNormal, _ReflectionDistort));
    float3 worldReflDir = reflect(-params.viewDir, reflNormal);
    float4 reflCol = _ReflectionColor;
    float4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldReflDir);
    float3 skyColor = DecodeHDR(skyData, unity_SpecCube0_HDR);
    reflCol.rgb *= lerp(skyColor.rgb, _SkyboxFog.rgb, _SkyboxFog.a);

    // Calculate screenspace reflection.
    Ray ray;
    ray.origin = params.viewPos;
    if (ray.origin.z < -_SSRMaxDistance)
    {
        return reflCol;
    }

    float3 viewNormal = mul((float3x3)UNITY_MATRIX_V, reflNormal);
    ray.direction = normalize(reflect(ray.origin, viewNormal));
    if (ray.direction.z > 0.0)
    {
        return reflCol;
    }

    float2 uv = params.screenPos.xy * _ScreenParams.xy;
    float c = (uv.x + uv.y) * 0.25;
    float jitter = fmod(c, 1.0);
    Result result = March(ray, jitter);

    if (result.isHit)
    {
        float confidence = _CalculateConfidence(result, ray, params);
        float4 ssrCol = tex2D(_GrabTexture, result.uv) * _ReflectionColor;
        reflCol = lerp(reflCol, ssrCol, confidence);
    }

    return reflCol;
}

#endif //WATER_REFLECTION_CGINC
