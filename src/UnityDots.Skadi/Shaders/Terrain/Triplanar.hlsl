﻿#ifndef TRIPLANAR_HLSL_INCLUDED
#define TRIPLANAR_HLSL_INCLUDED

float _TriplanarBlendSharpness;

float3 TriplanarGetBlendFactor(float3 normal, float blendSharpness)
{
    float3 blend = pow(abs(normal), blendSharpness);
    blend /= max(dot(blend, 1.0), 0.0001);
    return blend;
}

float4 TriplanarBlend(float4 cx, float4 cy, float4 cz, float3 blend)
{
    return cx * blend.x + cy * blend.y + cz * blend.z;
}

float3 TriplanarBlendNormal(float3 nx, float3 ny, float3 nz, float3 blend, float3 worldNormal)
{
    float3 tnormalX = float3(nx.xy + worldNormal.zy, abs(nx.z) * worldNormal.x);
    float3 tnormalY = float3(ny.xy + worldNormal.xz, abs(ny.z) * worldNormal.y);
    float3 tnormalZ = float3(nz.xy + worldNormal.xy, abs(nz.z) * worldNormal.z);
    return normalize(tnormalX.zyx * blend.x + tnormalY.xzy * blend.y + tnormalZ.xyz * blend.z);
}

// Implicit sampler
#define TRIPLANAR_SAMPLE_TEX2D_X(tex, st, pos) UNITY_SAMPLE_TEX2D(tex, (st).zw + (st).xy * (pos).zy)
#define TRIPLANAR_SAMPLE_TEX2D_Y(tex, st, pos) UNITY_SAMPLE_TEX2D(tex, (st).zw + (st).xy * (pos).xz)
#define TRIPLANAR_SAMPLE_TEX2D_Z(tex, st, pos) UNITY_SAMPLE_TEX2D(tex, (st).zw + (st).xy * (pos).xy)

#define TRIPLANAR_SAMPLE_TEX2D(tex, st, pos, blend) TriplanarBlend( \
    TRIPLANAR_SAMPLE_TEX2D_X(tex, st, pos), \
    TRIPLANAR_SAMPLE_TEX2D_Y(tex, st, pos), \
    TRIPLANAR_SAMPLE_TEX2D_Z(tex, st, pos), blend)

#define TRIPLANAR_SAMPLE_TEX2D_NORMAL(tex, st, pos, blend, normal) TriplanarBlendNormal( \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_X(tex, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_Y(tex, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_Z(tex, st, pos)), blend, normal)

#define TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, st, pos, blend, normal, scale) TriplanarBlendNormal( \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_X(tex, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_Y(tex, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_Z(tex, st, pos), scale), blend, normal)

// Explicit sampler
#define TRIPLANAR_SAMPLE_TEX2D_X_SAMPLER(tex, sampler, st, pos) UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (st).zw + (st).xy * (pos).zy)
#define TRIPLANAR_SAMPLE_TEX2D_Y_SAMPLER(tex, sampler, st, pos) UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (st).zw + (st).xy * (pos).xz)
#define TRIPLANAR_SAMPLE_TEX2D_Z_SAMPLER(tex, sampler, st, pos) UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (st).zw + (st).xy * (pos).xy)

#define TRIPLANAR_SAMPLE_TEX2D_SAMPLER(tex, sampler, st, pos, blend) TriplanarBlend( \
    TRIPLANAR_SAMPLE_TEX2D_X_SAMPLER(tex, sampler, st, pos), \
    TRIPLANAR_SAMPLE_TEX2D_Y_SAMPLER(tex, sampler, st, pos), \
    TRIPLANAR_SAMPLE_TEX2D_Z_SAMPLER(tex, sampler, st, pos), blend)

#define TRIPLANAR_SAMPLE_TEX2D_NORMAL_SAMPLER(tex, sampler, st, pos, blend, normal) TriplanarBlendNormal( \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_X_SAMPLER(tex, sampler, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_Y_SAMPLER(tex, sampler, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2D_Z_SAMPLER(tex, sampler, st, pos)), blend, normal)

#define TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(tex, sampler, st, pos, blend, normal, scale) TriplanarBlendNormal( \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_X_SAMPLER(tex, sampler, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_Y_SAMPLER(tex, sampler, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2D_Z_SAMPLER(tex, sampler, st, pos), scale), blend, normal)

// Array
#define TRIPLANAR_SAMPLE_TEX2DARRAY_X(tex, idx, st, pos) UNITY_SAMPLE_TEX2DARRAY(tex, float3((st).zw + (st).xy * (pos).zy, idx))
#define TRIPLANAR_SAMPLE_TEX2DARRAY_Y(tex, idx, st, pos) UNITY_SAMPLE_TEX2DARRAY(tex, float3((st).zw + (st).xy * (pos).xz, idx))
#define TRIPLANAR_SAMPLE_TEX2DARRAY_Z(tex, idx, st, pos) UNITY_SAMPLE_TEX2DARRAY(tex, float3((st).zw + (st).xy * (pos).xy, idx))

#define TRIPLANAR_SAMPLE_TEX2DARRAY(tex, idx, st, pos, blend) TriplanarBlend( \
    TRIPLANAR_SAMPLE_TEX2DARRAY_X(tex, idx, st, pos), \
    TRIPLANAR_SAMPLE_TEX2DARRAY_Y(tex, idx, st, pos), \
    TRIPLANAR_SAMPLE_TEX2DARRAY_Z(tex, idx, st, pos), blend)

#define TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL(tex, idx, st, pos, blend, normal) TriplanarBlendNormal( \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_X(tex, idx, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_Y(tex, idx, st, pos)), \
        UnpackNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_Z(tex, idx, st, pos)), blend, normal)

#define TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(tex, idx, st, pos, blend, normal, scale) TriplanarBlendNormal( \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_X(tex, idx, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_Y(tex, idx, st, pos), scale), \
        UnpackScaleNormal(TRIPLANAR_SAMPLE_TEX2DARRAY_Z(tex, idx, st, pos), scale), blend, normal)

#endif // TRIPLANAR_HLSL_INCLUDED
