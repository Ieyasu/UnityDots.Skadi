﻿#ifndef TERRAIN_SPLATMAP_EXTENSIONS_ARRAY_CGINC_INCLUDE
#define TERRAIN_SPLATMAP_EXTENSIONS_ARRAY_CGINC_INCLUDE

#include "DistanceBlend.hlsl"
#include "Triplanar.hlsl"

#define EPSILON 0.0001
#define MAX_TEXTURES 32

// Since 2018.3 we changed from _TERRAIN_NORMAL_MAP to _NORMALMAP to save 1 keyword.
// Since 2019.2 terrain keywords are changed to  local keywords so it doesn't really matter. You can use both.
#if defined(_NORMALMAP) && !defined(_TERRAIN_NORMAL_MAP)
    #define _TERRAIN_NORMAL_MAP
#elif !defined(_NORMALMAP) && defined(_TERRAIN_NORMAL_MAP)
    #define _NORMALMAP
#endif

struct Input
{
    INTERNAL_DATA

    float4 tc;
    float distance;
    float3 worldPos : SV_POSITION;
    float3 worldNormal : NORMAL;
};

float3 WorldToTangentNormalVector(Input IN, float3 normal)
{
    float3 t2w0 = WorldNormalVector(IN, float3(1, 0, 0));
    float3 t2w1 = WorldNormalVector(IN, float3(0, 1, 0));
    float3 t2w2 = WorldNormalVector(IN, float3(0, 0, 1));
    float3x3 t2w = float3x3(t2w0, t2w1, t2w2);
    return normalize(mul(t2w, normal));
}

// RedEngine3 way.
// https://www.gdcvault.com/play/1020197/Landscape-Creation-and-Rendering-in
float CalculateSlopeTangent(float3 vertexNormal, float3 combinedNormal, float4 blendSettings, float bgWeight, float fgWeight)
{
    const float3 up = float3(0, 1, 0);
    const float slopeMinThreshold = blendSettings.y - (1 - blendSettings.x);
    const float slopeMaxThreshold = blendSettings.y;
    const float slopeBasedDamp = blendSettings.z;

    // Calculate normal used for slope calculations.
    float vertexTangent = abs(dot(vertexNormal, up));
    float3 flattenedNormal  = lerp(combinedNormal, up, vertexTangent);
    float3 biasedFlattenedNormal  = normalize(lerp(combinedNormal, flattenedNormal, slopeBasedDamp));

    // Calculate blend between background and foreground.
    float slope = 1 - abs(dot(biasedFlattenedNormal, up));
    float blend = 1 - saturate((slope - slopeMinThreshold) / max(slopeMaxThreshold - slopeMinThreshold, EPSILON));
    blend *= fgWeight / max(bgWeight, EPSILON);

    blend = lerp(blend, 1, step(bgWeight, 0));
    blend = lerp(blend, 0, step(fgWeight, 0));

    return saturate(blend);
}

sampler2D _Splatmap;
sampler2D _SplatmapIndices;
float4 _Splatmap_ST;
float4 _Splatmap_TexelSize;

UNITY_DECLARE_TEX2DARRAY(_Splat);
float4 _SplatBounds[MAX_TEXTURES]; //
float4 _LayerData[MAX_TEXTURES]; // NormalScale, Metallic, Smoothness, BlendSharpness
float4 _BlendSettings[MAX_TEXTURES]; // 1 - BlendSharpness, SlopeTheshold, SlopeBasedDamp, SlopeBaseNormalDamp
float4 _Tints[MAX_TEXTURES];

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
    sampler2D _TerrainHeightmapTexture;
    sampler2D _TerrainNormalmapTexture;
    float4    _TerrainHeightmapRecipSize;   // float4(1.0f/width, 1.0f/height, 1.0f/(width-1), 1.0f/(height-1))
    float4    _TerrainHeightmapScale;       // float4(hmScale.x, hmScale.y / (float)(kMaxHeight), hmScale.z, 0.0f)
#endif

UNITY_INSTANCING_BUFFER_START(Terrain)
    UNITY_DEFINE_INSTANCED_PROP(float4, _TerrainPatchInstanceData) // float4(xBase, yBase, skipScale, ~)
UNITY_INSTANCING_BUFFER_END(Terrain)

#ifdef _NORMALMAP
    UNITY_DECLARE_TEX2DARRAY(_Normal);
#endif

#ifdef _MASKMAP
    UNITY_DECLARE_TEX2DARRAY(_Mask);
#endif

#ifdef _ALPHATEST_ON
    sampler2D _TerrainHolesTexture;

    void ClipHoles(float2 uv)
    {
        float hole = tex2D(_TerrainHolesTexture, uv).r;
        clip(hole == 0.0f ? -1 : 1);
    }
#endif

void SplatmapVert(inout appdata_full v, out Input data)
{
    UNITY_INITIALIZE_OUTPUT(Input, data);

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)

    float2 patchVertex = v.vertex.xy;
    float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);

    float4 uvscale = instanceData.z * _TerrainHeightmapRecipSize;
    float4 uvoffset = instanceData.xyxy * uvscale;
    uvoffset.xy += 0.5 * _TerrainHeightmapRecipSize.xy;
    float2 sampleCoords = (patchVertex.xy * uvscale.xy + uvoffset.xy);

    float hm = UnpackHeightmap(tex2Dlod(_TerrainHeightmapTexture, float4(sampleCoords, 0, 0)));
    v.vertex.xz = (patchVertex.xy + instanceData.xy) * _TerrainHeightmapScale.xz * instanceData.z;  //(x + xBase) * hmScale.x * skipScale;
    v.vertex.y = hm * _TerrainHeightmapScale.y;
    v.vertex.w = 1.0;

    v.texcoord.xy = (patchVertex.xy * uvscale.zw + uvoffset.zw);
    v.texcoord3 = v.texcoord2 = v.texcoord1 = v.texcoord;

    v.normal = float3(0, 1, 0);
    data.tc.zw = sampleCoords;
#endif

    v.tangent.xyz = cross(v.normal, float3(0, 0, 1));
    v.tangent.w = -1;

    data.tc.xy = v.texcoord;
    float4 pos = UnityObjectToClipPos(v.vertex);

    data.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
    data.distance = length(WorldSpaceViewDir(v.vertex));
}

void SplatmapMix(Input IN, inout SurfaceOutputStandard o)
{
    #ifdef _ALPHATEST_ON
        ClipHoles(IN.tc.xy);
    #endif

    // Get world normal for triplanar blend.
    #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
        float3 worldNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xyz;
    #else
        float3 worldNormal = WorldNormalVector(IN, o.Normal);
    #endif

    float3 triPlanarBlend = TriplanarGetBlendFactor(worldNormal, _TriplanarBlendSharpness);

    // Adjust splatUVs so the edges of the terrain tile lie on pixel centers
    float2 splatUV = (IN.tc.xy * (_Splatmap_TexelSize.zw - 1.0) + 0.5) * _Splatmap_TexelSize.xy;
    float4 splatControl = tex2D(_Splatmap, splatUV);
    float4 splatIndices = (255 * tex2D(_SplatmapIndices, splatUV));

    // Index 255 indicates an empty splat. Splat indices and splatControl are not always in sync so this is needed.
    splatControl *= step(splatIndices, 254);

    // Blend overlay layers and background layers separately before blending fg and bg together.
    float2 fgControl = splatControl.xy;
    float2 bgControl = splatControl.zw;
    float fgWeight = dot(fgControl, float2(1, 1));
    float bgWeight = dot(bgControl, float2(1, 1));
    fgControl = fgControl / max(fgWeight, EPSILON);
    bgControl = bgControl / max(bgWeight, EPSILON);

    // Get indices that tell which textures to sample.
    splatIndices = clamp(round(splatIndices), 0, MAX_TEXTURES - 1);
    float2 fgIndex = splatIndices.xy;
    float2 bgIndex = splatIndices.zw;

    // Distance blending to eliminate tiling.
    #ifdef _DISTANCE_BLEND
        float3 distanceBlend = GetDistanceBlend(IN.distance, _DistanceBlendInterval, _DistanceBlendScale, _DistanceBlendFalloff);
    #endif

    float4 fgST1 = _SplatBounds[fgIndex.x];
    float4 fgST2 = _SplatBounds[fgIndex.y];
    float4 bgST1 = _SplatBounds[bgIndex.x];
    float4 bgST2 = _SplatBounds[bgIndex.y];
    float4 fgLayer1 = _LayerData[fgIndex.x];
    float4 fgLayer2 = _LayerData[fgIndex.y];
    float4 bgLayer1 = _LayerData[bgIndex.x];
    float4 bgLayer2 = _LayerData[bgIndex.y];

    // Overlay layers are sampled using regular samples instead of triplanar for performance.
    float2 fgUv1 = IN.tc.xy * fgST1.xy + fgST1.zy;
    float2 fgUv2 = IN.tc.xy * fgST2.xy + fgST2.zy;
    float2 bgUv1 = IN.tc.xy * bgST1.xy + bgST1.zy;
    float2 bgUv2 = IN.tc.xy * bgST2.xy + bgST2.zy;

    #ifdef _NORMALMAP
        float3 fgNormal = 0;
        #ifdef _DISTANCE_BLEND
            fgNormal += (fgControl.x > 0) ? fgControl.x * UnpackNormalWithScale(DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Normal, float3(fgUv1, fgIndex.x), distanceBlend), fgLayer1.x) : 0;
            fgNormal += (fgControl.y > 0) ? fgControl.y * UnpackNormalWithScale(DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Normal, float3(fgUv2, fgIndex.y), distanceBlend), fgLayer2.x) : 0;
        #else
            fgNormal += (fgControl.x > 0) ? fgControl.x * UnpackNormalWithScale(UNITY_SAMPLE_TEX2DARRAY(_Normal, float3(fgUv1, fgIndex.x)), fgLayer1.x) : 0;
            fgNormal += (fgControl.y > 0) ? fgControl.y * UnpackNormalWithScale(UNITY_SAMPLE_TEX2DARRAY(_Normal, float3(fgUv2, fgIndex.y)), fgLayer2.x) : 0;
        #endif

        // Transform from tangent space to world space.
        fgNormal = float3(fgNormal.xy + worldNormal.xz, abs(fgNormal.z) * worldNormal.y).xzy;
        fgNormal = normalize(fgNormal + EPSILON);

        // Triplanar sampling, already in world space.
        float3 bgNormal = 0;
        #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
            bgNormal += (bgControl.x > 0) ? bgControl.x * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(_Normal, bgIndex.x, bgST1, distanceBlend, IN.worldPos, triPlanarBlend, worldNormal, bgLayer1.x) : 0;
            bgNormal += (bgControl.y > 0) ? bgControl.y * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(_Normal, bgIndex.y, bgST2, distanceBlend, IN.worldPos, triPlanarBlend, worldNormal, bgLayer2.x) : 0;
        #elif _DISTANCE_BLEND
            bgNormal += (bgControl.x > 0) ? bgControl.x * UnpackNormalWithScale(DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Normal, float3(bgUv1, bgIndex.x), distanceBlend), bgLayer1.x) : 0;
            bgNormal += (bgControl.y > 0) ? bgControl.y * UnpackNormalWithScale(DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Normal, float3(bgUv2, bgIndex.y), distanceBlend), bgLayer2.x) : 0;
        #elif _TRIPLANAR
            bgNormal += (bgControl.x > 0) ? bgControl.x * TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(_Normal, bgIndex.x, bgST1, IN.worldPos, triPlanarBlend, worldNormal, bgLayer1.x) : 0;
            bgNormal += (bgControl.y > 0) ? bgControl.y * TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(_Normal, bgIndex.y, bgST2, IN.worldPos, triPlanarBlend, worldNormal, bgLayer2.x) : 0;
        #else
            bgNormal += (bgControl.x > 0) ? bgControl.x * UnpackNormalWithScale(UNITY_SAMPLE_TEX2DARRAY(_Normal, float3(bgUv1, bgIndex.x)), bgLayer1.x) : 0;
            bgNormal += (bgControl.y > 0) ? bgControl.y * UnpackNormalWithScale(UNITY_SAMPLE_TEX2DARRAY(_Normal, float3(bgUv2, bgIndex.y)), bgLayer2.x) : 0;
        #endif
        bgNormal = normalize(bgNormal + EPSILON);
        float3 combinedNormal = bgNormal;
    #else
        float3 combinedNormal = worldNormal;
    #endif

    float4 blendSettings = lerp(_BlendSettings[bgIndex.x], _BlendSettings[bgIndex.y], bgControl.y);
    blendSettings.x = lerp(_BlendSettings[fgIndex.x].x, _BlendSettings[fgIndex.y].x, fgControl.y);
    float slopeBlend = CalculateSlopeTangent(worldNormal, combinedNormal, blendSettings, bgWeight, fgWeight);

    #ifdef _NORMALMAP
        float slopeBasedNormalDamp = lerp(blendSettings.w, 1, step(bgWeight, 0));
        slopeBasedNormalDamp *= fgWeight;
        o.Normal = normalize(lerp(fgNormal, bgNormal, (1 - slopeBlend) * (1 - slopeBasedNormalDamp))).xzy;
    #else
        o.Normal = worldNormal.xzy;
    #endif

    float4 fgDiffuse = 0.0;
    #ifdef _DISTANCE_BLEND
        fgDiffuse += (fgControl.x > 0) ? fgControl.x * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Splat, float3(fgUv1, fgIndex.x), distanceBlend) * _Tints[fgIndex.x] : 0;
        fgDiffuse += (fgControl.y > 0) ? fgControl.y * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Splat, float3(fgUv2, fgIndex.y), distanceBlend) * _Tints[fgIndex.y] : 0;
    #else
        fgDiffuse += (fgControl.x > 0) ? fgControl.x * UNITY_SAMPLE_TEX2DARRAY(_Splat, float3(fgUv1, fgIndex.x)) * _Tints[fgIndex.x] : 0;
        fgDiffuse += (fgControl.y > 0) ? fgControl.y * UNITY_SAMPLE_TEX2DARRAY(_Splat, float3(fgUv2, fgIndex.y)) * _Tints[fgIndex.y] : 0;
    #endif

    float4 bgDiffuse = 0.0;
    #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
        bgDiffuse += (bgControl.x > 0) ? bgControl.x * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY(_Splat, bgIndex.x, bgST1, distanceBlend, IN.worldPos, triPlanarBlend) * _Tints[bgIndex.x] : 0;
        bgDiffuse += (bgControl.y > 0) ? bgControl.y * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY(_Splat, bgIndex.y, bgST2, distanceBlend, IN.worldPos, triPlanarBlend) * _Tints[bgIndex.y] : 0;
    #elif _DISTANCE_BLEND
        bgDiffuse += (bgControl.x > 0) ? bgControl.x * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Splat, float3(bgUv1, bgIndex.x), distanceBlend) * _Tints[bgIndex.x] : 0;
        bgDiffuse += (bgControl.y > 0) ? bgControl.y * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Splat, float3(bgUv2, bgIndex.y), distanceBlend) * _Tints[bgIndex.y] : 0;
    #elif _TRIPLANAR
        bgDiffuse += (bgControl.x > 0) ? bgControl.x * TRIPLANAR_SAMPLE_TEX2DARRAY(_Splat, bgIndex.x, bgST1, IN.worldPos, triPlanarBlend) * _Tints[bgIndex.x] : 0;
        bgDiffuse += (bgControl.y > 0) ? bgControl.y * TRIPLANAR_SAMPLE_TEX2DARRAY(_Splat, bgIndex.y, bgST2, IN.worldPos, triPlanarBlend) * _Tints[bgIndex.y] : 0;
    #else
        bgDiffuse += (bgControl.x > 0) ? bgControl.x * UNITY_SAMPLE_TEX2DARRAY(_Splat, float3(bgUv1, bgIndex.x)) * _Tints[bgIndex.x] : 0;
        bgDiffuse += (bgControl.y > 0) ? bgControl.y * UNITY_SAMPLE_TEX2DARRAY(_Splat, float3(bgUv2, bgIndex.y)) * _Tints[bgIndex.y] : 0;
    #endif

    float4 diffuse = lerp(bgDiffuse, fgDiffuse, slopeBlend);

    o.Albedo = diffuse.rgb;
    o.Alpha = 1;

    #ifdef _MASKMAP
        float4 fgMask = 0;
        #ifdef _DISTANCE_BLEND
            fgMask += (fgControl.x > 0) ? fgControl.x * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Mask, float3(fgUv1, fgIndex.x), distanceBlend) : 0;
            fgMask += (fgControl.y > 0) ? fgControl.y * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Mask, float3(fgUv2, fgIndex.y), distanceBlend) : 0;
        #else
            fgMask += (fgControl.x > 0) ? fgControl.x * UNITY_SAMPLE_TEX2DARRAY(_Mask, float3(fgUv1, fgIndex.x)) : 0;
            fgMask += (fgControl.y > 0) ? fgControl.y * UNITY_SAMPLE_TEX2DARRAY(_Mask, float3(fgUv2, fgIndex.y)) : 0;
        #endif

        float4 bgMask = 0;
        #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
            bgMask += (bgControl.x > 0) ? bgControl.x * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY(_Mask, bgIndex.x, bgST1, distanceBlend, IN.worldPos, triPlanarBlend) : 0;
            bgMask += (bgControl.y > 0) ? bgControl.y * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY(_Mask, bgIndex.y, bgST2, distanceBlend, IN.worldPos, triPlanarBlend) : 0;
        #elif _DISTANCE_BLEND
            bgMask += (bgControl.x > 0) ? bgControl.x * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Mask, float3(bgUv1, bgIndex.x), distanceBlend) : 0;
            bgMask += (bgControl.y > 0) ? bgControl.y * DISTANCE_BLEND_SAMPLE_TEX2DARRAY(_Mask, float3(bgUv2, bgIndex.y), distanceBlend) : 0;
        #elif _TRIPLANAR
            bgMask += (bgControl.x > 0) ? bgControl.x * TRIPLANAR_SAMPLE_TEX2DARRAY(_Mask, bgIndex.x, bgST1, IN.worldPos, triPlanarBlend) : 0;
            bgMask += (bgControl.y > 0) ? bgControl.y * TRIPLANAR_SAMPLE_TEX2DARRAY(_Mask, bgIndex.y, bgST2, IN.worldPos, triPlanarBlend) : 0;
        #else
            bgMask += (bgControl.x > 0) ? bgControl.x * UNITY_SAMPLE_TEX2DARRAY(_Mask, float3(bgUv1, bgIndex.x)) : 0;
            bgMask += (bgControl.y > 0) ? bgControl.y * UNITY_SAMPLE_TEX2DARRAY(_Mask, float3(bgUv2, bgIndex.y)) : 0;
        #endif

        float4 mask = lerp(bgMask, fgMask, slopeBlend);

        o.Metallic = saturate(mask.r);
        o.Occlusion = saturate(mask.g);
        o.Smoothness = saturate(mask.a);
    #else
        float fgMetallic = dot(fgControl, float2(fgLayer1.y, fgLayer2.y));
        float fgSmoothness = dot(fgControl, float2(fgLayer1.z, fgLayer2.z));
        float bgMetallic = dot(bgControl, float2(bgLayer1.y, bgLayer2.y));
        float bgSmoothness = dot(bgControl, float2(bgLayer1.z, bgLayer2.z));
        o.Metallic = lerp(bgMetallic, fgMetallic, slopeBlend);
        o.Smoothness = lerp(bgSmoothness, bgSmoothness, slopeBlend);
    #endif

    #if defined(INSTANCING_ON) && defined(SHADER_TARGET_SURFACE_ANALYSIS)
        o.Normal = float3(0, 0, 1); // make sure that surface shader compiler realizes we write to normal, as UNITY_INSTANCING_ENABLED is not defined for SHADER_TARGET_SURFACE_ANALYSIS.
    #endif
}

#endif // TERRAIN_SPLATMAP_EXTENSIONS_ARRAY_CGINC_INCLUDE
