#ifndef DISTANCE_BLEND_CGINC_INCLUDE
#define DISTANCE_BLEND_CGINC_INCLUDE

#include "Triplanar.hlsl"

float _DistanceBlendScale;
float _DistanceBlendInterval;
float _DistanceBlendFalloff;

float3 GetDistanceBlend(float distance, float interval, float scale, float falloff)
{
    float level = pow(max(distance / interval - 1, 0), falloff);
    float3 blend = 0;
    blend.x = frac(level);

    float nearLevel = level - blend.x;
    blend.y = 1 / pow(scale, nearLevel);
    blend.z = lerp(blend.y / scale, blend.y, step(distance, interval));

    return blend;
}

// Implicit sampler
#define DISTANCE_BLEND_SAMPLE_TEX2D(tex, uv, blend) \
        lerp(UNITY_SAMPLE_TEX2D(tex, (uv).xy * (blend).y), \
             UNITY_SAMPLE_TEX2D(tex, (uv).xy * (blend).z), (blend).x)

#define DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED(tex, uv, blend, scale) \
        lerp(UnpackScaleNormal(UNITY_SAMPLE_TEX2D(tex, (uv).xy * (blend).y), scale), \
             UnpackScaleNormal(UNITY_SAMPLE_TEX2D(tex, (uv).xy * (blend).z), scale), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D(tex, st, blend, worldPos, triPlanarBlend) \
        lerp(TRIPLANAR_SAMPLE_TEX2D(tex, (st) * (blend).y, worldPos, triPlanarBlend), \
             TRIPLANAR_SAMPLE_TEX2D(tex, (st) * (blend).z, worldPos, triPlanarBlend), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, st, blend, worldPos, triPlanarBlend, worldNormal, scale) \
        lerp(TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, (st) * (blend).y, worldPos, triPlanarBlend, worldNormal, scale), \
             TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, (st) * (blend).z, worldPos, triPlanarBlend, worldNormal, scale), (blend).x)

// Explicit sampler
#define DISTANCE_BLEND_SAMPLE_TEX2D_SAMPLER(tex, sampler, uv, blend) \
        lerp(UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (uv).xy * (blend).y), \
             UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (uv).xy * (blend).z), (blend).x)

#define DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(tex, sampler, uv, blend, scale) \
        lerp(UnpackScaleNormal(UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (uv).xy * (blend).y), scale), \
             UnpackScaleNormal(UNITY_SAMPLE_TEX2D_SAMPLER(tex, sampler, (uv).xy * (blend).z), scale), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_SAMPLER(tex, sampler, st, blend, worldPos, triPlanarBlend) \
        lerp(TRIPLANAR_SAMPLE_TEX2D_SAMPLER(tex, sampler, (st) * (blend).y, worldPos, triPlanarBlend), \
             TRIPLANAR_SAMPLE_TEX2D_SAMPLER(tex, sampler, (st) * (blend).z, worldPos, triPlanarBlend), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(tex, sampler, st, blend, worldPos, triPlanarBlend, worldNormal, scale) \
        lerp(TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(tex, sampler, (st) * (blend).y, worldPos, triPlanarBlend, worldNormal, scale), \
             TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(tex, sampler, (st) * (blend).z, worldPos, triPlanarBlend, worldNormal, scale), (blend).x)

// Array
#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, st, blend, worldPos, triPlanarBlend, worldNormal, scale) \
        lerp(TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, (st) * (blend).y, worldPos, triPlanarBlend, worldNormal, scale), \
             TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(tex, (st) * (blend).z, worldPos, triPlanarBlend, worldNormal, scale), (blend).x)

#define DISTANCE_BLEND_SAMPLE_TEX2DARRAY(tex, uv, blend) \
        lerp(UNITY_SAMPLE_TEX2DARRAY(tex, float3((uv).xy * (blend).y, (uv).z)), \
             UNITY_SAMPLE_TEX2DARRAY(tex, float3((uv).xy * (blend).z, (uv).z)), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY(tex, i, st, blend, worldPos, triPlanarBlend) \
        lerp(TRIPLANAR_SAMPLE_TEX2DARRAY(tex, i, (st) * (blend).y, worldPos, triPlanarBlend), \
             TRIPLANAR_SAMPLE_TEX2DARRAY(tex, i, (st) * (blend).z, worldPos, triPlanarBlend), (blend).x)

#define DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(tex, i, st, blend, worldPos, triPlanarBlend, worldNormal, scale) \
        lerp(TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(tex, i, (st) * (blend).y, worldPos, triPlanarBlend, worldNormal, scale), \
             TRIPLANAR_SAMPLE_TEX2DARRAY_NORMAL_SCALED(tex, i, (st) * (blend).z, worldPos, triPlanarBlend, worldNormal, scale), (blend).x)
#endif // DISTANCE_BLEND_CGINC_INCLUDE
