// Upgrade NOTE: replaced '_CameraToWorld' with 'unity_CameraToWorld'

Shader "Skadi/Terrain/Water"
{
    Properties
    {
        [Header(Waves)]
        [NoScaleOffset] _BumpMap ("Waves (Bump)", 2D) = "bump" {}
        _BumpScale ("Wave strength", Float) = 1
        _WaveScale ("Wave scale", Range (0.01, 1)) = 0.1
        _WaveSpeed ("Wave speed", Float) = 1
        _WaveDirection ("Wave direction", Vector) = (1, 0.5, -1, -0.4)

        [Header(Attenuation)]
        _AttenuationDepth ("Attenuation depth", Float) = 1.0
        _AttenuationColor ("Attenuation color", Color) = (0, 0, 0, 1)
        _EdgeSmooth ("Edge smoothing", Float) = 0.2

        [Header(Refraction)]
        _RefractionColor ("Refraction color", Color)  = (1, 1, 1, 1)
        _RefractionDistort ("Refraction distort", Range (0, 1)) = 0.2

        [Header(Reflections)]
        _SkyboxFog ("Skybox fog", Color) = (1, 1, 1, 0)
        _ReflectionColor ("Reflection color", Color)  = (1, 1, 1, 1)
        _ReflectionDistort ("Reflection distort", Range (0, 1)) = 0.2

        [Header(Reflections (SSR))]
        _SSRStride ("SSR sampling stride", Float) = 16
        _SSRStrideDistance ("SSR stride convergence distance", Float) = 200
        _SSRIterations ("SSR sampling iterations", Int) = 64
        _SSRMaxDistance ("SSR sampling distance", Float) = 1000
        _SSRMaxPenetration ("SSR maximum penetration", Float) = 10
        _SSRPenetrationFade ("SSR penetration fade", Range(0, 1)) = 0.2
        _SSRScreenEdgeFade ("SSR screen edge fade ", Range(0, 1)) = 0.5
        _SSRSteepnessFade ("SSR camera steepness fade", Range(0, 1)) = 0.2

        [Header(Caustics)]
        [NoScaleOffset] _CausticsTex ("Caustics (RGB)", 2D) = "black" {}
        [HDR] _CausticsColor ("Caustics color", Color) = (1, 1, 1, 1)
        _CausticsSpeed ("Caustics speed", Float) = 1
        _CausticsScale ("Caustics scale", Range (0.01, 1)) = 0.05
        _CausticsStartDepth ("Caustics start depth", Float) = 2
        _CausticsEndDepth ("Caustics end depth", Float) = 10
        _CausticsFadeDistance ("Caustics fade distance", Float) = 1
    }

    Subshader
    {
        Tags
        {
            "Queue" = "Overlay"
        }

        GrabPass
        {
            Name "SkadiWater"
        }

        CGPROGRAM
        #pragma surface surf Water vertex:vert finalcolor:SplatmapFinalColor finalgbuffer:SplatmapFinalGBuffer noshadow
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma multi_compile_fog
        #pragma target 4.0
        #include "UnityPBSLighting.cginc"

        #pragma multi_compile_local __ _ALPHATEST_ON

        struct Input
        {
            float4 tc;
            float4 screenPos;
            float4 bumpuv;
            float3 viewPos;
            float3 waveNormal;
            float3 worldViewDir;
            float3 worldNormal; INTERNAL_DATA

            #ifndef TERRAIN_BASE_PASS
                UNITY_FOG_COORDS(6) // needed because finalcolor oppresses fog code generation.
            #endif
        };

        #define TERRAIN_INSTANCED_PERPIXEL_NORMAL
        #define TERRAIN_VERTEX_OUTPUT Input
        #define TERRAIN_SURFACE_OUTPUT SurfaceOutput
        #define WATER_VERTEX_OUTPUT Input
        #define WATER_SURFACE_OUTPUT SurfaceOutput
        #include "TerrainCommon.cginc"
        #include "../Water/WaterCommon.cginc"
        #include "../Water/WaterLighting.cginc"

		void vert (inout appdata_full v, out Input data)
		{
            TerrainVertex(v, data);
            WaterVertex(v, data);
		}

        void surf(Input IN, inout TERRAIN_SURFACE_OUTPUT o)
        {
            #ifdef _ALPHATEST_ON
                ClipHoles(IN.tc.xy);
            #endif

            // Initialize parameters used for water calculations
            #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
                float3 worldNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xyz;
            #else
                float3 worldNormal = WorldNormalVector(IN, o.Normal);
            #endif
            WaterParams params = CreateWaterParams(IN.screenPos, IN.bumpuv, IN.viewPos, normalize(IN.worldViewDir), worldNormal, IN.waveNormal);

            // Calculate water color
            CalculateWaterColor(params, o);
        }
        ENDCG

        UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
        UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
    }
}
