Shader "Skadi/Terrain/Triplanar Array"
{
    Properties
    {
        [Toggle(_TRIPLANAR)]
        _TriplanarToggle ("Triplanar", Float) = 0
        _TriplanarBlendSharpness ("Triplanar Blend Sharpness", Range(0.1, 10)) = 4

        [Toggle(_DISTANCE_BLEND)]
        _DistanceBlendToggle ("Distance Blend", Float) = 0
        _DistanceBlendScale ("Distance Blend Scale", Float) = 4.7
        _DistanceBlendInterval ("Distance Blend Interval", Float) = 100
        _DistanceBlendFalloff ("Distance Blend Falloff", Range(0, 1)) = 0.3

        [NoScaleOffset] _Splat ("Diffuse Array", 2DArray) = "white" {}
        [NoScaleOffset] _Normal ("Normal Array", 2DArray) = "bump" {}
        [NoScaleOffset] _Mask ("Mask Array", 2DArray) = "" {}
        [NoScaleOffset] _Splatmap ("Splatmap Texture", 2D) = "" {}
        [NoScaleOffset] _SplatmapIndices ("Splatmap Indices Texture", 2D) = "" {}
        [HideInInspector] _TerrainHolesTexture("Holes Map (RGB)", 2D) = "white" {}
   }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry-100"
            "RenderType" = "Opaque"
        }

        CGPROGRAM
        #pragma surface surf Standard vertex:SplatmapVert addshadow fullforwardshadows
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma multi_compile_fog // needed because finalcolor oppresses fog code generation.
        #pragma target 4.0
        #pragma require 2darray

        #pragma multi_compile_local_fragment __ _ALPHATEST_ON
        #pragma multi_compile_local_fragment __ _MASKMAP
        #pragma multi_compile_local __ _NORMALMAP
        #pragma shader_feature_local_fragment __ _TRIPLANAR
        #pragma shader_feature_local __ _DISTANCE_BLEND

        #include "UnityPBSLighting.cginc"
        #include "TerrainTriplanarArraySplatmapCommon.cginc"

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            SplatmapMix(IN, o);
        }
        ENDCG
    }
    Fallback "Nature/Terrain/Standard"
}
