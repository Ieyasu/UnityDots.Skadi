Shader "Skadi/Terrain/Height"
{
    Properties
    {
        _Color1 ("Color1", Color) = (0,1,0,1)
        _Color2 ("Color2", Color) = (1,0,0,1)
        _MinHeight ("Minimum Height", Range(0, 1)) = 0
        _MaxHeight ("Maximum Height", Range(0, 1)) = 1
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry-100"
            "RenderType" = "Opaque"
        }

        CGPROGRAM
        #pragma surface surf Standard vertex:vert finalgbuffer:SplatmapFinalGBuffer addshadow fullforwardshadows
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma multi_compile_fog // needed because finalcolor oppresses fog code generation.
        #pragma target 3.0
        #include "UnityPBSLighting.cginc"

        #pragma multi_compile_local_fragment __ _ALPHATEST_ON

        struct Input
        {
            float4 tc;
            float height;
            #ifndef TERRAIN_BASE_PASS
                UNITY_FOG_COORDS(0) // needed because finalcolor oppresses fog code generation.
            #endif
        };

        #define TERRAIN_INSTANCED_PERPIXEL_NORMAL
        #define TERRAIN_SURFACE_OUTPUT SurfaceOutputStandard
        #define TERRAIN_VERTEX_OUTPUT Input
        #include "TerrainCommon.cginc"

        float4 _Color1;
        float4 _Color2;
        float _MinHeight;
        float _MaxHeight;

        void vert(inout appdata_full v, out Input data)
        {
            TerrainVertex(v, data);
            #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
                data.height = saturate(v.vertex.y / _TerrainHeightmapScale.y);
            #endif
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            #ifdef _ALPHATEST_ON
                ClipHoles(IN.tc.xy);
            #endif

            float t = saturate((IN.height - _MinHeight) / (_MaxHeight - _MinHeight));
            o.Albedo = lerp(_Color1, _Color2, t);

            o.Alpha = 1;
            o.Smoothness = 0;
            o.Metallic = 0;

            // Make sure that surface shader compiler realizes we write to normal, as UNITY_INSTANCING_ENABLED is not defined for SHADER_TARGET_SURFACE_ANALYSIS.
            #if defined(INSTANCING_ON) && defined(SHADER_TARGET_SURFACE_ANALYSIS) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
                o.Normal = float3(0, 0, 1);
            #endif

            // Calculate normal
            #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
                o.Normal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xzy;
            #endif
        }

        ENDCG

        UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
        UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
    }
    Fallback "Nature/Terrain/Standard"
}
