Shader "Hidden/Skadi/TerrainEngine/Splatmap/Overlay-AddPass"
{
    Properties
    {
        [HideInInspector] _TerrainHolesTexture("Holes Map (RGB)", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry-99"
            "IgnoreProject" = "True"
            "RenderType" = "Opaque"
        }

        CGPROGRAM
        #pragma surface surf Standard decal:add vertex:SplatmapVert finalcolor:SplatmapFinalColor finalgbuffer:SplatmapFinalGBuffer addshadow fullforwardshadows nometa
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma multi_compile_fog // needed because finalcolor oppresses fog code generation.
        #pragma target 4.0
        #include "UnityPBSLighting.cginc"

        #pragma multi_compile_local_fragment __ _ALPHATEST_ON
        #pragma multi_compile_local_fragment __ _MASKMAP
        #pragma multi_compile_local __ _NORMALMAP
        #pragma shader_feature_local_fragment __ _TRIPLANAR
        #pragma shader_feature_local __ _DISTANCE_BLEND

        #define TERRAIN_SPLAT_ADDPASS
        #define TERRAIN_INSTANCED_PERPIXEL_NORMAL
        #define TERRAIN_SURFACE_OUTPUT SurfaceOutputStandard
        #include "TerrainOverlaySplatmapCommon.cginc"

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            SplatmapMix(IN, o);
        }
        ENDCG
    }

    Fallback "Hidden/TerrainEngine/Splatmap/Standard-AddPass"
}
