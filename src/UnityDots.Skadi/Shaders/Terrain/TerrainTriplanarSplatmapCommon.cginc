﻿#ifndef TERRAIN_SPLATMAP_EXTENSIONS_CGINC_INCLUDE
#define TERRAIN_SPLATMAP_EXTENSIONS_CGINC_INCLUDE

#include "DistanceBlend.hlsl"
#include "Triplanar.hlsl"

// Since 2018.3 we changed from _TERRAIN_NORMAL_MAP to _NORMALMAP to save 1 keyword.
// Since 2019.2 terrain keywords are changed to  local keywords so it doesn't really matter. You can use both.
#if defined(_NORMALMAP) && !defined(_TERRAIN_NORMAL_MAP)
    #define _TERRAIN_NORMAL_MAP
#elif !defined(_NORMALMAP) && defined(_TERRAIN_NORMAL_MAP)
    #define _NORMALMAP
#endif

struct Input
{
    float4 tc;

    INTERNAL_DATA
    #ifdef _TRIPLANAR
        float3 worldPos;
        float3 worldNormal;
    #endif

    #ifdef _DISTANCE_BLEND
        float distance;
    #endif

    #ifndef TERRAIN_BASE_PASS
        UNITY_FOG_COORDS(0) // needed because finalcolor oppresses fog code generation.
    #endif
};

sampler2D _Control;
float4 _Control_ST;
float4 _Control_TexelSize;

UNITY_DECLARE_TEX2D(_Splat0);
UNITY_DECLARE_TEX2D(_Splat1);
UNITY_DECLARE_TEX2D_NOSAMPLER(_Splat2);
UNITY_DECLARE_TEX2D_NOSAMPLER(_Splat3);
float4 _Splat0_ST, _Splat1_ST, _Splat2_ST, _Splat3_ST;

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
    sampler2D _TerrainHeightmapTexture;
    sampler2D _TerrainNormalmapTexture;
    float4    _TerrainHeightmapRecipSize;   // float4(1.0f/width, 1.0f/height, 1.0f/(width-1), 1.0f/(height-1))
    float4    _TerrainHeightmapScale;       // float4(hmScale.x, hmScale.y / (float)(kMaxHeight), hmScale.z, 0.0f)
#endif

UNITY_INSTANCING_BUFFER_START(Terrain)
    UNITY_DEFINE_INSTANCED_PROP(float4, _TerrainPatchInstanceData) // float4(xBase, yBase, skipScale, ~)
UNITY_INSTANCING_BUFFER_END(Terrain)

#ifdef _NORMALMAP
    UNITY_DECLARE_TEX2D(_Normal0);
    UNITY_DECLARE_TEX2D(_Normal1);
    UNITY_DECLARE_TEX2D_NOSAMPLER(_Normal2);
    UNITY_DECLARE_TEX2D_NOSAMPLER(_Normal3);
    float _NormalScale0, _NormalScale1, _NormalScale2, _NormalScale3;
#endif

#ifdef _MASKMAP
    UNITY_DECLARE_TEX2D(_Mask0);
    UNITY_DECLARE_TEX2D(_Mask1);
    UNITY_DECLARE_TEX2D_NOSAMPLER(_Mask2);
    UNITY_DECLARE_TEX2D_NOSAMPLER(_Mask3);
#else
    float _Metallic0, _Metallic1, _Metallic2, _Metallic3;
    float _Smoothness0, _Smoothness1, _Smoothness2, _Smoothness3;
#endif

#ifdef _ALPHATEST_ON
    sampler2D _TerrainHolesTexture;

    void ClipHoles(float2 uv)
    {
        float hole = tex2D(_TerrainHolesTexture, uv).r;
        clip(hole == 0.0f ? -1 : 1);
    }
#endif

#if defined(TERRAIN_BASE_PASS) && defined(UNITY_PASS_META)
    // When we render albedo for GI baking, we actually need to take the ST
    float4 _MainTex_ST;
#endif

float3 WorldToTangentNormalVector(Input IN, float3 normal)
{
    float3 t2w0 = WorldNormalVector(IN, float3(1, 0, 0));
    float3 t2w1 = WorldNormalVector(IN, float3(0, 1, 0));
    float3 t2w2 = WorldNormalVector(IN, float3(0, 0, 1));
    float3x3 t2w = float3x3(t2w0, t2w1, t2w2);
    return normalize(mul(t2w, normal));
}

void SplatmapVert(inout appdata_full v, out Input data)
{
    UNITY_INITIALIZE_OUTPUT(Input, data);

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)

    float2 patchVertex = v.vertex.xy;
    float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);

    float4 uvscale = instanceData.z * _TerrainHeightmapRecipSize;
    float4 uvoffset = instanceData.xyxy * uvscale;
    uvoffset.xy += 0.5 * _TerrainHeightmapRecipSize.xy;
    float2 sampleCoords = (patchVertex.xy * uvscale.xy + uvoffset.xy);

    float hm = UnpackHeightmap(tex2Dlod(_TerrainHeightmapTexture, float4(sampleCoords, 0, 0)));
    v.vertex.xz = (patchVertex.xy + instanceData.xy) * _TerrainHeightmapScale.xz * instanceData.z;  //(x + xBase) * hmScale.x * skipScale;
    v.vertex.y = hm * _TerrainHeightmapScale.y;
    v.vertex.w = 1.0;

    v.texcoord.xy = (patchVertex.xy * uvscale.zw + uvoffset.zw);
    v.texcoord3 = v.texcoord2 = v.texcoord1 = v.texcoord;

    #ifdef TERRAIN_INSTANCED_PERPIXEL_NORMAL
        v.normal = float3(0, 1, 0); // TODO: reconstruct the tangent space in the pixel shader. Seems to be hard with surface shader especially when other attributes are packed together with tSpace.
        data.tc.zw = sampleCoords;
    #else
        float3 nor = tex2Dlod(_TerrainNormalmapTexture, float4(sampleCoords, 0, 0)).xyz;
        v.normal = 2.0f * nor - 1.0f;
    #endif

#endif

    v.tangent.xyz = cross(v.normal, float3(0,0,1));
    v.tangent.w = -1;

    data.tc.xy = v.texcoord;
#ifdef TERRAIN_BASE_PASS
    #ifdef UNITY_PASS_META
        data.tc.xy = TRANSFORM_TEX(v.texcoord.xy, _MainTex);
    #endif
#else
    float4 pos = UnityObjectToClipPos(v.vertex);
    UNITY_TRANSFER_FOG(data, pos);
#endif

#ifdef _DISTANCE_BLEND
    data.distance = length(WorldSpaceViewDir(v.vertex));
#endif
}

#ifndef TERRAIN_BASE_PASS

void SplatmapMix(Input IN, inout SurfaceOutputStandard o)
{
    #ifdef _ALPHATEST_ON
        ClipHoles(IN.tc.xy);
    #endif

    // adjust splatUVs so the edges of the terrain tile lie on pixel centers
    float2 splatUV = (IN.tc.xy * (_Control_TexelSize.zw - 1.0) + 0.5) * _Control_TexelSize.xy;
    float4 splatControl = tex2D(_Control, splatUV);
    float weight = dot(splatControl, half4(1, 1, 1, 1));

    #if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
        clip(weight == 0.0 ? -1 : 1);
    #endif

    // Normalize weights before lighting and restore weights in final modifier
    // functions so that the overal lighting result can be correctly weighted.
    splatControl /= (weight + 1e-3f);

    // If we use triplanar, the uv's are scaled to world space
    #ifdef _TRIPLANAR
        #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
            float2 scale = _TerrainHeightmapScale.xz / _TerrainHeightmapRecipSize.zw;
            float4 splatScale = float4(1.0 / scale.xy, 1, 1);
        #else
            float4 splatScale = 1;
        #endif
        float4 uvSplat0 = splatScale * _Splat0_ST;
        float4 uvSplat1 = splatScale * _Splat1_ST;
        float4 uvSplat2 = splatScale * _Splat2_ST;
        float4 uvSplat3 = splatScale * _Splat3_ST;
    #else
        float2 uvSplat0 = TRANSFORM_TEX(IN.tc.xy, _Splat0);
        float2 uvSplat1 = TRANSFORM_TEX(IN.tc.xy, _Splat1);
        float2 uvSplat2 = TRANSFORM_TEX(IN.tc.xy, _Splat2);
        float2 uvSplat3 = TRANSFORM_TEX(IN.tc.xy, _Splat3);
    #endif

    // Calculate triplanar blend
    #ifdef _TRIPLANAR

        #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
            float3 worldNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xyz;
        #else
            float3 worldNormal = WorldNormalVector(IN, o.Normal);
        #endif
        float3 triplanarBlend = TriplanarGetBlendFactor(worldNormal, _TriplanarBlendSharpness);
    #endif

    // Calculate distance blend
    #ifdef _DISTANCE_BLEND
        float3 distanceBlend = GetDistanceBlend(IN.distance, _DistanceBlendInterval, _DistanceBlendScale, _DistanceBlendFalloff);
    #endif

    // Calculate diffuse
    float4 diffuse0 = 0.0;
    float4 diffuse1 = 0.0;
    float4 diffuse2 = 0.0;
    float4 diffuse3 = 0.0;
    #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
        diffuse0 += splatControl.r * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D(_Splat0, uvSplat0, distanceBlend, IN.worldPos, triplanarBlend);
        diffuse1 += splatControl.g * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D(_Splat1, uvSplat1, distanceBlend, IN.worldPos, triplanarBlend);
        diffuse2 += splatControl.b * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Splat2, _Splat0, uvSplat2, distanceBlend, IN.worldPos, triplanarBlend);
        diffuse3 += splatControl.a * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Splat3, _Splat1, uvSplat3, distanceBlend, IN.worldPos, triplanarBlend);
    #elif _TRIPLANAR
        diffuse0 += splatControl.r * TRIPLANAR_SAMPLE_TEX2D(_Splat0, uvSplat0, IN.worldPos, triplanarBlend);
        diffuse1 += splatControl.g * TRIPLANAR_SAMPLE_TEX2D(_Splat1, uvSplat1, IN.worldPos, triplanarBlend);
        diffuse2 += splatControl.b * TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Splat2, _Splat0, uvSplat2, IN.worldPos, triplanarBlend);
        diffuse3 += splatControl.a * TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Splat3, _Splat1, uvSplat3, IN.worldPos, triplanarBlend);
    #elif _DISTANCE_BLEND
        diffuse0 += splatControl.r * DISTANCE_BLEND_SAMPLE_TEX2D(_Splat0, uvSplat0, distanceBlend);
        diffuse1 += splatControl.g * DISTANCE_BLEND_SAMPLE_TEX2D(_Splat1, uvSplat1, distanceBlend);
        diffuse2 += splatControl.b * DISTANCE_BLEND_SAMPLE_TEX2D_SAMPLER(_Splat2, _Splat0, uvSplat2, distanceBlend);
        diffuse3 += splatControl.a * DISTANCE_BLEND_SAMPLE_TEX2D_SAMPLER(_Splat3, _Splat1, uvSplat3, distanceBlend);
    #else
        diffuse0 += splatControl.r * UNITY_SAMPLE_TEX2D(_Splat0, uvSplat0);
        diffuse1 += splatControl.g * UNITY_SAMPLE_TEX2D(_Splat1, uvSplat1);
        diffuse2 += splatControl.b * UNITY_SAMPLE_TEX2D_SAMPLER(_Splat2, _Splat0, uvSplat2);
        diffuse3 += splatControl.a * UNITY_SAMPLE_TEX2D_SAMPLER(_Splat3, _Splat1, uvSplat3);
    #endif

    #ifndef _MASKMAP
        diffuse0.a *= _Smoothness0;
        diffuse1.a *= _Smoothness1;
        diffuse2.a *= _Smoothness2;
        diffuse3.a *= _Smoothness3;
    #endif

    float4 diffuse = diffuse0 + diffuse1 + diffuse2 + diffuse3;
    o.Albedo = diffuse.rgb;
    o.Alpha = weight;

    #ifndef _NORMALMAP
        #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
            o.Normal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xzy;
        #endif
    #else
        #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
            float3 normal = 0;
            normal += splatControl.r * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(_Normal0, uvSplat0, distanceBlend, IN.worldPos, triplanarBlend, worldNormal, _NormalScale0);
            normal += splatControl.g * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(_Normal1, uvSplat1, distanceBlend, IN.worldPos, triplanarBlend, worldNormal, _NormalScale1);
            normal += splatControl.b * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal2, _Normal0, uvSplat2, distanceBlend, IN.worldPos, triplanarBlend, worldNormal, _NormalScale2);
            normal += splatControl.a * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal3, _Normal1, uvSplat3, distanceBlend, IN.worldPos, triplanarBlend, worldNormal, _NormalScale3);
            normal.z += 1e-5; // to avoid nan after normalizing
            o.Normal = WorldToTangentNormalVector(IN, normal);
        #elif _TRIPLANAR
            float3 normal = 0;
            normal += splatControl.r * TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(_Normal0, uvSplat0, IN.worldPos, triplanarBlend, worldNormal, _NormalScale0);
            normal += splatControl.g * TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED(_Normal1, uvSplat1, IN.worldPos, triplanarBlend, worldNormal, _NormalScale1);
            normal += splatControl.b * TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal2, _Normal0, uvSplat2, IN.worldPos, triplanarBlend, worldNormal, _NormalScale2);
            normal += splatControl.a * TRIPLANAR_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal3, _Normal1, uvSplat3, IN.worldPos, triplanarBlend, worldNormal, _NormalScale3);
            normal.z += 1e-5; // to avoid nan after normalizing
            o.Normal = WorldToTangentNormalVector(IN, normal);
        #elif _DISTANCE_BLEND
            o.Normal += splatControl.r * DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED(_Normal0, uvSplat0, distanceBlend, _NormalScale0);
            o.Normal += splatControl.g * DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED(_Normal1, uvSplat1, distanceBlend, _NormalScale1);
            o.Normal += splatControl.b * DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal2, _Normal0, uvSplat2, distanceBlend, _NormalScale2);
            o.Normal += splatControl.a * DISTANCE_BLEND_SAMPLE_TEX2D_NORMAL_SCALED_SAMPLER(_Normal3, _Normal1, uvSplat3, distanceBlend, _NormalScale3);
            o.Normal.z += 1e-5; // to avoid nan after normalizing
        #else
            o.Normal  = UnpackNormalWithScale(UNITY_SAMPLE_TEX2D(_Normal0, uvSplat0), _NormalScale0) * splatControl.r;
            o.Normal += UnpackNormalWithScale(UNITY_SAMPLE_TEX2D(_Normal1, uvSplat1), _NormalScale1) * splatControl.g;
            o.Normal += UnpackNormalWithScale(UNITY_SAMPLE_TEX2D_SAMPLER(_Normal2, _Normal0, uvSplat2), _NormalScale2) * splatControl.b;
            o.Normal += UnpackNormalWithScale(UNITY_SAMPLE_TEX2D_SAMPLER(_Normal3, _Normal1, uvSplat3), _NormalScale3) * splatControl.a;
            o.Normal.z += 1e-5; // to avoid nan after normalizing
        #endif
    #endif

    #ifdef _MASKMAP
        float4 mask = 0;
        #if defined(_DISTANCE_BLEND) && defined(_TRIPLANAR)
            mask += splatControl.r * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D(_Mask0, uvSplat0, distanceBlend, IN.worldPos, triplanarBlend);
            mask += splatControl.g * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D(_Mask1, uvSplat1, distanceBlend, IN.worldPos, triplanarBlend);
            mask += splatControl.b * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Mask2, _Mask0, uvSplat2, distanceBlend, IN.worldPos, triplanarBlend);
            mask += splatControl.a * DISTANCE_BLEND_TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Mask3, _Mask1, uvSplat3, distanceBlend, IN.worldPos, triplanarBlend);
        #elif _TRIPLANAR
            mask += splatControl.r * TRIPLANAR_SAMPLE_TEX2D(_Mask0, uvSplat0, IN.worldPos, triplanarBlend);
            mask += splatControl.g * TRIPLANAR_SAMPLE_TEX2D(_Mask1, uvSplat1, IN.worldPos, triplanarBlend);
            mask += splatControl.b * TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Mask2, _Mask0, uvSplat2, IN.worldPos, triplanarBlend);
            mask += splatControl.a * TRIPLANAR_SAMPLE_TEX2D_SAMPLER(_Mask3, _Mask1, uvSplat3, IN.worldPos, triplanarBlend);
        #elif _DISTANCE_BLEND
            mask += splatControl.r * DISTANCE_BLEND_SAMPLE_TEX2D(_Mask0, uvSplat0, distanceBlend);
            mask += splatControl.g * DISTANCE_BLEND_SAMPLE_TEX2D(_Mask1, uvSplat1, distanceBlend);
            mask += splatControl.b * DISTANCE_BLEND_SAMPLE_TEX2D_SAMPLER(_Mask2, _Mask0, uvSplat2, distanceBlend);
            mask += splatControl.a * DISTANCE_BLEND_SAMPLE_TEX2D_SAMPLER(_Mask3, _Mask1, uvSplat3, distanceBlend);
        #else
            mask += splatControl.r * UNITY_SAMPLE_TEX2D(_Mask0, uvSplat0);
            mask += splatControl.g * UNITY_SAMPLE_TEX2D(_Mask1, uvSplat1);
            mask += splatControl.b * UNITY_SAMPLE_TEX2D_SAMPLER(_Mask2, _Mask0, uvSplat2);
            mask += splatControl.a * UNITY_SAMPLE_TEX2D_SAMPLER(_Mask3, _Mask1, uvSplat3);
        #endif

        o.Metallic = saturate(mask.r);
        o.Occlusion = saturate(mask.g);
        o.Smoothness = saturate(mask.a);
    #else
        float4 metallic = float4(_Metallic0, _Metallic1, _Metallic2, _Metallic3);
        o.Metallic = dot(splatControl, metallic);
        o.Smoothness = diffuse.a;
    #endif

    #if defined(INSTANCING_ON) && defined(SHADER_TARGET_SURFACE_ANALYSIS)
        o.Normal = float3(0, 0, 1); // make sure that surface shader compiler realizes we write to normal, as UNITY_INSTANCING_ENABLED is not defined for SHADER_TARGET_SURFACE_ANALYSIS.
    #endif

    #if !defined(_TRIPLANAR) && defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
        float3 geomNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1);
        #ifdef _NORMALMAP
            float3 geomTangent = normalize(cross(geomNormal, float3(0, 0, 1)));
            float3 geomBitangent = normalize(cross(geomTangent, geomNormal));
            o.Normal = o.Normal.x * geomTangent
                          + o.Normal.y * geomBitangent
                          + o.Normal.z * geomNormal;
        #else
            o.Normal = geomNormal;
        #endif
        o.Normal = o.Normal.xzy;
    #endif
}

void SplatmapFinalColor(Input IN, SurfaceOutputStandard o, inout fixed4 color)
{
    color *= o.Alpha;
    #ifdef TERRAIN_SPLAT_ADDPASS
        UNITY_APPLY_FOG_COLOR(IN.fogCoord, color, fixed4(0,0,0,0));
    #else
        UNITY_APPLY_FOG(IN.fogCoord, color);
    #endif
}

void SplatmapFinalPrepass(Input IN, SurfaceOutputStandard o, inout fixed4 normalSpec)
{
    normalSpec *= o.Alpha;
}

void SplatmapFinalGBuffer(Input IN, SurfaceOutputStandard o, inout half4 outGBuffer0, inout half4 outGBuffer1, inout half4 outGBuffer2, inout half4 emission)
{
    UnityStandardDataApplyWeightToGbuffer(outGBuffer0, outGBuffer1, outGBuffer2, o.Alpha);
    emission *= o.Alpha;
}

#endif // TERRAIN_BASE_PASS

#endif // TERRAIN_SPLATMAP_COMMON_CGINC_INCLUDE
