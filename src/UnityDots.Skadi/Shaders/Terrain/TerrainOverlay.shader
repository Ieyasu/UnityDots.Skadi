Shader "Skadi/Terrain/Overlay"
{
    Properties
    {
        [Toggle(_TRIPLANAR)]
        _TriplanarToggle ("Triplanar", Float) = 0
        _TriplanarBlendSharpness ("Triplanar Blend Sharpness", Range(0.1, 10)) = 4

        [Toggle(_DISTANCE_BLEND)]
        _DistanceBlendToggle ("Distance Blend", Float) = 0
        _DistanceBlendScale ("Distance Blend Scale", Float) = 4.7
        _DistanceBlendInterval ("Distance Blend Interval", Float) = 100
        _DistanceBlendFalloff ("Distance Blend Falloff", Range(0, 1)) = 0.3

        _FgMask ("Foreground Mask", Vector) = (1, 1, 0, 0)
        _BlendSettings0 ("Blend Settings 0", Vector) = (0.27, 0.15, 0.75, 0)
        _BlendSettings1 ("Blend Settings 1", Vector) = (0.9, 0.15, 0.75, 0)
        _BlendSettings2 ("Blend Settings 2", Vector) = (0.5, 0.15, 0.75, 0)
        _BlendSettings3 ("Blend Settings 3", Vector) = (0.5, 0.15, 0.75, 0)

        [HideInInspector] _MainTex ("BaseMap (RGB)", 2D) = "white" {}
        [HideInInspector] _Color ("Main Color", Color) = (1, 1, 1, 1)
        [HideInInspector] _TerrainHolesTexture("Holes Map (RGB)", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry-100"
            "RenderType" = "Opaque"
        }

        CGPROGRAM
        #pragma surface surf Standard vertex:SplatmapVert finalcolor:SplatmapFinalColor finalgbuffer:SplatmapFinalGBuffer addshadow fullforwardshadows
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma multi_compile_fog // needed because finalcolor oppresses fog code generation.
        #pragma target 4.0
        #include "UnityPBSLighting.cginc"

        #pragma multi_compile_local_fragment __ _ALPHATEST_ON
        #pragma multi_compile_local_fragment __ _MASKMAP
        #pragma multi_compile_local __ _NORMALMAP
        #pragma shader_feature_local_fragment __ _TRIPLANAR
        #pragma shader_feature_local __ _DISTANCE_BLEND

        #define TERRAIN_INSTANCED_PERPIXEL_NORMAL
        #define TERRAIN_SURFACE_OUTPUT SurfaceOutputStandard
        #include "TerrainOverlaySplatmapCommon.cginc"

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            SplatmapMix(IN, o);
        }
        ENDCG

        UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
        UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
    }
    Dependency "AddPassShader" = "Hidden/Skadi/TerrainEngine/Splatmap/Overlay-AddPass"
    Dependency "BaseMapShader"    = "Hidden/TerrainEngine/Splatmap/Standard-Base"
    Dependency "BaseMapGenShader" = "Hidden/TerrainEngine/Splatmap/Standard-BaseGen"

    Fallback "Nature/Terrain/Standard"
}
