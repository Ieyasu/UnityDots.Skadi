using UnityEngine;
using UnityEditor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;

namespace UnityDots.Skadi.Editors
{
    public static class CreateTerrainGenerator
    {
        [MenuItem("Assets/Create/Skadi/Terrain Generator")]
        public static void Create()
        {
            // Get graph path
            var graph = ScriptableObject.CreateInstance<TerrainGenerator>();
            var assetName = "Skadi Terrain Generator";
            var assetPath = AssetDatabase.GenerateUniqueAssetPath($"{GetActivePath()}/{assetName}.asset");

            // Create graph and default nodes
            AssetDatabase.CreateAsset(graph, assetPath);
            var node1 = GraphDataBuilder.AddNode(graph, new Vector2(0, 0), typeof(SimplexNoiseNode));
            var node2 = GraphDataBuilder.AddNode(graph, new Vector2(400, 0), typeof(HeightmapOutputNode));
            GraphDataBuilder.AddEdge(graph, new EdgeData(node1, 0, node2, 0));
            AssetDatabase.SaveAssets();

            // Focus the graph
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = graph;
        }

        private static string GetActivePath()
        {
            var path = "Assets";
            var obj = Selection.activeObject;
            if (obj != null)
            {
                path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
            }
            return path;
        }
    }
}
