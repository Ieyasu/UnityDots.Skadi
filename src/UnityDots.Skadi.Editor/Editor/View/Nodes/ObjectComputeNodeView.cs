using UnityDots.Editor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;

namespace UnityDots.Skadi.Editors
{
    [CustomNodeView(typeof(ObjectsComputeNode), true)]
    public class ObjectComputeNodeView : NodeView
    {
        public ObjectComputeNodeView()
        {
            this.LoadAndAddStyleSheet("Styles/Nodes/ObjectComputeNodeView");
        }
    }
}
