using System.Collections.Generic;
using UnityEditor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomNodeView(typeof(MapPortalOutputNode), false)]
    public sealed class MapPortalOutputNodeView : MapComputeNodeView
    {
        private readonly static string _emptyName = "<None>";
        private readonly List<IPortalInputNode<MapHandle>> _inputPortals = new List<IPortalInputNode<MapHandle>>();
        private PopupField<IPortalInputNode<MapHandle>> _inputField;

        private MapPortalOutputNode PortalNode => Node as MapPortalOutputNode;

        protected override void BuildControls(VisualElement contents, SerializedObject serializedObject)
        {
            UpdatePortals();

            _inputField = new PopupField<IPortalInputNode<MapHandle>>("Input", _inputPortals, 0, FormatValue, FormatValue)
            {
                value = _inputPortals.Contains(PortalNode.PortalInput) ? PortalNode.PortalInput : null
            };

            _inputField.RegisterCallback<ChangeEvent<IPortalInputNode<MapHandle>>>(OnInputChanged);

            // Update the list of portals just before the list is displayed
            _inputField.RegisterCallback<KeyDownEvent>(OnKeyDown, TrickleDown.TrickleDown);
            _inputField.RegisterCallback<MouseDownEvent>(OnMouseDown, TrickleDown.TrickleDown);

            contents.Add(_inputField);
        }

        private void UpdatePortals()
        {
            _inputPortals.Clear();

            _inputPortals.Add(null);
            foreach (var node in Node.Graph.Nodes)
            {
                if (node is IPortalInputNode<MapHandle> inputNode && !string.IsNullOrWhiteSpace(inputNode.PortalName))
                {
                    _inputPortals.Add(inputNode);
                }
            }
        }

        private string FormatValue(IPortalInputNode<MapHandle> value)
        {
            if (value == null)
            {
                return _emptyName;
            }

            // We can't have duplicate names so make sure we have a unique name
            if (_inputPortals.Exists(x => x != null && x.PortalName == value.PortalName && x != value))
            {
                return GetUniqueName(value);
            }
            return value.PortalName;
        }

        private void OnInputChanged(ChangeEvent<IPortalInputNode<MapHandle>> e)
        {
            if (e.newValue != PortalNode.PortalInput)
            {
                PortalNode.PortalInput = e.newValue;
                OnNodeChanged();
            }
        }

        private void OnKeyDown(KeyDownEvent _)
        {
            UpdatePortals();
        }

        private void OnMouseDown(MouseDownEvent _)
        {
            UpdatePortals();
        }

        private string GetUniqueName(IPortalInputNode<MapHandle> value)
        {
            var index = 0;
            foreach (var inputPortal in _inputPortals)
            {
                if (inputPortal == value)
                {
                    break;
                }

                if (inputPortal != null && inputPortal.PortalName == value.PortalName)
                {
                    index++;
                }
            }
            return $"{value.PortalName} <{index}>"; ;
        }
    }
}
