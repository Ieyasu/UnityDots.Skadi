using UnityDots.Editor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;

namespace UnityDots.Skadi.Editors
{
    [CustomNodeView(typeof(PathComputeNode), true)]
    public class PathComputeNodeView : NodeView
    {
        public PathComputeNodeView()
        {
            this.LoadAndAddStyleSheet("Styles/Nodes/PathComputeNodeView");
        }
    }
}
