using System.Collections.Generic;
using UnityEditor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomNodeView(typeof(ObjectPortalOutputNode), false)]
    public sealed class ObjectPortalOutputNodeView : ObjectComputeNodeView
    {
        private readonly static string _emptyName = "<None>";
        private readonly List<IPortalInputNode<ObjectsHandle>> _inputPortals = new List<IPortalInputNode<ObjectsHandle>>();
        private PopupField<IPortalInputNode<ObjectsHandle>> _inputField;

        private ObjectPortalOutputNode PortalNode => Node as ObjectPortalOutputNode;

        protected override void BuildControls(VisualElement contents, SerializedObject serializedObject)
        {
            UpdatePortals();

            _inputField = new PopupField<IPortalInputNode<ObjectsHandle>>("Input", _inputPortals, 0, FormatValue, FormatValue)
            {
                value = _inputPortals.Contains(PortalNode.PortalInput) ? PortalNode.PortalInput : null
            };

            _inputField.RegisterCallback<ChangeEvent<IPortalInputNode<ObjectsHandle>>>(OnInputChanged);

            // Update the list of portals just before the list is displayed
            _inputField.RegisterCallback<KeyDownEvent>(OnKeyDown, TrickleDown.TrickleDown);
            _inputField.RegisterCallback<MouseDownEvent>(OnMouseDown, TrickleDown.TrickleDown);

            contents.Add(_inputField);
        }

        private void UpdatePortals()
        {
            _inputPortals.Clear();

            _inputPortals.Add(null);
            foreach (var node in Node.Graph.Nodes)
            {
                if (node is IPortalInputNode<ObjectsHandle> inputNode && !string.IsNullOrWhiteSpace(inputNode.PortalName))
                {
                    _inputPortals.Add(inputNode);
                }
            }
        }

        private string FormatValue(IPortalInputNode<ObjectsHandle> value)
        {
            if (value == null)
            {
                return _emptyName;
            }

            // We can't have duplicate names so make sure we have a unique name
            if (_inputPortals.Exists(x => x != null && x.PortalName == value.PortalName && x != value))
            {
                return GetUniqueName(value);
            }
            return value.PortalName;
        }

        private void OnInputChanged(ChangeEvent<IPortalInputNode<ObjectsHandle>> e)
        {
            if (e.newValue != PortalNode.PortalInput)
            {
                PortalNode.PortalInput = e.newValue;
                OnNodeChanged();
            }
        }

        private void OnKeyDown(KeyDownEvent _)
        {
            UpdatePortals();
        }

        private void OnMouseDown(MouseDownEvent _)
        {
            UpdatePortals();
        }

        private string GetUniqueName(IPortalInputNode<ObjectsHandle> value)
        {
            var index = 0;
            foreach (var inputPortal in _inputPortals)
            {
                if (inputPortal == value)
                {
                    break;
                }

                if (inputPortal != null && inputPortal.PortalName == value.PortalName)
                {
                    index++;
                }
            }
            return $"{value.PortalName} <{index}>"; ;
        }
    }
}
