using UnityDots.Editor;
using UnityDots.Graph;
using UnityDots.Graph.Editors;

namespace UnityDots.Skadi.Editors
{
    [CustomNodeView(typeof(MapComputeNode), true)]
    public class MapComputeNodeView : NodeView
    {
        public MapComputeNodeView()
        {
            this.LoadAndAddStyleSheet("Styles/Nodes/MapComputeNodeView");
        }
    }
}
