using System;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    public sealed class TerrainPreviewView : VisualElement, IDisposable
    {
        private readonly TerrainPreviewScene _scene;
        private readonly Image _image;

        public bool IsVisible
        {
            get => style.display != DisplayStyle.None;
            set => style.display = value ? DisplayStyle.Flex : DisplayStyle.None;
        }

        public TerrainPreviewView(VisualElement parentWindow, TerrainGenerator generator)
        {
            this.LoadAndAddStyleSheet("Styles/TerrainPreviewView");

            _scene = new TerrainPreviewScene(generator);

            var topContainer = new VisualElement() { name = "top" };
            topContainer.Add(new Label("Terrain Preview") { name = "title" });
            Add(topContainer);

            // Preview image that holds the render texture rendering the preview terrain
            var middleContainer = new VisualElement { name = "middle" };
            _image = new Image();
            _image.style.width = _scene.Resolution.x;
            _image.style.height = _scene.Resolution.y;
            _image.image = _scene.RenderTexture;
            middleContainer.Add(_image);
            Add(middleContainer);

            // Show preview settings under the preview image
            var bottomContainer = new VisualElement { name = "bottom" };
            var serializedObject = new SerializedObject(generator);
            var settingsProperty = serializedObject.FindProperty("_settings");
            var settingsField = new PropertyField(settingsProperty);
            settingsField.AddToClassList("terrain-settings");
            bottomContainer.Add(settingsField);
            Add(bottomContainer);

            this.AddManipulator(new WindowDraggable(topContainer, parentWindow));
            middleContainer.AddManipulator(new Scrollable(OnScroll));
            middleContainer.AddManipulator(new Draggable(OnDrag));

            this.Bind(serializedObject);
        }

        public void Dispose()
        {
            _scene.Dispose();
        }

        public void Update(RawTerrainData data)
        {
            _scene.UpdateTerrain(data);
            Render();
        }

        public void FinishUpdate()
        {
            _scene.SyncHeightmaps();
            Render();
        }

        private void Render()
        {
            _scene.Render();
            MarkDirtyRepaint();
        }

        private void OnScroll(float scrollValue)
        {
            _scene.Distance += _scene.Distance * scrollValue / (Event.current.shift ? 5f : 15f);
            Render();
        }

        private void OnDrag(MouseMoveEvent e)
        {
            var previewSize = _image.contentRect.size;
            var speedMultiplier = Event.current.shift ? 3f : 1f;
            var dragDelta = e.mouseDelta * 150f * speedMultiplier / Mathf.Min(previewSize.x, previewSize.y);
            _scene.TiltAngle += dragDelta.y;
            _scene.RotationAngle += dragDelta.x;
            Render();
        }
    }
}
