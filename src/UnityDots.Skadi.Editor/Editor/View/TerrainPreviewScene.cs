using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using System.ComponentModel;

namespace UnityDots.Skadi.Editors
{
    internal sealed class TerrainPreviewScene : IDisposable
    {
        private static readonly Color _backgroundColor = Color.clear;

        private bool _isDisposed;

        private readonly Scene _scene;
        private readonly Camera _camera;
        private readonly SkadiTerrain _terrain;
        private readonly List<GameObject> _gameObjects = new List<GameObject>();
        private readonly TerrainGenerator _generator;

        public int2 Resolution => new int2(520, 390);
        public RenderTexture RenderTexture { get; private set; }

        private float _distance;
        public float Distance
        {
            get => _distance;
            set => UpdateCameraTransform(value, TiltAngle, RotationAngle);
        }

        private float _tiltAngle;
        public float TiltAngle
        {
            get => _tiltAngle;
            set => UpdateCameraTransform(Distance, value, RotationAngle);
        }

        private float _rotationAngle;
        public float RotationAngle
        {
            get => _rotationAngle;
            set => UpdateCameraTransform(Distance, TiltAngle, value);
        }

        public TerrainPreviewScene(TerrainGenerator generator)
        {
            _generator = generator;
            _scene = CreateScene();
            _camera = CreateCamera();
            _terrain = CreateTerrain();

            _terrain.GroundTemplate = CreateTerrainTemplate();
            _terrain.WaterTemplate = CreateTerrainTemplate();
            CreateLights();

            UpdateCameraTransform(0.22f, 40, 70);
        }

        ~TerrainPreviewScene()
        {
            if (!_isDisposed)
            {
                Debug.LogWarning("TerrainPreviewScene not disposed properly");
                Dispose();
            }
        }

        public void Dispose()
        {
            _isDisposed = true;

            if (RenderTexture)
            {
                RenderTexture.Release();
            }

            foreach (var gameObject in _gameObjects)
            {
                UnityEngine.Object.DestroyImmediate(gameObject);
            }
            _gameObjects.Clear();

            if (!EditorSceneManager.ClosePreviewScene(_scene))
            {
                Debug.LogWarning("Failed to close preview scene");
            }

            GC.SuppressFinalize(this);
        }

        public void Render()
        {
            UpdateCameraTransform(Distance, TiltAngle, RotationAngle);

            try
            {
                if (Unsupported.SetOverrideLightingSettings(_scene))
                {
                    RenderSettings.ambientIntensity = 0;
                    RenderSettings.fog = false;
                    RenderSettings.defaultReflectionMode = DefaultReflectionMode.Custom;
                }

                _camera.Render();
            }
            finally
            {
                Unsupported.RestoreOverrideLightingSettings();
            }
        }

        public void SyncHeightmaps()
        {
            _terrain.SyncHeightmap();
            _terrain.SyncWatermap();
        }

        public void UpdateTerrain(RawTerrainData data)
        {
            if (_terrain == null)
            {
                return;
            }

            try
            {
                UpdateTerrainMaterials();
                _terrain.Initialize(data);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void AddGameObject(GameObject gameObject)
        {
            if (_gameObjects.Contains(gameObject))
            {
                return;
            }

            SceneManager.MoveGameObjectToScene(gameObject, _scene);
            _gameObjects.Add(gameObject);
        }

        private Scene CreateScene()
        {
            var scene = EditorSceneManager.NewPreviewScene();
            scene.name = "Terrain Preview Scene";

            if (!scene.IsValid())
            {
                throw new InvalidOperationException("Preview scene could not be created");
            }

            return scene;
        }

        private Camera CreateCamera()
        {
            // Create camera
            var cameraObject = EditorUtility.CreateGameObjectWithHideFlags("Terrain Preview Camera", HideFlags.HideAndDontSave, typeof(Camera));
            AddGameObject(cameraObject);

            var camera = cameraObject.GetComponent<Camera>();
            camera.name = "Terrain Preview Camera";
            camera.scene = _scene;
            camera.cameraType = CameraType.Preview;
            camera.enabled = false;
            camera.clearFlags = CameraClearFlags.Color;
            camera.fieldOfView = 60;
            camera.nearClipPlane = 0.3f;
            camera.farClipPlane = 250000.0f;
            camera.useOcclusionCulling = false;
            camera.backgroundColor = (QualitySettings.activeColorSpace == ColorSpace.Gamma) ? _backgroundColor : _backgroundColor.linear;
            camera.renderingPath = RenderingPath.UsePlayerSettings;

            // Create render texture
            RenderTexture = new RenderTexture(Resolution.x, Resolution.y, 32, TextureUtility.ColorFormat)
            {
                hideFlags = HideFlags.HideAndDontSave
            };
            camera.targetTexture = RenderTexture;
            return camera;
        }

        private void CreateLights()
        {
            var mainLightObject = EditorUtility.CreateGameObjectWithHideFlags("Terrain Preview Main Light", HideFlags.HideAndDontSave, typeof(Light));
            AddGameObject(mainLightObject);
            var mainLight = mainLightObject.GetComponent<Light>();
            mainLight.transform.rotation = Quaternion.Euler(50, 30, 0);
            mainLight.color = Color.white;
            mainLight.type = LightType.Directional;
            mainLight.intensity = 1f;
            mainLight.enabled = true;

            var secondaryLightObject = EditorUtility.CreateGameObjectWithHideFlags("Terrain Preview Secondary Light", HideFlags.HideAndDontSave, typeof(Light));
            AddGameObject(secondaryLightObject);
            var secondaryLight = secondaryLightObject.GetComponent<Light>();
            secondaryLight.transform.rotation = Quaternion.Euler(50, 210, 0);
            secondaryLight.color = mainLight.color;
            secondaryLight.type = LightType.Directional;
            secondaryLight.intensity = mainLight.intensity * 0.7f;
            secondaryLight.enabled = true;
        }

        private SkadiTerrain CreateTerrain()
        {
            var terrainObject = EditorUtility.CreateGameObjectWithHideFlags("Terrain Preview Object", HideFlags.HideAndDontSave, typeof(SkadiTerrain));
            AddGameObject(terrainObject);

            // Center the created terrain

            var terrain = terrainObject.GetComponent<SkadiTerrain>();
            terrain.TilingMode = TerrainTilingMode.Single;
            terrain.Data.SyncAlphamapsToCPU = false;
            terrain.Data.SyncHeightmapToCPU = false;
            terrain.Data.SyncWatermapToCPU = false;
            return terrain;
        }

        private void UpdateCameraTransform(float distance, float tiltAngle, float rotationAngle)
        {
            _distance = clamp(distance, 0, 1);
            _tiltAngle = clamp(tiltAngle, 20, 90);
            _rotationAngle = rotationAngle;

            var size = _generator.Settings.TerrainSize;
            var maxSize = max(size.x, size.y);
            _camera.transform.rotation = Quaternion.Euler(_tiltAngle, _rotationAngle, 0);

            var offset = 0.5f * new float3(size.x, size.y * (size.y / maxSize - 0.25f), size.z);
            _camera.transform.position = offset - (float3)_camera.transform.forward * lerp(maxSize / 2, maxSize * 4, _distance);
        }

        private void UpdateTerrainMaterials()
        {
            switch (_generator.Settings.Preview.RenderMode)
            {
                case TerrainPreviewRenderMode.Height:
                    _terrain.GroundTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewHeight");
                    _terrain.WaterTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewHeight");
                    _terrain.DrawObjects = false;
                    _terrain.DrawDetails = false;
                    break;
                case TerrainPreviewRenderMode.StandardShader:
                    _terrain.GroundTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewStandard");
                    _terrain.WaterTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewWater");
                    _terrain.DrawObjects = true;
                    _terrain.DrawDetails = false;
                    break;
                case TerrainPreviewRenderMode.SkadiShader:
                    _terrain.GroundTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewSkadi");
                    _terrain.WaterTemplate.materialTemplate = Resources.Load<Material>("Materials/TerrainPreviewWater");
                    _terrain.DrawObjects = true;
                    _terrain.DrawDetails = false;
                    break;
                default:
                    throw new InvalidEnumArgumentException(_generator.Settings.Preview.RenderMode.ToString());
            }
        }

        private Terrain CreateTerrainTemplate()
        {
            var terrainObject = EditorUtility.CreateGameObjectWithHideFlags("Terrain Preview Object", HideFlags.HideAndDontSave, typeof(Terrain));
            var terrain = terrainObject.GetComponent<Terrain>();
            terrain.enabled = true;

            terrain.terrainData = new TerrainData
            {
                heightmapResolution = 33,
                alphamapResolution = 16,
                baseMapResolution = 16,
            };
            terrain.terrainData.size = new float3(1, 1, 1);
            terrain.terrainData.SetDetailResolution(0, 8);

            terrain.basemapDistance = 20000;
            terrain.collectDetailPatches = false;
            terrain.drawHeightmap = true;
            terrain.drawInstanced = true;
            terrain.drawTreesAndFoliage = false;
            terrain.heightmapPixelError = 1;
            terrain.shadowCastingMode = ShadowCastingMode.TwoSided;
            terrain.treeBillboardDistance = 1000;
            terrain.treeCrossFadeLength = 10;
            terrain.treeDistance = 100;

            return terrain;
        }
    }
}
