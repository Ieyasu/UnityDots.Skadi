using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using UnityDots.Editor;
using UnityDots.Graph.Editors;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomGraphEditorView(typeof(TerrainGenerator))]
    public sealed class TerrainGeneratorEditor : GraphEditorView
    {
        private readonly TerrainGeneratorBuilder _builder;

        private bool _isPreviewDirty;
        private EditorCoroutine _previewRenderCoroutine;
        private TerrainPreviewView _previewView;

        private Dictionary<IPreviewNode, RenderTexture> _previewTextures;
        private SerializableCommandBuffer _previewCommandBuffer;
        private TerrainGeneratorResources _previewResources;

        public TerrainGenerator Generator => Graph as TerrainGenerator;

        public TerrainGeneratorEditor()
        {
            this.LoadAndAddStyleSheet("Styles/ComputeGraphEditor");

            _builder = new TerrainGeneratorBuilder();
        }

        protected override void CreateView()
        {
            base.CreateView();

            CreateToolBar();

            CreateTerrainPreview();

            Build(true);

            if (_previewRenderCoroutine == null)
            {
                _previewRenderCoroutine = EditorCoroutineUtility.StartCoroutine(RenderPreviewCoroutine(), this);
            }
        }

        protected override void OnClose()
        {
            _builder.Dispose();
            _previewView.Dispose();

            if (_previewRenderCoroutine != null)
            {
                EditorCoroutineUtility.StopCoroutine(_previewRenderCoroutine);
                _previewRenderCoroutine = null;
            }
        }

        protected override void OnGraphChange()
        {
            BuildAutoRefresh();
        }

        private void CreateTerrainPreview()
        {
            _previewView = new TerrainPreviewView(this, Generator);
            _previewView.RegisterToAllChanges(BuildAutoRefresh);
            Add(_previewView);
        }

        private void CreateToolBar()
        {
            var toolbar = new IMGUIContainer(() =>
            {
                GUILayout.BeginHorizontal(EditorStyles.toolbar);
                GUILayout.FlexibleSpace();
                var oldValue = Generator.Settings.Preview.Show;
                var newValue = GUILayout.Toggle(oldValue, "Show Preview", EditorStyles.toolbarButton);
                _previewView.IsVisible = newValue;
                if (oldValue != newValue)
                {
                    Generator.Settings.Preview.Show = newValue;
                    EditorUtility.SetDirty(Generator);
                }
                GUILayout.EndHorizontal();
            })
            {
                name = "toolbar"
            };
            Add(toolbar);
        }

        private void BuildAutoRefresh()
        {
            Build(Generator.Settings.Preview.AutoRefresh);
        }

        private void Build(bool updatePreview)
        {
            // Build the GPU command buffer based on the generator nodes
            _builder.Build(Generator, false);

            // Only update if the generated buffer or resources have changed
            var newCommandBuffer = _builder.GetSerializableCommandBuffer();
            var newResources = _builder.GetSerializableResources();
            if (Generator.CommandBuffer.SequenceEquals(newCommandBuffer) && Generator.Resources.SequenceEquals(newResources))
            {
                return;
            }

            Generator.CommandBuffer = newCommandBuffer;
            Generator.Resources = newResources;

            if (updatePreview)
            {
                BuildPreview();
            }
        }

        private void BuildPreview()
        {
            // Unfortunately we'll have to build the command buffer again so that we can insert
            // rendering to preview textures into the command buffer
            _builder.Build(Generator, true);
            _previewTextures = _builder.GetPreviewTextures();
            _previewResources = _builder.GetSerializableResources();
            _previewCommandBuffer = _builder.GetSerializableCommandBuffer();
            _isPreviewDirty = true;

            // Apply preview textures to the preview view
            GraphView.nodes.ForEach(view =>
            {
                if (view is NodeView nodeView
                        && nodeView.userData is IPreviewNode node
                        && nodeView.Preview != null)
                {
                    if (_previewTextures.TryGetValue(node, out RenderTexture texture))
                    {
                        nodeView.Preview.Image = texture;
                    }
                    else
                    {
                        nodeView.Preview.Image = null;
                    }
                }
            });
        }

        private IEnumerator RenderPreviewCoroutine()
        {
            var isTerrainDirty = false;
            var lastUpdateTime = 0.0;

            while (true)
            {
                // We update the preview continuously until there are no longer changes
                // happening in the graph editor.
                while (_isPreviewDirty)
                {
                    isTerrainDirty = true;
                    lastUpdateTime = EditorApplication.timeSinceStartup;
                    _isPreviewDirty = false;

                    var resources = _previewResources;
                    yield return Generator.GenerateAsync(_previewCommandBuffer);

                    // Apply the generated data to the terrain
                    var data = Generator.GetRawData(resources);
                    if (Generator.Settings.Preview.Show)
                    {
                        _previewView.Update(data);
                    }
                    else
                    {
                        data.Dispose();
                    }

                    // This avoids choking up the GPU by limiting the frequency of dispatch calls
                    yield return new EditorWaitForSeconds(0.1f);
                }

                // At the end we can sync heightmaps so that the terrain is displayed properly.
                // We add a small delay to when the syncing happens so that there isn't noticeable lag
                // when the user is interacting with the controls.
                if (isTerrainDirty && EditorApplication.timeSinceStartup - lastUpdateTime > 1.0)
                {
                    isTerrainDirty = false;
                    if (Generator.Settings.Preview.Show)
                    {
                        _previewView.FinishUpdate();
                    }
                }

                yield return null;
            }
        }
    }
}
