using System.Linq;
using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomEditor(typeof(SkadiTerrainSplats))]
    public sealed class SkadiTerrainSplatsEditor : UnityEditor.Editor
    {
        private SkadiTerrainSplats Target => target as SkadiTerrainSplats;

        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            var clearSplatmapButton = new Button(ClearSplatmap)
            {
                text = "Clear Splatmap"
            };

            var clearSplatsButton = new Button(ClearSplats)
            {
                text = "Clear Textures"
            };


            var generateSplatsButton = new Button(GenerateTextureArrays)
            {
                text = "Generate Textures"
            };

            container.Add(generateSplatsButton);
            container.Add(clearSplatsButton);
            container.Add(clearSplatmapButton);
            container.Add(new PropertyField(serializedObject.FindProperty("_splatmapResolution")));
            container.Add(new PropertyField(serializedObject.FindProperty("_layers")));

            return container;
        }

        private void ClearSplatmap()
        {
            AssetDatabaseHelper.DestroySubassets(Target, "Splatmap");
        }

        private void ClearSplats()
        {
            AssetDatabaseHelper.DestroySubassets<Texture2DArray>(Target);
        }

        private void GenerateTextureArrays()
        {
            if (Target.Layers.Count == 0)
            {
                Debug.LogError("No splats defined. Can't create texture arrays.");
                return;
            }

            ClearSplats();

            var diffuseTextures = Target.Layers.Select(x => x.TerrainLayer.diffuseTexture).ToArray();
            var diffuseArray = AssetDatabaseHelper.CreateTextureArray(Target, "Diffuse", diffuseTextures, false);
            Target.DiffuseTextures = diffuseArray;

            var normalTextures = Target.Layers.Select(x => x.TerrainLayer.normalMapTexture).ToArray();
            if (normalTextures.All(x => x != null))
            {
                var normalArray = AssetDatabaseHelper.CreateTextureArray(Target, "NormalMap", normalTextures, true);
                Target.NormalMaps = normalArray;
            }

            var maskTextures = Target.Layers.Select(x => x.TerrainLayer.maskMapTexture).ToArray();
            if (maskTextures.All(x => x != null))
            {
                var maskArray = AssetDatabaseHelper.CreateTextureArray(Target, "MaskMap", maskTextures, false);
                Target.MaskMaps = maskArray;
            }
        }
    }
}
