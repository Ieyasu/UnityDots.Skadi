using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomEditor(typeof(SkadiTerrainLayer))]
    [CanEditMultipleObjects]
    public sealed class SkadiTerrainLayerEditor : UnityEditor.Editor
    {
        private SkadiTerrainLayer Target => target as SkadiTerrainLayer;

        public override VisualElement CreateInspectorGUI()
        {
            var typeProperty = serializedObject.FindProperty("_layerType");
            var typeField = new EnumField("Layer Type", SkadiTerrainLayerType.Overlay)
            {
                bindingPath = "_layerType"
            };
            var layerField = new PropertyField(serializedObject.FindProperty("_terrainLayer"));
            var tintField = new PropertyField(serializedObject.FindProperty("_tint"));
            var blendSharpnessField = new SliderField("Blend Sharpness", 0, 1)
            {
                BindingPath = "_blendSharpness"
            };
            var slopeThresholdField = new SliderField("Slope Threshold", 0, 1)
            {
                BindingPath = "_slopeThreshold"
            };
            var slopeBasedDampField = new SliderField("Slope Based Damp", 0, 1)
            {
                BindingPath = "_slopeBasedDamp"
            };
            var slopeBasedNormalDampField = new SliderField("Slope Based Normal Damp", 0, 1)
            {
                BindingPath = "_slopeBasedNormalDamp"
            };

            var backgroundContainer = new VisualElement();
            backgroundContainer.Add(slopeThresholdField);
            backgroundContainer.Add(slopeBasedDampField);
            backgroundContainer.Add(slopeBasedNormalDampField);

            var container = new VisualElement();
            container.Add(layerField);
            container.Add(typeField);
            container.Add(tintField);
            container.Add(new Title("Blending"));
            container.Add(blendSharpnessField);
            container.Add(backgroundContainer);

            var typeName = typeProperty.enumNames[typeProperty.enumValueIndex];
            UpdateTypeChange(typeName, blendSharpnessField, SkadiTerrainLayerType.Overlay);
            UpdateTypeChange(typeName, backgroundContainer, SkadiTerrainLayerType.Background);
            typeField.RegisterCallback<ChangeEvent<System.Enum>>(e =>
            {
                UpdateTypeChange((SkadiTerrainLayerType)e.newValue, blendSharpnessField, SkadiTerrainLayerType.Overlay);
                UpdateTypeChange((SkadiTerrainLayerType)e.newValue, backgroundContainer, SkadiTerrainLayerType.Background);
            });

            return container;
        }

        public override bool HasPreviewGUI()
        {
            return Target != null && Target.TerrainLayer != null && Target.TerrainLayer.diffuseTexture != null;
        }

        public override void OnPreviewGUI(Rect rect, GUIStyle background)
        {
            if (Event.current.type == EventType.Repaint)
            {
                background.Draw(rect, false, false, false, false);
            }

            var mask = Target.TerrainLayer.diffuseTexture ?? EditorGUIUtility.whiteTexture;
            var texWidth = Mathf.Min(mask.width, 256);
            var texHeight = Mathf.Min(mask.height, 256);

            var margin = 4;
            var zoomLevel = Mathf.Min(Mathf.Min(rect.width / texWidth, rect.height / texHeight), 1);
            float x = 0.5f * (rect.width - texWidth * zoomLevel) + margin;
            float y = 0.5f * (rect.height - texHeight * zoomLevel) + 20;
            var wantedRect = new Rect(x, y, texWidth * zoomLevel, texHeight * zoomLevel);

            EditorGUI.DrawPreviewTexture(wantedRect, mask);
        }

        private static void UpdateTypeChange(string value, VisualElement element, SkadiTerrainLayerType elementType)
        {
            if (System.Enum.TryParse(value, out SkadiTerrainLayerType type))
            {
                UpdateTypeChange(type, element, elementType);
            }
        }

        private static void UpdateTypeChange(SkadiTerrainLayerType type, VisualElement element, SkadiTerrainLayerType elementType)
        {
            element.style.display = (elementType == type) ? DisplayStyle.Flex : DisplayStyle.None;
        }
    }
}
