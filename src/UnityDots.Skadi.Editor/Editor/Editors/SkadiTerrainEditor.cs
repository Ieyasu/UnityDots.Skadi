using UnityDots.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomEditor(typeof(SkadiTerrain))]
    public sealed class SkadiTerrainEditor : UnityEditor.Editor
    {
        private SkadiTerrain Target => target as SkadiTerrain;

        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            var tilingModeField = new PropertyField(serializedObject.FindProperty("_tilingMode"), "Mode");
            var tilingGridSizeField = new PropertyField(serializedObject.FindProperty("_tilingGridSize"), "Grid Size");

            container.Add(new Button(Generate)
            {
                text = "Generate"
            });
            container.Add(new Title("Terrain"));
            container.Add(new PropertyField(serializedObject.FindProperty("_generator")));
            container.Add(new PropertyField(serializedObject.FindProperty("_groundTemplate")));
            container.Add(new PropertyField(serializedObject.FindProperty("_waterTemplate")));
            container.Add(new Title("Tiling"));
            container.Add(tilingModeField);
            container.Add(tilingGridSizeField);
            container.Add(new Title("Objects"));
            container.Add(new PropertyField(serializedObject.FindProperty("_drawObjects"), "Draw"));
            container.Add(new Title("Details"));
            container.Add(new PropertyField(serializedObject.FindProperty("_drawDetails"), "Draw"));
            container.Add(new PropertyField(serializedObject.FindProperty("_drawDetailDebug"), "Draw Debug"));
            container.Add(new PropertyField(serializedObject.FindProperty("_detailResolutionPerPatch"), "Patch Resolution"));
            container.Add(new PropertyField(serializedObject.FindProperty("_detailDensity"), "Patch Density"));
            container.Add(new Title("Data"));
            var dataProperty = serializedObject.FindProperty("_data");
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_syncHeightmapToCPU"), "Sync Heightmap"));
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_syncWatermapToCPU"), "Sync Watermap"));
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_syncAlphamapsToCPU"), "Sync Alphamaps"));
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_heightmapDownsampling"), "Downscale Height"));
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_alphamapDownsampling"), "Downscale Alpha"));
            container.Add(new PropertyField(dataProperty.FindPropertyRelative("_detailmapDownsampling"), "Downscale Details"));

            container.RegisterToAllChanges(() => Target.Refresh());

            var modeProperty = serializedObject.FindProperty("_tilingMode");
            var modeName = modeProperty.enumNames[modeProperty.enumValueIndex];
            UpdateMode(modeName, tilingGridSizeField);
            tilingModeField.RegisterCallback<ChangeEvent<string>>(e =>
            {
                UpdateMode(e.newValue, tilingGridSizeField);
            });

            return container;
        }

        private static void UpdateMode(string value, VisualElement element)
        {
            if (System.Enum.TryParse(value, out TerrainTilingMode type))
            {
                element.style.display = (type == TerrainTilingMode.Single) ? DisplayStyle.None : DisplayStyle.Flex;
            }
        }

        private void Generate()
        {
            if (Target.Generator == null)
            {
                return;
            }

            Target.Generator.Generate();
            var rawData = Target.Generator.GetRawData();
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Target.Initialize(rawData);
            Debug.Log($"Update terrain in {watch.ElapsedMilliseconds} ms");
        }
    }
}
