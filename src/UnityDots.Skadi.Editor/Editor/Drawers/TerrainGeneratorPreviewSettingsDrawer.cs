using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using UnityDots.Editor;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(TerrainGeneratorPreviewSettings))]
    public sealed class TerrainGeneratorPreviewSettingsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new PropertyField(property.FindPropertyRelative("_renderMode")));
            container.Add(new Toggle("Auto Refresh") { bindingPath = "_autoRefresh" });

            return container;
        }
    }
}
