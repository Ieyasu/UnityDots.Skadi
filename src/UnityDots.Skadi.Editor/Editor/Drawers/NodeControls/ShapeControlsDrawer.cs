using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(ShapeControls))]
    public sealed class ShapeControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var typeProperty = property.FindPropertyRelative("_type");
            var wrapModeProperty = property.FindPropertyRelative("_wrapMode");
            var originProperty = property.FindPropertyRelative("_origin");

            var container = new VisualElement();
            var transformSettings = new VisualElement();
            var typeField = new PropertyField(typeProperty);
            var wrapField = new PropertyField(wrapModeProperty);

            container.Add(typeField);
            container.Add(wrapField);
            container.Add(new SliderField("Intensity", 0, 1, 2) { BindingPath = "_intensity" });

            container.Add(transformSettings);
            transformSettings.Add(new Title("Transform"));
            transformSettings.Add(new PropertyField(originProperty));
            transformSettings.Add(new FloatRangeField("Scale", 0, float.MaxValue) { bindingPath = "_scale" });
            transformSettings.Add(new SliderField("Rotation", 0, 360, 1) { BindingPath = "_rotation" });

            UpdateAdvancedDisplay(wrapField, typeProperty.enumNames[typeProperty.enumValueIndex]);
            UpdateAdvancedDisplay(transformSettings, typeProperty.enumNames[typeProperty.enumValueIndex]);
            typeField.RegisterCallback<ChangeEvent<string>>(e =>
            {
                UpdateAdvancedDisplay(wrapField, e.newValue);
                UpdateAdvancedDisplay(transformSettings, e.newValue);
            });

            return container;
        }

        private static void UpdateAdvancedDisplay(VisualElement element, string value)
        {
            if (System.Enum.TryParse(value.Replace(" ", string.Empty), true, out ShapeType type))
            {
                element.style.display = (type != ShapeType.Fill) ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }
    }
}
