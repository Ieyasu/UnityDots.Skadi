using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(SlideObjectsControls))]
    public sealed class SlideObjectsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderIntField("Iterations", 0, 100) { BindingPath = "_iterations" });
            container.Add(new SliderField("Distance Per Iteration", 1, 30, 2) { BindingPath = "_distancePerIteration" });

            return container;
        }
    }
}
