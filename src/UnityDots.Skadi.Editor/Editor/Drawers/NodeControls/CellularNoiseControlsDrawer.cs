using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(CellularNoiseControls))]
    public sealed class CellularNoiseControlsDrawer : PropertyDrawer
    {
        private const int _minWeight = -4;
        private const int _maxWeight = 4;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            var typeProperty = property.FindPropertyRelative("_type");
            var typeField = new PropertyField(typeProperty, "Type");
            var advancedSettings = CreateAdvancedGUI(property);

            container.Add(typeField);
            container.Add(new IntegerRangeField("Seed", 1, int.MaxValue) { bindingPath = "_seed" });
            container.Add(new FloatRangeField("Size", 0, float.MaxValue) { bindingPath = "_size" });
            container.Add(new SliderField("Intensity", 0, 10) { BindingPath = "_intensity" });
            container.Add(new SliderField("Uniformity", 0, 1) { BindingPath = "_uniformity" });
            container.Add(advancedSettings);

            // Handle showing advanced settings
            UpdateAdvancedDisplay(advancedSettings, typeProperty.enumNames[typeProperty.enumValueIndex]);
            typeField.RegisterCallback<ChangeEvent<string>>(e =>
            {
                UpdateAdvancedDisplay(advancedSettings, e.newValue);
            });

            return container;
        }

        private VisualElement CreateAdvancedGUI(SerializedProperty property)
        {
            var distanceProperty = property.FindPropertyRelative("_distanceMetric");
            var weightsProperty = property.FindPropertyRelative("_weights");

            var container = new VisualElement();
            container.Add(new Title("Weights"));
            container.Add(new PropertyField(distanceProperty, "Distance"));
            container.Add(CreateWeightSlider(weightsProperty, null, "x"));
            container.Add(CreateWeightSlider(weightsProperty, null, "y"));
            return container;
        }

        private SliderField CreateWeightSlider(SerializedProperty property, string label, string component)
        {
            return new SliderField(label, _minWeight, _maxWeight)
            {
                BindingPath = property.FindPropertyRelative(component).propertyPath
            };
        }

        private static void UpdateAdvancedDisplay(VisualElement element, string value)
        {
            if (System.Enum.TryParse(value.Replace(" ", string.Empty), true, out CellularNoiseType type))
            {
                element.style.display = (type == CellularNoiseType.Custom) ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }
    }
}
