using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(BlurControls))]
    public sealed class BlurControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderIntField("Radius", 1, 100) { BindingPath = "_radius" });

            return container;
        }
    }
}
