using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(RangeMaskControls))]
    public sealed class RangeMaskControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderField("Start Range", 0, 1) { BindingPath = "_minRange" });
            container.Add(new SliderField("End Range", 0, 1) { BindingPath = "_maxRange" });
            container.Add(new SliderField("Start Transition", 0, 1) { BindingPath = "_startTransition" });
            container.Add(new SliderField("End Transition", 0, 1) { BindingPath = "_endTransition" });

            return container;
        }
    }
}
