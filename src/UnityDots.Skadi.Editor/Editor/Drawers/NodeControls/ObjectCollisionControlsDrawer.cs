using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(ObjectCollisionControls))]
    public sealed class ObjectCollisionControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new FloatRangeField("Radius Size", 0, float.MaxValue) { bindingPath = "_radiusInfluence" });
            container.Add(new FloatRangeField("Scale Influence", 0, float.MaxValue) { bindingPath = "_scaleInfluence" });

            return container;
        }
    }
}
