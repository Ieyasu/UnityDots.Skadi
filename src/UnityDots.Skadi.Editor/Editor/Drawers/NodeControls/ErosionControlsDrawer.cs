using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(ErosionControls))]
    public sealed class ErosionControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            CreateMiscGUI(container, property);
            CreateWaterSimulationGUI(container);
            CreateSedimentTransportationGUI(container);
            CreateThermalErosionGUI(container);

            return container;
        }

        private void CreateMiscGUI(VisualElement container, SerializedProperty property)
        {
            container.Add(new SliderIntField("Iterations", 0, 50) { BindingPath = "_iterations" });
            container.Add(new PropertyField(property.FindPropertyRelative("_smoothing")));
            container.Add(new SliderField("Height Scale", 0, 2, 3) { BindingPath = "_heightScale" });
            container.Add(new SliderField("Size Scale", 0, 2, 3) { BindingPath = "_sizeScale" });
        }

        private void CreateWaterSimulationGUI(VisualElement container)
        {
            container.Add(new Title("Water Simulation"));
            container.Add(new SliderField("Time Step", 0, 1, 3) { BindingPath = "_hydraulicTimeStep" });
            container.Add(new SliderField("Rain", 0, 1, 3) { BindingPath = "_rain" });
            container.Add(new SliderField("Evaporation", 0, 1, 3) { BindingPath = "_evaporation" });
        }

        private void CreateSedimentTransportationGUI(VisualElement container)
        {
            container.Add(new Title("Hydraulic Erosion"));
            container.Add(new SliderField("WaterCapacity", 0, 10, 3) { BindingPath = "_waterCapacity" });
            container.Add(new SliderField("Capacity", 0, 1, 3) { BindingPath = "_capacity" });
            container.Add(new SliderField("Deposition", 0, 1, 3) { BindingPath = "_deposition" });
            container.Add(new SliderField("Dissolve", 0, 1, 3) { BindingPath = "_dissolve" });
        }

        private void CreateThermalErosionGUI(VisualElement container)
        {
            container.Add(new Title("Thermal Erosion"));
            container.Add(new SliderField("Time Step", 0, 1, 3) { BindingPath = "_thermalTimeStep" });
            container.Add(new SliderField("Talus Angle", 0, 89, 1) { BindingPath = "_talusAngle" });
        }
    }
}
