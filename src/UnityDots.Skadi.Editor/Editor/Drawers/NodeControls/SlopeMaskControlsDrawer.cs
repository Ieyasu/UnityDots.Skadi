using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(SlopeMaskControls))]
    public sealed class SlopeMaskControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderField("Min Slope", 0, 90, 1) { BindingPath = "_minSlope" });
            container.Add(new SliderField("Max Slope", 0, 90, 1) { BindingPath = "_maxSlope" });
            container.Add(new SliderField("Transition", 0, 90, 1) { BindingPath = "_transition" });

            return container;
        }
    }
}
