using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(SubdividePathsControls))]
    public sealed class SubdividePathsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderIntField("Iterations", 0, 10) { BindingPath = "_iterations" });
            container.Add(new FloatRangeField("Max Length", 0, 10000) { bindingPath = "_maxLength" });

            return container;
        }
    }
}
