using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(PlaceObjectsControls))]
    public sealed class PlaceObjectsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var obj = EditorUtil.GetParent(property);
            var typeProperty = property.FindPropertyRelative("_type");

            var container = new VisualElement();
            var typeField = new PropertyField(typeProperty);
            var uniformitySlider = new SliderField("Uniformity", 0, 1) { BindingPath = "_uniformity" };
            var objectsList = new PropertyField(property.FindPropertyRelative("_prototypes"))
            {
                viewDataKey = (obj as UnityEngine.Object).name
            };

            container.Add(typeField);
            container.Add(new IntegerRangeField("Seed", 1, int.MaxValue) { bindingPath = "_seed" });
            container.Add(new SliderIntField("Density", 0, 10000) { BindingPath = "_density" });
            container.Add(uniformitySlider);
            container.Add(objectsList);

            UpdateDisplay(uniformitySlider, typeProperty.enumNames[typeProperty.enumValueIndex]);
            typeField.RegisterCallback<ChangeEvent<string>>(e =>
            {
                UpdateDisplay(uniformitySlider, e.newValue);
            });

            return container;
        }

        private static void UpdateDisplay(VisualElement element, string value)
        {
            if (System.Enum.TryParse(value.Replace(" ", string.Empty), true, out ObjectPlacementType type))
            {
                element.style.display = (type == ObjectPlacementType.Grid) ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }
    }
}
