using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(CloneObjectsControls))]
    public sealed class CloneObjectsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var obj = EditorUtil.GetParent(property);
            var countProperty = property.FindPropertyRelative("_count");
            var modeProperty = property.FindPropertyRelative("_mode");
            var distanceProperty = property.FindPropertyRelative("_distance");

            var container = new VisualElement();
            var modeField = new PropertyField(modeProperty);
            var objectsList = new PropertyField(property.FindPropertyRelative("_prototypes"))
            {
                viewDataKey = (obj as UnityEngine.Object).name
            };

            container.Add(new IntegerRangeField("Seed", 1, int.MaxValue) { bindingPath = "_seed" });
            container.Add(modeField);
            container.Add(new FloatRangeField("Scale Influence", 0, float.MaxValue) { bindingPath = "_scaleInfluence" });

            container.Add(new Title("Clones"));
            container.Add(new MinMaxCurveField("Count", countProperty, 0, 50));
            container.Add(new MinMaxCurveField("Distance", distanceProperty));
            container.Add(objectsList);

            UpdateDisplay(objectsList, modeProperty.enumNames[modeProperty.enumValueIndex]);
            modeField.RegisterCallback<ChangeEvent<string>>(e =>
            {
                UpdateDisplay(objectsList, e.newValue);
            });

            return container;
        }

        private static void UpdateDisplay(VisualElement element, string value)
        {
            if (System.Enum.TryParse(value.Replace(" ", string.Empty), true, out CloneMode mode))
            {
                element.style.display = (mode == CloneMode.Preselected) ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }
    }
}
