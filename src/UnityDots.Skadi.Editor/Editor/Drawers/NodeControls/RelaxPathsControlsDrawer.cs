using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(RelaxPathsControls))]
    public sealed class RelaxPathsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new SliderIntField("Iterations", 0, 100) { BindingPath = "_iterations" });
            container.Add(new SliderField("Strength", 0, 1) { BindingPath = "_strength" });

            return container;
        }
    }
}
