using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(SimplexNoiseControls))]
    public sealed class SimplexNoiseControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();
            var typeProperty = property.FindPropertyRelative("_type");

            container.Add(new PropertyField(typeProperty));
            container.Add(new IntegerRangeField("Seed", 1, int.MaxValue) { bindingPath = "_seed" });
            container.Add(new FloatRangeField("Size", 0, float.MaxValue) { bindingPath = "_size" });
            container.Add(new SliderField("Intensity", 0, 10) { BindingPath = "_intensity" });

            container.Add(new Title("Fractals Settings"));
            container.Add(new SliderIntField("Octaves", 1, 10) { BindingPath = "_octaves" });
            container.Add(new SliderField("Persistance", 0, 1) { BindingPath = "_persistance" });
            container.Add(new SliderField("Lacunarity", 1, 4) { BindingPath = "_lacunarity" });

            return container;
        }
    }
}
