using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEngine;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(ImprintControls))]
    public sealed class ImprintControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new PropertyField(property.FindPropertyRelative("_collision")));
            container.Add(new ClampedCurveField("Profile", new Rect(0, 0, 1, 1)) { BindingPath = "_profileCurve" });

            return container;
        }
    }
}
