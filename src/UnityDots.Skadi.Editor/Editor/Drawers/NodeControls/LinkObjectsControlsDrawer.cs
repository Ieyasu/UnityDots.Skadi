using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(LinkObjectsControls))]
    public sealed class LinkObjectsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new PropertyField(property.FindPropertyRelative("_density")));
            container.Add(new PropertyField(property.FindPropertyRelative("_pinVertices")));

            return container;
        }
    }
}
