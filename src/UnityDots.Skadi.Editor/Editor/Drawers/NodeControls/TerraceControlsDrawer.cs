using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(TerraceControls))]
    public sealed class TerraceControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new IntegerRangeField("Steps", 0, int.MaxValue) { bindingPath = "_steps", isDelayed = true });
            container.Add(new SliderField("Uniformity", 0, 1) { BindingPath = "_uniformity" });
            container.Add(new SliderField("Steepness", 0, 1) { BindingPath = "_steepness" });

            return container;
        }
    }
}
