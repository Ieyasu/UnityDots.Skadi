using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(TransformObjectsControls))]
    public sealed class TransformObjectsControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();
            var spaceProperty = property.FindPropertyRelative("_space");
            var heightProperty = property.FindPropertyRelative("_height");
            var radiusProperty = property.FindPropertyRelative("_radius");
            var rotationProperty = property.FindPropertyRelative("_rotation");
            var scaleProperty = property.FindPropertyRelative("_scale");

            container.Add(new PropertyField(spaceProperty));
            container.Add(new IntegerRangeField("Seed", 1, int.MaxValue) { bindingPath = "_seed" });

            container.Add(new Title("Values"));
            container.Add(new MinMaxCurveField("Height", heightProperty));
            container.Add(new MinMaxCurveField("Rotation", rotationProperty));
            container.Add(new MinMaxCurveField("Scale", scaleProperty));
            container.Add(new MinMaxCurveField("Radius", radiusProperty));

            return container;
        }
    }
}
