using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityEditor.UIElements;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(CurvatureMaskControls))]
    public sealed class CurvatureMaskControlsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            container.Add(new PropertyField(property.FindPropertyRelative("_type")));
            container.Add(new SliderIntField("Radius", 1, 100) { BindingPath = "_radius" });
            container.Add(new SliderField("Intensity", 0, 100) { BindingPath = "_intensity" });

            return container;
        }
    }
}
