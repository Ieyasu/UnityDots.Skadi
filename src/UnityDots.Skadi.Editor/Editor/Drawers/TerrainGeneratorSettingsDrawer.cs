using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using UnityDots.Editor;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Skadi.Editors
{
    [CustomPropertyDrawer(typeof(TerrainGeneratorSettings))]
    public sealed class TerrainGeneratorSettingsDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();
            var resolutionField = new IntegerField("Map Resolution") { bindingPath = "_mapResolution", isDelayed = true };
            var sizeProperty = property.FindPropertyRelative("_terrainSize");

            container.Add(resolutionField);
            container.Add(new IntegerField("Map Seed") { bindingPath = "_seed", isDelayed = true });
            container.Add(new FloatRangeField("Terrain Width", 0, 100000) { bindingPath = sizeProperty.FindPropertyRelative("x").propertyPath, isDelayed = true });
            container.Add(new FloatRangeField("Terrain Length", 0, 100000) { bindingPath = sizeProperty.FindPropertyRelative("z").propertyPath, isDelayed = true });
            container.Add(new FloatRangeField("Terrain Height", 0, 10000) { bindingPath = sizeProperty.FindPropertyRelative("y").propertyPath, isDelayed = true });
            container.Add(new Title("Preview"));
            container.Add(new PropertyField(property.FindPropertyRelative("_preview")));

            resolutionField.RegisterValueChangedCallback(e =>
            {
                var value = clamp(Mathf.NextPowerOfTwo(e.newValue), 32, 4096);
                if (value != e.newValue)
                {
                    resolutionField.SetValueWithoutNotify(value);
                }
            });

            return container;
        }
    }
}
